import React from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import { HelmetProvider } from 'react-helmet-async';
import configureStore from './redux/store';
// material
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
// routes
import Routes from './routes';
// theme
import ThemeConfig from './theme';
import GlobalStyles from './theme/globalStyles';
// components
import ScrollToTop from 'components/ScrollToTop';
import Notif from 'components/Notif';
import ModalConfirmation from 'components/ModalConfirmation';
import { BaseOptionChartStyle } from 'components/charts/BaseOptionChart';
// utils
import history from 'utils/history';

export const store = configureStore();

export default function App() {
  return (
    <HelmetProvider>
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <ThemeConfig>
            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <ScrollToTop />
              <GlobalStyles />
              <BaseOptionChartStyle />
              <Routes />
              <Notif />
              <ModalConfirmation />
            </LocalizationProvider>
          </ThemeConfig>
        </ConnectedRouter>
      </Provider>
    </HelmetProvider>
  );
}
