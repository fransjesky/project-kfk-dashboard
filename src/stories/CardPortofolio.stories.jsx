import React from 'react';
import { Grid } from '@mui/material';
import CardPorto from '../components/CardOurPortofolio';

export default {
  title: 'Components/CardPortofolio',
  component: CardPorto
};

/*
 * This is basicly a modified version of Material UI Button
 * check https://mui.com/api/button/ for more information
 */

const CardPortofolio = (args) => (
  <Grid container spacing={2}>
    <Grid item xs={4}>
      <CardPorto {...args} />
    </Grid>
  </Grid>
);

export const WithActions = CardPortofolio.bind({});
WithActions.args = {
  thumbnail: '',
  name: '',
  projectCompany: '',
  status: '',
  onEdit: () => {},
  onArchive: () => {},
  onDelete: () => {}
};

export const WithoutActions = CardPortofolio.bind({});
WithoutActions.args = {
  thumbnail: '',
  name: '',
  projectCompany: '',
  status: '',
  hideActions: true
};
