import CardCareer from 'components/CardCareer';

export default {
  title: 'Components/CardCareer',
  component: CardCareer
};

const Template = (args) => <CardCareer {...args} />;

export const WithActions = Template.bind({});
WithActions.args = {
  color: '#36CECE',
  date: '17 Jan 2022',
  title: 'Mobile Developer (React Native) - Fulltime',
  views: 3,
  isArchived: true,
  onEdit: () => {},
  onArchive: () => {},
  onDelete: () => {}
};

export const WithoutActions = Template.bind({});
WithoutActions.args = {
  color: '#36CECE',
  date: '17 Jan 2022',
  title: 'Mobile Developer (React Native) - Fulltime',
  views: 3,
  isArchived: true,
  hideActions: true
};
