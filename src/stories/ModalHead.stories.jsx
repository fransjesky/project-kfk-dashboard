import React, { useState } from 'react';
import { Box, Button, TextField } from '@mui/material';
import Modal, { ModalContent, ModalHead } from '../components/Modal';

export default {
  title: 'Components/Modal/ModalHead',
  component: ModalHead
};

const Template = (args) => {
  const [open, setOpen] = useState(false);
  return (
    <Box>
      <Button onClick={() => setOpen((prev) => !prev)}>Open Modal</Button>
      <Modal isOpen={open} onClose={() => setOpen(false)}>
        <ModalHead title={args.title} subtitle={args.subtitle} />
        <ModalContent>
          <Box display="flex" flexDirection="column">
            <TextField label="Input 1" variant="outlined" sx={{ width: '500px' }} />
            <br />
            <TextField label="Input 2" variant="outlined" sx={{ width: '500px' }} />
          </Box>
        </ModalContent>
      </Modal>
    </Box>
  );
};

export const TitleOnly = Template.bind({});
TitleOnly.args = {
  title: 'Modal Title'
};

export const TitleAndSubtitle = Template.bind({});
TitleAndSubtitle.args = {
  title: 'Modal Title',
  subtitle: 'This is modal subtitle'
};
