import React, { useState } from 'react';
import { Box, Button, TextField } from '@mui/material';
import Modal, { ModalContent, ModalButtons } from '../components/Modal';

export default {
  title: 'Components/Modal/ModalButtons',
  component: ModalButtons
};

export const Basic = (args) => {
  const [open, setOpen] = useState(false);
  return (
    <Box>
      <Button onClick={() => setOpen((prev) => !prev)}>Open Modal</Button>
      <Modal isOpen={open} onClose={() => setOpen(false)}>
        <ModalContent>
          <h2>This is a Modal</h2>
          <Box display="flex" flexDirection="column">
            <TextField label="Input 1" variant="outlined" sx={{ width: '500px' }} />
            <br />
            <TextField label="Input 2" variant="outlined" sx={{ width: '500px' }} />
          </Box>
        </ModalContent>
        <ModalButtons background={args.background}>
          <Button variant="outlined">Primary</Button>
          <Button variant="contained">Secondary</Button>
        </ModalButtons>
      </Modal>
    </Box>
  );
};

export const WithBackgroundColor = Basic.bind({});
WithBackgroundColor.args = {
  background: '#FFF5EB'
};
