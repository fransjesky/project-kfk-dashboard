import Box from '@mui/material/Box';
import Page from 'components/Page';
import Header from 'components/Header';
import BodyLayout from 'layouts/dashboard/BodyLayout';

export default {
  title: 'Layouts/Page'
};

const ROUTES = [
  { label: 'First page', link: '/' },
  { label: 'Second Page page', link: '/' }
];

const Template = (args) => (
  <Box bgcolor="#F3F3F3" sx={{ py: 1 }}>
    <Page title={args.title}>
      <BodyLayout padded={args.padded}>
        <Header routes={ROUTES} withButton buttonText="Button Text" onButtonClick={() => {}} />
        <h2>Body layout</h2>
      </BodyLayout>
    </Page>
  </Box>
);

export const Basic = Template.bind({});
Basic.args = {
  title: 'Basic page',
  padded: false
};

export const Paddedlayout = Template.bind({});
Paddedlayout.args = {
  title: 'Page with padded layout',
  padded: true
};
