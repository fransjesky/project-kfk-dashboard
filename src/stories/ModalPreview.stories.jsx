import React, { useState } from 'react';
import ModalPreview from 'components/ModalPreview';
import { PrimaryButton } from 'components/Button';

export default {
  title: 'Components/Modal/ModalPreview',
  component: ModalPreview
};

export const Basic = (args) => {
  const [open, setOpen] = useState(false);

  return (
    <>
      <PrimaryButton onClick={() => setOpen(true)}>Open Modal</PrimaryButton>
      <ModalPreview open={open} onClose={() => setOpen(false)} title="Modal Title">
        <h5>Modal Body</h5>
      </ModalPreview>
    </>
  );
};
