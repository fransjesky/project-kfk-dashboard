import { AnyUploader } from 'components/Uploader/UploaderContainer';

export default {
  title: 'components/DropzoneUploader',
  component: AnyUploader
};

const Template = (args) => {
  return (
    <AnyUploader
      elementId={args.elementId}
      type="dropzone"
      allowedTypes={args.allowedTypes}
      maxSizeMb={args.maxSizeMb}
      onChange={args.onChange}
      onError={args.onError}
    />
  );
};

export const Basic = Template.bind({});
Basic.args = {
  elementId: 'elementId',
  allowedTypes: ['png', 'jpg'],
  maxSizeMb: 1,
  onChange: () => {},
  onError: () => {}
};
