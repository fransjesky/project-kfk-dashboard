import Dropdown from '../components/Dropdown';
import { IconButton, Box } from '@mui/material';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import EditIcon from '@mui/icons-material/Edit';
import MoreVertIcon from '@mui/icons-material/MoreVert';

export default {
  title: 'Components/Dropdown',
  component: Dropdown
};

const getItem = (label, icon, onClick) => ({ label, icon, onClick });

const ButtonComponent = () => (
  <IconButton>
    <MoreVertIcon />
  </IconButton>
);

export const Basic = (args) => <Dropdown {...args} />;
Basic.args = {
  button: <ButtonComponent />,
  items: [
    getItem('Edit', <EditIcon />, () => {}),
    getItem('Delete', <DeleteOutlineIcon />, () => {})
  ]
};
