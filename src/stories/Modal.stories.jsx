import React, { useState } from 'react';
import { Box, Button, TextField } from '@mui/material';
import Modal, { ModalContent, ModalHead, ModalButtons } from '../components/Modal';

export default {
  title: 'Components/Modal/Modal',
  component: Modal
};

export const Basic = (args) => {
  const [open, setOpen] = useState(false);
  return (
    <Box>
      <Button onClick={() => setOpen((prev) => !prev)}>Open Modal</Button>
      <Modal isOpen={open} onClose={() => setOpen(false)}>
        <ModalContent>
          <Box display="flex" flexDirection="column">
            <h1>This is a Modal</h1>
            <br />
            <TextField label="Input 1" variant="outlined" sx={{ width: '500px' }} />
            <br />
            <TextField label="Input 2" variant="outlined" sx={{ width: '500px' }} />
          </Box>
        </ModalContent>
      </Modal>
    </Box>
  );
};

export const WithHeadAndButtons = (args) => {
  const [open, setOpen] = useState(false);
  return (
    <Box>
      <Button onClick={() => setOpen((prev) => !prev)}>Open Modal</Button>
      <Modal isOpen={open} onClose={() => setOpen(false)}>
        <ModalHead title={args.title} subtitle={args.subtitle} />
        <ModalContent>
          <Box display="flex" flexDirection="column">
            <TextField label="Input 1" variant="outlined" sx={{ width: '500px' }} />
            <br />
            <TextField label="Input 2" variant="outlined" sx={{ width: '500px' }} />
          </Box>
        </ModalContent>
        <ModalButtons background={args.background}>
          <Button variant="outlined">Primary</Button>
          <Button variant="contained">Secondary</Button>
        </ModalButtons>
      </Modal>
    </Box>
  );
};

WithHeadAndButtons.args = {
  title: 'Modal Title',
  subtitle: 'This is modal subtitle',
  background: '#FFF5EB'
};

export const WithScroll = (args) => {
  const [open, setOpen] = useState(false);
  return (
    <Box>
      <Button onClick={() => setOpen((prev) => !prev)}>Open Modal</Button>
      <Modal isOpen={open} onClose={() => setOpen(false)}>
        <ModalHead title={args.title} subtitle={args.subtitle} />
        <ModalContent>
          <Box display="flex" flexDirection="column">
            <TextField label="Input 1" variant="outlined" sx={{ width: '500px' }} />
            <br />
            <TextField label="Input 2" variant="outlined" sx={{ width: '500px' }} />
            <br />
            <TextField label="Input 2" variant="outlined" sx={{ width: '500px' }} />
            <br />
            <TextField label="Input 2" variant="outlined" sx={{ width: '500px' }} />
            <br />
            <TextField label="Input 2" variant="outlined" sx={{ width: '500px' }} />
            <br />
            <TextField label="Input 2" variant="outlined" sx={{ width: '500px' }} />
            <br />
            <TextField label="Input 2" variant="outlined" sx={{ width: '500px' }} />
            <br />
            <TextField label="Input 2" variant="outlined" sx={{ width: '500px' }} />
            <br />
            <TextField label="Input 2" variant="outlined" sx={{ width: '500px' }} />
            <br />
            <TextField label="Input 2" variant="outlined" sx={{ width: '500px' }} />
            <br />
            <TextField label="Input 2" variant="outlined" sx={{ width: '500px' }} />
            <br />
            <TextField label="Input 2" variant="outlined" sx={{ width: '500px' }} />
            <br />
            <TextField label="Input 2" variant="outlined" sx={{ width: '500px' }} />
          </Box>
        </ModalContent>
        <ModalButtons background={args.background}>
          <Button variant="outlined">Primary</Button>
          <Button variant="contained">Secondary</Button>
        </ModalButtons>
      </Modal>
    </Box>
  );
};

WithScroll.args = {
  title: 'Modal Title',
  subtitle: 'This is modal subtitle',
  background: '#FFF5EB'
};
