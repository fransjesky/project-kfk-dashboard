import PageTitle from '../components/PageTitle';

export default {
  title: 'components/PageTitle',
  component: PageTitle
};

const Template = (args) => {
  return <PageTitle {...args} />;
};

export const Basic = Template.bind({});
Basic.args = {
  title: 'Page Title'
};

export const WithBackButton = Template.bind({});
WithBackButton.args = {
  title: 'Page Title with Back Button',
  withBackButton: true
};
