import CardTeam from '../components/CardTeam';

export default {
  title: 'components/CardTeam',
  component: CardTeam
};

export const Basic = (args) => <CardTeam {...args} />;
Basic.args = {
  name: 'full name',
  position: 'role',
  photo: 'https://discordhome.com/user_upload/emojis/ShibaCool-pngemoji.png',
  order: 1,
  description:
    'Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus alias voluptatem veniam minus. Exercitationem, totam blanditiis. Repellendus tenetur esse at omnis nobis possimus voluptatem necessitatibus provident alias ab. Sint, molestias.',
  createdAt: '05 January 2022',
  color: '#00A3FF',
  width: '290px',
  onDelete: () => {},
  onEdit: () => {}
};
