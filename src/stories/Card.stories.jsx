import Card from '../components/Card';

export default {
  title: 'components/Card',
  component: Card
};

export const Default = () => <Card variant="default" />;

export const Secondary = () => <Card variant="secondary" />;

export const CustomColor = () => <Card color="#00A3FF" />;
