import { Button, Stack } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import { PrimaryButton, SecondaryButton } from '../components/Button';

export default {
  title: 'Components/Button',
  component: Button
};

/*
 * This is basicly a modified version of Material UI Button
 * check https://mui.com/api/button/ for more information
 */
const Template = (args) => (
  <Stack spacing={2} direction="row">
    <PrimaryButton {...args}>Primary</PrimaryButton>
    <SecondaryButton {...args}>Secondary</SecondaryButton>
  </Stack>
);

export const Basic = Template.bind({});
Basic.args = {
  disabled: false,
  arise: false
};

export const Disabled = Template.bind({});
Disabled.args = {
  disabled: true,
  arise: false
};

export const Arise = Template.bind({});
Arise.args = {
  disabled: false,
  arise: true
};

export const WithIcon = (args) => (
  <Stack spacing={2} direction="row">
    <PrimaryButton startIcon={<AddIcon />} {...args}>
      Primary
    </PrimaryButton>
    <SecondaryButton endIcon={<AddIcon />} {...args}>
      Secondary
    </SecondaryButton>
  </Stack>
);
WithIcon.args = {
  disabled: false,
  arise: false
};
