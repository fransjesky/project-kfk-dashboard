import { Box } from '@mui/material';
import AutoGrid from 'components/AutoGrid';
import { mockImgAvatar } from 'utils/mockImages';
import CardTestimony from 'components/CardTestimony';

export default {
  title: 'Components/AutoGrid',
  component: AutoGrid,
  argTypes: {
    containerWidth: {
      control: { type: 'range', min: 400, max: 1440, step: 20 }
    }
  }
};

const getCardProperties = (n, obj) => {
  let arr = [];
  for (let i = 0; i < n; i++) {
    arr.push(obj);
  }
  return arr;
};

const Template = ({ containerWidth, ...args }) => {
  return (
    <Box sx={{ maxWidth: `${containerWidth}px` }}>
      <AutoGrid {...args}>
        {getCardProperties(6, {
          clientName: 'Long name that need at least two rows of container to fit',
          companyName: 'Xcidic',
          position: 'Manager',
          testimony:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sollicitudin dolor at metus cursus pharetra. Nulla quam justo, ullamcorper quis finibus vel, ultrices ut felis. Maecenas rutrum auctor sodales.',
          photo: mockImgAvatar(4),
          onDelete: () => {},
          onEdit: () => {}
        }).map((props) => (
          <CardTestimony {...props} />
        ))}
      </AutoGrid>
    </Box>
  );
};

export const Basic = Template.bind({});
Basic.args = {
  containerWidth: 1200
};

export const CustomGap = Template.bind({});
CustomGap.args = {
  containerWidth: 1200,
  gap: 24
};
