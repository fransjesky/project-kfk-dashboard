import { Stack, Typography } from '@mui/material';

export default {
  title: 'Components/Typography',
  component: Typography
};

/*
 * This is basicly a modified version of Material UI Typography
 * check https://mui.com/api/typography/#main-content for more information
 */
export const Basic = (args) => <Typography {...args} />;
Basic.args = {
  children: 'Typography'
};

/*
 * Check other variants in src/theme/overrides/Typography.js
 */
export const WithVariant = (args) => (
  <Stack>
    <Typography variant="h1">Variant h1</Typography>
    <Typography variant="h2">Variant h2</Typography>
    <Typography variant="h3">Variant h3</Typography>
    <Typography variant="s1">Variant s1</Typography>
    <Typography variant="s2">Variant s2</Typography>
    <Typography variant="b1">Variant b1</Typography>
    <Typography variant="c1">Variant c1</Typography>
  </Stack>
);

/*
 * Check other variants in src/theme/typography.js
 */
export const WithFontWeight = (args) => (
  <Stack>
    <Typography fontWeight="light">Font Light</Typography>
    <Typography fontWeight="regular">Font Regular</Typography>
    <Typography fontWeight="medium">Font Medium</Typography>
    <Typography fontWeight="bold">Font Bold</Typography>
  </Stack>
);

/*
 * Check other colors in src/theme/palette.js
 */
export const WithColor = (args) => (
  <Stack>
    <Typography color="black">Black</Typography>
    <Typography color="primary">Primary</Typography>
    <Typography color="#324aa8">Blue</Typography>
  </Stack>
);
