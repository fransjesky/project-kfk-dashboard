import React, { useState } from 'react';
import MultiSelect from 'components/MultiSelect';
import MenuItem from 'components/MenuItem';

export default {
  title: 'Components/MultiSelect',
  components: MultiSelect
};

const initialOptions = [
  'UX Design',
  'UX Research',
  'Design Thinking',
  'Graphic Design',
  'UI Design'
];

export const Basic = () => {
  const [value, setValue] = useState([]);

  return (
    <MultiSelect
      name="BasicMultiSelect"
      label="Basic"
      chipcolor="primary"
      value={value}
      onBlur={() => {}}
      error={false}
      onChange={setValue}
      options={initialOptions}
    />
  );
};

export const WithCustomValue = () => {
  const [options, setOptions] = useState(initialOptions);
  const [value, setValue] = useState([]);

  const handleAddCustomValue = (val) => {
    if (!val) return;
    if (options.includes(val)) return;
    setOptions((prev) => [...prev, val]);
  };

  return (
    <MultiSelect
      name="BasicMultiSelect"
      label="Basic"
      chipcolor="primary"
      value={value}
      onBlur={() => {}}
      error={false}
      onChange={setValue}
      onAddCustomValue={handleAddCustomValue}
      options={options}
      withCustomValue
    />
  );
};
