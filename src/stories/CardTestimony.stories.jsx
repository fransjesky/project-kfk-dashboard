import CardTestimony from 'components/CardTestimony';
import { mockImgAvatar } from 'utils/mockImages';

export default {
  title: 'Components/CardTestimony',
  component: CardTestimony
};

export const Basic = (args) => <CardTestimony {...args} />;
Basic.args = {
  clientName: 'Long name that need at least two rows of container to fit',
  companyName: 'Xcidic',
  position: 'Manager',
  testimony:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sollicitudin dolor at metus cursus pharetra. Nulla quam justo, ullamcorper quis finibus vel, ultrices ut felis. Maecenas rutrum auctor sodales.',
  photo: mockImgAvatar(4),
  onDelete: () => {},
  onEdit: () => {}
};
