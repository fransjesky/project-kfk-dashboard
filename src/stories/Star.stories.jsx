import Star from '../components/Star';

export default {
  title: 'components/Star',
  component: Star
};

export const Default = () => <Star />;

export const Small = () => <Star size="small" />;

export const CustomColor = () => <Star color="#C81D23" />;
