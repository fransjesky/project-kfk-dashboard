import React, { useState } from 'react';
import { Box, Button } from '@mui/material';
import ModalConfirmation from '../components/ModalConfirmation';

export default {
  title: 'Components/Modal/ModalConfirmation',
  component: ModalConfirmation
};

export const ModalConfirmations = (args) => {
  const [open, setOpen] = useState(false);
  return (
    <Box>
      <Button onClick={() => setOpen((prev) => !prev)}>Open Modal Confirmation</Button>
      <ModalConfirmation {...args} onClose={() => setOpen(false)} open={open} />
    </Box>
  );
};

ModalConfirmations.args = {
  variant: '',
  title: '',
  message: '',
  handleYes: () => {}
};
