import { Stack, TextField } from '@mui/material';
import MenuItem from '../components/MenuItem';

export default {
  title: 'Components/TextField',
  component: TextField
};

const Template = (args) => <TextField label="Textfield" {...args} />;

export const Basic = Template.bind({});
Basic.args = {
  value: 'Basic field'
};

export const TextArea = Template.bind({});
TextArea.args = {
  value: 'Text area',
  multiline: true,
  minRows: 4,
  maxRows: 8,
  error: false,
  helperText: ''
};

export const Select = (args) => (
  <TextField label="Select field" select {...args}>
    <MenuItem value="1">Option 1</MenuItem>
    <MenuItem value="2">Option 2</MenuItem>
    <MenuItem value="3">Option 3</MenuItem>
  </TextField>
);
Select.args = {
  value: '1',
  error: false,
  helperText: ''
};

export const Error = Template.bind({});
Error.args = {
  error: true,
  value: 'Error field'
};

export const WithHelperText = (args) => (
  <Stack spacing={1}>
    <TextField label="Basic field" helperText="Plain message" />
    <TextField label="Error field" error helperText="Error message" />
  </Stack>
);
