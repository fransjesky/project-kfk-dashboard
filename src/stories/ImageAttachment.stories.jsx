import ImageAttachment from 'components/ImageAttachment';

export default {
  title: 'Components/ImageAttachment',
  component: ImageAttachment
};

export const Basic = (args) => <ImageAttachment {...args} />;
Basic.args = {
  fileData: {
    location:
      'https://cdn.dribbble.com/users/2947819/screenshots/17582865/media/bde889a3ab4c568aac2a2883c349dfea.png?compress=1&resize=1200x900&vertical=top',
    name: 'Sample Image.png'
  },
  onDelete: () => {}
};
