import React from 'react';
import Table from '../components/Table';
import { Avatar, Box, Typography } from '@mui/material';

export default {
  title: 'components/Table',
  component: Table
};

const Template = (args) => {
  return (
    <Table
      heads={args.heads}
      rows={args.rows}
      specialColumns={args.specialColumns}
      withAction={args.withAction}
      actions={args.actions}
      onClickColumn={args.handleClick}
    />
  );
};

const HEADS = ['Name', 'Email', 'Subject', 'Date Added'];

const DUMMY_DATA = [
  {
    name: 'test1',
    email: 'test1@email.com',
    subject: 'test1 subject',
    dateAdded: 'test1 date'
  },
  {
    name: 'test2',
    email: 'test2@email.com',
    subject: 'test2 subject',
    dateAdded: 'test2 date'
  },
  {
    name: 'test3',
    email: 'test3@email.com',
    subject: 'test3 subject',
    dateAdded: 'test3 date'
  },
  {
    name: 'test4',
    email: 'test4@email.com',
    subject: 'test4 subject',
    dateAdded: 'test4 date'
  }
];

const CreateRow = (arr) => {
  let data = [];
  for (let i = 0; i < arr?.length; i++) {
    data.push([arr[i].name, arr[i].email, arr[i].subject, arr[i].dateAdded]);
  }
  return data;
};

const actions = [
  {
    type: 'edit',
    handler: (rowData) => {
      console.log('edit: ', rowData);
    }
  },
  {
    type: 'delete',
    handler: (rowData) => {
      console.log('delete: ', rowData);
    }
  }
];

const CustomName = ({ value }) => (
  <Box sx={{ display: 'flex', alignItems: 'center', maxWidth: '240px' }}>
    <Avatar sx={{ width: 32, height: 32 }}>A</Avatar>
    <Typography variant="b4" fontWeight="medium" ml={3}>
      {value}
    </Typography>
  </Box>
);

const handleClick = (val) => console.log('click: ', val);

export const Basic = Template.bind({});
Basic.args = {
  heads: HEADS,
  rows: CreateRow(DUMMY_DATA),
  handleClick: handleClick
};

export const WithSpecialColumn = Template.bind({});
WithSpecialColumn.args = {
  heads: HEADS,
  rows: CreateRow(DUMMY_DATA),
  withSpecialColumn: true,
  specialColumns: { 0: (val) => <CustomName value={val} /> },
  handleClick: handleClick
};

export const WithAction = Template.bind({});
WithAction.args = {
  heads: HEADS,
  rows: CreateRow(DUMMY_DATA),
  withAction: true,
  actions: actions,
  handleClick: handleClick
};

export const WithActionAndSpecialColumn = Template.bind({});
WithActionAndSpecialColumn.args = {
  heads: HEADS,
  rows: CreateRow(DUMMY_DATA),
  withSpecialColumn: true,
  specialColumns: { 0: (val) => <CustomName value={val} /> },
  withAction: true,
  actions: actions,
  handleClick: handleClick
};
