import Breadcrumbs from '../components/Breadcrumbs';

export default {
  title: 'components/Breadcrumbs',
  component: Breadcrumbs
};

const Template = (args) => {
  return <Breadcrumbs {...args} />;
};

const PAGE_ROUTE_2 = [{ label: 'Overview', link: '/' }, { label: 'Roles Management' }];

const PAGE_ROUTE_3 = [
  { label: 'Overview', link: '/' },
  { label: 'Roles Management', link: '/roles' },
  { label: 'Edit Roles' }
];

export const Basic = Template.bind({});
Basic.args = {
  items: PAGE_ROUTE_2
};

export const MorePages = Template.bind({});
MorePages.args = {
  items: PAGE_ROUTE_3
};
