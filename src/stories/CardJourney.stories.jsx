import CardJourney from 'components/CardJourney';

export default {
  title: 'components/CardJourney',
  component: CardJourney
};

export const Basic = (args) => <CardJourney {...args} />;
Basic.args = {
  year: '2016',
  title:
    'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Veritatis eligendi ratione minus deleniti fugit voluptatem, necessitatibus quos nobis ex laudantium et delectus quisquam totam cumque fugiat accusamus expedita dolore explicabo',
  backgroundColor: '#673ab7',
  foregroundColor: '#9c27b0',
  onDelete: () => {},
  onEdit: () => {}
};
