import React, { useState } from 'react';
import Pagination from '../components/Pagination';

export default {
  title: 'components/Pagination',
  component: Pagination
};

const Template = (args) => {
  const [currentPage, setCurrentPage] = useState(1);
  const handlePageChange = (event, value) => {
    setCurrentPage(value);
  };
  return (
    <Pagination totalPage={args.totalPage} currentPage={currentPage} onChange={handlePageChange} />
  );
};

export const Basic = Template.bind({});
Basic.args = {
  totalPage: 10
};
