import CardTechStack from 'components/CardTechStack';

export default {
  title: 'components/CardTech',
  component: CardTechStack
};

export const Basic = (args) => <CardTechStack {...args} />;
Basic.args = {
  title: 'react',
  category: 'tech stack',
  color: '#00A3FF',
  logo: 'https://reactjs.org/logo-og.png',
  order: 1,
  createdAt: '05 January 2022',
  onDelete: () => {},
  onEdit: () => {}
};
