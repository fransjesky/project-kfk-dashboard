import Header from '../components/Header';

export default {
  title: 'components/Header',
  component: Header
};

const Template = (args) => {
  return <Header {...args} />;
};

const PAGE_ROUTE = [
  { label: 'Overview', link: '/' },
  { label: 'Roles Management', link: '/roles' },
  { label: 'Edit Roles' }
];

export const Basic = Template.bind({});
Basic.args = {
  routes: PAGE_ROUTE
};

export const withButton = Template.bind({});
withButton.args = {
  routes: PAGE_ROUTE,
  withButton: true,
  buttonText: 'Button Text',
  onButtonClick: () => {}
};

export const WithBackButton = Template.bind({});
WithBackButton.args = {
  routes: PAGE_ROUTE,
  withBackButton: true
};

export const withButtonAndBackButton = Template.bind({});
withButtonAndBackButton.args = {
  routes: PAGE_ROUTE,
  withBackButton: true,
  withButton: true,
  buttonText: 'Button Text',
  onButtonClick: () => {}
};

export const withSearchBar = Template.bind({});
withSearchBar.args = {
  routes: PAGE_ROUTE,
  withSearchBar: true,
  searchValue: 'Search Value',
  onSearch: () => {}
};

export const withBackButtonAndSearchBar = Template.bind({});
withBackButtonAndSearchBar.args = {
  routes: PAGE_ROUTE,
  withBackButton: true,
  withSearchBar: true,
  searchValue: 'Search Value',
  onSearch: () => {}
};

export const withButtonAndSearchBar = Template.bind({});
withButtonAndSearchBar.args = {
  routes: PAGE_ROUTE,
  withSearchBar: true,
  searchValue: 'Search Value',
  onSearch: () => {},
  withButton: true,
  buttonText: 'Button Text',
  onButtonClick: () => {}
};

export const withAllButtonAndSearchBar = Template.bind({});
withAllButtonAndSearchBar.args = {
  routes: PAGE_ROUTE,
  withBackButton: true,
  withSearchBar: true,
  searchValue: 'Search Value',
  onSearch: () => {},
  withButton: true,
  buttonText: 'Button Text',
  onButtonClick: () => {}
};
