import { alpha } from '@mui/material/styles';

// ----------------------------------------------------------------------

function createGradient(color1, color2) {
  return `linear-gradient(to bottom, ${color1}, ${color2})`;
}

// SETUP COLORS
const GREY = {
  10: '#F3F3F3',
  20: '#E5E5E5',
  30: '#D8D8D8',
  40: '#BEBEBE',
  50: '#A9A9A9',
  60: '#979797',
  70: '#858585',
  80: '#727272',
  90: '#616161',
  100: '#3D3D3D',
  500: '#919EAB',
  600: '#637381',
  700: '#454F5B',
  800: '#212B36',
  900: '#161C24',
  500_8: alpha('#919EAB', 0.08),
  500_12: alpha('#919EAB', 0.12),
  500_16: alpha('#919EAB', 0.16),
  500_24: alpha('#919EAB', 0.24),
  500_32: alpha('#919EAB', 0.32),
  500_48: alpha('#919EAB', 0.48),
  500_56: alpha('#919EAB', 0.56),
  500_80: alpha('#919EAB', 0.8)
};

const PRIMARY = {
  main: '#FF9D42',
  tertiary: '#FF9330',
  dark: '#F18119',
  10: '#FFFBF7',
  20: '#FFF8F2',
  30: '#FFF3E8',
  40: '#FFE7D2',
  50: '#FFDAB7',
  60: '#FFD9B6',
  70: '#FFD8B4',
  80: '#FFB571',
  90: '#FFA551',
  100: '#FF9D42',
  110: '#FF9330',
  130: '#F18119'
};

const SECONDARY = {
  main: '#00A3FF',
  tertiary: '#0093E5',
  dark: '#0083CC',
  10: '#BCE7FF',
  20: '#A3DEFF',
  30: '#8FD7FF',
  40: '#7ACFFF',
  50: '#66C8FF',
  60: '#52C1FF',
  70: '#3DB9FF',
  80: '#29B2FF',
  90: '#14ABFF',
  100: '#00A3FF',
  110: '#0093E5',
  130: '#0083CC'
};

const TERTIARY = {
  main: '#0F1A2A',
  tertiary: '#0A182E',
  dark: '#031228',
  10: '#5394F0',
  20: '#4A85DA',
  30: '#3D70B9',
  40: '#335E9B',
  50: '#2C5084',
  60: '#25426C',
  70: '#1E3557',
  80: '#162740',
  90: '#132135',
  100: '#0F1A2A',
  110: '#0A182E',
  130: '#031228'
};

const NEUTRAL = {
  main: '#0f1012',
  10: '#94a3b8',
  20: '#8593a6',
  30: '#8593a6',
  40: '#768293',
  50: '#687281',
  60: '#59626e',
  70: '#4a525c',
  80: '#3b414a',
  90: '#2c3137',
  100: '#0f1012'
};

const OTHER = {
  main: '#ffffff',
  bodyDivider: '#e8e8e8',
  inputBorder: '#8c8c8d',
  inputHoverBorder: '#474749',
  dashboardBackground: '#f7fafe'
};

const INFO = {
  main: '#009ACB',
  lighter: '#D0F2FF',
  light: '#74CAFF',
  dark: '#0C53B7',
  darker: '#04297A',
  contrastText: '#fff'
};
const SUCCESS = {
  main: '#36BB20',
  fill: '#DEFFB5',
  lighter: '#E9FCD4',
  light: '#AAF27F',
  dark: '#229A16',
  darker: '#08660D',
  contrastText: GREY[800]
};
const WARNING = {
  main: '#F9CF3C',
  fill: '#FBF8BF',
  lighter: '#FFF7CD',
  light: '#FFE16A',
  dark: '#B78103',
  darker: '#7A4F01',
  contrastText: GREY[800]
};
const ERROR = {
  main: '#DB2525',
  fill: '#FFCACA',
  lighter: '#FFE7D9',
  light: '#FFA48D',
  dark: '#B72136',
  darker: '#7A0C2E',
  contrastText: '#fff'
};

const GRADIENTS = {
  primary: createGradient('#FF9D42', '#E53945'),
  sidenav: createGradient('#414141', '#070707'),
  info: createGradient(INFO.light, INFO.main),
  success: createGradient(SUCCESS.light, SUCCESS.main),
  warning: createGradient(WARNING.light, WARNING.main),
  error: createGradient(ERROR.light, ERROR.main)
};

const CHART_COLORS = {
  violet: ['#826AF9', '#9E86FF', '#D0AEFF', '#F7D2FF'],
  blue: ['#2D99FF', '#83CFFF', '#A5F3FF', '#CCFAFF'],
  green: ['#2CD9C5', '#60F1C8', '#A4F7CC', '#C0F2DC'],
  yellow: ['#FFE700', '#FFEF5A', '#FFF7AE', '#FFF3D6'],
  red: ['#FF6C40', '#FF8F6D', '#FFBD98', '#FFF2D4']
};

const palette = {
  common: { black: '#000', white: '#fff' },
  primary: { ...PRIMARY },
  secondary: { ...SECONDARY },
  tertiary: { ...TERTIARY },
  neutral: { ...NEUTRAL },
  info: { ...INFO },
  other: { ...OTHER },
  success: { ...SUCCESS },
  warning: { ...WARNING },
  error: { ...ERROR },
  grey: GREY,
  white: {
    main: '#ffffff'
  },
  black: {
    main: '#19191b'
  },
  gradients: GRADIENTS,
  chart: CHART_COLORS,
  divider: GREY[500_24],
  text: { primary: GREY[800], secondary: GREY[600], disabled: GREY[500] },
  background: { paper: '#fff', default: '#fff', neutral: GREY[200] },
  action: {
    active: GREY[600],
    hover: GREY[500_8],
    selected: GREY[500_16],
    disabled: GREY[500_80],
    disabledBackground: GREY[500_24],
    focus: GREY[500_24],
    hoverOpacity: 0.08,
    disabledOpacity: 0.48
  }
};

export default palette;
