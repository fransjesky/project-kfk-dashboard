// material
import { useTheme } from '@mui/material/styles';
import { GlobalStyles as GlobalThemeStyles } from '@mui/material';

// ----------------------------------------------------------------------

export default function GlobalStyles() {
  const theme = useTheme();

  return (
    <GlobalThemeStyles
      styles={{
        '*': {
          margin: 0,
          padding: 0,
          boxSizing: 'border-box',
          fontFamily: 'Satoshi'
        },
        html: {
          width: '100%',
          height: '100%',
          WebkitOverflowScrolling: 'touch'
        },
        body: {
          width: '100%',
          height: '100%',
          fontFamily: 'Satoshi'
        },
        '#root': {
          width: '100%',
          height: '100%'
        },
        input: {
          fontFamily: 'Satoshi !important',

          '&[type=number]': {
            MozAppearance: 'textfield',
            '&::-webkit-outer-spin-button': {
              margin: 0,
              WebkitAppearance: 'none'
            },
            '&::-webkit-inner-spin-button': {
              margin: 0,
              WebkitAppearance: 'none'
            }
          }
        },
        textarea: {
          '&::-webkit-input-placeholder': {
            color: theme.palette.text.disabled
          },
          '&::-moz-placeholder': {
            opacity: 1,
            color: theme.palette.text.disabled
          },
          '&:-ms-input-placeholder': {
            color: theme.palette.text.disabled
          },
          '&::placeholder': {
            color: theme.palette.text.disabled
          }
        },
        'h1, h2, h3, h4, h5, h6, a, p, span, caption': {
          fontFamily: 'Satoshi'
        },

        img: { display: 'block', maxWidth: '100%' },

        '.MuiInputLabel-root': {
          fontFamily: 'Satoshi !important'
        },
        '.MuiButton-root': {
          fontFamily: 'Satoshi !important'
        },
        '.MuiFormHelperText-root': {
          fontFamily: 'Satoshi !important'
        },
        '.MuiOutlinedInput-input': {
          fontFamily: 'Satoshi !important'
        },
        '.MuiTypography-root': {
          fontFamily: 'Satoshi !important'
        },

        // Lazy Load Img
        '.blur-up': {
          WebkitFilter: 'blur(5px)',
          filter: 'blur(5px)',
          transition: 'filter 400ms, -webkit-filter 400ms'
        },
        '.blur-up.lazyloaded ': {
          WebkitFilter: 'blur(0)',
          filter: 'blur(0)'
        }
      }}
    />
  );
}
