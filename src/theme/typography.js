// ----------------------------------------------------------------------

export function pxToRem(value) {
  return `${value / 16}rem`;
}

export function responsiveFontSizes({ sm, md, lg }) {
  return {
    '@media (min-width:576px)': {
      fontSize: pxToRem(sm)
    },
    '@media (min-width:768px)': {
      fontSize: pxToRem(md)
    },
    '@media (min-width:1280px)': {
      fontSize: pxToRem(lg)
    }
  };
}

const FONT_PRIMARY = 'sans-serif';

const typography = {
  fontFamily: FONT_PRIMARY,
  fontWeightLight: 300,
  fontWeightRegular: 400,
  fontWeightMedium: 500,
  fontWeightBold: 700,
  greeting: {
    lineHeight: '93.52px',
    lineHeight: 1.5,
    fontSize: pxToRem(58.45)
  },
  h1: {
    lineHeight: '83.136px',
    lineHeight: 1.5,
    fontSize: pxToRem(51.96)
  },
  h2: {
    lineHeight: '73.888px',
    lineHeight: 1.5,
    fontSize: pxToRem(46.18)
  },
  h3: {
    lineHeight: '65.68px',
    lineHeight: 1.5,
    fontSize: pxToRem(41.05)
  },
  h4: {
    lineHeight: '58.384px',
    lineHeight: 1.5,
    fontSize: pxToRem(36.49)
  },
  h5: {
    lineHeight: '51.904px',
    lineHeight: 1.5,
    fontSize: pxToRem(32.44)
  },
  h6: {
    lineHeight: '46.128px',
    lineHeight: 1.5,
    fontSize: pxToRem(28.83)
  },
  subtitle1: {
    lineHeight: '30.756px',
    lineHeight: 1.5,
    fontSize: pxToRem(25.63)
  },
  subtitle2: {
    lineHeight: '27.336px',
    lineHeight: 1.5,
    fontSize: pxToRem(22.78)
  },
  subtitle3: {
    lineHeight: '24.3px',
    lineHeight: 1.5,
    fontSize: pxToRem(20.25)
  },
  subtitle4: {
    lineHeight: '21.6px',
    lineHeight: 1.5,
    fontSize: pxToRem(18)
  },
  body1: {
    lineHeight: '19.2px',
    lineHeight: 1.5,
    fontSize: pxToRem(16)
  },
  body2: {
    lineHeight: '17.604px',
    lineHeight: 1.5,
    fontSize: pxToRem(14.22)
  },
  body3: {
    lineHeight: '15.168px',
    lineHeight: 1.5,
    fontSize: pxToRem(12.64)
  },
  caption1: {
    lineHeight: '13.488px',
    lineHeight: 1.5,
    fontSize: pxToRem(11.24)
  },
  caption2: {
    lineHeight: '11.988px',
    lineHeight: 1.5,
    fontSize: pxToRem(9.99)
  },
  overline: {
    fontWeight: 700,
    lineHeight: 1.5,
    fontSize: pxToRem(12),
    letterSpacing: 1.1,
    textTransform: 'uppercase'
  },
  button: {
    fontWeight: 700,
    lineHeight: 24 / 14,
    fontSize: pxToRem(14),
    textTransform: 'capitalize'
  }
};

export default typography;
