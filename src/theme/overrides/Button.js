import { pxToRem } from 'theme/typography';

const Button = (theme) => {
  return {
    MuiButton: {
      defaultProps: {
        disableRipple: true
      },
      styleOverrides: {
        root: {
          minWidth: 128,
          minHeight: 42,
          borderRadius: 12,
          textTransform: 'none',
          fontFamily: 'Satoshi',
          fontSize: pxToRem(14),
          fontWeight: 500
        },
        endIcon: {
          '& .MuiSvgIcon-root': {
            fontSize: pxToRem(16),
            fontWeight: theme.typography.fontWeightMedium
          }
        }
      },
      variants: [
        {
          props: { variant: 'primary' },
          style: {
            backgroundColor: theme.palette.primary.main,
            color: theme.palette.white.main,
            '&:hover': {
              backgroundColor: theme.palette.primary.tertiary
            },
            '&:active': {
              backgroundColor: theme.palette.primary.dark
            },
            '&[disabled]': {
              color: theme.palette.grey[10],
              backgroundColor: theme.palette.grey[50]
            }
          }
        },
        {
          props: { variant: 'secondary' },
          style: {
            border: `solid 1px ${theme.palette.primary.main}`,
            backgroundColor: theme.palette.white.main,
            color: theme.palette.primary.main,
            '&:hover': {
              borderColor: theme.palette.primary.tertiary,
              backgroundColor: theme.palette.white.main
            },
            '&:active': {
              borderColor: theme.palette.primary.dark
            },
            '&[disabled]': {
              color: theme.palette.grey[50],
              borderColor: theme.palette.grey[50]
            }
          }
        },
        {
          props: { variant: 'link' },
          style: {
            border: 'none',
            backgroundColor: 'transparent',
            color: theme.palette.primary.main,
            '&:hover': {
              backgroundColor: 'transparent'
            }
          }
        },
        {
          props: { variant: 'link-emphasized' },
          style: {
            border: 'none',
            backgroundColor: 'transparent',
            color: theme.palette.primary.main,
            '&:hover': {
              backgroundColor: 'transparent'
            }
          }
        },
        {
          props: { variant: 'primary-emphasized' },
          style: {
            backgroundColor: theme.palette.primary.main,
            color: theme.palette.white.main,
            '&:hover': {
              backgroundColor: theme.palette.primary.dark
            },
            '&:active': {
              backgroundColor: theme.palette.primary.tertiary
            },
            '&[disabled]': {
              color: theme.palette.grey[10],
              backgroundColor: theme.palette.grey[50]
            }
          }
        },
        {
          props: { variant: 'secondary-emphasized' },
          style: {
            border: `solid 1px ${theme.palette.primary.main}`,
            backgroundColor: theme.palette.white.main,
            color: theme.palette.primary.main,
            '&:hover': {
              borderColor: theme.palette.primary.dark,
              backgroundColor: theme.palette.white.main
            },
            '&:active': {
              borderColor: theme.palette.primary.tertiary,
              color: theme.palette.primary.tertiary
            },
            '&[disabled]': {
              color: theme.palette.grey[40],
              borderColor: theme.palette.grey[40]
            }
          }
        },
        {
          props: { arise: true },
          style: {
            boxShadow: '-1px 1px 4px 0px #170D4742'
          }
        }
      ]
    }
  };
};
export default Button;
