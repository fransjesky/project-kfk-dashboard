import { pxToRem } from 'theme/typography';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';

const Form = (theme) => {
  return {
    MuiFormControl: {
      defaultProps: {
        fullWidth: true,
        margin: 'normal'
      },
      styleOverrides: {
        root: {
          minHeight: 56
        }
      }
    },
    MuiTextField: {
      defaultProps: {
        key: 'Random String',
        margin: 'dense',
        fullWidth: true,
        SelectProps: {
          IconComponent: () => (
            <KeyboardArrowDownIcon sx={{ position: 'absolute', right: 14, bottom: 12 }} />
          )
        }
      }
    },
    MuiFormHelperText: {
      styleOverrides: {
        root: {
          fontFamily: 'Satoshi',
          fontWeight: theme.typography.fontWightLight,
          color: theme.palette.black.main,
          fontSize: pxToRem(12) // Caption
        }
      }
    },
    MuiRadio: {
      defaultProps: {
        disableRipple: true
      },
      styleOverrides: {
        root: {
          '&.Mui-checked': {
            color: theme.palette.secondary.main
          },
          '&:hover': {
            backgroundColor: 'transparent'
          }
        }
      }
    },
    MuiFormControlLabel: {
      styleOverrides: {
        root: {
          '& .MuiTypography-root': {
            fontFamily: 'Satoshi',
            fontSize: pxToRem(14)
          }
        }
      }
    }
  };
};

export default Form;
