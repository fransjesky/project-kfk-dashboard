// ----------------------------------------------------------------------

export default function Autocomplete(theme) {
  return {
    MuiAutocomplete: {
      styleOverrides: {
        paper: {
          boxShadow: theme.customShadows.z20
        },
        input: {
          '&.MuiOutlinedInput-input.MuiAutocomplete-inputFocused': {
            marginTop: '4px'
          }
        }
      }
    }
  };
}
