import { pxToRem } from 'theme/typography';

const Typography = () => {
  return {
    MuiTypography: {
      variants: [
        {
          props: { variant: 'greeting' },
          style: {
            fontSize: pxToRem(58.45)
          }
        },
        {
          props: { variant: 'h1' },
          style: {
            fontSize: pxToRem(51.96)
          }
        },
        {
          props: { variant: 'h2' },
          style: {
            fontSize: pxToRem(46.18)
          }
        },
        {
          props: { variant: 'h3' },
          style: {
            fontSize: pxToRem(41.05)
          }
        },
        {
          props: { variant: 'h4' },
          style: {
            fontSize: pxToRem(36.49)
          }
        },
        {
          props: { variant: 'h5' },
          style: {
            fontSize: pxToRem(32.44)
          }
        },
        {
          props: { variant: 'h6' },
          style: {
            fontSize: pxToRem(28.83)
          }
        },
        {
          props: { variant: 's1' },
          style: {
            fontSize: pxToRem(25.63)
          }
        },
        {
          props: { variant: 's2' },
          style: {
            fontSize: pxToRem(22.78)
          }
        },
        {
          props: { variant: 's3' },
          style: {
            fontSize: pxToRem(20.25)
          }
        },
        {
          props: { variant: 's4' },
          style: {
            fontSize: pxToRem(18)
          }
        },
        {
          props: { variant: 'b1' },
          style: {
            fontSize: pxToRem(16)
          }
        },
        {
          props: { variant: 'b2' },
          style: {
            fontSize: pxToRem(14.22)
          }
        },
        {
          props: { variant: 'b3' },
          style: {
            fontSize: pxToRem(12.64)
          }
        },
        {
          props: { variant: 'c1' },
          style: {
            fontSize: pxToRem(11.24)
          }
        },
        {
          props: { variant: 'c2' },
          style: {
            fontSize: pxToRem(9.99)
          }
        }
      ]
    }
  };
};

export default Typography;
