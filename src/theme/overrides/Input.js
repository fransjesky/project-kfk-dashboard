import { pxToRem } from 'theme/typography';

// ----------------------------------------------------------------------

export default function Input(theme) {
  return {
    MuiInputBase: {
      styleOverrides: {
        root: {
          height: 52,
          padding: '16.5px 0 5px',
          '&.Mui-disabled': {
            '& svg': { color: theme.palette.text.disabled }
          }
        },
        input: {
          fontSize: 14,
          '&::placeholder': {
            opacity: 1,
            color: theme.palette.text.disabled
          }
        },
        multiline: {
          height: 'auto'
        }
      }
    },
    MuiInput: {
      styleOverrides: {
        underline: {
          '&:before': {
            borderBottomColor: theme.palette.grey[500_56]
          }
        }
      }
    },
    MuiInputLabel: {
      styleOverrides: {
        root: {
          color: theme.palette.grey[80],
          fontSize: pxToRem(14), // c2
          fontWeight: 500,
          '&.Mui-focused': {
            color: theme.palette.grey[80]
          },
          '&.Mui-error': {
            color: theme.palette.grey[80]
          }
        },
        shrink: {
          marginTop: 15
        }
      }
    },
    MuiFilledInput: {
      styleOverrides: {
        root: {
          backgroundColor: theme.palette.grey[500_12],
          '&:hover': {
            backgroundColor: theme.palette.grey[500_16]
          },
          '&.Mui-focused': {
            backgroundColor: theme.palette.action.focus
          },
          '&.Mui-disabled': {
            backgroundColor: theme.palette.action.disabledBackground
          }
        },
        underline: {
          '&:before': {
            borderBottomColor: theme.palette.grey[500_56]
          }
        }
      }
    },
    MuiOutlinedInput: {
      styleOverrides: {
        root: {
          '& .MuiOutlinedInput-notchedOutline': {
            borderColor: theme.palette.grey[40]
          },
          '&:hover': {
            '.MuiOutlinedInput-notchedOutline': {
              borderColor: theme.palette.secondary[90]
            }
          },
          '&.Mui-focused': {
            '.MuiOutlinedInput-notchedOutline': {
              borderColor: theme.palette.secondary[90],
              boxShadow: `0 0 4px 1px ${theme.palette.secondary[90]}`
            }
          },
          '&.Mui-error': {
            '.MuiOutlinedInput-notchedOutline': {
              borderColor: theme.palette.error.main,
              boxShadow: `0 0 4px 1px ${theme.palette.error.main}`
            }
          },
          '&.Mui-disabled': {
            '& .MuiOutlinedInput-notchedOutline': {
              borderColor: theme.palette.action.disabledBackground
            }
          }
        },
        notchedOutline: {
          borderWidth: 1,
          '& legend': {
            width: 0
          }
        }
      }
    }
  };
}
