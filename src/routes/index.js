import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { AuthRoute, OtherRoute, ShouldRoute } from 'utils/accessRoutes';

import NotFound from 'modules/Page404';
// overview
import Overview from 'modules/Overview';
import Enquiries from 'modules/Overview/Enquiries';
// our journey
import OurJourney from 'modules/OurJourney';
// tech stack
import TechStack from 'modules/TechStack';
// company profile
import CompanyProfile from 'modules/CompanyProfile';
// our amazing partner
import OurAmazingPartner from 'modules/OurAmazingPartner';
// testimonies
import Testimonies from 'modules/Testimonies';
import AddTestimonies from 'modules/Testimonies/Add';
import EditTestimonies from 'modules/Testimonies/Edit';
// auth
import Login from 'modules/Auth/Login';
// portofolio
import OurPortfolio from 'modules/OurPortfolio';
// career
import Career from 'modules/Career';
import AddCareer from 'modules/Career/Add';
import EditCareer from 'modules/Career/Edit';
// teams
import OurTeams from 'modules/OurTeams';
// user management
import UserManagement from 'modules/UserManagement';
// user roles
import UserRoles from 'modules/UserRoles';
import AddUserRoles from 'modules/UserRoles/Add';
import EditUserRoles from 'modules/UserRoles/Edit';
// settings
import Settings from 'modules/Settings';

const Index = ({ childProps }) => {
  return (
    <Switch>
      {/* Auth */}
      <ShouldRoute path="/login" component={Login} />

      {/* Overview */}
      <AuthRoute exact path="/" component={Overview} />
      <AuthRoute exact path="/enquiries" component={Enquiries} />

      {/* Our Journey */}
      <AuthRoute exact path="/about/journey" component={OurJourney} />

      {/* Tech Stack */}
      <AuthRoute exact path="/about/tech-stack" component={TechStack} />

      {/* Company Profile */}
      <AuthRoute exact path="/about/company-profile" component={CompanyProfile} />

      {/* Our Amazing Partner */}
      <AuthRoute exact path="/home/partner" component={OurAmazingPartner} />

      {/* Testimonies */}
      <AuthRoute exact path="/works/testimony" component={Testimonies} />
      <AuthRoute exact path="/works/testimony/add" component={AddTestimonies} />
      <AuthRoute exact path="/works/testimony/edit/:testimonyId" component={EditTestimonies} />

      {/* Portofolio */}
      <AuthRoute path="/works/portfolio" component={OurPortfolio} />

      {/* Career */}
      <AuthRoute exact path="/career" component={Career} />
      <AuthRoute exact path="/career/add" component={AddCareer} />
      <AuthRoute exact path="/career/edit/:careerId" component={EditCareer} />

      {/* Teams */}
      <AuthRoute exact path="/team" component={OurTeams} />

      {/* User Management */}
      <AuthRoute path="/users/user" component={UserManagement} />

      {/* User Roles */}
      <AuthRoute exact path="/users/role" component={UserRoles} />
      <AuthRoute exact path="/users/role/create" component={AddUserRoles} />
      <AuthRoute exact path="/users/role/:id/edit" component={EditUserRoles} />

      {/* Settings */}
      <AuthRoute exact path="/settings" component={Settings} />

      <Route exact path="/404" component={NotFound} />
      <OtherRoute props={childProps} />
    </Switch>
  );
};

export default Index;
