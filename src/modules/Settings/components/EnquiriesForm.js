import React from 'react';
import { connect } from 'react-redux';
import { edit } from 'redux/actions';
import withFormik from 'utils/withFormik';
import * as Yup from 'yup';
import { Box, Button, TextField, Typography } from '@mui/material';
import { styled } from '@mui/material/styles';
import Switch from 'components/Form/Switch';

// schema validation
const schemeValidations = Yup.object().shape({
  recipientEmail: Yup.string()
    .email('Email must be a valid email address')
    .required('Field cannot be empty'),
  senderEmail: Yup.string()
    .email('Email must be a valid email address')
    .required('Field cannot be empty')
});

// initial values
const initialValueForm = {
  isEnableEnquiries: false,
  recipientEmail: '',
  senderEmail: ''
};

const mapPropsToValues = ({ settings }) => {
  const data = settings?.data;
  if (data) {
    return {
      recipientEmail: data.find((x) => x.optionName === 'recipientEmail').optionValue,
      senderEmail: data.find((x) => x.optionName === 'senderEmail').optionValue,
      isEnableEnquiries: data.find((x) => x.optionName === 'isEnableEnquiries').optionValue
    };
  }
  return initialValueForm;
};

const handleSubmitForm = (payload, ctx) => {
  const { isEnableEnquiries, recipientEmail, senderEmail } = payload;
  ctx.props.edit({
    name: 'setting',
    noLink: true,
    data: {
      isEnableEnquiries,
      recipientEmail,
      senderEmail
    },
    onSuccess: () => {
      ctx.resetForm();
      ctx.props.onSuccess();
    }
  });
};

const ColumnContainer = styled(Box)({
  display: 'flex',
  flexDirection: 'column'
});

const RowContainer = styled(Box)({
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'space-between',
  marginTop: '24px'
});

const EnquiriesForm = (props) => {
  const {
    values,
    touched,
    errors,
    handleChange,
    handleBlur,
    handleSubmit,
    setFieldValue,
    isValid,
    dirty
  } = props;
  const error = (val) => Boolean(touched[val] && errors[val]);

  const handleSwitchHighlight = (value) => {
    setFieldValue('isEnableEnquiries', value.target.checked);
  };

  return (
    <ColumnContainer>
      <Typography variant="s2" fontWeight="bold">
        Enquiries Form
      </Typography>
      <RowContainer>
        <ColumnContainer>
          <Typography variant="s4" fontWeight="medium" color="grey.130">
            Do you want to enable Enquiries Form?
          </Typography>
          <Typography variant="b1" color="grey.100">
            By doing this, visitors will be shown enquiries page and they will not be able to see
            the form.
          </Typography>
        </ColumnContainer>
        <Switch
          label={eval(values.isEnableEnquiries) ? 'On' : 'Off'}
          onChange={handleSwitchHighlight}
          checked={eval(values.isEnableEnquiries)}
        />
      </RowContainer>
      <RowContainer>
        <ColumnContainer>
          <Typography variant="s4" fontWeight="medium" color="grey.130">
            Recipient Email
          </Typography>
          <Typography variant="b1" color="grey.100">
            Set up the email which receives enquiry submissions from visitors.
          </Typography>
        </ColumnContainer>
        <TextField
          name="recipientEmail"
          label="Email Address"
          sx={{ width: '320px' }}
          value={values.recipientEmail}
          onChange={handleChange}
          onBlur={handleBlur}
          error={error('recipientEmail')}
          helperText={touched.recipientEmail && errors.recipientEmail}
          InputLabelProps={error('recipientEmail') ? { shrink: true } : {}}
        />
      </RowContainer>
      <RowContainer>
        <ColumnContainer>
          <Typography variant="s4" fontWeight="medium" color="grey.130">
            Sender Email
          </Typography>
          <Typography variant="b1" color="grey.100">
            Set up the email which will send email to visitors.
          </Typography>
        </ColumnContainer>
        <TextField
          name="senderEmail"
          label="Email Address"
          sx={{ width: '320px' }}
          value={values.senderEmail}
          onChange={handleChange}
          onBlur={handleBlur}
          error={error('senderEmail')}
          helperText={touched.senderEmail && errors.senderEmail}
          InputLabelProps={error('senderEmail') ? { shrink: true } : {}}
        />
      </RowContainer>
      <Box alignSelf="flex-end" mt={4}>
        <Button variant="primary" onClick={handleSubmit} disabled={!(dirty && isValid)}>
          Save
        </Button>
      </Box>
    </ColumnContainer>
  );
};

const FormikCreate = withFormik(
  EnquiriesForm,
  schemeValidations,
  mapPropsToValues,
  handleSubmitForm
);

const mapStateToProps = (state) => ({
  settings: state.module.view.settings
});

export default connect(mapStateToProps, { edit })(FormikCreate);
