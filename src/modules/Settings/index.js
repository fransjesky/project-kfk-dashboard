import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { load } from 'redux/actions';
import BodyLayout from 'layouts/dashboard/BodyLayout';
import Header from 'components/Header';
import Page from 'components/Page';
import EnquiriesForm from './components/EnquiriesForm';

const Settings = () => {
  const PAGE_ROUTE = [
    { label: 'Overview', link: '/' },
    { label: 'Settings', link: '/settings' }
  ];
  const [onSave, setOnSave] = useState(false);
  const dispatch = useDispatch();

  useEffect(
    () => {
      dispatch(
        load({
          name: 'setting',
          customName: 'settings'
        })
      );
    },
    [onSave] // eslint-disable-line
  );

  return (
    <Page title="Settings">
      <BodyLayout padded={false}>
        <Header routes={PAGE_ROUTE} />
        <EnquiriesForm
          onSuccess={() => {
            setOnSave(!onSave);
          }}
        />
      </BodyLayout>
    </Page>
  );
};

export default Settings;
