import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { load } from 'redux/actions';
import moment from 'moment';
import { Edit } from 'react-iconly';
import { Box, Button, Typography } from '@mui/material';
import { styled } from '@mui/material/styles';
import BodyLayout from 'layouts/dashboard/BodyLayout';
import Header from 'components/Header';
import Page from 'components/Page';
import ModalEdit from './components/ModalEdit';

const CompanyProfileContainer = styled(Box)({
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
  padding: '40px 32px',
  paddingLeft: '0px',
  borderRadius: '16px',
  backgroundColor: 'white',
  boxShadow: '2px 0px 3px rgba(115, 115, 139, 0.1), 0px 4px 20px rgba(115, 115, 139, 0.15)'
});

const CompanyProfile = () => {
  const PAGE_ROUTE = [
    { label: 'Overview', link: '/' },
    { label: 'Company Profile', link: '/about/company-profile' }
  ];
  const [isEditOpen, setIsEditOpen] = useState(false);
  const { companyProfile } = useSelector((state) => state.module.view);
  const dispatch = useDispatch();
  useEffect(
    () => {
      dispatch(
        load({
          name: 'profile',
          customName: 'companyProfile'
        })
      );
    },
    [isEditOpen] // eslint-disable-line
  );
  const companyProfileData = companyProfile?.data;
  return (
    <Page title="Company Profile">
      <BodyLayout padded={false}>
        <Header routes={PAGE_ROUTE} />
        <Box display="flex" flexDirection="column">
          <CompanyProfileContainer>
            <Box
              display="flex"
              flexDirection="column"
              borderLeft="8px solid #ff9d42"
              pl={4}
              py={3}
              flex={1}
            >
              <Typography variant="s2" fontWeight="bold" color="grey.100">
                {companyProfileData?.title}
              </Typography>
              <Typography variant="b1" fontWeight="medium" color="grey.90" mt={1}>
                Last update: {moment(companyProfileData?.updatedAt).format('DD MMM YYYY')}
              </Typography>
            </Box>
            <Box flex={2} sx={{ wordBreak: 'break-all' }}>
              <Typography variant="b1" fontWeight="medium" color="grey.80">
                {companyProfileData?.description}
              </Typography>
            </Box>
          </CompanyProfileContainer>
          <Box alignSelf="flex-end" mt={4}>
            <Button
              variant="primary"
              endIcon={<Edit set="broken" size="small" />}
              onClick={() => setIsEditOpen(true)}
            >
              Edit Company Profile
            </Button>
          </Box>
        </Box>
        <ModalEdit
          open={isEditOpen}
          id={1}
          onClose={() => setIsEditOpen(false)}
          onSuccess={() => {
            setIsEditOpen(false);
          }}
        />
      </BodyLayout>
    </Page>
  );
};

export default CompanyProfile;
