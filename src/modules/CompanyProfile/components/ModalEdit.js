import React, { useEffect } from 'react';
import { connect, useDispatch } from 'react-redux';
import { Box, TextField, Button } from '@mui/material';
import Modal, { ModalContent, ModalButtons, ModalHead } from 'components/Modal';
import * as Yup from 'yup';
import withFormik from 'utils/withFormik';
import { edit, load } from 'redux/actions';

// schema validation
const schemeValidations = Yup.object().shape({
  title: Yup.string()
    .required('Field cannot be empty')
    .test('Contains any non-whitespace character', 'Field cannot be empty', (str) =>
      isContainsNonWhitespace(str)
    ),
  description: Yup.string()
    .required('Please input at least 100 characters')
    .test(
      'Contains at least 100 character',
      'Please input at least 100 characters',
      (str) => str?.length >= 100 && isContainsNonWhitespace(str)
    )
});

const isContainsNonWhitespace = (str) => {
  if (str && str.replace(/\s/g, '').length > 0) {
    return true;
  } else {
    return false;
  }
};

// initial values
const initialValueForm = {
  id: '',
  title: '',
  description: ''
};

const mapPropsToValues = ({ companyProfile }) => {
  const data = companyProfile?.data;
  if (data) {
    return {
      id: data.id,
      title: data.title,
      description: data.description
    };
  }
  return initialValueForm;
};

const handleSubmitForm = (payload, ctx) => {
  const { id, title, description } = payload;
  ctx.props.edit({
    name: 'profile',
    id: id,
    noLink: true,
    data: {
      title,
      description
    },
    onSuccess: () => {
      ctx.props.onSuccess();
    }
  });
};

function ModalEdit(props) {
  const { open, onClose } = props;
  const {
    values,
    touched,
    errors,
    handleChange,
    handleBlur,
    handleSubmit,
    resetForm,
    isValid,
    dirty
  } = props;
  const error = (val) => Boolean(touched[val] && errors[val]);
  const dispatch = useDispatch();

  useEffect(
    () => {
      dispatch(
        load({
          name: 'profile',
          customName: 'companyProfile'
        })
      );
    },
    [] // eslint-disable-line
  );

  return (
    <Modal
      isOpen={open}
      onClose={() => {
        resetForm();
        onClose();
      }}
      maxWidth={1025}
    >
      <ModalHead title="Edit Company Profile" />
      <ModalContent>
        <Box display="flex" flexDirection="column" mt={3} width="900px">
          <TextField
            name="title"
            label="Title"
            sx={{ width: '320px' }}
            value={values.title}
            onChange={handleChange}
            onBlur={handleBlur}
            error={error('title')}
            helperText={touched.title && errors.title}
            InputLabelProps={error('title') ? { shrink: true } : {}}
          />
          <TextField
            label="Description"
            multiline
            minRows={3}
            sx={{ width: '100%' }}
            name="description"
            value={values.description}
            onChange={handleChange}
            onBlur={handleBlur}
            error={error('description')}
            helperText={
              error('description')
                ? touched.description && errors.description
                : 'Please input at least 100 characters'
            }
            InputLabelProps={error('description') ? { shrink: true } : {}}
          />
        </Box>
        <ModalButtons>
          <Button variant="primary" onClick={handleSubmit} disabled={!(dirty && isValid)}>
            Save
          </Button>
        </ModalButtons>
      </ModalContent>
    </Modal>
  );
}

const FormikCreate = withFormik(ModalEdit, schemeValidations, mapPropsToValues, handleSubmitForm);

const mapStateToProps = (state) => ({
  companyProfile: state.module.view.companyProfile
});

export default connect(mapStateToProps, { edit })(FormikCreate);
