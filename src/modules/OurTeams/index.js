import React, { useEffect, useState } from 'react';
// components
import { Box } from '@mui/material';
import Page from 'components/Page';
import BodyLayout from 'layouts/dashboard/BodyLayout';
import Header from 'components/Header';
import Pagination from 'components/Pagination';
import AutoGrid from 'components/AutoGrid';
import CardTeam from 'components/CardTeam';
import MenuItem from 'components/MenuItem';
import { DepartmentsDropdown } from './components/styled';
import ModalTeamsAdd from './components/ModalTeamsAdd';
import ModalTeamsEdit from './components/ModalTeamsEdit';
import EmptyState from 'components/EmptyState';
// redux
import { useSelector } from 'react-redux';
// utils
import {
  useLister,
  useRemover,
  useConfirm,
  useDepartments,
  useToggle,
  useSearch
} from 'utils/hooks';

const PAGE_TITLE = 'Our Teams';
const ROUTES = [
  { label: 'Overview', link: '/' },
  { label: 'Our Teams', link: '/team' }
];
const DEFAULT_TEAMS_QUERY = {
  sort: 'order',
  order: 'ASC',
  limit: 0,
  offset: 0,
  keyword: ''
};

const OurTeams = () => {
  const [teamsQuery, setTeamsQuery] = useState(DEFAULT_TEAMS_QUERY);
  const { keywordTeam, keywordGlobal } = useSelector((state) => state.module.keyword);
  const [departmentSearch, setDepartmentSearch] = useState('all');
  const { dispatchRemove } = useRemover('member', 'team');
  const { dispatchList } = useLister('member', 'teams');
  const { teams } = useSelector((state) => state.module.list);
  const { confirmInfo } = useConfirm();
  const { departments } = useDepartments();
  const { dispatchSearch, dispatchSearchGlobal } = useSearch('keywordTeam');
  const [openModalAdd, toggleModalAdd] = useToggle(false);
  const [openModalEdit, setOpenModalEdit] = useState(false);
  const [teamId, setTeamId] = useState('');
  const [loading, setLoading] = useState(true);

  const handleHeaderButtonClick = () => toggleModalAdd(true);
  const handlePagination = (event, value) => {
    setTeamsQuery((prev) => ({
      ...prev,
      offset: (value - 1) * prev.limit
    }));
  };

  useEffect(() => {
    dispatchSearch(keywordGlobal || '');

    return () => {
      dispatchSearchGlobal('');
    };
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    const timeout = setTimeout(() => {
      setTeamsQuery((prev) => ({
        ...DEFAULT_TEAMS_QUERY,
        limit: prev.limit,
        keyword: keywordTeam
      }));
    }, 500);

    return () => clearTimeout(timeout);
    // eslint-disable-next-line
  }, [keywordTeam]);

  // load members
  useEffect(() => {
    if (teamsQuery.limit > 0) {
      setLoading(true);
      dispatchList(teamsQuery, () => setLoading(false));
    }
    // eslint-disable-next-line
  }, [teamsQuery]);

  const handleDeleteTeam = (team) => () => {
    // open modal confirm
    confirmInfo({
      title: `Are you sure you want to delete '${team.name}'?`,
      message: 'You won’t be able to recover this in the future!',
      handler: () => {
        dispatchRemove(team.id, () => dispatchList(teamsQuery));
      }
    });
  };

  // sync pagination and autogrid
  const handleAutoGridChangeWidth = ({ width, cardWidth, gap }) => {
    setTeamsQuery((prev) => {
      if (width < cardWidth) return prev;
      const limit = Math.floor((width + gap) / (cardWidth + gap)) * 2;
      if (limit !== prev.limit) {
        return { ...prev, limit };
      }
      return prev;
    });
  };

  const handleSearchChange = (value) => {
    resetPage();
    dispatchSearch(value);
  };

  const handleDepartmentChange = (event) => {
    const value = event.target.value;
    resetPage();
    setDepartmentSearch(value);
    setTeamsQuery((prev) => ({
      ...DEFAULT_TEAMS_QUERY,
      limit: prev.limit,
      department: value == 'all' ? '' : value
    }));
  };

  const resetPage = () => setTeamsQuery((prev) => ({ ...prev, offset: 0 }));

  return (
    <Page title={PAGE_TITLE}>
      <BodyLayout>
        {openModalAdd && (
          <ModalTeamsAdd
            open={openModalAdd}
            onClose={() => toggleModalAdd(false)}
            onSuccess={() => dispatchList(teamsQuery)}
          />
        )}
        {openModalEdit && (
          <ModalTeamsEdit
            open={openModalEdit}
            onClose={() => setOpenModalEdit(false)}
            onSuccess={() => dispatchList(teamsQuery)}
            id={teamId}
          />
        )}
        <Header
          routes={ROUTES}
          withButton
          keyword={keywordTeam}
          buttonText="Add New Member"
          onButtonClick={handleHeaderButtonClick}
          withSearchBar
          onSearch={handleSearchChange}
        />
        <Box display="flex" justifyContent="flex-end" marginBottom={3}>
          <DepartmentsDropdown onChange={handleDepartmentChange} select value={departmentSearch}>
            <MenuItem value="all">All</MenuItem>
            {departments?.map((department) => (
              <MenuItem key={department.name} value={department.name}>
                {department.name}
              </MenuItem>
            ))}
          </DepartmentsDropdown>
        </Box>
        <AutoGrid onChangeWidth={handleAutoGridChangeWidth}>
          {teams?.data?.map((team) => (
            <CardTeam
              key={team.name + team.id}
              width="290px"
              name={team.name}
              position={team.role}
              photo={team.avatar}
              order={team.order}
              description={team.text}
              createdAt={team.createdAt}
              color={team.Department.color}
              onEdit={() => {
                setTeamId(team.id);
                setOpenModalEdit(true);
              }}
              onDelete={handleDeleteTeam(team)}
            />
          ))}
        </AutoGrid>
        {!loading && !teams?.data?.length && <EmptyState name="Teams" />}
        {teams?.data?.length && (
          <Box
            sx={{
              mt: 5,
              pb: 3,
              display: 'flex',
              flexGrow: 1,
              justifyContent: 'center',
              alignItems: 'flex-end'
            }}
          >
            <Pagination
              totalPage={Math.ceil((teams?.count ?? 0) / teamsQuery.limit) || 1}
              currentPage={Math.floor(teamsQuery.offset / teamsQuery.limit) + 1}
              onChange={handlePagination}
            />
          </Box>
        )}
      </BodyLayout>
    </Page>
  );
};

export default OurTeams;
