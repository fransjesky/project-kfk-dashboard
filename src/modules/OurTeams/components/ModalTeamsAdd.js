import React, { useCallback } from 'react';
// components
import Modal, { ModalHead } from 'components/Modal';
import ModalForm from './ModalForm';
// utils
import withFormik from 'utils/withFormik';
// actions
import { add } from 'redux/actions';
import { connect } from 'react-redux';
// schema validations
import SchemaValidations from './SchemaValidations';

// initial values
const initialValueForm = {
  name: '',
  role: '',
  department: '',
  text: '',
  avatar: '',
  order: ''
};

// handle submit
const handleSubmitForm = (payload, ctx) => {
  const { department, ...others } = payload;
  ctx.setStatus({ loading: true });
  ctx.props.add({
    name: 'member',
    data: {
      ...others,
      departmentId: JSON.parse(department).id
    },
    notLink: true,
    onSuccess: () => {
      ctx.props.onClose();
      ctx.resetForm();
      ctx.props.onSuccess();
    }
  });
};

const ModalTeamsAdd = ({ open, onClose, ...formikProps }) => {
  const { resetForm } = formikProps;

  const handleCloseModal = useCallback(() => {
    onClose();
    resetForm();
  }, [onClose, resetForm]);

  return (
    <Modal isOpen={open} onClose={handleCloseModal}>
      <ModalHead title="Add New Member" />
      <ModalForm {...formikProps} isAdd />
    </Modal>
  );
};

const FormikModalTeamsAdd = withFormik(
  ModalTeamsAdd,
  SchemaValidations,
  initialValueForm,
  handleSubmitForm
);

export default connect(null, { add })(FormikModalTeamsAdd);
