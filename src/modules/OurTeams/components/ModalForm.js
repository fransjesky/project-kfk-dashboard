import React, { useEffect, useState } from 'react';
// components
import { Box, TextField, Typography, Stack } from '@mui/material';
import { ModalContent, ModalButtons } from 'components/Modal';
import { AnyUploader } from 'components/Uploader/UploaderContainer';
import ImageAttachment from 'components/ImageAttachment';
import { PrimaryButton } from 'components/Button';
import { SectionTitle } from './styled';
import MenuItem from 'components/MenuItem';
// redux
import { useSelector } from 'react-redux';
// utils
import { useDepartments, useLister } from 'utils/hooks';

const getTextFieldProps = (
  { values, handleChange, handleBlur, touched, errors },
  name,
  helperText
) => ({
  name,
  value: values[name],
  onChange: handleChange,
  onBlur: handleBlur,
  error: Boolean(touched[name] && errors[name]),
  helperText: (touched[name] && errors[name]) || helperText
});

const ModalForm = (props) => {
  const {
    values,
    touched,
    errors,
    setFieldValue,
    handleSubmit,
    dirty,
    isValid,
    isAdd,
    oldDepartmentId
  } = props;
  const { departments } = useDepartments();
  const { dispatchList } = useLister('member/number', 'teamOrderOptions');
  const { teamOrderOptions } = useSelector((state) => state.module.list);
  const [departmentId, setDepartmentId] = useState(null);

  const handleDropzoneChange = (value) => {
    const { data } = value[0];
    setFieldValue('avatar', data.location);
    setFieldValue('avatarData', data);
  };

  const handleDeleteAttachment = () => {
    setFieldValue('avatar', '');
    setFieldValue('avatarData', null);
  };

  // get order options
  useEffect(() => {
    if (!values.department) return;
    dispatchList({ departmentId: JSON.parse(values.department).id }, (res) => {
      if (isAdd) {
        setFieldValue('order', res.data[res.data.length - 1]);
      }

      if (!oldDepartmentId) return;

      if (oldDepartmentId !== JSON.parse(values.department).id) {
        setFieldValue('order', res.data[res.data.length - 1]);
      }
    });

    setDepartmentId(JSON.parse(values.department).id);
    // eslint-disable-next-line
  }, [values.department]);

  return (
    <>
      <ModalContent>
        <Box sx={{ width: 'min(100vw, 646px)' }}>
          <Box sx={{ mb: 2 }}>
            <SectionTitle>Upload avatar</SectionTitle>
            <Typography variant="b1" color="grey.50">
              Files should be PNG
            </Typography>
          </Box>
          <Box>
            <Box sx={{ mb: 2 }}>
              {!values.avatar ? (
                <AnyUploader
                  elementId="dropzone__add-teams"
                  type="dropzone"
                  allowedTypes={['png']}
                  maxSizeMb={1}
                  onChange={handleDropzoneChange}
                  onError={() => {}}
                  isErrorInput={Boolean(touched.avatar && errors.avatar)}
                />
              ) : (
                <ImageAttachment
                  fileData={values.avatarData ?? { location: values?.avatar }}
                  onDelete={() => handleDeleteAttachment()}
                />
              )}
            </Box>
            <TextField label="Full Name" {...getTextFieldProps(props, 'name')} />
            <Stack direction="row" columnGap={2}>
              <TextField label="Role" {...getTextFieldProps(props, 'role')} />
              <TextField label="Department" select {...getTextFieldProps(props, 'department')}>
                {departments?.map((department) => (
                  <MenuItem key={department.name} value={JSON.stringify(department)}>
                    {department.name}
                  </MenuItem>
                ))}
              </TextField>
            </Stack>
            <TextField
              label="Description"
              multiline
              minRows={4}
              maxRows={4}
              helperText="Please share what good things our staff has to say and input a maximum of 180 characters"
              {...getTextFieldProps(
                props,
                'text',
                'Please share what good things our staff has to say and input a maximum of 180 characters'
              )}
            />
            <TextField
              label="Choose order"
              select
              disabled={isAdd || departmentId !== oldDepartmentId}
              {...getTextFieldProps(props, 'order')}
              sx={{ width: '213px' }}
            >
              {teamOrderOptions?.data.map((option, index) => {
                return oldDepartmentId === departmentId ? (
                  index !== teamOrderOptions.data.length - 1 && (
                    <MenuItem value={option} key={index}>
                      {option}
                    </MenuItem>
                  )
                ) : (
                  <MenuItem value={option} key={index}>
                    {option}
                  </MenuItem>
                );
              })}
            </TextField>
          </Box>
        </Box>
      </ModalContent>
      <ModalButtons>
        <PrimaryButton onClick={handleSubmit} disabled={!dirty || !isValid}>
          Save
        </PrimaryButton>
      </ModalButtons>
    </>
  );
};

export default ModalForm;
