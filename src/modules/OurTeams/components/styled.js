import { styled } from '@mui/material/styles';
import { TextField, Typography } from '@mui/material';

export const DepartmentsDropdown = styled((props) => <TextField {...props} />)({
  maxWidth: '257px',
  '& .MuiOutlinedInput-root': {
    padding: 0
  }
});

export const SectionTitle = (props) => (
  <Typography component="p" variant="s1" fontWeight="bold" {...props} />
);
