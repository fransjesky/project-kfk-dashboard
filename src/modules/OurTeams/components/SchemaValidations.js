import * as Yup from 'yup';

export default Yup.object().shape({
  name: Yup.string().trim().required('Field cannot be empty'),
  role: Yup.string().trim().required('Field cannot be empty'),
  department: Yup.string().required('Please choose the department'),
  text: Yup.string()
    .trim()
    .required('Field cannot be empty')
    .max(180, 'Please input a maximum of 180 characters'),
  order: Yup.number().nullable().required('Please choose a number'),
  avatar: Yup.string().required('Please choose a number')
});
