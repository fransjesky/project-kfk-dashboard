import React, { useEffect, useCallback } from 'react';
// components
import Modal, { ModalHead } from 'components/Modal';
import ModalForm from './ModalForm';
// utils
import withFormik from 'utils/withFormik';
import { useLoader } from 'utils/hooks';
// actions
import { edit } from 'redux/actions';
import { connect, useSelector } from 'react-redux';
// schema validations
import SchemaValidations from './SchemaValidations';

// initial values
const initialValueForm = {
  name: '',
  role: '',
  department: '',
  text: '',
  avatar: '',
  order: ''
};

const mapPropsToValues = ({ team }) => {
  const data = team?.data;

  if (data) {
    return {
      id: data.id,
      role: data.role,
      name: data.name,
      text: data.text,
      avatar: data.avatar,
      order: data.order,
      department: JSON.stringify({ id: data.Department.id, name: data.Department.name })
    };
  }
  return initialValueForm;
};

// handle submit
const handleSubmitForm = (payload, ctx) => {
  const { id, department, ...others } = payload;
  ctx.setStatus({ loading: true });
  ctx.props.edit({
    name: 'member',
    id,
    data: {
      ...others,
      departmentId: JSON.parse(department).id
    },
    noLink: true,
    onSuccess: () => {
      ctx.props.onClose();
      ctx.resetForm();
      ctx.props.onSuccess();
    }
  });
};

const ModalTeamsEdit = ({ open, onClose, id, ...formikProps }) => {
  const { resetForm } = formikProps;
  const { dispatchLoad } = useLoader('member', 'team');
  const { team } = useSelector((state) => state.module.view);

  const handleCloseModal = useCallback(() => {
    onClose();
    resetForm();
  }, [onClose, resetForm]);

  useEffect(() => {
    dispatchLoad(id);
    // eslint-disable-next-line
  }, [open, id]);

  return (
    <Modal isOpen={open} onClose={handleCloseModal}>
      <ModalHead title="Edit Member" />
      <ModalForm {...formikProps} oldDepartmentId={team?.data.Department.id} />
    </Modal>
  );
};

const FormikModalTeamsEdit = withFormik(
  ModalTeamsEdit,
  SchemaValidations,
  mapPropsToValues,
  handleSubmitForm
);

const mapStateToProps = (state) => ({
  team: state.module.view.team
});

export default connect(mapStateToProps, { edit })(FormikModalTeamsEdit);
