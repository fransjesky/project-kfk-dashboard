import React, { useState, useEffect } from 'react';
import { Box, TextField } from '@mui/material';
import MenuItem from 'components/MenuItem';
import Page from 'components/Page';
import BodyLayout from 'layouts/dashboard/BodyLayout';
import Header from 'components/Header';
import Pagination from 'components/Pagination';
import AutoGrid from 'components/AutoGrid';
import CardTechStack from 'components/CardTechStack';
import EmptyState from 'components/EmptyState';

// add new tech stack
import AddTechStack from './Add';

// edit tech stack
import EditTechStack from './Edit';

// redux
import { useSelector, useDispatch } from 'react-redux';
import { useLister, useSearch, useConfirm } from 'utils/hooks';
import { remove, load } from 'redux/actions';
const TECH_STACKS_QUERY = {
  // category: 'techStack',
  sort: 'order',
  order: 'DESC',
  limit: 0,
  offset: 0,
  keyword: ''
};

function TechStack() {
  // redux
  const [techQuery, setTechQuery] = useState(TECH_STACKS_QUERY);
  const [refetch, setRefetch] = useState(false);
  const [searchCategory, setSearchCategory] = useState('all');
  const { dispatchList } = useLister('tech-stack', 'techStack');
  const { dispatchSearch, dispatchSearchGlobal } = useSearch('keywordTechStack');
  const { techStack } = useSelector((state) => state.module.list);
  const { keywordTechStack, keywordGlobal } = useSelector((state) => state.module.keyword);
  const dispatch = useDispatch();
  const { confirmInfo } = useConfirm();

  // props logic
  const [open, setOpen] = useState(false);
  const [openEdit, setOpenEdit] = useState(false);
  const [idEdit, setIdEdit] = useState(null);
  const [loading, setLoading] = useState(true);
  const PAGE_TITLE = 'Our Tech Stack';
  const ROUTES = [
    { label: 'Overview', link: '/' },
    { label: 'Our Tech Stack', link: '/about/tech-stack' }
  ];

  useEffect(() => {
    dispatchSearch(keywordGlobal || '');

    return () => {
      dispatchSearchGlobal('');
    };
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    const timeout = setTimeout(() => {
      setTechQuery((prev) => ({
        ...TECH_STACKS_QUERY,
        limit: prev.limit,
        keyword: keywordTechStack
      }));
    }, 500);

    return () => clearTimeout(timeout);
    // eslint-disable-next-line
  }, [keywordTechStack]);

  useEffect(() => {
    if (techQuery.limit > 0) {
      setLoading(true);
      dispatchList(techQuery, () => setLoading(false));
    }
    // eslint-disable-next-line
  }, [techQuery, refetch]);

  const handleColor = (category) => {
    switch (category) {
      case 'techStack':
        return '#36CECE';
      case 'database':
        return '#FF9D42';
      case 'cyberSecurity':
        return '#335E9B';
      case 'product':
        return '#00A3FF';
      default:
        return '#00A3FF';
    }
  };

  const handleEdit = (id) => {
    dispatch(load({ name: 'tech-stack', id, customName: 'detailStack' }));
    setIdEdit(id);
    setOpenEdit(true);
  };

  const handleDelete = (id, name) => {
    confirmInfo({
      title: `Are you sure you want to delete '${name}'?`,
      message: 'You won’t be able to recover this in the future!',
      handler: () => {
        dispatch(
          remove({
            name: 'tech-stack',
            noLink: true,
            id,
            onSuccess: () => {
              resetPage();
              setRefetch((prev) => !prev);
            }
          })
        );
      }
    });
  };

  const handlePagination = (event, value) => {
    setTechQuery((prev) => ({
      ...prev,
      offset: (value - 1) * prev.limit
    }));
  };

  // sync pagination and autogrid
  const handleAutoGridChangeWidth = ({ width, cardWidth, gap }) => {
    setTechQuery((prev) => {
      if (width < cardWidth) return prev;
      const limit = Math.floor((width + gap) / (cardWidth + gap)) * 2;
      if (limit !== prev.limit) {
        return { ...prev, limit };
      }
      return prev;
    });
  };

  const handleSearch = (val) => {
    resetPage();
    dispatchSearch(val);
  };

  const resetPage = () => setTechQuery((prev) => ({ ...prev, offset: 0 }));

  const handleCategoryChange = (event) => {
    const value = event.target.value;
    resetPage();
    setSearchCategory(value);
    setTechQuery((prev) => ({
      ...TECH_STACKS_QUERY,
      limit: prev.limit,
      category: value == 'all' ? '' : value
    }));
  };

  return (
    <Page title={PAGE_TITLE}>
      <BodyLayout>
        <AddTechStack
          open={open}
          onClose={() => setOpen(false)}
          onSuccess={() => {
            resetPage();
            setRefetch((prev) => !prev);
            setOpen(false);
          }}
        />
        <EditTechStack
          openEdit={openEdit}
          id={idEdit}
          onCloseEdit={() => setOpenEdit(false)}
          onSuccess={() => {
            resetPage();
            setRefetch((prev) => !prev);
            setOpenEdit(false);
          }}
        />
        <Header
          routes={ROUTES}
          withButton
          keyword={keywordTechStack}
          buttonText="Add Tech Stack"
          onButtonClick={() => setOpen(true)}
          withSearchBar
          onSearch={handleSearch}
        />
        <Box display="flex" justifyContent="flex-end" marginBottom={3}>
          <TextField
            select
            value={searchCategory}
            onChange={handleCategoryChange}
            sx={{
              maxWidth: '257px',
              '& .MuiOutlinedInput-root': {
                padding: 0
              }
            }}
          >
            <MenuItem value="all">All</MenuItem>
            <MenuItem value="techStack">Tech Stack</MenuItem>
            <MenuItem value="database">Database</MenuItem>
            <MenuItem value="cyberSecurity">Cyber Security</MenuItem>
            <MenuItem value="product">Product</MenuItem>
          </TextField>
        </Box>
        <AutoGrid onChangeWidth={handleAutoGridChangeWidth}>
          {techStack?.data?.map((value, index) => {
            return (
              <CardTechStack
                key={index}
                id={value.id}
                width="290px"
                title={value.name}
                category={value.category}
                color={handleColor(value.category)}
                logo={value.logo}
                order={value.order}
                onDelete={(id) => handleDelete(id, value.name)}
                onEdit={(id) => handleEdit(id)}
              />
            );
          })}
        </AutoGrid>
        {!loading && !techStack?.data?.length && <EmptyState name="Tech Stack" />}
        {techStack?.data?.length && (
          <Box
            sx={{
              pb: 3,
              display: 'flex',
              mt: 5,
              flexGrow: 1,
              justifyContent: 'center',
              alignItems: 'flex-end'
            }}
          >
            <Pagination
              totalPage={Math.ceil((techStack?.count ?? 0) / techQuery.limit)}
              currentPage={Math.floor(techQuery.offset / techQuery.limit) + 1}
              onChange={handlePagination}
            />
          </Box>
        )}
      </BodyLayout>
    </Page>
  );
}

export default TechStack;
