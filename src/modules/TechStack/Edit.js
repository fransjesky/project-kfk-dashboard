import React, { useState, useEffect } from 'react';
import Modal, { ModalContent, ModalButtons, ModalHead } from 'components/Modal';
import { connect, useSelector, useDispatch } from 'react-redux';
import { Box, TextField, Button } from '@mui/material';
import { AnyUploader } from 'components/Uploader/UploaderContainer';
import MenuItem from 'components/MenuItem';
import * as Yup from 'yup';
import withFormik from 'utils/withFormik';
import { edit, list } from 'redux/actions';
import ImageAttachment from 'components/ImageAttachment';

// schema validation
const schemeValidations = Yup.object().shape({
  name: Yup.string().trim().required('Field cannot be empty'),
  logo: Yup.string().required('Field cannot be empty'),
  category: Yup.string().required('Field select an option'),
  order: Yup.number().required('Please select an option').nullable()
});

// initial values
const initialValueForm = {
  name: '',
  logo: '',
  category: '',
  order: null
};

const mapPropsToValues = ({ detailStack }) => {
  const data = detailStack?.data;

  if (data) {
    return {
      name: data?.name,
      logo: data?.logo,
      category: data?.category,
      order: data?.order
    };
  }
  return initialValueForm;
};

const handleSubmitForm = (payload, ctx) => {
  const { name, logo, category, order } = payload;

  ctx.props.edit({
    name: 'tech-stack',
    id: ctx.props.id,
    noLink: true,
    data: {
      name,
      logo,
      category,
      order
    },
    onSuccess: () => {
      ctx.resetForm();
      ctx.props.onSuccess();
    }
  });
};

function EditTechStack(props) {
  const MODAL_TITLE = 'Edit Tech Stack';
  const { openEdit, onCloseEdit } = props;
  const {
    values,
    setFieldValue,
    touched,
    errors,
    handleChange,
    handleBlur,
    handleSubmit,
    dirty,
    isValid,
    resetForm
  } = props;
  const error = (val) => Boolean(touched[val] && errors[val]);
  const dispatch = useDispatch();
  const { orderTechStack } = useSelector((state) => state.module.list);
  const { detailStack } = useSelector((state) => state.module.view);
  const categories = [
    { label: 'Tech Stack', value: 'techStack' },
    { label: 'Database', value: 'database' },
    { label: 'Cyber Security', value: 'cyberSecurity' },
    { label: 'Product', value: 'product' }
  ];

  const [selectedImage, setSelectedImage] = useState(null);
  const [changedCategory, setChangedCategory] = useState(false);
  const handleDropzoneChange = (value) => {
    setSelectedImage(value[0]?.data);
    setFieldValue('logo', value[0]?.data?.location);
  };

  const handleDeleteAttachment = () => {
    setSelectedImage(null);
    setFieldValue('logo', '');
  };

  useEffect(() => {
    if (values.category)
      dispatch(
        list({
          name: 'tech-stack/number',
          query: `category=${values.category}`,
          customName: 'orderTechStack',
          onSuccess: () => {
            setChangedCategory((prev) => !prev);
          }
        })
      );
  }, [values.category, dispatch]);

  const handleChangeCategory = (e, val) => {
    if (e?.target?.value == detailStack?.data.category) {
      setFieldValue('order', detailStack?.data.order);
    }
    return handleChange(e, val);
  };

  useEffect(() => {
    if (values.category !== detailStack?.data.category) {
      setFieldValue('order', orderTechStack?.data[orderTechStack?.data.length - 1]);
    }
  }, [
    values.category,
    changedCategory,
    detailStack?.data.category,
    setFieldValue,
    orderTechStack?.data
  ]);

  return (
    <Modal
      isOpen={openEdit}
      onClose={() => {
        resetForm();
        onCloseEdit();
      }}
      maxWidth={1025}
    >
      <ModalHead title={MODAL_TITLE} />
      <ModalContent>
        <Box width={'646px'}>
          <h2>Upload a thumbnail</h2>
          <p>Files should be PNG</p>
          <Box width="100%" marginTop={3}>
            {!values.logo ? (
              <AnyUploader
                elementId="dropzone-edit-stack"
                type="dropzone"
                allowedTypes={['png']}
                maxSizeMb={1}
                onChange={handleDropzoneChange}
                onError={() => {}}
              />
            ) : (
              <ImageAttachment
                fileData={selectedImage ? selectedImage : { location: values?.logo, name: '.png' }}
                onDelete={() => handleDeleteAttachment()}
              />
            )}
          </Box>
          <Box display="flex" flexDirection="column" sx={{ mt: 3 }}>
            <TextField
              name="name"
              placeholder="Tech Stack Name"
              label="Tech Stack Name"
              sx={{ width: '100%' }}
              value={values.name}
              onChange={handleChange}
              onBlur={handleBlur}
              error={error('name')}
              helperText={touched.name && errors.name}
              InputLabelProps={error('name') || values.name ? { shrink: true } : {}}
            />
            <Box display="flex" justifyContent="space-between">
              <TextField
                label="Tech Stack Category"
                name="category"
                select
                sx={{ width: '62.5%' }}
                value={values.category}
                onChange={handleChangeCategory}
                onBlur={handleBlur}
                error={error('category')}
                helperText={touched.category && errors.category}
                InputLabelProps={error('category') || values.category ? { shrink: true } : {}}
              >
                {categories.map((value, index) => {
                  return (
                    <MenuItem value={value.value} key={index}>
                      {value.label}
                    </MenuItem>
                  );
                })}
              </TextField>
              <TextField
                label="Choose Order"
                name="order"
                select
                sx={{ width: '35%' }}
                disabled={!(values?.category === detailStack?.data.category)}
                value={values.order}
                onChange={handleChange}
                onBlur={handleBlur}
                error={error('order')}
                helperText={touched.order && errors.order}
                InputLabelProps={error('order') || values.order ? { shrink: true } : {}}
              >
                {orderTechStack?.data.map((value, index) => {
                  return detailStack?.data?.category === values.category ? (
                    index !== orderTechStack.data.length - 1 ? (
                      <MenuItem value={value} key={index}>
                        {value}
                      </MenuItem>
                    ) : null
                  ) : (
                    <MenuItem value={value} key={index}>
                      {value}
                    </MenuItem>
                  );
                })}
              </TextField>
            </Box>
          </Box>
          <ModalButtons>
            <Button
              variant="contained"
              disabled={!(dirty && isValid)}
              onClick={handleSubmit}
              sx={{ color: '#fff' }}
            >
              Save
            </Button>
          </ModalButtons>
        </Box>
      </ModalContent>
    </Modal>
  );
}

const FormikCreate = withFormik(
  EditTechStack,
  schemeValidations,
  mapPropsToValues,
  handleSubmitForm
);

const mapStateToProps = (state) => ({
  detailStack: state.module.view.detailStack
});

export default connect(mapStateToProps, { edit })(FormikCreate);
