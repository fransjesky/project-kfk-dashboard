import React, { useState, useEffect } from 'react';
import { connect, useDispatch, useSelector } from 'react-redux';
import Modal, { ModalContent, ModalButtons, ModalHead } from 'components/Modal';
import { Box, TextField, Button } from '@mui/material';
import { AnyUploader } from 'components/Uploader/UploaderContainer';
import * as Yup from 'yup';
import withFormik from 'utils/withFormik';
import { add, list } from 'redux/actions';
import ImageAttachment from 'components/ImageAttachment';
import MenuItem from 'components/MenuItem';

// schema validation
const schemeValidations = Yup.object().shape({
  name: Yup.string().trim().required('Field cannot be empty'),
  logo: Yup.string().required('Field cannot be empty'),
  category: Yup.string()
    .required('Field cannot be empty')
    .oneOf(['database', 'techStack', 'cyberSecurity', 'product']),
  order: Yup.number().min(1, 'Field cannot be empty').nullable()
});

// initial values
const initialValueForm = {
  name: '',
  logo: '',
  category: '',
  order: 0
};

const handleSubmitForm = (payload, ctx) => {
  const { name, logo, category, order } = payload;

  ctx.props.add({
    name: 'tech-stack',
    data: {
      name,
      logo,
      category,
      order
    },
    customRedirect: 'about/tech-stack',
    onSuccess: () => {
      ctx.resetForm();
      ctx.props.onSuccess();
    }
  });
};

function AddTechStack(props) {
  // variable init
  const { open, onClose } = props;
  const {
    values,
    touched,
    errors,
    handleChange,
    handleBlur,
    handleSubmit,
    resetForm,
    dirty,
    isValid,
    setFieldValue
  } = props;
  const categories = [
    { label: 'Tech Stack', value: 'techStack' },
    { label: 'Database', value: 'database' },
    { label: 'Cyber Security', value: 'cyberSecurity' },
    { label: 'Product', value: 'product' }
  ];
  const MODAL_TITLE = 'Add New Tech Stack';
  const error = (val) => Boolean(touched[val] && errors[val]);
  const dispatch = useDispatch();
  const { orderTechStack } = useSelector((state) => state.module.list);

  // upload image state init
  const [selectedImage, setSelectedImage] = useState(null);
  const handleDropzoneChange = (value) => {
    setSelectedImage(value[0]?.data);
    setFieldValue('logo', value[0]?.data?.location);
  };

  const handleDeleteAttachment = () => {
    setSelectedImage(null);
    setFieldValue('logo', '');
  };

  useEffect(() => {
    if (values.category) {
      dispatch(
        list({
          name: `tech-stack/number`,
          query: `category=${values.category}`,
          customName: 'orderTechStack'
        })
      );
    }
  }, [values?.category, dispatch]);

  useEffect(() => {
    if (values.category)
      setFieldValue('order', orderTechStack?.data[orderTechStack?.data.length - 1]);
  }, [orderTechStack, setFieldValue, values?.category]);

  return (
    <Modal
      isOpen={open}
      onClose={() => {
        resetForm();
        onClose();
      }}
      maxWidth={1025}
    >
      <ModalHead title={MODAL_TITLE} />
      <ModalContent>
        <Box width={'646px'}>
          <h2>Upload a logo</h2>
          <p>Files should be PNG</p>
          <Box width="100%" marginTop={3}>
            {!values.logo /*need to change this later */ ? (
              <AnyUploader
                elementId="dropzone-portfolio"
                type="dropzone"
                allowedTypes={['png']}
                maxSizeMb={1}
                onChange={handleDropzoneChange}
              />
            ) : (
              <ImageAttachment fileData={selectedImage} onDelete={() => handleDeleteAttachment()} />
            )}
          </Box>
          <Box display="flex" flexDirection="column" sx={{ mt: 3 }}>
            <TextField
              name="name"
              label="Tech Stack Name"
              placeholder="Tech Stack Name"
              sx={{ width: '100%' }}
              value={values.name}
              onChange={handleChange}
              onBlur={handleBlur}
              error={error('name')}
              helperText={touched.name && errors.name}
              InputLabelProps={error('name') ? { shrink: true } : {}}
            />
            <Box display="flex" justifyContent="space-between">
              <TextField
                label="Tech Stack Category"
                placeholder="Tech Stack Category"
                name="category"
                select
                sx={{ width: '62.5%' }}
                value={values.category}
                onChange={handleChange}
                onBlur={handleBlur}
                error={error('category')}
                helperText={touched.category && errors.category}
                InputLabelProps={error('category') ? { shrink: true } : {}}
              >
                {categories.map((value, index) => {
                  return (
                    <MenuItem value={value.value} key={index}>
                      {value.label}
                    </MenuItem>
                  );
                })}
              </TextField>
              <TextField
                label="Choose Order"
                name="order"
                placeholder="Choose Order"
                select
                disabled
                sx={{ width: '35%' }}
                value={values.order}
                onChange={handleChange}
                onBlur={handleBlur}
                error={error('order')}
                helperText={touched.order && errors.order}
                InputLabelProps={error('order') ? { shrink: true } : {}}
              >
                {orderTechStack?.data.map((value, index) => {
                  return (
                    <MenuItem value={value + 1} key={index}>
                      {value + 1}
                    </MenuItem>
                  );
                })}
              </TextField>
            </Box>
          </Box>
          <ModalButtons>
            <Button
              disabled={!(dirty && isValid)}
              variant="contained"
              onClick={handleSubmit}
              sx={{ color: '#fff' }}
            >
              Save
            </Button>
          </ModalButtons>
        </Box>
      </ModalContent>
    </Modal>
  );
}

const FormikCreate = withFormik(
  AddTechStack,
  schemeValidations,
  initialValueForm,
  handleSubmitForm
);

export default connect(null, { add })(FormikCreate);
