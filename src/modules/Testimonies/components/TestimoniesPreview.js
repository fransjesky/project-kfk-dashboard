import React from 'react';
import ModalPreview from 'components/ModalPreview';
import { Box, Typography, Avatar } from '@mui/material';

const TestimoniesPreview = ({ open, onClose, values }) => {
  return (
    <ModalPreview open={open} onClose={onClose} title="Preview post">
      <Box
        display="flex"
        flexDirection="column"
        alignItems="center"
        flexGrow={1}
        justifyContent="center"
      >
        <Avatar src={values.photo} sx={{ width: '120px', height: '120px', marginBottom: '32px' }} />
        <Box
          sx={{
            wordWrap: 'break-word',
            width: '100%',
            textAlign: 'center'
          }}
        >
          <Typography
            component="p"
            variant="s2"
            fontWeight="medium"
            marginBottom={2}
            sx={{ wordWrap: 'break-word' }}
          >
            {values.name}
          </Typography>
          <Typography component="p" variant="b1" fontWeight="medium" marginBottom="28px">
            {values.position}
          </Typography>
          <Box display="flex" justifyContent="center">
            <Box sx={{ maxWidth: '1114px' }}>
              <Typography
                component="p"
                variant="s2"
                fontWeight="medium"
                lineHeight="27.65px"
                textAlign="center"
                sx={{ wordWrap: 'break-word' }}
              >
                {values.clientTestimony}
              </Typography>
            </Box>
          </Box>
        </Box>
      </Box>
    </ModalPreview>
  );
};

export default TestimoniesPreview;
