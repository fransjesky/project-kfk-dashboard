import { styled } from '@mui/material/styles';
import { Box, Typography, TextField } from '@mui/material';
import palette from 'theme/palette';

export const BlueBox = ({ children }) => (
  <Box
    sx={{
      border: `solid 1px ${palette.secondary[100]}`,
      padding: '12px',
      borderRadius: '8px',
      mb: 3,
      width: 'fit-content'
    }}
  >
    <Typography>{children}</Typography>
  </Box>
);

export const SectionTitle = (props) => (
  <Typography component="p" variant="s1" fontWeight="bold" {...props} />
);

export const CustomTextField = styled((props) => <TextField {...props} />)({
  maxWidth: '323.83px'
});

export const SectionGroup = styled('div')({
  marginBottom: 24
});

export const ButtonGroups = styled('div')({
  display: 'flex',
  justifyContent: 'flex-end',
  gap: '16px'
});
