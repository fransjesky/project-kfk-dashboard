import React from 'react';
// components
import { Box, Typography, TextField, Stack } from '@mui/material';
import { AnyUploader } from 'components/Uploader/UploaderContainer';
import { PrimaryButton, SecondaryButton } from 'components/Button';
import { SectionGroup, SectionTitle, CustomTextField, ButtonGroups } from './styled';
import TestimoniesPreview from './TestimoniesPreview';
import ImageAttachment from 'components/ImageAttachment';
// utils
import { useToggle, useNotifier } from 'utils/hooks';

const getTextFieldProps = (
  { values, handleChange, handleBlur, touched, errors },
  name,
  helperText
) => ({
  name,
  value: values[name],
  onChange: handleChange,
  onBlur: handleBlur,
  error: Boolean(touched[name] && errors[name]),
  helperText: (touched[name] && errors[name]) || helperText
});

const TestimonyForm = (props) => {
  const { values, handleSubmit, setFieldValue, dirty, isValid } = props;
  const [openPreview, togglePreview] = useToggle(false);
  const { notifError } = useNotifier();

  const handleDropzoneChange = (value) => {
    const { data } = value[0];
    setFieldValue('photo', data.location);
    setFieldValue('photoData', data);
  };

  const handleDeleteAttachment = () => {
    setFieldValue('photo', '');
    setFieldValue('photoData', null);
  };

  return (
    <Box>
      <TestimoniesPreview open={openPreview} onClose={() => togglePreview(false)} values={values} />
      <Box sx={{ mb: 12 }}>
        <SectionGroup>
          <SectionTitle>Upload a Photo</SectionTitle>
          <Typography variant="b1" color="grey.50">
            Files should be PNG
          </Typography>
          <Box sx={{ maxWidth: '534.33px', mt: 2 }}>
            {!values.photo ? (
              <AnyUploader
                elementId="dropzone__add-testimonies"
                type="dropzone"
                allowedTypes={['png']}
                maxSizeMb={1}
                onChange={handleDropzoneChange}
                onError={(err) => notifError(err, 'Failed to upload image')}
              />
            ) : (
              <ImageAttachment
                fileData={values.photoData ?? { location: values?.photo }}
                onDelete={() => handleDeleteAttachment()}
              />
            )}
          </Box>
        </SectionGroup>

        <SectionGroup>
          <SectionTitle>Client Profile</SectionTitle>
          <CustomTextField label="Client Name" {...getTextFieldProps(props, 'name')} />
          <Stack spacing={2} direction="row">
            <CustomTextField
              label="Position Name"
              margin="none"
              {...getTextFieldProps(props, 'position')}
            />
            <CustomTextField
              label="Company Name"
              margin="none"
              {...getTextFieldProps(props, 'companyName')}
            />
          </Stack>
        </SectionGroup>

        <SectionGroup>
          <SectionTitle>Client Testimony</SectionTitle>
          <TextField
            label="Their awesome quote"
            multiline
            minRows={4}
            {...getTextFieldProps(
              props,
              'clientTestimony',
              'Please tell us client testimony and input a maximum of 380 characters'
            )}
          />
        </SectionGroup>
      </Box>

      <ButtonGroups>
        <SecondaryButton onClick={() => togglePreview(true)} disabled={!dirty}>
          Preview
        </SecondaryButton>
        <PrimaryButton onClick={handleSubmit} disabled={!dirty || !isValid}>
          Save
        </PrimaryButton>
      </ButtonGroups>
    </Box>
  );
};

export default TestimonyForm;
