import * as Yup from 'yup';

export default Yup.object().shape({
  photo: Yup.string().required(''),
  name: Yup.string().trim().required('Field cannot be empty'),
  position: Yup.string().trim().required('Field cannot be empty'),
  companyName: Yup.string().trim().required('Field cannot be empty'),
  clientTestimony: Yup.string()
    .trim()
    .required('Field cannot be empty')
    .test('len', 'Please input a maximum of 380 characters', (val) => val?.length <= 380)
});
