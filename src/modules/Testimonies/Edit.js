import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router';
// components
import BodyLayout from 'layouts/dashboard/BodyLayout';
import Page from 'components/Page';
import Header from 'components/Header';
import { BlueBox } from './components/styled';
import TestimonyForm from './components/TestimonyForm';
// utils
import withFormik from 'utils/withFormik';
import { useLoader } from 'utils/hooks';
// redux
import { connect, useSelector } from 'react-redux';
import { edit } from 'redux/actions';
// schema validations
import SchemaValidations from './components/SchemaValidations';

// initial values
const initialValueForm = {
  photo: '',
  name: '',
  position: '',
  companyName: '',
  clientTestimony: ''
};

const handleSubmitForm = ({ id, ...payload }, ctx) => {
  ctx.props.edit({
    name: 'testimony',
    id,
    data: payload,
    linkSuccess: 'works/testimony'
  });
};

const PAGE_TITLE = 'Testimonies';
const routes = (id) => {
  return [
    { label: 'Overview', link: '/' },
    { label: 'Testimonies', link: '/works/testimony' },
    { label: 'Edit Testimonies', link: `/testimony/edit/${id}` }
  ];
};

const EditTestimonies = (formikProps) => {
  const { testimonyId } = useParams();
  const { dispatchLoad } = useLoader('testimony');
  const { testimony } = useSelector((state) => state.module.view);
  const [loading, setLoading] = useState(true);
  const handleHeaderButtonClick = () => {};

  // load current value
  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => dispatchLoad(testimonyId), []);

  // input current values
  useEffect(() => {
    if (!testimony) return;
    const { setFieldValue } = formikProps;
    const objectKeys = ['id', 'name', 'position', 'companyName', 'clientTestimony', 'photo'];
    for (let key of objectKeys) {
      setFieldValue(key, testimony.data[key]);
    }
    setLoading(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [testimony]);

  return (
    <Page title={PAGE_TITLE}>
      <BodyLayout padded>
        <Header
          withBackButton
          routes={routes(testimonyId)}
          onButtonClick={handleHeaderButtonClick}
        />
        <BlueBox>
          This post will go public, please becareful with information you will share.
        </BlueBox>
        {!loading && <TestimonyForm {...formikProps} />}
      </BodyLayout>
    </Page>
  );
};

const FormikEditTestimonies = withFormik(
  EditTestimonies,
  SchemaValidations,
  initialValueForm,
  handleSubmitForm
);

export default connect(null, { edit })(FormikEditTestimonies);
