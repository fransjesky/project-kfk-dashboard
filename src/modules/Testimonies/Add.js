import React from 'react';
// components
import BodyLayout from 'layouts/dashboard/BodyLayout';
import Page from 'components/Page';
import Header from 'components/Header';
import { BlueBox } from './components/styled';
import TestimonyForm from './components/TestimonyForm';
// utils
import withFormik from 'utils/withFormik';
// redux
import { connect } from 'react-redux';
import { add } from 'redux/actions';
// schema validations
import SchemaValidations from './components/SchemaValidations';

// initial values
const initialValueForm = {
  photo: '',
  name: '',
  position: '',
  companyName: '',
  clientTestimony: ''
};

const handleSubmitForm = (payload, ctx) => {
  ctx.props.add({
    name: 'testimony',
    data: payload,
    customRedirect: 'works/testimony'
  });
};

const PAGE_TITLE = 'Testimonies';
const ROUTES = [
  { label: 'Overview', link: '/' },
  { label: 'Testimonies', link: '/works/testimony' },
  { label: 'Add Testimonies', link: '/works/testimony/add' }
];

const AddTestimonies = (formikProps) => {
  return (
    <Page title={PAGE_TITLE}>
      <BodyLayout padded>
        <Header withBackButton routes={ROUTES} />
        <BlueBox>
          This post will go public, please becareful with information you will share.
        </BlueBox>
        <TestimonyForm {...formikProps} />
      </BodyLayout>
    </Page>
  );
};

const FormikAddTestimonies = withFormik(
  AddTestimonies,
  SchemaValidations,
  initialValueForm,
  handleSubmitForm
);

export default connect(null, { add })(FormikAddTestimonies);
