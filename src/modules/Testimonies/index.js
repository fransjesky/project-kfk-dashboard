import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
// components
import { Box } from '@mui/material';
import Page from 'components/Page';
import BodyLayout from 'layouts/dashboard/BodyLayout';
import Header from 'components/Header';
import Pagination from 'components/Pagination';
import AutoGrid from 'components/AutoGrid';
import CardTestimony from 'components/CardTestimony';
import EmptyState from 'components/EmptyState';
// redux
import { useSelector } from 'react-redux';
// utils
import { useLister, useRemover, useConfirm, useSearch } from 'utils/hooks';

const PAGE_TITLE = 'Testimonies';
const ROUTES = [
  { label: 'Overview', link: '/' },
  { label: 'Testimonies', link: '/works/testimony' }
];
const DEFAULT_TESTIMONY_QUERY = {
  order: 'ASC',
  limit: 0,
  offset: 0,
  keyword: ''
};

const Testimonies = () => {
  const [testimoniesQuery, setTestimoniesQuery] = useState(DEFAULT_TESTIMONY_QUERY);
  const history = useHistory();
  const { dispatchRemove } = useRemover('testimony', 'works/testimony');
  const { dispatchList } = useLister('testimony', 'testimonies');
  const { dispatchSearch, dispatchSearchGlobal } = useSearch('keywordTestimoni');
  const { testimonies } = useSelector((state) => state.module.list);
  const { keywordTestimoni, keywordGlobal } = useSelector((state) => state.module.keyword);
  const { confirmInfo } = useConfirm();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    dispatchSearch(keywordGlobal || '');

    return () => {
      dispatchSearchGlobal('');
    };
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    const timeout = setTimeout(() => {
      setTestimoniesQuery((prev) => ({
        ...DEFAULT_TESTIMONY_QUERY,
        limit: prev.limit,
        keyword: keywordTestimoni
      }));
    }, 500);

    return () => clearTimeout(timeout);
    // eslint-disable-next-line
  }, [keywordTestimoni]);

  const handleHeaderButtonClick = () => history.push('/works/testimony/add');
  const handlePagination = (event, value) => {
    setTestimoniesQuery((prev) => ({
      ...prev,
      offset: (value - 1) * prev.limit
    }));
  };

  // load testimonies
  useEffect(() => {
    if (testimoniesQuery.limit > 0) {
      setLoading(true);
      dispatchList(testimoniesQuery, () => setLoading(false));
    }
    // eslint-disable-next-line
  }, [testimoniesQuery]);

  const handleDeleteTestimony = (testimony) => () => {
    // open modal confirm
    confirmInfo({
      title: `Are you sure you want to delete '${testimony.name}'?`,
      message: 'You won’t be able to recover this in the future!',
      handler: () => {
        dispatchRemove(testimony.id, () => dispatchList(testimoniesQuery));
      }
    });
  };

  // sync pagination and autogrid
  const handleAutoGridChangeWidth = ({ width, cardWidth, gap }) => {
    setTestimoniesQuery((prev) => {
      if (width < cardWidth) return prev;
      const limit = Math.floor((width + gap) / (cardWidth + gap)) * 2;
      if (limit !== prev.limit) {
        return { ...prev, limit };
      }
      return prev;
    });
  };

  const handleSearch = (val) => {
    resetPage();
    dispatchSearch(val);
  };

  const resetPage = () => setTestimoniesQuery((prev) => ({ ...prev, offset: 0 }));

  return (
    <Page title={PAGE_TITLE}>
      <BodyLayout>
        <Header
          routes={ROUTES}
          withButton
          buttonText="Add Testimonies"
          keyword={keywordTestimoni}
          onButtonClick={handleHeaderButtonClick}
          withSearchBar
          onSearch={handleSearch}
        />
        <AutoGrid onChangeWidth={handleAutoGridChangeWidth}>
          {testimonies?.data?.map((testimony) => (
            <CardTestimony
              key={testimony.name + testimony.id} // in case there are people with the same name
              clientName={testimony.name}
              companyName={testimony.companyName}
              position={testimony.position}
              testimony={testimony.clientTestimony}
              photo={testimony.photo}
              onDelete={handleDeleteTestimony(testimony)}
              onEdit={() => history.push(`/works/testimony/edit/${testimony.id}`)}
            />
          ))}
        </AutoGrid>
        {!loading && !testimonies?.data?.length && <EmptyState name="Testimonies" />}
        {testimonies?.data?.length && (
          <Box
            sx={{
              mt: 5,
              pb: 3,
              display: 'flex',
              flexGrow: 1,
              justifyContent: 'center',
              alignItems: 'flex-end'
            }}
          >
            <Pagination
              totalPage={Math.ceil((testimonies?.count ?? 0) / testimoniesQuery.limit)}
              currentPage={Math.floor(testimoniesQuery.offset / testimoniesQuery.limit) + 1}
              onChange={handlePagination}
            />
          </Box>
        )}
      </BodyLayout>
    </Page>
  );
};

export default Testimonies;
