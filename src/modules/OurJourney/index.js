import React, { useState, useEffect } from 'react';
import Page from 'components/Page';
import BodyLayout from 'layouts/dashboard/BodyLayout';
import Header from 'components/Header';
import { Box } from '@mui/material';
import CardJourney from 'components/CardJourney';
import AutoGrid from 'components/AutoGrid';
import Pagination from 'components/Pagination';
import EmptyState from 'components/EmptyState';

// add new journey
import AddJourney from './Add';

// edit journey
import EditJourney from './Edit';

// redux
import { useSelector, useDispatch } from 'react-redux';
import { remove, load } from 'redux/actions';
import { useLister, useSearch, useConfirm } from 'utils/hooks';
const OUR_JOURNEY_QUERY = {
  sort: 'year',
  order: 'DESC',
  limit: 0,
  offset: 0,
  keyword: ''
};

function OurJourney() {
  // variable init
  const [open, setOpen] = useState(false);
  const [openEdit, setOpenEdit] = useState(false);
  const [idEdit, setIdEdit] = useState(null);
  const [changed, setChanged] = useState(false);
  const [journeyQuery, setJourneyQuery] = useState(OUR_JOURNEY_QUERY);
  const { confirmInfo } = useConfirm();
  const { dispatchSearch, dispatchSearchGlobal } = useSearch('keywordJourney');
  const { dispatchList } = useLister('journey', 'ourjourney');
  const { ourjourney } = useSelector((state) => state.module.list);
  const { keywordJourney, keywordGlobal } = useSelector((state) => state.module.keyword);
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(true);

  useEffect(
    () => {
      dispatchSearch(keywordGlobal || '');
      return () => {
        dispatchSearchGlobal('');
      };
    },
    // eslint-disable-next-line
    []
  );

  useEffect(() => {
    const timeout = setTimeout(() => {
      setJourneyQuery((prev) => ({
        ...OUR_JOURNEY_QUERY,
        limit: prev.limit,
        keyword: keywordJourney
      }));
    }, 500);

    return () => clearTimeout(timeout);
    // eslint-disable-next-line
  }, [keywordJourney]);

  useEffect(() => {
    if (journeyQuery.limit > 0) {
      setLoading(true);
      dispatchList(journeyQuery, () => setLoading(false));
    }
    // eslint-disable-next-line
  }, [journeyQuery, changed]);

  const PAGE_TITLE = 'Our Journey';
  const ROUTES = [
    { label: 'Overview', link: '/' },
    { label: 'Our Journey', link: '/about/journey' }
  ];

  const handleEdit = (id) => {
    dispatch(load({ name: 'journey', id, customName: 'journeyData' }));
    setIdEdit(id);
    setOpenEdit(true);
  };

  const handleDelete = (id, year) => {
    confirmInfo({
      title: `Are you sure you want to delete '${year}'?`,
      message: 'You won’t be able to recover this in the future!',
      handler: () => {
        dispatch(
          remove({
            name: 'journey',
            noLink: true,
            id,
            onSuccess: () => {
              setJourneyQuery((prev) => {
                return { ...prev };
              });
              setChanged((prev) => !prev);
            }
          })
        );
      }
    });
  };

  const handlePagination = (event, value) => {
    setJourneyQuery((prev) => ({
      ...prev,
      offset: (value - 1) * prev.limit
    }));
  };

  // sync pagination and autogrid
  const handleAutoGridChangeWidth = ({ width, cardWidth, gap }) => {
    setJourneyQuery((prev) => {
      if (width < cardWidth) return prev;
      const limit = Math.floor((width + gap) / (cardWidth + gap)) * 2;
      if (limit !== prev.limit) {
        return { ...prev, limit };
      }
      return prev;
    });
  };

  const handleSearchChange = (value) => {
    resetPage();
    dispatchSearch(value);
  };

  const resetPage = () => setJourneyQuery((prev) => ({ ...prev, offset: 0 }));

  return (
    <Page title={PAGE_TITLE}>
      <BodyLayout>
        <AddJourney
          open={open}
          onClose={() => setOpen(false)}
          onSuccess={() => {
            setJourneyQuery((prev) => {
              return { ...prev };
            });
            setChanged((prev) => !prev);
            setOpen(false);
          }}
        />
        <EditJourney
          id={idEdit}
          openEdit={openEdit}
          onCloseEdit={() => setOpenEdit(false)}
          onSuccess={() => {
            setJourneyQuery((prev) => {
              return { ...prev };
            });
            setChanged((prev) => !prev);
            setOpenEdit(false);
          }}
        />
        <Header
          routes={ROUTES}
          withButton
          keyword={keywordJourney}
          buttonText="Add Planet"
          onButtonClick={() => setOpen(true)}
          typeSearch="number"
          withSearchBar
          onSearch={handleSearchChange}
        />
        <AutoGrid onChangeWidth={handleAutoGridChangeWidth}>
          {ourjourney?.data?.map((value, index) => {
            return (
              <CardJourney
                key={index}
                id={value.id}
                year={value.year}
                title={value.text}
                backgroundColor={value.mainColor}
                foregroundColor={value.secondaryColor}
                width="290px"
                onEdit={(id) => handleEdit(id)}
                onDelete={(id, year) => handleDelete(id, year)}
              />
            );
          })}
        </AutoGrid>
        {!loading && !ourjourney?.data?.length && <EmptyState name="Journeys" />}
        {ourjourney?.data?.length && (
          <Box
            sx={{
              pb: 3,
              display: 'flex',
              mt: 5,
              flexGrow: 1,
              justifyContent: 'center',
              alignItems: 'flex-end'
            }}
          >
            <Pagination
              totalPage={Math.ceil((ourjourney?.count ?? 0) / journeyQuery.limit) || 1}
              currentPage={Math.floor(journeyQuery.offset / journeyQuery.limit) + 1}
              onChange={handlePagination}
            />
          </Box>
        )}
      </BodyLayout>
    </Page>
  );
}

export default OurJourney;
