import React from 'react';
import Modal, { ModalContent, ModalButtons, ModalHead } from 'components/Modal';
import { Box, TextField, Button } from '@mui/material';
import Planet from './Planet';
import * as Yup from 'yup';
import withFormik from 'utils/withFormik';

// redux
import { connect } from 'react-redux';
import { edit } from 'redux/actions';

// schema validation
const schemeValidations = Yup.object().shape({
  year: Yup.string().trim().required('Field cannot be empty'),
  text: Yup.string()
    .trim()
    .required('Field cannot be empty')
    .max(80, 'Description length cannot exceeds 80 characters'),
  mainColor: Yup.string().trim().required('Field cannot be empty'),
  secondaryColor: Yup.string().trim().required('Field cannot be empty'),
  dimension: Yup.number()
    .required('Field cannot be empty')
    .min(85, 'Planet dimension minimum size must be at least 85 pixels')
    .max(220, 'Planet dimension maximum size cannot exceeds 220 pixels')
});

// initial values
const initialValueForm = {
  year: '',
  text: '',
  mainColor: '#EFB008',
  secondaryColor: '#FFDC81',
  dimension: 100
};

const mapPropsToValues = ({ journeyData }) => {
  const data = journeyData?.data;

  if (data) {
    return {
      year: data?.year,
      text: data?.text,
      mainColor: data?.mainColor,
      secondaryColor: data?.secondaryColor,
      dimension: data?.dimension
    };
  }
  return initialValueForm;
};

const handleSubmitForm = (payload, ctx) => {
  const { year, text, mainColor, secondaryColor, dimension } = payload;
  ctx.props.edit({
    name: 'journey',
    id: ctx.props.id,
    noLink: true,
    data: {
      year,
      text,
      mainColor,
      secondaryColor,
      dimension
    },
    onSuccess: () => {
      ctx.resetForm();
      ctx.props.onSuccess();
    },
    onError: (msg) => {
      for (const key in msg) {
        ctx.setFieldError(key, msg[key]);
      }
    }
  });
};

function EditJourney(props) {
  const { openEdit, onCloseEdit } = props;
  const MODAL_TITLE = 'Edit Journey';
  const MODAL_SUB_TITLE = 'Congrats, we reached a new milestone!';
  const {
    values,
    touched,
    errors,
    handleChange,
    handleBlur,
    handleSubmit,
    resetForm,
    dirty,
    isValid,
    setFieldValue
  } = props;
  const error = (val) => Boolean(touched[val] && errors[val]);

  const handleYear = (e) => {
    if (e.target.value.length > 4) {
      return;
    }
    setFieldValue('year', e.target.value);
  };

  return (
    <Modal
      isOpen={openEdit}
      onClose={() => {
        resetForm();
        onCloseEdit();
      }}
      maxWidth={1025}
    >
      <ModalHead title={MODAL_TITLE} subtitle={MODAL_SUB_TITLE} />
      <ModalContent>
        <Box width={'646px'}>
          <Box sx={{ marginBottom: '1.5rem' }}>
            <TextField
              name="year"
              label="Milestone Year"
              type="number"
              sx={{ width: '50%' }}
              onChange={handleYear}
              onBlur={handleBlur}
              error={error('year')}
              value={values.year}
              helperText={touched.year && errors.year}
            />
            <TextField
              name="text"
              label="Journey Description"
              multiline
              minRows={4}
              onChange={handleChange}
              onBlur={handleBlur}
              error={error('text')}
              value={values.text}
              helperText={
                error('text')
                  ? touched.text && errors.text
                  : 'Please share stories during this period and input a maximum of 80 characters'
              }
            />
          </Box>
          <Box display="flex" justifyContent="space-between">
            <Box
              sx={{
                marginRight: '1.125rem',
                width: '100%',
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'flex-start'
              }}
            >
              <h5>Color Picker</h5>
              <TextField
                name="mainColor"
                label="Background Color"
                type="color"
                sx={{ width: '100%' }}
                onChange={handleChange}
                onBlur={handleBlur}
                error={error('mainColor')}
                value={values.mainColor}
                helperText={touched.mainColor && errors.mainColor}
              />
              <TextField
                name="secondaryColor"
                label="Foreground Color"
                type="color"
                sx={{ border: 'none', width: '100%' }}
                onChange={handleChange}
                onBlur={handleBlur}
                error={error('secondaryColor')}
                value={values.secondaryColor}
                helperText={touched.secondaryColor && errors.secondaryColor}
              />
              <TextField
                name="dimension"
                label="Dimension"
                type="number"
                placeholder="Dimension"
                sx={{ width: '100%' }}
                onChange={handleChange}
                onBlur={handleBlur}
                error={error('dimension')}
                value={values.dimension}
                helperText={touched.dimension && errors.dimension}
              />
            </Box>
            <Box
              sx={{
                marginLeft: '1.125rem',
                width: '100%',
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'flex-start'
              }}
            >
              <h5>Planet Preview</h5>
              <Box
                sx={{
                  height: '100%',
                  width: '100%',
                  backgroundColor: '#0F1A2A',
                  borderRadius: '0.5rem',
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
              >
                <Planet
                  bgColor={values.mainColor}
                  fgColor={values.secondaryColor}
                  dimension={values.dimension ? values.dimension : 85}
                />
              </Box>
            </Box>
          </Box>
          <ModalButtons>
            <Button
              variant="contained"
              disabled={!(dirty && isValid)}
              onClick={handleSubmit}
              sx={{ color: '#fff' }}
            >
              Save
            </Button>
          </ModalButtons>
        </Box>
      </ModalContent>
    </Modal>
  );
}

const FormikCreate = withFormik(EditJourney, schemeValidations, mapPropsToValues, handleSubmitForm);

const mapStateToProps = (state) => ({
  journeyData: state.module.view.journeyData
});

export default connect(mapStateToProps, { edit })(FormikCreate);
