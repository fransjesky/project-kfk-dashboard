import React, { useState } from 'react';
import Modal, { ModalContent, ModalButtons, ModalHead } from 'components/Modal';
import { Box, TextField, Button } from '@mui/material';
import Planet from './Planet';
import * as Yup from 'yup';
import withFormik from 'utils/withFormik';

// redux
import { connect } from 'react-redux';
import { add } from 'redux/actions';

// schema validation
const schemeValidations = Yup.object().shape({
  year: Yup.string()
    .trim()
    .required('Field cannot be empty')
    .min(4, 'Year must be in 4 digits format')
    .max(4, 'Year must be in 4 digits format'),
  text: Yup.string()
    .trim()
    .required('Field cannot be empty')
    .max(80, 'Please input a maximum of 80 characters'),
  mainColor: Yup.string().trim().required('Field cannot be empty'),
  secondaryColor: Yup.string().trim().required('Field cannot be empty'),
  dimension: Yup.number()
    .min(85, 'Dimension minimum size must be at least 85 pixels')
    .max(220, 'Dimension maximum size cannot exceeds 220 pixels')
    .required('Field cannot be empty')
});

// initial values
const initialValueForm = {
  year: '',
  text: '',
  mainColor: '#EFB008',
  secondaryColor: '#FFDC81',
  dimension: 100
};

const handleSubmitForm = (payload, ctx) => {
  const { year, text, mainColor, secondaryColor, dimension } = payload;

  ctx.props.add({
    name: 'journey',
    data: {
      year,
      text,
      mainColor,
      secondaryColor,
      dimension
    },
    notLink: true,
    onSuccess: () => {
      ctx.resetForm();
      ctx.props.onSuccess();
    },
    onError: (msg) => {
      for (const key in msg) {
        ctx.setFieldError(key, msg[key]);
      }
    }
  });
};

function AddJourney(props) {
  const [invalidYear, setInvalidYear] = useState(false);
  // props init
  const { open, onClose } = props;
  const {
    values,
    touched,
    errors,
    handleChange,
    handleBlur,
    handleSubmit,
    resetForm,
    dirty,
    isValid,
    setFieldValue
  } = props;

  const MODAL_TITLE = 'Add New Journey';
  const MODAL_SUB_TITLE = 'Congrats, we reached a new milestone!';
  const error = (val) => Boolean(touched[val] && errors[val]);
  const handleYear = (e) => {
    if (e.target.value.length > 4) {
      return;
    }
    setFieldValue('year', checkYear(e.target.value));
  };

  // this function act as a validation which would automatically detect if the year user put is between range or not
  const checkYear = (year) => {
    const convertYear = parseInt(year);
    // set the accepted valid year range
    if (year.length == 4 && (convertYear < 1950 || convertYear > 2050)) {
      setInvalidYear(true);
      return year;
    } else {
      setInvalidYear(false);
      return year;
    }
  };

  return (
    <Modal
      isOpen={open}
      onClose={() => {
        resetForm();
        onClose();
      }}
      maxWidth={1025}
    >
      <ModalHead title={MODAL_TITLE} subtitle={MODAL_SUB_TITLE} />
      <ModalContent>
        <Box width={'646px'}>
          <Box sx={{ marginBottom: '1.5rem' }}>
            <TextField
              name="year"
              label="Milestone Year"
              type="number"
              sx={{ width: '50%' }}
              onChange={handleYear}
              onBlur={handleBlur}
              error={invalidYear || error('year')}
              value={values.year}
              helperText={
                invalidYear
                  ? 'Invalid year detected. Make sure the year range is between 1950 and 2050'
                  : touched.year && errors.year
              }
            />
            <TextField
              name="text"
              label="Journey Description"
              multiline
              minRows={4}
              onChange={handleChange}
              onBlur={handleBlur}
              error={error('text')}
              value={values.text}
              helperText={
                error('text')
                  ? touched.text && errors.text
                  : 'Please share stories during this period and input a maximum of 80 characters'
              }
            />
          </Box>
          <Box display="flex" justifyContent="space-between">
            <Box
              sx={{
                marginRight: '1.125rem',
                width: '100%',
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'flex-start'
              }}
            >
              <h5>Color Picker</h5>
              <TextField
                name="mainColor"
                label="Background Color"
                type="color"
                sx={{ width: '100%' }}
                onChange={handleChange}
                onBlur={handleBlur}
                error={error('mainColor')}
                value={values.mainColor}
                helperText={touched.mainColor && errors.mainColor}
              />
              <TextField
                name="secondaryColor"
                label="Foreground Color"
                type="color"
                sx={{ border: 'none', width: '100%' }}
                onChange={handleChange}
                onBlur={handleBlur}
                error={error('secondaryColor')}
                value={values.secondaryColor}
                helperText={touched.secondaryColor && errors.secondaryColor}
              />
              <TextField
                name="dimension"
                label="Dimension"
                type="number"
                placeholder="Dimension"
                sx={{ width: '100%' }}
                InputProps={{
                  inputProps: {
                    max: 220,
                    min: 85
                  }
                }}
                onChange={handleChange}
                onBlur={handleBlur}
                error={error('dimension')}
                value={values.dimension}
                helperText={touched.dimension && errors.dimension}
              />
            </Box>
            <Box
              sx={{
                marginLeft: '1.125rem',
                width: '100%',
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'flex-start'
              }}
            >
              <h5>Planet Preview</h5>
              <Box
                sx={{
                  height: '100%',
                  width: '100%',
                  maxHeight: '12.5rem',
                  maxWidth: '18rem',
                  overflow: 'hidden',
                  backgroundColor: '#0F1A2A',
                  borderRadius: '0.5rem',
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
              >
                <Planet
                  bgColor={values.mainColor}
                  fgColor={values.secondaryColor}
                  dimension={values.dimension ? values.dimension : 85}
                />
              </Box>
            </Box>
          </Box>
          <ModalButtons>
            <Button
              variant="contained"
              disabled={!(dirty && isValid)}
              onClick={handleSubmit}
              sx={{ color: '#fff' }}
            >
              Save
            </Button>
          </ModalButtons>
        </Box>
      </ModalContent>
    </Modal>
  );
}

const FormikCreate = withFormik(AddJourney, schemeValidations, initialValueForm, handleSubmitForm);

export default connect(null, { add })(FormikCreate);
