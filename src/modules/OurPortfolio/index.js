import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Box } from '@mui/material';
import ModalAdd from './components/ModalAdd';
import Header from 'components/Header';
import CardPortfolio from 'components/CardOurPortofolio';
import Pagination from 'components/Pagination';
import { list, edit, remove } from 'redux/actions';
import ModalEdit from './components/ModalEdit';
import Page from 'components/Page';
import AutoGrid from 'components/AutoGrid';
import { useConfirm, useSearch } from 'utils/hooks';
import BodyLayout from 'layouts/dashboard/BodyLayout';
import EmptyState from 'components/EmptyState';

const PAGE_TITLE = 'Portfolio';
const DEFAULT_PORTFOLIO_QUERY = {
  limit: 0,
  offset: 0
};

function OurPortfolio() {
  const dispatch = useDispatch();
  const { portfolios } = useSelector((state) => state.module.list);
  const [open, setOpen] = useState(false);
  const [portfolioQuery, setPortfolioQuery] = useState(DEFAULT_PORTFOLIO_QUERY);
  const [keyword, setKeyword] = useState('');
  const [openEdit, setOpenEdit] = useState(false);
  const [idEdit, setIdEdit] = useState(null);
  const [refetch, setRefetch] = useState(false);
  const { keywordPortfolio, keywordGlobal } = useSelector((state) => state.module.keyword);
  const { confirmInfo } = useConfirm();
  const { dispatchSearch, dispatchSearchGlobal } = useSearch('keywordPortfolio');
  const [loading, setLoading] = useState(true);
  const BREADCRUMBS = [
    { label: 'Overview', link: '/' },
    { label: 'Our Portfolio', link: '/works/portfolio' }
  ];

  useEffect(() => {
    dispatchSearch(keywordGlobal || '');
    return () => {
      dispatchSearchGlobal('');
    };
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    setKeyword(keywordPortfolio);
  }, [keywordPortfolio]);

  const handleArchive = (id, status) => {
    return dispatch(
      edit({
        name: 'portfolio',
        noLink: true,
        titleNotifSucces: `Successfully ${status == 'active' ? 'Archived' : 'Showed'}`,
        id,
        data: { status: status == 'archive' ? 'active' : 'archive' },
        onSuccess: () => {
          dispatchSearch('');
          resetPage();
          setRefetch((prev) => !prev);
        }
      })
    );
  };

  const handleEdit = (id) => {
    setIdEdit(id);
    setOpenEdit(true);
  };

  const handleDelete = (id, name) => {
    confirmInfo({
      title: `Are you sure you want to delete '${name}'?`,
      message: 'You won’t be able to recover this in the future!',
      handler: () => {
        dispatch(
          remove({
            name: 'portfolio',
            noLink: true,
            id,
            onSuccess: () => {
              dispatchSearch('');
              resetPage();
              setRefetch((prev) => !prev);
            }
          })
        );
      }
    });
  };

  const handleAutoGridChangeWidth = ({ width, cardWidth, gap }) => {
    setPortfolioQuery((prev) => {
      if (width < cardWidth) return prev;
      const limit = Math.floor((width + gap) / (cardWidth + gap)) * 2;
      if (limit !== prev.limit) {
        return { ...prev, limit };
      }
      return prev;
    });
  };

  useEffect(() => {
    if (portfolioQuery.limit > 0) {
      setLoading(true);
      dispatch(
        list({
          name: 'portfolio',
          customName: 'portfolios',
          query: `sort=createdAt&order=DESC&limit=${portfolioQuery.limit}&offset=${portfolioQuery.offset}&keyword=${keyword}`,
          onSuccess: () => setLoading(false)
        })
      );
    }
    // eslint-disable-next-line
  }, [keyword, refetch, portfolioQuery]);

  const handleSearch = (val) => {
    dispatchSearch(val);
  };

  const handlePagination = (event, value) => {
    setPortfolioQuery((prev) => ({
      ...prev,
      offset: (value - 1) * prev.limit
    }));
  };

  const resetPage = () => setPortfolioQuery((prev) => ({ ...prev, offset: 0 }));

  return (
    <Page title={PAGE_TITLE}>
      <BodyLayout>
        <ModalAdd
          open={open}
          onClose={() => setOpen(false)}
          onSuccess={() => {
            resetPage();
            dispatchSearch('');
            setRefetch((prev) => !prev);
            setOpen(false);
          }}
        />
        <ModalEdit
          open={openEdit}
          id={idEdit}
          onClose={() => setOpenEdit(false)}
          onSuccess={() => {
            resetPage();
            dispatchSearch('');
            setRefetch((prev) => !prev);
            setOpenEdit(false);
          }}
        />
        <Header
          routes={BREADCRUMBS}
          withButton={true}
          keyword={keyword}
          withSearchBar={true}
          onSearch={handleSearch}
          buttonText="Add New Work"
          onButtonClick={() => {
            setOpen(true);
          }}
        />
        <AutoGrid onChangeWidth={handleAutoGridChangeWidth} gap={25} cardWidth={350}>
          {portfolios?.data?.map((val) => (
            <Box sx={{ width: '340px' }} key={val.id}>
              <CardPortfolio
                detailLink={val?.detailLink}
                thumbnail={val?.thumbnail}
                name={val?.name}
                projectCompany={val?.projectCompany}
                status={val?.status}
                onEdit={() => {
                  handleEdit(val.id);
                }}
                onArchive={() => {
                  handleArchive(val.id, val.status);
                }}
                onDelete={() => {
                  handleDelete(val.id, val.name);
                }}
              />
            </Box>
          ))}
        </AutoGrid>
        {!loading && !portfolios?.data?.length && <EmptyState name="Portfolios" />}
        {portfolios?.data?.length && (
          <Box
            sx={{
              pb: 3,
              display: 'flex',
              mt: 5,
              flexGrow: 1,
              justifyContent: 'center',
              alignItems: 'flex-end'
            }}
          >
            <Pagination
              totalPage={Math.ceil((portfolios?.count ?? 0) / portfolioQuery.limit)}
              currentPage={Math.floor(portfolioQuery.offset / portfolioQuery.limit) + 1}
              onChange={handlePagination}
            />
          </Box>
        )}
      </BodyLayout>
    </Page>
  );
}

export default OurPortfolio;
