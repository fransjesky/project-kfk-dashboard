import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Box, TextField, Button } from '@mui/material';
import Modal, { ModalContent, ModalButtons, ModalHead } from 'components/Modal';
import { AnyUploader } from 'components/Uploader/UploaderContainer';
import * as Yup from 'yup';
import withFormik from 'utils/withFormik';
import { add } from 'redux/actions';
import MultiSelect from 'components/MultiSelect';
import ImageAttachment from 'components/ImageAttachment';
import R from 'utils/regex';

// schema validation
const schemeValidations = Yup.object().shape({
  thumbnail: Yup.string().required('Field cannot be empty'),
  projectCompany: Yup.string().trim().required('Field cannot be empty'),
  name: Yup.string().trim().required('Field cannot be empty'),
  description: Yup.string()
    .trim()
    .required('Field cannot be empty')
    .min(200, 'Please input a minimum of 200 characters'),
  projectType: Yup.array().min(1, 'Please choose the project type'),
  detailLink: Yup.string()
    .trim()
    .url('URL Link must be a valid URL')
    .matches(R.behanceUrl, 'Please input Behance Link')
    .required('Field cannot be empty')
});

// initial values
const initialValueForm = {
  name: '',
  description: '',
  projectCompany: '',
  projectType: [],
  thumbnail: '',
  detailLink: ''
};

const handleSubmitForm = (payload, ctx) => {
  const { name, description, projectCompany, projectType, thumbnail, detailLink } = payload;

  ctx.props.add({
    name: 'portfolio',
    data: {
      name,
      description,
      projectCompany,
      projectType: projectType.filter((val) => val !== undefined),
      thumbnail,
      detailLink
    },
    customRedirect: 'works/portfolio',
    onSuccess: () => {
      ctx.resetForm();
      ctx.props.onSuccess();
    }
  });
};

const INITIAL_PROJECT_TYPES = ['Website', 'Mobile', 'IOT', 'Dashboard', 'System Architecture'];

function ModalAdd(props) {
  const { open, onClose } = props;
  const {
    values,
    touched,
    errors,
    handleChange,
    handleBlur,
    handleSubmit,
    resetForm,
    isValid,
    dirty,
    setFieldValue
  } = props;
  const error = (val) => Boolean(touched[val] && errors[val]);

  const [options, setOptions] = useState(INITIAL_PROJECT_TYPES);

  const onAddType = (value) => {
    if (!options.includes(value)) {
      setOptions([...options, value]);
    }
  };

  const [selectedImage, setSelectedImage] = useState(null);
  const handleDropzoneChange = (value) => {
    setSelectedImage(value[0]?.data);
    setFieldValue('thumbnail', value[0]?.data?.location);
  };

  const handleDeleteAttachment = () => {
    setSelectedImage(null);
    setFieldValue('thumbnail', '');
  };

  return (
    <Modal
      isOpen={open}
      onClose={() => {
        setOptions(INITIAL_PROJECT_TYPES);
        resetForm();
        onClose();
      }}
      maxWidth={679}
    >
      <ModalHead title="Add Portfolio" />
      <ModalContent>
        <Box width={'900px'}>
          <h2>Upload a thumbnail</h2>
          <p>Files should be PNG</p>
          <Box width="60%" marginTop={3}>
            {!values.thumbnail ? (
              <AnyUploader
                elementId="dropzone-portfolio"
                type="dropzone"
                allowedTypes={['png']}
                maxSizeMb={1}
                onChange={handleDropzoneChange}
                onError={() => {}}
              />
            ) : (
              <ImageAttachment fileData={selectedImage} onDelete={() => handleDeleteAttachment()} />
            )}
          </Box>
        </Box>
        <Box display="flex" flexDirection="column" sx={{ mt: 3 }}>
          <h2>Project Details</h2>
          <TextField
            name="projectCompany"
            label="Project Company"
            sx={{ width: '320px' }}
            value={values.projectCompany}
            onChange={handleChange}
            onBlur={handleBlur}
            error={error('projectCompany')}
            helperText={touched.projectCompany && errors.projectCompany}
            InputLabelProps={error('projectCompany') ? { shrink: true } : {}}
          />
          <TextField
            label="Project Name"
            name="name"
            sx={{ width: '100%' }}
            value={values.name}
            onChange={handleChange}
            onBlur={handleBlur}
            error={error('name')}
            helperText={touched.name && errors.name}
            InputLabelProps={error('name') ? { shrink: true } : {}}
          />
          <TextField
            label="Project Description"
            multiline
            minRows={3}
            sx={{ width: '100%' }}
            name="description"
            value={values.description}
            onChange={handleChange}
            onBlur={handleBlur}
            error={error('description')}
            helperText={
              error('description')
                ? touched.description && errors.description
                : 'Please tell us briefly about this portfolio piece and input a minimum of 200 characters'
            }
            InputLabelProps={error('description') ? { shrink: true } : {}}
          />
          <MultiSelect
            name="projectType"
            withCustomValue
            label="Project Type"
            chipcolor="primary"
            value={values.projectType}
            onBlur={handleBlur}
            error={Boolean(touched.projectType && errors.projectType)}
            onChange={(val) => setFieldValue('projectType', val)}
            onAddCustomValue={onAddType}
            helperText={touched.projectType && errors.projectType}
            options={options}
          />
        </Box>
        <Box display="flex" flexDirection="column" sx={{ mt: 3 }}>
          <h2>Portfolio Link</h2>
          <TextField
            label="URL Link"
            name="detailLink"
            fullWidth
            value={values.detailLink}
            onChange={handleChange}
            onBlur={handleBlur}
            error={error('detailLink')}
            helperText={
              error('detailLink')
                ? touched.detailLink && errors.detailLink
                : 'Please input Behance Link'
            }
            InputLabelProps={error('detailLink') ? { shrink: true } : {}}
          />
        </Box>
        <ModalButtons>
          <Button
            disabled={!(dirty && isValid)}
            variant="contained"
            sx={{ color: '#fff' }}
            onClick={() => {
              setOptions(INITIAL_PROJECT_TYPES);
              handleSubmit();
            }}
          >
            Save
          </Button>
        </ModalButtons>
      </ModalContent>
    </Modal>
  );
}

const FormikCreate = withFormik(ModalAdd, schemeValidations, initialValueForm, handleSubmitForm);

export default connect(null, { add })(FormikCreate);
