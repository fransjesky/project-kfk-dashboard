import React, { useState, useEffect } from 'react';
import { connect, useDispatch } from 'react-redux';
import { Box, TextField, Button } from '@mui/material';
import Modal, { ModalContent, ModalButtons, ModalHead } from 'components/Modal';
import { AnyUploader } from 'components/Uploader/UploaderContainer';
import * as Yup from 'yup';
import withFormik from 'utils/withFormik';
import MultiSelect from 'components/MultiSelect';
import { edit, load } from 'redux/actions';
import ImageAttachment from 'components/ImageAttachment';
import R from 'utils/regex';

// schema validation
const schemeValidations = Yup.object().shape({
  thumbnail: Yup.string().required('Field cannot be empty'),
  projectCompany: Yup.string().trim().required('Field cannot be empty'),
  name: Yup.string().trim().required('Field cannot be empty'),
  description: Yup.string()
    .trim()
    .required('Field cannot be empty')
    .min(200, 'Please input a minimum of 200 characters'),
  projectType: Yup.array().min(1, 'Please choose the project type'),
  detailLink: Yup.string()
    .trim()
    .matches(R.behanceUrl, 'Please input Behance Link')
    .required('Field cannot be empty')
});

// initial values
const initialValueForm = {
  name: '',
  description: '',
  projectCompany: '',
  projectType: [],
  thumbnail: '',
  detailLink: ''
};

const mapPropsToValues = ({ portfolio }) => {
  const data = portfolio?.data;

  if (data) {
    return {
      name: data.name,
      description: data.description,
      projectCompany: data.projectCompany,
      projectType: data.projectType,
      thumbnail: data.thumbnail,
      detailLink: data.detailLink
    };
  }
  return initialValueForm;
};

const handleSubmitForm = (payload, ctx) => {
  const { name, description, projectCompany, projectType, thumbnail, detailLink } = payload;

  ctx.props.edit({
    name: 'portfolio',
    id: ctx.props.id,
    noLink: true,
    data: {
      name,
      description,
      projectCompany,
      projectType,
      thumbnail,
      detailLink
    },
    onSuccess: () => {
      ctx.resetForm();
      ctx.props.onSuccess();
    }
  });
};

const INITIAL_PROJECT_TYPES = ['Website', 'Mobile', 'IOT', 'Dashboard', 'System Architecture'];

function ModalEdit(props) {
  const { open, onClose, id } = props;
  const {
    values,
    touched,
    errors,
    handleChange,
    dirty,
    isValid,
    handleBlur,
    handleSubmit,
    resetForm,
    setFieldValue
  } = props;
  const error = (val) => Boolean(touched[val] && errors[val]);
  const dispatch = useDispatch();
  const [options, setOptions] = useState(INITIAL_PROJECT_TYPES);

  const onAddType = (value) => {
    if (!options.includes(value)) {
      setOptions([...options, value]);
    }
  };

  const [selectedImage, setSelectedImage] = useState(null);
  const handleDropzoneChange = (value) => {
    setSelectedImage(value[0]?.data);
    setFieldValue('thumbnail', value[0]?.data?.location);
  };

  const handleDeleteAttachment = () => {
    setSelectedImage(null);
    setFieldValue('thumbnail', '');
  };

  useEffect(() => {
    dispatch(load({ name: 'portfolio', id }));
  }, [id, dispatch, open]);

  useEffect(() => {
    if (!values.projectType) return;

    values.projectType.forEach((value) => {
      if (!options.includes(value)) {
        setOptions((prev) => [...prev, value]);
      }
    });

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [values.projectType]);

  return (
    <Modal
      isOpen={open}
      onClose={() => {
        setOptions(INITIAL_PROJECT_TYPES);
        resetForm();
        onClose();
      }}
      maxWidth={679}
    >
      <ModalHead title="Edit Portfolio" />
      <ModalContent>
        <Box width={'900px'}>
          <h2>Upload a thumbnail</h2>
          <p>Files should be PNG</p>
          <Box width="60%" marginTop={3}>
            {!values.thumbnail ? (
              <AnyUploader
                elementId="dropzone-portfolio"
                type="dropzone"
                allowedTypes={['png']}
                maxSizeMb={1}
                onChange={handleDropzoneChange}
                onError={() => {}}
              />
            ) : (
              <ImageAttachment
                fileData={selectedImage ? selectedImage : { location: values?.thumbnail }}
                onDelete={() => handleDeleteAttachment()}
              />
            )}
          </Box>
        </Box>
        <Box display="flex" flexDirection="column" sx={{ mt: 3 }}>
          <h2>Project Details</h2>
          <TextField
            name="projectCompany"
            label="Project Company"
            sx={{ width: '320px' }}
            value={values.projectCompany}
            onChange={handleChange}
            onBlur={handleBlur}
            error={error('projectCompany')}
            helperText={touched.projectCompany && errors.projectCompany}
            InputLabelProps={
              error('projectCompany') || values.projectCompany ? { shrink: true } : {}
            }
          />
          <TextField
            label="Project Name"
            name="name"
            sx={{ width: '100%' }}
            value={values.name}
            onChange={handleChange}
            onBlur={handleBlur}
            error={error('name')}
            helperText={touched.name && errors.name}
            InputLabelProps={error('name') || values.name ? { shrink: true } : {}}
          />
          <TextField
            label="Project Description"
            multiline
            minRows={3}
            sx={{ width: '100%' }}
            name="description"
            value={values.description}
            onChange={handleChange}
            onBlur={handleBlur}
            error={error('description')}
            helperText={
              error('description')
                ? touched.description && errors.description
                : 'Please tell us briefly about this portfolio piece and input a minimum of 200 characters'
            }
            InputLabelProps={error('description') || values.description ? { shrink: true } : {}}
          />
          <MultiSelect
            name="projectType"
            withCustomValue
            label="Project Type"
            chipcolor="primary"
            value={values.projectType || []}
            onBlur={handleBlur}
            error={Boolean(touched.projectType && errors.projectType)}
            onChange={(val) => setFieldValue('projectType', val)}
            onAddCustomValue={onAddType}
            helperText={touched.projectType && errors.projectType}
            options={options}
          />
        </Box>
        <Box display="flex" flexDirection="column" sx={{ mt: 3 }}>
          <h2>Portfolio Link</h2>
          <TextField
            label="URL Link"
            name="detailLink"
            fullWidth
            value={values.detailLink}
            onChange={handleChange}
            onBlur={handleBlur}
            error={error('detailLink')}
            helperText={
              error('detailLink')
                ? touched.detailLink && errors.detailLink
                : 'Please input Behance Link'
            }
            InputLabelProps={error('detailLink') || values.detailLink ? { shrink: true } : {}}
          />
        </Box>
        <ModalButtons>
          <Button
            disabled={!(dirty && isValid)}
            variant="contained"
            sx={{ color: '#fff' }}
            onClick={() => {
              setOptions(INITIAL_PROJECT_TYPES);
              handleSubmit();
              setChanged((prev) => !prev);
            }}
          >
            Save
          </Button>
        </ModalButtons>
      </ModalContent>
    </Modal>
  );
}

const FormikCreate = withFormik(ModalEdit, schemeValidations, mapPropsToValues, handleSubmitForm);

const mapStateToProps = (state) => ({
  portfolio: state.module.view.portfolio
});

export default connect(mapStateToProps, { edit })(FormikCreate);
