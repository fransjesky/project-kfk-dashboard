import React, { useEffect, useState } from 'react';
import { connect, useSelector, useDispatch } from 'react-redux';
import * as Yup from 'yup';
import { v4 as uuidv4 } from 'uuid';
// material
import { Box, TextField, Button, MenuItem } from '@mui/material';
// components
import Modal, { ModalContent, ModalButtons, ModalHead } from 'components/Modal';
import { AnyUploader } from 'components/Uploader/UploaderContainer';
import ImageAttachment from 'components/ImageAttachment';
// utils
import withFormik from 'utils/withFormik';
import { useLister } from 'utils/hooks';
// actions
import { add, notif } from 'redux/actions';

const isContainsNonWhitespace = (str) => {
  if (str && str.replace(/\s/g, '').length > 0) {
    return true;
  } else {
    return false;
  }
};

// schema validation
const schemeValidations = Yup.object().shape({
  logo: Yup.string().required('Field cannot be empty'),
  name: Yup.string()
    .required('Field cannot be empty')
    .test('Contains any non-whitespace character', 'Field cannot be empty', (str) =>
      isContainsNonWhitespace(str)
    ),
  order: Yup.string().required('Please choose a number')
});

// initial values
const initialValueForm = {
  name: '',
  logo: '',
  order: ''
};

const handleSubmitForm = (payload, ctx) => {
  const { name, logo, order } = payload;

  ctx.props.add({
    name: 'patner',
    data: {
      name,
      logo,
      order
    },
    notLink: true,
    onSuccess: () => {
      ctx.resetForm();
      ctx.props.onSuccess();
    }
  });
};

const ModalAdd = (props) => {
  const { open, onClose } = props;
  const {
    values,
    touched,
    errors,
    handleChange,
    handleBlur,
    handleSubmit,
    resetForm,
    dirty,
    isValid,
    setFieldValue
  } = props;
  const error = (val) => Boolean(touched[val] && errors[val]);

  const dispatch = useDispatch();
  const [selectedImage, setSelectedImage] = useState(null);
  const { dispatchList } = useLister('patner/number', 'partnerOrderOptions');
  const { partnerOrderOptions } = useSelector((state) => state.module.list);

  const handleDropzoneChange = (value) => {
    setSelectedImage(value[0]?.data);
    setFieldValue('logo', value[0]?.data?.location);
  };

  const handleDeleteAttachment = () => {
    setSelectedImage(null);
    setFieldValue('logo', '');
  };

  // get testimony order options
  // eslint-disable-next-line
  useEffect(() => dispatchList({}), []);

  useEffect(() => {
    if (partnerOrderOptions?.data.length) {
      setFieldValue('order', partnerOrderOptions?.data[partnerOrderOptions?.data.length - 1]);
    }
    // eslint-disable-next-line
  }, [partnerOrderOptions]);

  return (
    <Modal
      isOpen={open}
      onClose={() => {
        resetForm();
        onClose();
      }}
    >
      <ModalHead title="Add New Partner" />
      <ModalContent>
        <Box width="450px">
          <h2>Upload a logo</h2>
          <p>Files should be PNG</p>
          <Box marginTop={3}>
            {!values.logo ? (
              <AnyUploader
                elementId="dropzone-portfolio"
                type="dropzone"
                allowedTypes={['png']}
                maxSizeMb={1}
                onChange={handleDropzoneChange}
                onError={(err) => {
                  dispatch(
                    notif({
                      open: true,
                      variant: 'error',
                      title: 'Failed to uploud image',
                      message: err
                    })
                  );
                }}
              />
            ) : (
              <ImageAttachment fileData={selectedImage} onDelete={() => handleDeleteAttachment()} />
            )}
          </Box>
          <Box sx={{ mt: 3 }}>
            <TextField
              name="name"
              label="Partner Name"
              value={values.name}
              onChange={handleChange}
              onBlur={handleBlur}
              error={error('name')}
              helperText={touched.name && errors.name}
              InputLabelProps={error('name') ? { shrink: true } : {}}
            />
            <TextField
              select
              disabled
              label="Choose Order"
              name="order"
              value={values.order}
              onChange={handleChange}
              onBlur={handleBlur}
              error={error('order')}
              helperText={touched.order && errors.order}
              InputLabelProps={error('order') ? { shrink: true } : {}}
              sx={{ width: '40%' }}
            >
              {partnerOrderOptions?.data?.map((value) => (
                <MenuItem key={uuidv4()} value={value}>
                  {value}
                </MenuItem>
              ))}
            </TextField>
          </Box>
        </Box>
        <ModalButtons>
          <Button
            variant="contained"
            disabled={!(dirty && isValid)}
            sx={{ color: '#fff' }}
            onClick={handleSubmit}
          >
            Save
          </Button>
        </ModalButtons>
      </ModalContent>
    </Modal>
  );
};

const FormikCreate = withFormik(ModalAdd, schemeValidations, initialValueForm, handleSubmitForm);

export default connect(null, { add })(FormikCreate);
