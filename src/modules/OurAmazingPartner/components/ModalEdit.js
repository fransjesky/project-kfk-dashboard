import React, { useState, useEffect } from 'react';
import { connect, useDispatch, useSelector } from 'react-redux';
import * as Yup from 'yup';
import { v4 as uuidv4 } from 'uuid';
import { Box, TextField, Button, MenuItem } from '@mui/material';
// components
import Modal, { ModalContent, ModalButtons, ModalHead } from 'components/Modal';
import { AnyUploader } from 'components/Uploader/UploaderContainer';
import ImageAttachment from 'components/ImageAttachment';
// actions
import { edit, load, notif } from 'redux/actions';
// utils
import withFormik from 'utils/withFormik';
import { useLister } from 'utils/hooks';

const isContainsNonWhitespace = (str) => {
  if (str && str.replace(/\s/g, '').length > 0) {
    return true;
  } else {
    return false;
  }
};

// schema validation
const schemeValidations = Yup.object().shape({
  logo: Yup.string().required('Field cannot be empty'),
  name: Yup.string()
    .required('Field cannot be empty')
    .test('Contains any non-whitespace character', 'Field cannot be empty', (str) =>
      isContainsNonWhitespace(str)
    ),
  order: Yup.string().required('Please choose a number')
});

// initial values
const initialValueForm = {
  name: '',
  logo: 'https://cdn.dribbble.com/users/33073/screenshots/14218457/media/1c1c43c4358439d227b01950716f30be.png?compress=1&resize=1200x900&vertical=top',
  order: ''
};

const mapPropsToValues = ({ partner }) => {
  const data = partner?.data;

  if (data) {
    return {
      name: data.name,
      logo: data.logo,
      order: data.order
    };
  }
  return initialValueForm;
};

const handleSubmitForm = (payload, ctx) => {
  const { name, logo, order } = payload;

  ctx.props.edit({
    name: 'patner',
    id: ctx.props.id,
    noLink: true,
    data: {
      name,
      logo,
      order
    },
    onSuccess: () => {
      ctx.resetForm();
      ctx.props.onSuccess();
    }
  });
};

const ModalEdit = (props) => {
  const { open, onClose, id } = props;
  const {
    values,
    touched,
    errors,
    handleChange,
    handleBlur,
    handleSubmit,
    resetForm,
    dirty,
    isValid,
    setFieldValue
  } = props;
  const error = (val) => Boolean(touched[val] && errors[val]);
  const dispatch = useDispatch();

  const [selectedImage, setSelectedImage] = useState(null);
  const { dispatchList } = useLister('patner/number', 'partnerOrderOptions');
  const { partnerOrderOptions } = useSelector((state) => state.module.list);

  const handleDropzoneChange = (value) => {
    setSelectedImage(value[0]?.data);
    setFieldValue('logo', value[0]?.data?.location);
  };

  const handleDeleteAttachment = () => {
    setSelectedImage(null);
    setFieldValue('logo', '');
  };

  // get testimony order options
  // eslint-disable-next-line
  useEffect(() => dispatchList({}), []);

  useEffect(() => {
    dispatch(load({ name: 'patner', id }));
    // eslint-disable-next-line
  }, [id, open]);

  return (
    <Modal
      isOpen={open}
      onClose={() => {
        resetForm();
        onClose();
      }}
    >
      <ModalHead title="Edit Partner" />
      <ModalContent>
        <Box width="450px">
          <h2>Upload a logo</h2>
          <p>Files should be PNG</p>
          <Box marginTop={3}>
            {!values.logo ? (
              <AnyUploader
                elementId="dropzone-partner"
                type="dropzone"
                allowedTypes={['png']}
                maxSizeMb={1}
                onChange={handleDropzoneChange}
                onError={(err) => {
                  dispatch(
                    notif({
                      open: true,
                      variant: 'error',
                      title: 'Failed to uploud image',
                      message: err
                    })
                  );
                }}
              />
            ) : (
              <ImageAttachment
                fileData={selectedImage ? selectedImage : { location: values?.logo }}
                onDelete={() => handleDeleteAttachment()}
              />
            )}
          </Box>
          <Box sx={{ mt: 3 }}>
            <TextField
              name="name"
              label="Partner Name"
              value={values.name}
              onChange={handleChange}
              onBlur={handleBlur}
              error={error('name')}
              helperText={touched.name && errors.name}
              InputLabelProps={error('name') ? { shrink: true } : {}}
            />
            <TextField
              select
              label="Choose Order"
              name="order"
              value={values.order}
              onChange={handleChange}
              onBlur={handleBlur}
              error={error('order')}
              helperText={touched.order && errors.order}
              InputLabelProps={error('order') ? { shrink: true } : {}}
              sx={{ width: '40%' }}
            >
              {partnerOrderOptions?.data?.map((value, index) => {
                return (
                  index !== partnerOrderOptions.data.length - 1 && (
                    <MenuItem key={uuidv4()} value={value}>
                      {value}
                    </MenuItem>
                  )
                );
              })}
            </TextField>
          </Box>
        </Box>
        <ModalButtons>
          <Button
            variant="contained"
            disabled={!(dirty && isValid)}
            sx={{ color: '#fff' }}
            onClick={handleSubmit}
          >
            Save
          </Button>
        </ModalButtons>
      </ModalContent>
    </Modal>
  );
};

const FormikCreate = withFormik(ModalEdit, schemeValidations, mapPropsToValues, handleSubmitForm);

const mapStateToProps = (state) => ({
  partner: state.module.view.patner
});

export default connect(mapStateToProps, { edit })(FormikCreate);
