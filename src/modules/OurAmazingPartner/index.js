import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
// components
import { Box } from '@mui/material';
import Page from 'components/Page';
import BodyLayout from 'layouts/dashboard/BodyLayout';
import Header from 'components/Header';
import Pagination from 'components/Pagination';
import AutoGrid from 'components/AutoGrid';
import CardPartner from 'components/CardPartner';
import ModalAdd from './components/ModalAdd';
import ModalEdit from './components/ModalEdit';
import EmptyState from 'components/EmptyState';
// utils
import { useLister, useRemover, useConfirm, useSearch } from 'utils/hooks';

const PAGE_TITLE = 'Our Amazing Partners';

const ROUTES = [
  { label: 'Overview', link: '/' },
  { label: 'Our Amazing Partners', link: '/home/partner' }
];

const DEFAULT_PATNER_QUERY = {
  sort: 'order',
  order: 'ASC',
  limit: 0,
  offset: 0,
  keyword: ''
};

const OurAmazingPartners = () => {
  const [patnerQuery, setPatnerQuery] = useState(DEFAULT_PATNER_QUERY);
  const [openCreate, setOpenCreate] = useState(false);
  const [openEdit, setOpenEdit] = useState(false);
  const [idEdit, setIdEdit] = useState(null);
  const [isSuccess, setIsSuccess] = useState(false);
  const { dispatchRemove } = useRemover('patner', 'home/partner', false);
  const { dispatchList } = useLister('patner', 'partners');
  const { partners } = useSelector((state) => state.module.list);
  const { keywordPartner, keywordGlobal } = useSelector((state) => state.module.keyword);
  const { confirmInfo } = useConfirm();
  const { dispatchSearch, dispatchSearchGlobal } = useSearch('keywordPartner');
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    dispatchSearch(keywordGlobal || '');
    return () => {
      dispatchSearchGlobal('');
    };
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    const timeout = setTimeout(() => {
      setPatnerQuery((prev) => ({
        ...DEFAULT_PATNER_QUERY,
        limit: prev.limit,
        keyword: keywordPartner
      }));
    }, 500);

    return () => clearTimeout(timeout);
    // eslint-disable-next-line
  }, [keywordPartner]);

  const handlePagination = (event, value) => {
    setPatnerQuery((prev) => ({
      ...prev,
      offset: (value - 1) * prev.limit
    }));
  };

  // load testimonies
  useEffect(() => {
    if (patnerQuery.limit > 0) {
      setLoading(true);
      dispatchList(patnerQuery, () => setLoading(false));
    }
    // eslint-disable-next-line
  }, [patnerQuery, isSuccess]);

  const handleDelete = (partner) => () => {
    // open modal confirm
    confirmInfo({
      title: `Are you sure you want to delete '${partner.name}'?`,
      message: 'You won’t be able to recover this in the future!',
      handler: () => {
        dispatchRemove(partner.id, () => setIsSuccess(!isSuccess));
      }
    });
  };

  // sync pagination and autogrid
  const handleAutoGridChangeWidth = ({ width, cardWidth, gap }) => {
    setPatnerQuery((prev) => {
      if (width < cardWidth) return prev;
      const limit = Math.floor((width + gap) / (cardWidth + gap)) * 2;
      if (limit !== prev.limit) {
        return { ...prev, limit };
      }
      return prev;
    });
  };

  const handleSearchChange = (value) => {
    resetPage();
    dispatchSearch(value);
  };

  const handleEdit = (id) => {
    setIdEdit(id);
    setOpenEdit(true);
  };

  const resetPage = () => setPatnerQuery((prev) => ({ ...prev, offset: 0 }));

  return (
    <Page title={PAGE_TITLE}>
      <BodyLayout>
        <Header
          routes={ROUTES}
          withSearchBar
          withButton
          keyword={keywordPartner}
          buttonText="Add Partner"
          onButtonClick={() => setOpenCreate(true)}
          onSearch={handleSearchChange}
        />

        {openCreate && (
          <ModalAdd
            open={openCreate}
            onClose={() => setOpenCreate(false)}
            onSuccess={() => {
              setIsSuccess(!isSuccess);
              setOpenCreate(false);
            }}
          />
        )}

        {openEdit && (
          <ModalEdit
            open={openEdit}
            id={idEdit}
            onClose={() => setOpenEdit(false)}
            onSuccess={() => {
              setIsSuccess(!isSuccess);
              setOpenEdit(false);
            }}
          />
        )}

        <AutoGrid onChangeWidth={handleAutoGridChangeWidth}>
          {partners?.data?.map((partner) => (
            <CardPartner
              key={partner.name + partner.id} // in case there are people with the same name
              name={partner.name}
              logo={partner.logo}
              order={partner.order}
              date={partner.createdAt}
              onDelete={handleDelete(partner)}
              onEdit={() => handleEdit(partner.id)}
            />
          ))}
        </AutoGrid>
        {!loading && !partners?.data?.length && <EmptyState name="Partners" />}
        {partners?.data?.length && (
          <Box
            sx={{
              mt: 5,
              pb: 3,
              display: 'flex',
              flexGrow: 1,
              justifyContent: 'center',
              alignItems: 'flex-end'
            }}
          >
            <Pagination
              totalPage={Math.ceil((partners?.count ?? 0) / patnerQuery.limit)}
              currentPage={Math.floor(patnerQuery.offset / patnerQuery.limit) + 1}
              onChange={handlePagination}
            />
          </Box>
        )}
      </BodyLayout>
    </Page>
  );
};

export default OurAmazingPartners;
