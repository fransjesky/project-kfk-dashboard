import { Fragment } from 'react';
import Grid from '@mui/material/Grid';
import { AnyUploader } from '../../components/Uploader/UploaderContainer';

export default function UploaderDemo() {
  const handleSingleChange = (value) => {
    console.log('singleImageUploader1:', value);
  };

  const handleMultiChange = (value) => {
    console.log('multiImageUploader1:', value);
  };

  const handleBtnUploadChange = (value) => {
    console.log('btnUploader1:', value);
  };

  const handleRoundedUploaderChange = (value) => {
    console.log('roundedImageUploader1:', value);
  };

  const handleRoundedUploader2Change = (value) => {
    console.log('roundedImageUploader2:', value);
  };

  const handleDropzoneChange = (value) => {
    console.log('dropzone:', value);
  };

  return (
    <Fragment>
      <Grid container spacing={2} sx={{ my: '30px' }}>
        <Grid item xs={2}>
          Single Image
        </Grid>
        <Grid item xs={6} md={4} lg={2}>
          <AnyUploader
            elementId={'singleImageUploader1'}
            isMulti={false}
            onChange={handleSingleChange}
            maxSizeMb={1}
            onError={(msg) => console.log(msg)}
          />
        </Grid>
      </Grid>
      <Grid container spacing={2} sx={{ my: '30px' }}>
        <Grid item xs={2}>
          Multi Image
        </Grid>
        <Grid item xs={6} md={4} lg={2}>
          <AnyUploader
            elementId={'multiImageUploader1'}
            isMulti={true}
            onChange={handleMultiChange}
          />
        </Grid>
      </Grid>
      <Grid container spacing={2} sx={{ my: '30px' }}>
        <Grid item xs={2}>
          Button + Specific File Types
        </Grid>
        <Grid item xs={6} md={4} lg={2}>
          <AnyUploader
            elementId={'btnUploader1'}
            isMulti={false}
            onChange={handleBtnUploadChange}
            allowedTypes={['pdf', 'xls', 'doc']}
            type={'button'}
            color={'secondary'}
          />
        </Grid>
      </Grid>
      <Grid container spacing={2} sx={{ my: '30px' }}>
        <Grid item xs={2}>
          Rounded for Profile Picture (120px)
        </Grid>
        <Grid item xs={6} md={4} lg={2}>
          <AnyUploader
            elementId={'roundedImageUploader1'}
            onChange={handleRoundedUploaderChange}
            maxSizeMb={1}
            type={'profpic'}
          />
        </Grid>
        <Grid item xs={1}>
          Smaller (80px)
        </Grid>
        <Grid item xs={6} md={4} lg={2}>
          <AnyUploader
            elementId={'roundedImageUploader2'}
            onChange={handleRoundedUploader2Change}
            maxSizeMb={1}
            type={'profpic'}
            size={'small'}
            initialImageSrc={
              'https://kbri-lima-bucket.s3.ap-southeast-1.amazonaws.com/development/attachments/1639611735949-attachments-small'
            }
          />
        </Grid>
      </Grid>
      <Grid container spacing={2} sx={{ my: '30px' }}>
        <Grid item xs={2}>
          Dropzone
        </Grid>
        <Grid item xs={6} md={4} lg={4}>
          <AnyUploader
            elementId={'dropzoneUploader1'}
            type={'dropzone'}
            onChange={handleDropzoneChange}
            maxSizeMb={1}
            allowedTypes={['pdf', 'doc']}
            isMulti={true}
            onError={(msg) => console.log('drop error:', msg)}
          />
        </Grid>
      </Grid>
    </Fragment>
  );
}
