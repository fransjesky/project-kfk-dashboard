import Typography from '@mui/material/Typography';
import TableGeneric from 'components/TableGeneric';
import React, { Fragment } from 'react';

function TableDemo1() {
  const endpointPath = 'potential-buyer';
  const cellDetails = [
    {
      label: 'Name',
      propertyName: 'name',
      custom: (rowData) => {
        return <strong>{rowData.name}</strong>;
      }
    },
    {
      label: 'Link',
      propertyName: 'link',
      isSortable: false
    },
    {
      label: 'File Type',
      propertyName: 'fileType',
      isSortable: false
    },
    {
      label: 'File Size',
      propertyName: 'fileSize'
    },
    {
      label: 'Updated At',
      propertyName: 'updatedAt'
    }
  ];
  const rowDetail = {
    key: '_id'
  };
  const actions = [
    {
      type: 'view',
      handler: (rowData) => {
        console.log(rowData);
      }
    },
    {
      type: 'delete',
      handler: (rowData) => {
        console.log(rowData);
      }
    },
    {
      type: 'draft',
      handler: (rowData) => {
        console.log(rowData);
      }
    }
  ];
  const withNumbering = {
    label: 'No.',
    align: 'center',
    headerAlign: 'center'
  };
  const sorts = [
    {
      label: 'Newest',
      orderBy: 'createdAt',
      order: 'desc'
    },
    {
      label: 'Oldest',
      orderBy: 'createdAt',
      order: 'asc'
    }
  ];
  const filters = [
    // {
    //   type: 'date',
    //   label: 'Creation Date',
    //   paramName: 'createdOn',
    //   yearOnly: false
    // },
    {
      type: 'daterange',
      startLabel: 'Start Year',
      startParamName: 'start',
      endLabel: 'End Year',
      endParamName: 'end',
      yearOnly: false
    },
    {
      type: 'select',
      label: 'File Type',
      paramName: 'type',
      options: [
        { label: 'JPG', value: 'jpg' },
        { label: 'PDF', value: 'pdf' },
        { label: 'PNG', value: 'png' }
      ]
    }
    // {
    //   type: 'multiselect',
    //   label: 'Category',
    //   paramName: 'category',
    //   options: [
    //     { label: 'Technology', value: 'technology' },
    //     { label: 'Economy', value: 'economy' },
    //     { label: 'Renewable Energy', value: 'renewable' },
    //     { label: 'Video Games', value: 'game' },
    //     { label: 'Education', value: 'education' },
    //     { label: 'Finance', value: 'finance' }
    //   ]
    // }
  ];
  const baseQuery = { hello: 'world', abc: 'xyz' };
  const queryTerm = { search: 'keyword' };
  const initialPerPage = 5;

  // sample action to reload property
  // let [initialOrder, setInitialOrder] = useState('asc');

  return (
    <Fragment>
      <Typography component={'h2'}>Demo 1</Typography>
      <TableGeneric
        endpointPath={endpointPath}
        cellDetails={cellDetails}
        rowDetail={rowDetail}
        actions={actions}
        withNumbering={withNumbering}
        sorts={sorts}
        filters={filters}
        baseQuery={baseQuery}
        queryTerm={queryTerm}
        initialPerPage={initialPerPage}
      />
    </Fragment>
  );
}

function TableDemo2() {
  // minimal
  const endpointPath = 'potential-buyer';
  const cellDetails = [
    {
      label: 'Name',
      propertyName: 'name'
    },
    {
      label: 'Link',
      propertyName: 'link',
      isSortable: false
    },
    {
      label: 'File Type',
      propertyName: 'fileType',
      isSortable: false
    },
    {
      label: 'File Size',
      propertyName: 'fileSize'
    },
    {
      label: 'Updated At',
      propertyName: 'updatedAt'
    }
  ];

  return (
    <Fragment>
      <Typography component={'h2'}>Demo 2 - Minimal</Typography>
      <TableGeneric endpointPath={endpointPath} cellDetails={cellDetails} />
    </Fragment>
  );
}

function TableDemo() {
  return (
    <Fragment>
      <TableDemo1 />
      <TableDemo2 />
    </Fragment>
  );
}

export default TableDemo;
