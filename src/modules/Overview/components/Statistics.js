import React from 'react';
import { Grid } from '@mui/material';
import TodayVisitor from './TodayVisitor';
import Vacancy from './Vacancy';
import XcidicTeams from './XcidicTeams';

const Statistics = () => {
  return (
    <Grid container spacing={2}>
      <Grid item sm={12} md={12}>
        <TodayVisitor />
      </Grid>
      <Grid item sm={12} md={12}>
        <Vacancy />
      </Grid>
      <Grid item sm={12} md={12}>
        <XcidicTeams />
      </Grid>
    </Grid>
  );
};

export default Statistics;
