import React from 'react';
import { Box, Grid, Skeleton } from '@mui/material';

const SkeletonLoading = ({ type }) => {
  return (
    <>
      {type == 'portfolio' && (
        <>
          <Grid item sm={12} md={6}>
            <Skeleton variant="rectangle" sx={{ height: '221px', borderRadius: '0.625rem' }} />
          </Grid>
          <Grid item sm={12} md={6}>
            <Skeleton variant="rectangle" sx={{ height: '221px', borderRadius: '0.625rem' }} />
          </Grid>
        </>
      )}

      {type == 'job' && (
        <>
          <Grid item sm={12} md={6}>
            <Skeleton variant="rectangle" sx={{ height: '200px', borderRadius: '0.625rem' }} />
          </Grid>
          <Grid item sm={12} md={6}>
            <Skeleton variant="rectangle" sx={{ height: '200px', borderRadius: '0.625rem' }} />
          </Grid>
          <Grid item sm={12} md={6}>
            <Skeleton variant="rectangle" sx={{ height: '200px', borderRadius: '0.625rem' }} />
          </Grid>
          <Grid item sm={12} md={6}>
            <Skeleton variant="rectangle" sx={{ height: '200px', borderRadius: '0.625rem' }} />
          </Grid>
        </>
      )}

      {type == 'enquiry' && (
        <Box px={1}>
          <Box display="flex" alignItems="center" className="item" py={1.5} width="100%">
            <Skeleton variant="circular" width={40} height={40} />
            <Box display="flex" flexDirection="column" ml={2} width="60%">
              <Skeleton height={16} />
              <Skeleton height={16} />
            </Box>
          </Box>
          <Box display="flex" alignItems="center" className="item" py={1.5} width="100%">
            <Skeleton variant="circular" width={40} height={40} />
            <Box display="flex" flexDirection="column" ml={2} width="60%">
              <Skeleton height={16} />
              <Skeleton height={16} />
            </Box>
          </Box>
          <Box display="flex" alignItems="center" className="item" py={1.5} width="100%">
            <Skeleton variant="circular" width={40} height={40} />
            <Box display="flex" flexDirection="column" ml={2} width="60%">
              <Skeleton height={16} />
              <Skeleton height={16} />
            </Box>
          </Box>
          <Box display="flex" alignItems="center" className="item" py={1.5} width="100%">
            <Skeleton variant="circular" width={40} height={40} />
            <Box display="flex" flexDirection="column" ml={2} width="60%">
              <Skeleton height={16} />
              <Skeleton height={16} />
            </Box>
          </Box>
          <Box display="flex" alignItems="center" className="item" py={1.5} width="100%">
            <Skeleton variant="circular" width={40} height={40} />
            <Box display="flex" flexDirection="column" ml={2} width="60%">
              <Skeleton height={16} />
              <Skeleton height={16} />
            </Box>
          </Box>
        </Box>
      )}

      {type == 'statistic' && (
        <Box display="flex" flexDirection="column">
          <Skeleton height={42} />
          <Skeleton height={16} />
        </Box>
      )}
    </>
  );
};

export default SkeletonLoading;
