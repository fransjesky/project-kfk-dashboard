import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { list } from 'redux/actions';
import { Box, Button, Grid, Typography } from '@mui/material';
import { useLoader, useToggle } from 'utils/hooks';
import CardCareer from 'components/CardCareer';
import CareerPreview from 'modules/Career/components/CareerPreview';
import SkeletonLoading from './SkeletonLoading';
import EmptyState from 'components/EmptyState';

const ActiveJobs = () => {
  const { overviewJobs } = useSelector((state) => state.module.list);
  const { dispatchLoad } = useLoader('career');
  const [isLoading, setIsLoading] = useState(false);
  const [previewValues, setPreviewValues] = useState({});
  const [openPreview, togglePreview] = useToggle(false);
  const dispatch = useDispatch();
  const history = useHistory();
  useEffect(
    () => {
      setIsLoading(true);
      dispatch(
        list({
          name: 'overview/jobs',
          customName: 'overviewJobs',
          onSuccess: () => setIsLoading(false)
        })
      );
    },
    [] // eslint-disable-line
  );
  const jobsData = overviewJobs?.data;

  const handleClickCard = (id) => {
    dispatchLoad(id, (res) => {
      setPreviewValues(res.data);
      togglePreview(true);
    });
  };

  return (
    <Box>
      <Box width="100%" display="flex" justifyContent="space-between" mb={2}>
        <Typography variant="s1" fontWeight="bold">
          Active Jobs
        </Typography>
        <Button variant="primary" onClick={() => history.push('/career')}>
          View All
        </Button>
      </Box>
      <Grid container spacing={3}>
        {isLoading ? (
          <SkeletonLoading type="job" />
        ) : (
          <>
            {jobsData?.map((item) => (
              <Grid item sm={12} md={6} key={`${item.name}-${item.id}`}>
                <CardCareer
                  color={item.Department.color}
                  date={item.createdAt}
                  title={`${item.name} - ${item.jobType}`}
                  views={item.view}
                  isArchived={!(item.status == 'active')}
                  hideActions={true}
                  width="100%"
                  onClick={() => handleClickCard(item.id)}
                />
              </Grid>
            ))}
            {jobsData?.length === 0 && <EmptyState name="Active Jobs" />}
          </>
        )}
      </Grid>
      {openPreview && (
        <CareerPreview
          open={openPreview}
          onClose={() => togglePreview(false)}
          values={previewValues}
        />
      )}
    </Box>
  );
};

export default ActiveJobs;
