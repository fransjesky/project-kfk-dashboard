import { Box, Typography } from '@mui/material';
import React from 'react';
import AstronautWhite from 'assets/svg/AstronautWhite.svg';
import BannerVector from 'assets/svg/BannerVector.svg';
import { getUser } from 'utils/localStorage';

const Banner = () => {
  const { user } = getUser();

  return (
    <Box
      display="flex"
      alignItems="center"
      position="relative"
      height="204px"
      borderRadius={2}
      backgroundColor="tertiary.main"
      boxShadow={'2px 0px 3px rgba(115, 115, 139, 0.1), 0px 4px 20px rgba(115, 115, 139, 0.15)'}
    >
      <Box display="flex" flexDirection="column" ml={6}>
        <Typography variant="s1" fontWeight="bold" color="white.main">
          Hello{' '}
          <Typography variant="s1" fontWeight="bold" color="primary.main">
            {user.user.name}
          </Typography>{' '}
        </Typography>
        <Typography variant="s4" fontWeight="light" color="white.main">
          Welcome back to the Xcidic CMS Dashboard
        </Typography>
      </Box>
      <img
        src={BannerVector}
        alt="Banner Vector"
        style={{ position: 'absolute', left: '0', bottom: '0' }}
      />
      <img
        src={AstronautWhite}
        alt="Banner Vector"
        style={{ position: 'absolute', right: '30px', bottom: '0' }}
      />
    </Box>
  );
};

export default Banner;
