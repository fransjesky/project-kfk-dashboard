import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { load } from 'redux/actions';
import { Work } from 'react-iconly';
import { Avatar, Box, Typography } from '@mui/material';
import { styled } from '@mui/material/styles';
import SkeletonLoading from './SkeletonLoading';

const StatisticCard = styled(Box)({
  backgroundColor: 'white',
  borderRadius: '16px',
  padding: '24px',
  paddingRight: '0px',
  boxShadow: '2px 0px 3px rgba(115, 115, 139, 0.1), 0px 4px 20px rgba(115, 115, 139, 0.15)'
});

const Icon = styled(Avatar)(({ theme }) => ({
  backgroundColor: '#000',
  width: 42,
  height: 42,
  [theme.breakpoints.up('xl')]: {
    width: 56,
    height: 56
  }
}));

const BoldText = styled(Typography)(({ theme }) => ({
  color: theme.palette.grey['130'],
  fontWeight: 700,
  fontSize: '22px',
  [theme.breakpoints.up('xl')]: {
    fontSize: '25px'
  }
}));

const Vacancy = () => {
  const { overviewVacancy } = useSelector((state) => state.module.view);
  const [isLoading, setIsLoading] = useState(false);
  const dispatch = useDispatch();
  useEffect(
    () => {
      setIsLoading(true);
      dispatch(
        load({
          name: 'overview/vacancy',
          customName: 'overviewVacancy',
          onSuccess: () => setIsLoading(false)
        })
      );
    },
    [] // eslint-disable-line
  );
  const vacancyData = overviewVacancy?.data;
  return (
    <StatisticCard>
      <Typography variant="s4" color="grey.80">
        Vacancy
      </Typography>
      <Box display="flex" alignItems="center" mt={1.5}>
        <Icon>
          <Work set="broken" style={{ color: 'white', width: 28, height: 28 }} />
        </Icon>
        <>
          {isLoading ? (
            <Box width="50%" ml={2}>
              <SkeletonLoading type="statistic" />
            </Box>
          ) : (
            <Box display="flex" flexDirection="column" ml={2}>
              <BoldText variant="s1">{vacancyData?.activeJob} Active</BoldText>
              <Typography variant="b3" color="tertiary.main">
                From{' '}
                <Typography variant="b3" color="primary.main" fontWeight="medium">
                  {vacancyData?.allJob}
                </Typography>{' '}
                Jobs
              </Typography>
            </Box>
          )}
        </>
      </Box>
    </StatisticCard>
  );
};

export default Vacancy;
