import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { list } from 'redux/actions';
import { Box, Button, Grid, Typography } from '@mui/material';
import CardPortofolio from 'components/CardOurPortofolio';
import SkeletonLoading from './SkeletonLoading';
import EmptyState from 'components/EmptyState';

const OurPortfolio = () => {
  const { overviewPortfolio } = useSelector((state) => state.module.list);
  const [isLoading, setIsLoading] = useState(false);
  const history = useHistory();
  const dispatch = useDispatch();
  useEffect(
    () => {
      setIsLoading(true);
      dispatch(
        list({
          name: 'overview/portfolio',
          customName: 'overviewPortfolio',
          onSuccess: () => setIsLoading(false)
        })
      );
    },
    [] // eslint-disable-line
  );
  const portfolioData = overviewPortfolio?.data;
  return (
    <Box>
      <Box width="100%" display="flex" justifyContent="space-between" mb={2}>
        <Typography variant="s1" fontWeight="bold">
          Our Portfolio
        </Typography>
        <Button variant="primary" onClick={() => history.push('/works/portfolio')}>
          View All
        </Button>
      </Box>
      <Grid container spacing={3}>
        {isLoading ? (
          <SkeletonLoading type="portfolio" />
        ) : (
          portfolioData?.map((item, idx) => (
            <Grid item sm={12} md={6} key={idx}>
              <Box>
                <CardPortofolio
                  detailLink={item.detailLink}
                  thumbnail={item.thumbnail}
                  name={item.description}
                  projectCompany={item.name}
                  status={item.status}
                  hideActions={true}
                />
              </Box>
            </Grid>
          ))
        )}
        {!isLoading && portfolioData?.length === 0 && <EmptyState name="Portofolio" />}
      </Grid>
    </Box>
  );
};

export default OurPortfolio;
