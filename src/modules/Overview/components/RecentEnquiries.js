import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { list } from 'redux/actions';
import moment from 'moment';
import { Avatar, Box, Button, Divider, Typography } from '@mui/material';
import { styled } from '@mui/material/styles';
import Modal, { ModalContent, ModalHead, ModalButtons } from 'components/Modal';
import SkeletonLoading from './SkeletonLoading';

const EnquiriesContainer = styled(Box)({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  padding: '16px',
  marginTop: '16px',
  borderRadius: '16px',
  backgroundColor: 'white',
  boxShadow: '2px 0px 3px rgba(115, 115, 139, 0.1), 0px 4px 20px rgba(115, 115, 139, 0.15)',
  minHeight: '151px'
});

const EnquiriesItem = styled(Box)(({ theme }) => ({
  cursor: 'pointer',
  '& .item': {
    paddingLeft: '8px',
    paddingRight: '8px'
  },
  '& .item:hover': {
    paddingLeft: '8px',
    paddingRight: '8px',
    backgroundColor: `${theme.palette.primary[10]}`
  }
}));

const InitialIcon = styled(Avatar)({
  width: 40,
  height: 40,
  backgroundColor: 'black'
});

const RecentEnquiries = () => {
  const { overviewEnquiries } = useSelector((state) => state.module.list);
  const [isLoading, setIsLoading] = useState(false);
  const history = useHistory();
  const dispatch = useDispatch();
  useEffect(
    () => {
      setIsLoading(true);
      dispatch(
        list({
          name: 'inquiry',
          customName: 'overviewEnquiries',
          query: 'limit=5',
          onSuccess: () => setIsLoading(false)
        })
      );
    },
    [] // eslint-disable-line
  );
  const [openModal, setOpenModal] = useState(false);
  const [selectedEnquiry, setSelectedEnquiry] = useState(null);
  const handleClick = (val) => {
    setSelectedEnquiry(val);
    setOpenModal(true);
  };

  const enquiryData = overviewEnquiries?.data;
  const isEmptyEnquiry = !enquiryData || enquiryData?.length === 0;
  return (
    <Box>
      <Typography variant="s1" fontWeight="bold">
        Recent Enquiries
      </Typography>
      <EnquiriesContainer>
        {isLoading ? (
          <SkeletonLoading type="enquiry" />
        ) : (
          enquiryData?.map((item, idx) => (
            <EnquiriesItem key={idx} onClick={() => handleClick(item)}>
              <Box display="flex" alignItems="center" className="item" py={1.5}>
                <InitialIcon>{item.name[0]}</InitialIcon>
                <Box display="flex" flexDirection="column" ml={2}>
                  <Typography variant="b1" fontWeight="medium" color="grey.130">
                    {item.name}
                  </Typography>
                  <Typography variant="b3" fontWeight="light" color="grey.100">
                    {moment(item.createdAt).format('DD MMM YYYY')}
                  </Typography>
                </Box>
              </Box>
              {idx == enquiryData?.length - 1 && <Divider />}
            </EnquiriesItem>
          ))
        )}
        {!isEmptyEnquiry && (
          <Button variant="link" sx={{ mt: 1 }} onClick={() => history.push('/enquiries')}>
            View All
          </Button>
        )}
        {isEmptyEnquiry && (
          <Typography color="grey.60" textAlign="center">
            We don't have enquiries yet!
          </Typography>
        )}
      </EnquiriesContainer>
      <Modal isOpen={openModal} onClose={() => setOpenModal(false)}>
        <ModalHead title="Detail Enquiry" />
        <ModalContent>
          {selectedEnquiry && (
            <Box display="flex" flexDirection="column" sx={{ width: '500px' }}>
              <Box display="flex" flexDirection="column" my={1}>
                <Typography variant="b1" fontWeight="bold" color="grey.70">
                  Name
                </Typography>
                <Typography variant="s4" color="grey.110">
                  {selectedEnquiry.name}
                </Typography>
              </Box>
              <Box display="flex" flexDirection="column" my={1}>
                <Typography variant="b1" fontWeight="bold" color="grey.70">
                  Email Address
                </Typography>
                <Typography variant="s4" color="grey.110">
                  {selectedEnquiry.email}
                </Typography>
              </Box>
              <Box display="flex" flexDirection="column" my={1}>
                <Typography variant="b1" fontWeight="bold" color="grey.70">
                  Subject
                </Typography>
                <Typography variant="s4" color="grey.110">
                  {selectedEnquiry.subject}
                </Typography>
              </Box>
              <Box display="flex" flexDirection="column" my={1}>
                <Typography variant="b1" fontWeight="bold" color="grey.70">
                  Message
                </Typography>
                <Typography variant="s4" color="grey.110">
                  {selectedEnquiry.message}
                </Typography>
              </Box>
            </Box>
          )}
        </ModalContent>
        <ModalButtons>
          <Button variant="primary" onClick={() => setOpenModal(false)}>
            Done
          </Button>
        </ModalButtons>
      </Modal>
    </Box>
  );
};

export default RecentEnquiries;
