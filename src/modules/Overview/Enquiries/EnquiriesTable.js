import React, { useState } from 'react';
import moment from 'moment';
import { Box, Button, Typography } from '@mui/material';
import Table from 'components/Table';
import Modal, { ModalContent, ModalHead, ModalButtons } from 'components/Modal';
import EmptyState from 'components/EmptyState';

const HEADS = ['Name', 'Email', 'Subject', 'Date Added'];

const CreateRow = (arr) => {
  let data = [];
  for (let i = 0; i < arr?.length; i++) {
    data.push([arr[i].name, arr[i].email, arr[i].subject, arr[i].createdAt, arr[i].message]);
  }
  return data;
};

const EnquiriesTable = ({ data, loading }) => {
  const [openModal, setOpenModal] = useState(false);
  const [enquiry, setEnquiry] = useState(null);
  const handleClick = (val) => {
    setEnquiry(val);
    setOpenModal(true);
  };
  return (
    <>
      <Table
        heads={HEADS}
        rows={CreateRow(data)}
        specialColumns={{
          0: (val) => (
            <Typography variant="b1" fontWeight="bold">
              {val}
            </Typography>
          ),
          3: (val) => <Typography variant="b1">{moment(val).format('DD/MM/YYYY')}</Typography>,
          4: () => null
        }}
        onClickColumn={handleClick}
      />
      {!loading && !data?.length && <EmptyState name="Enquiries" />}
      <Modal isOpen={openModal} onClose={() => setOpenModal(false)}>
        <ModalHead title="Detail Enquiry" />
        <ModalContent>
          {enquiry && (
            <Box display="flex" flexDirection="column" sx={{ width: '500px' }}>
              <Box display="flex" flexDirection="column" my={1}>
                <Typography variant="b1" fontWeight="bold" color="grey.70">
                  Name
                </Typography>
                <Typography variant="s4" color="grey.110">
                  {enquiry[0]}
                </Typography>
              </Box>
              <Box display="flex" flexDirection="column" my={1}>
                <Typography variant="b1" fontWeight="bold" color="grey.70">
                  Email Address
                </Typography>
                <Typography variant="s4" color="grey.110">
                  {enquiry[1]}
                </Typography>
              </Box>
              <Box display="flex" flexDirection="column" my={1}>
                <Typography variant="b1" fontWeight="bold" color="grey.70">
                  Subject
                </Typography>
                <Typography variant="s4" color="grey.110">
                  {enquiry[2]}
                </Typography>
              </Box>
              <Box display="flex" flexDirection="column" my={1}>
                <Typography variant="b1" fontWeight="bold" color="grey.70">
                  Message
                </Typography>
                <Typography variant="s4" color="grey.110">
                  {enquiry[4]}
                </Typography>
              </Box>
            </Box>
          )}
        </ModalContent>
        <ModalButtons>
          <Button variant="primary" onClick={() => setOpenModal(false)}>
            Done
          </Button>
        </ModalButtons>
      </Modal>
    </>
  );
};

export default EnquiriesTable;
