import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { list } from 'redux/actions';
import { Box } from '@mui/material';
import Header from 'components/Header';
import Page from 'components/Page';
import Pagination from 'components/Pagination';
import EnquiriesTable from './EnquiriesTable';
import queryString from 'utils/queryString';
import { useSearch } from 'utils/hooks';
import BodyLayout from 'layouts/dashboard/BodyLayout';

const Enquiries = () => {
  const PAGE_ROUTE = [
    { label: 'Overview', link: '/' },
    { label: 'List of Enquiries', link: '/enquiries' }
  ];
  const { keywordInquiry, keywordGlobal } = useSelector((state) => state.module.keyword);
  const [searchValue, setSearchValue] = useState(keywordGlobal || '');
  const [page, setPage] = useState(1);
  const { enquiries } = useSelector((state) => state.module.list);
  const dispatch = useDispatch();
  const { dispatchSearch, dispatchSearchGlobal } = useSearch('keywordInquiry');
  const [loading, setLoading] = useState(true);

  const queryObject = {
    keyword: searchValue,
    limit: 10,
    offset: (page - 1) * 10
  };
  const query = queryString(queryObject);

  useEffect(() => {
    dispatchSearch(keywordGlobal || '');
    return () => {
      dispatchSearchGlobal('');
    };
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    setSearchValue(keywordInquiry);
  }, [keywordInquiry]);

  useEffect(
    () => {
      setLoading(true);
      dispatch(
        list({
          name: 'inquiry',
          customName: 'enquiries',
          query,
          onSuccess: () => setLoading(false)
        })
      );
    },
    [searchValue, page] // eslint-disable-line
  );

  const enquiryData = enquiries?.data;
  const enquiryCount = enquiries?.count;
  const handlePageChange = (event, value) => {
    setPage(value);
  };

  const handleSearch = (val) => {
    setPage(1);
    dispatchSearch(val);
  };

  return (
    <Page title="List of Enquiries">
      <BodyLayout padded={false}>
        <Header
          routes={PAGE_ROUTE}
          withSearchBar
          keyword={searchValue}
          onSearch={handleSearch}
          name="keywordInquiries"
        />
        <Box minHeight={342}>
          <EnquiriesTable data={enquiryData} loading={loading} />
        </Box>
        {enquiryData?.length && (
          <Box my={4} display="flex" justifyContent="center">
            <Pagination
              totalPage={Math.ceil(enquiryCount / 10) || 1}
              currentPage={page}
              onChange={handlePageChange}
            />
          </Box>
        )}
      </BodyLayout>
    </Page>
  );
};

export default Enquiries;
