import React from 'react';
import { Box, Grid } from '@mui/material';
import Page from 'components/Page';
import ActiveJobs from './components/ActiveJobs';
import Banner from './components/Banner';
import OurPortfolio from './components/OurPortfolio';
import RecentEnquiries from './components/RecentEnquiries';
import Statistics from './components/Statistics';
import BodyLayout from 'layouts/dashboard/BodyLayout';

const Overview = () => {
  return (
    <Page title="Overview">
      <BodyLayout padded={false}>
        <Grid container spacing={3}>
          <Grid item sm={12} md={9}>
            <Banner />
            <Box mt={5}>
              <OurPortfolio />
            </Box>
            <Box mt={5}>
              <ActiveJobs />
            </Box>
          </Grid>
          <Grid item sm={12} md={3}>
            <Statistics />
            <Box mt={4}>
              <RecentEnquiries />
            </Box>
          </Grid>
        </Grid>
      </BodyLayout>
    </Page>
  );
};

export default Overview;
