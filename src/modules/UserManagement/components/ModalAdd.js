import React, { useState, useEffect } from 'react';
import { connect, useSelector, useDispatch } from 'react-redux';
import { Box, TextField, Button, MenuItem, InputAdornment, IconButton } from '@mui/material';
import { VisibilityOutlined, VisibilityOffOutlined } from '@mui/icons-material';
import Modal, { ModalContent, ModalButtons, ModalHead } from 'components/Modal';
import * as Yup from 'yup';
import withFormik from 'utils/withFormik';
import R from 'utils/regex';
import { add, list } from 'redux/actions';

// schema validation
const schemeValidations = Yup.object().shape({
  name: Yup.string().trim().required('Field cannot be empty'),
  email: Yup.string().trim().email('Invalid email').required('Field cannot be empty'),
  password: Yup.string()
    .trim()
    .matches(
      R.passwordValidation,
      'A password contains at least eight characters, including at least one number and includes both lower and uppercase letters.'
    )
    .required('Field cannot be empty'),
  confirmPassword: Yup.string()
    .oneOf([Yup.ref('password'), null], 'Passwords do not match')
    .trim()
    .required('Field cannot be empty'),
  role: Yup.number().required('Please select an option').nullable()
});

// initial values
const initialValueForm = {
  name: '',
  email: '',
  password: '',
  confirmPassword: '',
  role: null
};

const handleSubmitForm = (payload, ctx) => {
  const { name, email, password, confirmPassword, role } = payload;

  ctx.props.add({
    name: 'user',
    data: {
      name: name.trim(),
      email: email.trim(),
      password,
      confirmPassword,
      RoleId: role
    },
    customRedirect: 'users/user',
    onSuccess: () => {
      ctx.resetForm();
      ctx.props.onSuccess();
    },
    onError: (msg) => {
      for (const key in msg) {
        ctx.setFieldError(key, msg[key]);
      }
    }
  });
};

function ModalAdd(props) {
  const { open, onClose } = props;
  const dispatch = useDispatch();
  const {
    values,
    touched,
    errors,
    handleChange,
    handleBlur,
    handleSubmit,
    resetForm,
    dirty,
    isValid
  } = props;
  const { listRole } = useSelector((state) => state.module.list);
  const [showPass, setShowPass] = useState(false);
  const [showConfPass, setShowConfPass] = useState(false);
  const error = (val) => Boolean(touched[val] && errors[val]);

  useEffect(() => {
    dispatch(
      list({
        name: 'user/role',
        customName: 'listRole'
      })
    );
  }, [open, dispatch]);

  return (
    <Modal
      isOpen={open}
      onClose={() => {
        resetForm();
        onClose();
      }}
      maxWidth="646px"
    >
      <ModalHead title="Add New User" />
      <ModalContent>
        <Box display="flex" flexDirection="column" sx={{ mt: 3, width: '646px' }}>
          <TextField
            name="name"
            label="Full Name"
            placeholder="Full Name"
            fullWidth
            value={values.name}
            onChange={handleChange}
            onBlur={handleBlur}
            error={error('name')}
            helperText={touched.name && errors.name}
            InputLabelProps={error('name') ? { shrink: true } : {}}
          />
          <TextField
            label="Email Address"
            placeholder="Email Address"
            name="email"
            fullWidth
            value={values.email}
            onChange={handleChange}
            onBlur={handleBlur}
            error={error('email')}
            helperText={touched.email && errors.email}
            InputLabelProps={error('email') ? { shrink: true } : {}}
          />
          <TextField
            label="Password"
            placeholder="Password"
            type={!showPass ? 'password' : 'text'}
            fullWidth
            name="password"
            value={values.password}
            onChange={handleChange}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end" sx={{ mb: 1 }}>
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={() => setShowPass((prev) => !prev)}
                    edge="end"
                  >
                    {showPass ? <VisibilityOffOutlined /> : <VisibilityOutlined />}
                  </IconButton>
                </InputAdornment>
              )
            }}
            onBlur={handleBlur}
            error={error('password')}
            helperText={
              error('password')
                ? touched.password && errors.password
                : 'A password contains at least eight characters, including at least one number and includes both lower and uppercase letters.'
            }
            InputLabelProps={error('password') ? { shrink: true } : {}}
          />
          <TextField
            label="Confirm Password"
            placeholder="Confirm Password"
            type={!showConfPass ? 'password' : 'text'}
            fullWidth
            name="confirmPassword"
            value={values.confirmPassword}
            onChange={handleChange}
            onBlur={handleBlur}
            error={error('confirmPassword')}
            helperText={touched.confirmPassword && errors.confirmPassword}
            InputLabelProps={error('confirmPassword') ? { shrink: true } : {}}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end" sx={{ mb: 1 }}>
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={() => setShowConfPass((prev) => !prev)}
                    edge="end"
                  >
                    {showConfPass ? <VisibilityOffOutlined /> : <VisibilityOutlined />}
                  </IconButton>
                </InputAdornment>
              )
            }}
          />
          <TextField
            label="Select Role"
            placeholder="Select Role"
            fullWidth
            name="role"
            select
            value={values.role}
            onChange={handleChange}
            onBlur={handleBlur}
            error={error('role')}
            helperText={touched.role && errors.role}
            InputLabelProps={error('role') ? { shrink: true } : {}}
          >
            {listRole?.data.map((role) => (
              <MenuItem key={role.id} value={role.id}>
                {role.name}
              </MenuItem>
            ))}
          </TextField>
        </Box>
        <ModalButtons>
          <Button
            disabled={!(dirty && isValid)}
            variant="contained"
            sx={{ color: '#fff' }}
            onClick={() => {
              handleSubmit();
            }}
          >
            Save
          </Button>
        </ModalButtons>
      </ModalContent>
    </Modal>
  );
}

const FormikCreate = withFormik(ModalAdd, schemeValidations, initialValueForm, handleSubmitForm);

export default connect(null, { add })(FormikCreate);
