import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Grid, Box, Avatar, Typography } from '@mui/material';
import Header from 'components/Header';
import { list, remove, load } from 'redux/actions';
import { useConfirm, useSearch } from 'utils/hooks';
import { Person } from '@mui/icons-material';
import Table from 'components/Table';
import moment from 'moment';
import Pagination from 'components/Pagination';
import ModalAdd from './components/ModalAdd';
import ModalEdit from './components/ModalEdit';
import Page from 'components/Page';
import BodyLayout from 'layouts/dashboard/BodyLayout';

const CustomName = ({ value }) => (
  <Box sx={{ display: 'flex', alignItems: 'center', maxWidth: '240px' }}>
    <Avatar sx={{ width: 32, height: 32, backgroundColor: '#8593A6' }} src={value?.image}>
      <Person fontSize="small" />
    </Avatar>
    <Typography variant="b4" fontWeight="medium" ml={3}>
      {value.name}
    </Typography>
  </Box>
);

const Heads = ['Account Name', 'Email', 'Roles', 'Date Added', ''];

const CreateRow = (arr) => {
  let data = [];
  for (let i = 0; i < arr?.length; i++) {
    data.push([
      { name: arr[i]?.name, image: arr[i]?.photo, id: arr[i]?.id },
      arr[i]?.email,
      arr[i]?.Role?.name,
      moment(arr[i]?.createdAt).format('DD/MM/YYYY')
    ]);
  }
  return data;
};

function UserManagement() {
  const dispatch = useDispatch();
  const { users } = useSelector((state) => state.module.list);
  const { keywordUser, keywordGlobal } = useSelector((state) => state.module.keyword);
  const { dispatchSearch, dispatchSearchGlobal } = useSearch('keywordUser');
  const [open, setOpen] = useState(false);
  const [openEdit, setOpenEdit] = useState(false);
  const [changed, setChanged] = useState(false);
  const [page, setPage] = useState(1);
  const [idEdit, setIdEdit] = useState(null);
  const { confirmInfo } = useConfirm();
  const [keyword, setKeyword] = useState('');
  const BREADCRUMBS = [
    { label: 'Overview', link: '/' },
    { label: 'User Management', link: '/users/user' }
  ];

  useEffect(() => {
    dispatchSearch(keywordGlobal || '');

    return () => {
      dispatchSearchGlobal('');
    };
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    setKeyword(keywordUser);
  }, [keywordUser]);

  const handleEdit = (id) => {
    dispatch(
      load({
        name: 'user',
        id
      })
    );
    setIdEdit(id);
    setOpenEdit(true);
  };

  const handleDelete = (id, name) => {
    confirmInfo({
      title: `Are you sure you want to delete '${name}'?`,
      message: 'You won’t be able to recover this in the future!',
      handler: () => {
        dispatch(
          remove({
            name: 'user',
            noLink: true,
            id,
            onSuccess: () => {
              setPage(1);
              setKeyword('');
              setChanged((prev) => !prev);
            }
          })
        );
      }
    });
    return;
  };

  const handleSearch = (val) => {
    setPage(1);
    dispatchSearch(val);
  };

  const actions = [
    {
      type: 'edit',
      handler: (rowData) => {
        handleEdit(rowData[0].id);
      }
    },
    {
      type: 'delete',
      handler: (rowData) => {
        handleDelete(rowData[0].id, rowData[0].name);
      }
    }
  ];

  useEffect(() => {
    dispatch(
      list({
        name: 'user',
        customName: 'users',
        query: `sort=createdAt&order=DESC&limit=10&offset=${10 * page - 10}&keyword=${keyword}`
      })
    );
  }, [changed, dispatch, keyword, page]);

  return (
    <Page title="User Management">
      <BodyLayout>
        <Box display="flex" alignItems="center" justifyItems="center" flexDirection="column">
          <ModalAdd
            open={open}
            onClose={() => setOpen(false)}
            onSuccess={() => {
              setChanged((prev) => !prev);
              setOpen(false);
            }}
          />
          <ModalEdit
            open={openEdit}
            id={idEdit}
            onClose={() => setOpenEdit(false)}
            onSuccess={() => {
              setChanged((prev) => !prev);
              setOpenEdit(false);
            }}
          />
          <Header
            routes={BREADCRUMBS}
            withButton
            buttonText="Add User"
            onSearch={handleSearch}
            keyword={keyword}
            withSearchBar
            onButtonClick={() => {
              setOpen(true);
            }}
          />
          <Box minHeight={342} minWidth={'100%'}>
            <Grid container>
              <Table
                heads={Heads}
                rows={CreateRow(users?.data)}
                specialColumns={{ 0: (val) => <CustomName value={val} /> }}
                withAction={true}
                actions={actions}
              />
            </Grid>
          </Box>
          <Box my={4} display="flex" justifyContent="center">
            <Pagination
              onChange={(_, val) => setPage(val)}
              totalPage={Math.ceil(users?.count / 10)}
              currentPage={page}
            />
          </Box>
        </Box>
      </BodyLayout>
    </Page>
  );
}

export default UserManagement;
