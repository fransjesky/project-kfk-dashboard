import React from 'react';
import { Box, TextField } from '@mui/material';
import LoadingButton from '@mui/lab/LoadingButton';
import withFormik from 'utils/withFormik';
import * as Yup from 'yup';
import Star from 'components/Star';

// css
import styles from './Login.module.css';
import Logo from 'assets/svg/Logo.svg';
import Astronaut from 'assets/svg/Astronaut.svg';
import Jupiter from 'assets/svg/Jupiter.svg';
import Saturn from 'assets/svg/Saturn.svg';
import OutterOrbit from 'assets/svg/OutterOrbit.svg';
import MiddleOrbit from 'assets/svg/MiddleOrbit.svg';
import InnerOrbit from 'assets/svg/InnerOrbit.svg';

// redux
import { connect, useSelector } from 'react-redux';
import { signIn } from 'redux/actions';

// initial values
const initialValues = {
  email: '',
  password: ''
};

const schemeValidations = Yup.object().shape({
  email: Yup.string().email('Email is invalid').required('Please enter your email address'),
  password: Yup.string()
    .min(8, 'Password should be of minimum 8 characters length')
    .required('Please enter your password')
});

const handleLogin = (payload, ctx) => {
  const { email, password } = payload;

  ctx.props.signIn({
    email,
    password
  });
};

function Login(props) {
  // variable init
  const { loading } = useSelector((state) => state.global);
  const { error } = useSelector((state) => state.auth);
  const { values, touched, errors, handleChange, handleBlur, handleSubmit } = props;
  const errorCheck = (val) => Boolean(touched[val] && errors[val]);

  // star vector setup
  const starIllustrations = [
    {
      size: 'default',
      color: '#C81D23',
      position: {
        top: '30%',
        left: '27.5%'
      }
    },
    {
      size: 'small',
      color: '#C81D23',
      position: {
        top: '60%',
        left: '75%'
      }
    },
    {
      size: 'default',
      color: '#EFB008',
      position: {
        top: '77.5%',
        left: '70%'
      }
    },
    {
      size: 'small',
      color: '#EFB008',
      position: {
        top: '80%',
        left: '85%'
      }
    }
  ];

  return (
    <Box className={styles.login}>
      <Box className={styles.container}>
        <img src={Logo} alt="Xcidic" className={styles.logo} />
        <Box className={styles.head}>
          <h1>Let's Log In!</h1>
          <p>Welcome back, you’re about to log in to Xcidic’s dashboard</p>
        </Box>
        <Box
          component="form"
          onSubmit={handleSubmit}
          onKeyDown={(e) => {
            if (e.keyCode === 13) {
              handleSubmit();
            }
          }}
          noValidate
          autoComplete="off"
          className={styles.loginForm}
        >
          <TextField
            required
            name="email"
            label="Email Address"
            variant="outlined"
            type="email"
            value={values.email}
            onChange={handleChange}
            onBlur={handleBlur}
            error={errorCheck('email') || error}
            helperText={touched.email && errors.email}
          />
          <TextField
            required
            name="password"
            label="Password"
            variant="outlined"
            type="password"
            value={values.password}
            onChange={handleChange}
            onBlur={handleBlur}
            error={errorCheck('password') || error}
            helperText={touched.password && errors.password}
          />
        </Box>
        <LoadingButton
          fullWidth
          type="submit"
          variant="contained"
          loading={loading}
          disabled={false}
          onClick={handleSubmit}
        >
          Log In
        </LoadingButton>
        <img src={Astronaut} alt="Astronaut Illustration" className={styles.astronaut} />
      </Box>
      <div className={styles.cloud} />
      <img src={Jupiter} alt="Jupiter" className={styles.jupiter} />
      <img src={Saturn} alt="Saturn" className={styles.saturn} />
      <img src={OutterOrbit} alt="" className={styles.outterOrbit} />
      <img src={MiddleOrbit} alt="" className={styles.middleOrbit} />
      <img src={InnerOrbit} alt="" className={styles.innerOrbit} />
      {starIllustrations.map((value, index) => {
        return (
          <Star size={value.size} color={value.color} positioning={value.position} key={index} />
        );
      })}
    </Box>
  );
}

const FormikCreate = withFormik(Login, schemeValidations, initialValues, handleLogin);

export default connect(null, { signIn })(FormikCreate);
