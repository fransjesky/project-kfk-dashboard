import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router';
// components
import BodyLayout from 'layouts/dashboard/BodyLayout';
import Page from 'components/Page';
import Header from 'components/Header';
import Form from './components/Form';
// utils
import withFormik from 'utils/withFormik';
import { useLoader } from 'utils/hooks';
// redux
import { connect, useSelector } from 'react-redux';
import { edit } from 'redux/actions';
// schema validations
import SchemaValidations from './components/SchemaValidations';

// initial values
const initialValueForm = {
  name: '',
  masterMenuIds: [],
  aclIds: []
};

const handleSubmitForm = (payload, ctx) => {
  const { id, name, masterMenuIds, aclIds } = payload;
  ctx.props.edit({
    name: 'user-roles',
    id,
    data: {
      name,
      masterMenuIds,
      aclIds
    },
    linkSuccess: 'users/role'
  });
};

const PAGE_TITLE = 'Add Job Vacancy';

const Edit = (formikProps) => {
  const { id } = useParams();
  const { dispatchLoad } = useLoader('user-roles', 'role');
  const { role } = useSelector((state) => state.module.view);
  const [loading, setLoading] = useState(true);
  const ROUTES = [
    { label: 'Overview', link: '/' },
    { label: 'Roles Management', link: '/users/role' },
    { label: 'Edit Role', link: `/users/role/${id}/edit` }
  ];

  useEffect(() => dispatchLoad(id), [id, dispatchLoad]);

  useEffect(() => {
    if (!role) return;
    const { setFieldValue } = formikProps;
    const objectKeys = ['name', 'masterMenuIds', 'aclIds'];
    for (let key of objectKeys) {
      setFieldValue(key, role.data[key]);
    }
    setFieldValue('id', id);
    setLoading(false);
    // eslint-disable-next-line
  }, [role]);

  return (
    <Page title={PAGE_TITLE}>
      <BodyLayout padded>
        <Header withBackButton routes={ROUTES} />
        {!loading && <Form {...formikProps} />}
      </BodyLayout>
    </Page>
  );
};

const FormikEdit = withFormik(Edit, SchemaValidations, initialValueForm, handleSubmitForm);

export default connect(null, { edit })(FormikEdit);
