import React from 'react';
// components
import BodyLayout from 'layouts/dashboard/BodyLayout';
import Page from 'components/Page';
import Header from 'components/Header';
import Form from './components/Form';
// utils
import withFormik from 'utils/withFormik';
// redux
import { connect } from 'react-redux';
import { add } from 'redux/actions';
// schema validations
import SchemaValidations from './components/SchemaValidations';

// initial values
const initialValueForm = {
  name: '',
  masterMenuIds: [],
  aclIds: []
};

const handleSubmitForm = (payload, ctx) => {
  const { name, masterMenuIds, aclIds } = payload;
  ctx.props.add({
    name: 'user-roles',
    data: {
      name,
      masterMenuIds,
      aclIds
    },
    customRedirect: 'users/role'
  });
};

const PAGE_TITLE = 'Add New Role';
const ROUTES = [
  { label: 'Overview', link: '/' },
  { label: 'Roles Management', link: '/users/role' },
  { label: 'Add New Role', link: '/users/role/create' }
];

const Create = (formikProps) => {
  return (
    <Page title={PAGE_TITLE}>
      <BodyLayout padded>
        <Header withBackButton routes={ROUTES} />
        <Form {...formikProps} />
      </BodyLayout>
    </Page>
  );
};

const FormikCreate = withFormik(Create, SchemaValidations, initialValueForm, handleSubmitForm);

export default connect(null, { add })(FormikCreate);
