import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
// material
import { Grid, Box, Avatar, Typography } from '@mui/material';
import { Person } from '@mui/icons-material';
// components
import Header from 'components/Header';
import Table from 'components/Table';
import Pagination from 'components/Pagination';
import Page from 'components/Page';
import BodyLayout from 'layouts/dashboard/BodyLayout';
// redux
import { remove } from 'redux/actions';
// utils
import { useConfirm, useLister, useSearch } from 'utils/hooks';

const CustomName = ({ value }) => (
  <Box sx={{ display: 'flex', alignItems: 'center', maxWidth: '240px' }}>
    <Avatar sx={{ width: 32, height: 32, backgroundColor: '#8593A6' }} src={value?.image}>
      <Person fontSize="small" />
    </Avatar>
    <Typography variant="b4" fontWeight="medium" ml={3}>
      {value.name}
    </Typography>
  </Box>
);

const Permissions = ({ value }) => {
  const acls = value.map((n) => n.name);
  return (
    <Box
      sx={{
        textOverflow: 'ellipsis',
        overflow: 'hidden',
        width: '400px',
        whiteSpace: 'nowrap'
      }}
    >
      {acls.join(', ')}
    </Box>
  );
};

const Heads = ['Role Name', 'Users', 'Permissions', 'Date Added', ''];

const CreateRow = (arr) => {
  let data = [];
  for (let i = 0; i < arr?.length; i++) {
    data.push([
      { name: arr[i]?.name, image: arr[i]?.photo, id: arr[i]?.id },
      arr[i]?.CountUser,
      arr[i]?.Acls,
      moment(arr[i]?.createdAt).format('DD/MM/YYYY')
    ]);
  }
  return data;
};

const DEFAULT_CAREERS_QUERY = {
  sort: 'createdAt',
  order: 'ASC',
  limit: 10,
  offset: 0,
  keyword: '',
  department: ''
};

function UserRoles() {
  const history = useHistory();
  const dispatch = useDispatch();
  const { roles } = useSelector((state) => state.module.list);
  const { keywordRole, keywordGlobal } = useSelector((state) => state.module.keyword);
  const { dispatchList } = useLister('user-roles', 'roles');
  const { dispatchSearch, dispatchSearchGlobal } = useSearch('keywordRole');
  const [changed, setChanged] = useState(false);
  const [query, setQuery] = useState(DEFAULT_CAREERS_QUERY);
  const [page, setPage] = useState(1);
  const { confirmInfo } = useConfirm();

  const BREADCRUMBS = [
    { label: 'Overview', link: '/' },
    { label: 'Roles Management', link: '/users/role' }
  ];

  useEffect(() => {
    dispatchSearch(keywordGlobal || '');

    return () => {
      dispatchSearchGlobal('');
    };
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    setQuery({ ...DEFAULT_CAREERS_QUERY, keyword: keywordRole });
  }, [keywordRole]);

  const handlePagination = (event, value) => {
    setPage(value);
    setQuery((prev) => ({
      ...prev,
      offset: (value - 1) * prev.limit
    }));
  };

  const handleDelete = (data) => {
    // open modal confirm
    confirmInfo({
      title: `Are you sure you want to delete '${data.name}'?`,
      message: 'You won’t be able to recover this in the future!',
      handler: () => {
        dispatch(
          remove({
            name: 'user-roles',
            noLink: true,
            id: data.id,
            onSuccess: () => setChanged((prev) => !prev)
          })
        );
      }
    });
  };

  const handleSearchChange = (value) => {
    setPage(1);
    dispatchSearch(value);
  };

  const actions = [
    {
      type: 'edit',
      handler: (rowData) => {
        history.push(`/users/role/${rowData[0].id}/edit`);
      }
    },
    {
      type: 'delete',
      handler: (rowData) => {
        handleDelete(rowData[0]);
      }
    }
  ];

  useEffect(() => {
    dispatchList(query);
    // eslint-disable-next-line
  }, [changed, query]);

  return (
    <Page title="User Management">
      <BodyLayout>
        <Box display="flex" alignItems="center" justifyItems="center" flexDirection="column">
          <Header
            routes={BREADCRUMBS}
            withButton
            buttonText="Add Role"
            withSearchBar
            keyword={query.keyword}
            onSearch={handleSearchChange}
            onButtonClick={() => {
              history.push('/users/role/create');
            }}
          />
          <Box minHeight={342} minWidth={'100%'}>
            <Grid container>
              <Table
                heads={Heads}
                rows={CreateRow(roles?.data)}
                specialColumns={{
                  0: (val) => <CustomName value={val} />,
                  2: (val) => <Permissions value={val} />
                }}
                withAction={true}
                actions={actions}
              />
            </Grid>
          </Box>
          <Box my={4} display="flex" justifyContent="center">
            <Pagination
              totalPage={Math.ceil((roles?.count ?? 0) / query.limit)}
              currentPage={page}
              onChange={handlePagination}
            />
          </Box>
        </Box>
      </BodyLayout>
    </Page>
  );
}

export default UserRoles;
