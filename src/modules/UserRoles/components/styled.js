import { styled } from '@mui/material/styles';
import { TextField, Typography } from '@mui/material';

export const SectionTitle = (props) => (
  <Typography component="p" variant="s1" fontWeight="bold" {...props} />
);

export const CustomTextField = styled((props) => <TextField {...props} />)({
  maxWidth: '450px'
});

export const SectionGroup = styled('div')({
  marginBottom: 24
});

export const ButtonGroups = styled('div')({
  display: 'flex',
  justifyContent: 'flex-end',
  gap: '16px'
});
