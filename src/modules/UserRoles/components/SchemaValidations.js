import * as Yup from 'yup';

const isContainsNonWhitespace = (str) => {
  if (str && str.replace(/\s/g, '').length > 0) {
    return true;
  } else {
    return false;
  }
};

export default Yup.object().shape({
  name: Yup.string()
    .required('Field cannot be empty')
    .test('Contains any non-whitespace character', 'Field cannot be empty', (str) =>
      isContainsNonWhitespace(str)
    ),
  masterMenuIds: Yup.array(),
  aclIds: Yup.array()
});
