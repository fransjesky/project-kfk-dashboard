import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
// material
import { Box, Stack, Checkbox, Typography } from '@mui/material';
import { styled } from '@mui/material/styles';
// components
import { PrimaryButton } from 'components/Button';
import SelectMultiple from 'components/SelectMultipleItem';
import { SectionGroup, SectionTitle, CustomTextField, ButtonGroups } from './styled';
// utils
import { list } from 'redux/actions';

const getTextFieldProps = (
  { values, handleChange, handleBlur, touched, errors },
  name,
  helperText
) => ({
  name,
  value: values[name],
  onChange: handleChange,
  onBlur: handleBlur,
  error: Boolean(touched[name] && errors[name]),
  helperText: (touched[name] && errors[name]) || helperText
});

const BpIcon = styled('span')(({ theme }) => ({
  borderRadius: '4px',
  width: 15,
  height: 15,
  boxShadow:
    theme.palette.mode === 'dark'
      ? '0 0 0 1px rgb(16 22 26 / 40%)'
      : 'inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)',
  backgroundColor: theme.palette.mode === 'dark' ? '#394b59' : '#f5f8fa',
  backgroundImage:
    theme.palette.mode === 'dark'
      ? 'linear-gradient(180deg,hsla(0,0%,100%,.05),hsla(0,0%,100%,0))'
      : 'linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))',
  '.Mui-focusVisible &': {
    outline: '2px auto rgba(19,124,189,.6)',
    outlineOffset: 2
  },
  'input:hover ~ &': {
    backgroundColor: theme.palette.mode === 'dark' ? '#30404d' : '#ebf1f5'
  },
  'input:disabled ~ &': {
    boxShadow: 'none',
    background: theme.palette.mode === 'dark' ? 'rgba(57,75,89,.5)' : 'rgba(206,217,224,.5)'
  }
}));

const BpCheckedIcon = styled(BpIcon)({
  backgroundColor: '#3DB9FF',
  backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))',
  '&:before': {
    display: 'block',
    width: 15,
    height: 15,
    backgroundImage:
      "url(\"data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16'%3E%3Cpath" +
      " fill-rule='evenodd' clip-rule='evenodd' d='M12 5c-.28 0-.53.11-.71.29L7 9.59l-2.29-2.3a1.003 " +
      "1.003 0 00-1.42 1.42l3 3c.18.18.43.29.71.29s.53-.11.71-.29l5-5A1.003 1.003 0 0012 5z' fill='%23fff'/%3E%3C/svg%3E\")",
    content: '""'
  },
  'input:hover ~ &': {
    backgroundColor: '#1080DD'
  }
});

const Form = (props) => {
  const { setFieldValue, values, handleSubmit, dirty, isValid } = props;

  const dispatch = useDispatch();
  const { acls, masterMenus } = useSelector((state) => state.module.list);

  const dataAcl = acls?.data;
  const dataMasterMenus = masterMenus?.data;

  useEffect(() => {
    dispatch(list({ name: 'user-roles/acl', customName: 'acls' }));
    dispatch(list({ name: 'user-roles/master-menu', customName: 'masterMenus' }));
    // eslint-disable-next-line
  }, []);

  let options = [];
  if (dataMasterMenus?.length) {
    const menus = dataMasterMenus.map((n) => ({
      label: n.title,
      value: n.id
    }));
    options = [...options, ...menus];
  }

  const handleChangeOpt = (n) => {
    const idList = dataAcl[n.module].find((itm) => itm.action === 'list')?.id;
    const idRead = dataAcl[n.module].find((itm) => itm.action === 'read')?.id;
    if (values.aclIds.includes(n.id)) {
      setFieldValue(
        'aclIds',
        values.aclIds.filter((id) => id !== n.id)
      );
    } else {
      if (n.action === 'list' || n.action === 'read') {
        setFieldValue('aclIds', [...values.aclIds, n.id]);
      } else if (n.action === 'create') {
        values.aclIds.includes(idList)
          ? setFieldValue('aclIds', [...values.aclIds, n.id])
          : setFieldValue('aclIds', [...values.aclIds, n.id, idList]);
      } else if (n.action === 'delete') {
        if (!values.aclIds.includes(idList)) {
          setFieldValue('aclIds', [...values.aclIds, n.id, idList]);
        } else {
          setFieldValue('aclIds', [...values.aclIds, n.id]);
        }
      } else {
        if (!values.aclIds.includes(idList) && !values.aclIds.includes(idRead)) {
          setFieldValue('aclIds', [...values.aclIds, n.id, idList, idRead]);
        } else if (!values.aclIds.includes(idList) && values.aclIds.includes(idRead)) {
          setFieldValue('aclIds', [...values.aclIds, n.id, idList]);
        } else if (values.aclIds.includes(idList) && !values.aclIds.includes(idRead)) {
          setFieldValue('aclIds', [...values.aclIds, n.id, idRead]);
        } else {
          setFieldValue('aclIds', [...values.aclIds, n.id]);
        }
      }
    }
  };

  return (
    <Box>
      <SectionGroup>
        <Stack>
          <CustomTextField label="Role Name" {...getTextFieldProps(props, 'name')} />
          <SelectMultiple
            label="Master Menu"
            name="masterMenuIds"
            options={options}
            onChange={(val) => setFieldValue('masterMenuIds', val)}
            {...props}
          />
        </Stack>
      </SectionGroup>
      <SectionGroup>
        <Box sx={{ mb: '16px' }}>
          <SectionTitle>ACL Roles</SectionTitle>
          <Typography>You can choose which menus this role is allowed to change.</Typography>
        </Box>

        {dataAcl &&
          Object.keys(dataAcl).map((acl, i) => (
            <Box sx={{ mb: '16px' }} key={i}>
              <Typography variant="body1" fontWeight="bold">
                {acl}
              </Typography>
              {dataAcl[acl]?.map((n, idx) => (
                <Box
                  key={idx}
                  sx={{ pl: '12px', mb: '-8px' }}
                  display="flex"
                  justifyContent="space-between"
                  alignItems="center"
                >
                  <Typography component="span">{n.name}</Typography>
                  <Checkbox
                    checked={values.aclIds.includes(n.id)}
                    onChange={() => {
                      handleChangeOpt(n);
                    }}
                    color="secondary"
                    icon={<BpIcon />}
                    checkedIcon={<BpCheckedIcon />}
                  />
                </Box>
              ))}
            </Box>
          ))}
      </SectionGroup>

      <ButtonGroups sx={{ mt: '46px' }}>
        <PrimaryButton onClick={handleSubmit} disabled={!(dirty && isValid)}>
          Save
        </PrimaryButton>
      </ButtonGroups>
    </Box>
  );
};

export default Form;
