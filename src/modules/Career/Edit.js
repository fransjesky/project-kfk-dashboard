import React, { useEffect } from 'react';
import { useParams } from 'react-router';
// components
import BodyLayout from 'layouts/dashboard/BodyLayout';
import Page from 'components/Page';
import Header from 'components/Header';
import BlueBox from 'components/BlueBox';
import CareerForm from './components/CareerForm';
// utils
import withFormik from 'utils/withFormik';
import { useLoader } from 'utils/hooks';
// redux
import { connect } from 'react-redux';
import { edit } from 'redux/actions';
// schema validations
import SchemaValidations from './components/SchemaValidations';

// initial values
const initialValueForm = {
  name: '',
  department: null,
  numberVacancy: '1',
  jobType: 'Full-Time',
  jobDescription: '',
  requirementDescription: '',
  experienceYear: '',
  skillNeeded: [],
  additionalSkillNeeded: [],
  jobStreet: '',
  glints: '',
  techinAsia: '',
  linkedin: ''
};

const mapPropsToValues = ({ career }) => {
  const data = career?.data;

  if (data) {
    return {
      id: data.id,
      name: data.name,
      numberVacancy: data.numberVacancy,
      jobType: data.jobType,
      jobDescription: data.jobDescription,
      requirementDescription: data.requirementDescription,
      experienceYear: data.experienceYear,
      skillNeeded: data.skillNeeded,
      additionalSkillNeeded: data.additionalSkillNeeded,
      jobStreet: data.jobStreet,
      glints: data.glints,
      techinAsia: data.techinAsia,
      linkedin: data.linkedin,
      status: data.status,
      department: JSON.stringify({ id: data.Department.id, name: data.Department.name }),
      useJobStreet: Boolean(data.jobStreet),
      useGlints: Boolean(data.glints),
      useTechinAsia: Boolean(data.techinAsia),
      useLinkedin: Boolean(data.linkedin)
    };
  }
  return initialValueForm;
};

const handleSubmitForm = (payload, ctx) => {
  const {
    id,
    useJobStreet,
    useGlints,
    useTechinAsia,
    useLinkedin,
    jobStreet,
    glints,
    techinAsia,
    linkedin,
    department,
    ...others
  } = payload;
  ctx.props.edit({
    name: 'career',
    id,
    data: {
      ...others,
      departmentId: JSON.parse(department).id,
      jobStreet: useJobStreet ? jobStreet : '',
      glints: useGlints ? glints : '',
      techinAsia: useTechinAsia ? techinAsia : '',
      linkedin: useLinkedin ? linkedin : ''
    },
    linkSuccess: 'career'
  });
};

const PAGE_TITLE = 'Edit Job Vacancy';
const routes = (id) => {
  return [
    { label: 'Overview', link: '/' },
    { label: 'Career', link: '/career' },
    { label: 'Edit Job Vacancy', link: `/career/edit/${id}` }
  ];
};

const EditCareer = (formikProps) => {
  const { careerId } = useParams();
  const { dispatchLoad } = useLoader('career');

  useEffect(() => dispatchLoad(careerId), [careerId, dispatchLoad]);

  return (
    <Page title={PAGE_TITLE}>
      <BodyLayout padded>
        <Header withBackButton routes={routes(careerId)} />
        <BlueBox>
          This post will go public, please be careful with information you will share.
        </BlueBox>
        <CareerForm {...formikProps} />
      </BodyLayout>
    </Page>
  );
};

const FormikEditCareer = withFormik(
  EditCareer,
  SchemaValidations,
  mapPropsToValues,
  handleSubmitForm
);

const mapStateToProps = (state) => ({
  career: state.module.view.career
});

export default connect(mapStateToProps, { edit })(FormikEditCareer);
