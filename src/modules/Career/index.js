import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
// components
import { Box } from '@mui/material';
import Page from 'components/Page';
import BodyLayout from 'layouts/dashboard/BodyLayout';
import Header from 'components/Header';
import Pagination from 'components/Pagination';
import AutoGrid from 'components/AutoGrid';
import CardCareer from 'components/CardCareer';
import MenuItem from 'components/MenuItem';
import CareerPreview from './components/CareerPreview';
import { DepartmentsDropdown } from './components/styled';
import EmptyState from 'components/EmptyState';
// redux
import { useSelector } from 'react-redux';
// utils
import {
  useLister,
  useRemover,
  useEditer,
  useConfirm,
  useDepartments,
  useSearch,
  useToggle,
  useLoader
} from 'utils/hooks';

const PAGE_TITLE = 'Career - Jobs Opening';
const ROUTES = [
  { label: 'Overview', link: '/' },
  { label: 'Career', link: '/career' }
];
const DEFAULT_CAREERS_QUERY = {
  sort: 'createdAt',
  order: 'ASC',
  limit: 0,
  offset: 0,
  department: '',
  keyword: ''
};

const Career = () => {
  const [careersQuery, setCareersQuery] = useState({ ...DEFAULT_CAREERS_QUERY });
  const { keywordCareer, keywordGlobal } = useSelector((state) => state.module.keyword);
  const [departmentSearch, setDepartmentSearch] = useState('all');
  const history = useHistory();
  const { dispatchRemove } = useRemover('career', 'career');
  const { dispatchList } = useLister('career', 'careers');
  const { careers } = useSelector((state) => state.module.list);
  const { confirmInfo } = useConfirm();
  const { dispatchSearch, dispatchSearchGlobal } = useSearch('keywordCareer');
  const { departments } = useDepartments();
  const { dispatchEdit } = useEditer('career');
  const { dispatchLoad } = useLoader('career');
  const [openPreview, togglePreview] = useToggle(false);
  const [previewValues, setPreviewValues] = useState({});
  const [loading, setLoading] = useState(true);

  const handleHeaderButtonClick = () => history.push('/career/add');
  const handlePagination = (event, value) => {
    setCareersQuery((prev) => ({
      ...prev,
      offset: (value - 1) * prev.limit
    }));
  };

  useEffect(() => {
    dispatchSearch(keywordGlobal || '');
    return () => {
      dispatchSearchGlobal('');
    };
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    const timeout = setTimeout(() => {
      setCareersQuery((prev) => ({
        ...DEFAULT_CAREERS_QUERY,
        limit: prev.limit,
        keyword: keywordCareer
      }));
    }, 500);

    return () => clearTimeout(timeout);
    // eslint-disable-next-line
  }, [keywordCareer]);

  // load careers
  useEffect(() => {
    if (careersQuery.limit > 0) {
      setLoading(true);
      dispatchList(careersQuery, () => setLoading(false));
    }
    // eslint-disable-next-line
  }, [careersQuery]);

  const handleDeleteCareer = (career) => () => {
    // open modal confirm
    confirmInfo({
      title: `Are you sure you want to delete '${career.name}'?`,
      message: 'You won’t be able to recover this in the future!',
      handler: () => {
        dispatchRemove(career.id, () => dispatchList(careersQuery));
      }
    });
  };

  const handleClickCard = (id) => {
    dispatchLoad(id, (res) => {
      setPreviewValues(res.data);
      togglePreview(true);
    });
  };

  const resetPage = () => setCareersQuery((prev) => ({ ...prev, offset: 0 }));

  // sync pagination and autogrid
  const handleAutoGridChangeWidth = ({ width, cardWidth, gap }) => {
    setCareersQuery((prev) => {
      if (width < cardWidth) return prev;
      const limit = Math.floor((width + gap) / (cardWidth + gap)) * 2;
      if (limit !== prev.limit) {
        return { ...prev, limit };
      }
      return prev;
    });
  };

  const handleSearchChange = (value) => {
    resetPage();
    dispatchSearch(value);
  };

  const handleDepartmentChange = (event) => {
    const value = event.target.value;
    setDepartmentSearch(value);
    resetPage();
    setCareersQuery((prev) => ({
      ...DEFAULT_CAREERS_QUERY,
      limit: prev.limit,
      department: value == 'all' ? '' : value
    }));
  };

  return (
    <Page title={PAGE_TITLE}>
      {openPreview && (
        <CareerPreview
          open={openPreview}
          onClose={() => togglePreview(false)}
          values={previewValues}
        />
      )}
      <BodyLayout>
        <Header
          routes={ROUTES}
          withButton
          keyword={keywordCareer}
          buttonText="Add Job Vacancy"
          onButtonClick={handleHeaderButtonClick}
          withSearchBar
          onSearch={handleSearchChange}
        />
        <Box display="flex" justifyContent="flex-end" marginBottom={3}>
          <DepartmentsDropdown onChange={handleDepartmentChange} select value={departmentSearch}>
            <MenuItem value="all">All</MenuItem>
            {departments?.map((department) => (
              <MenuItem key={department.name} value={department.name}>
                {department.name}
              </MenuItem>
            ))}
          </DepartmentsDropdown>
        </Box>
        <AutoGrid onChangeWidth={handleAutoGridChangeWidth}>
          {careers?.data?.map((career) => (
            <CardCareer
              key={`${career.name}-${career.id}`}
              color={career.Department.color}
              date={career.createdAt}
              title={career.name}
              isArchived={career.status != 'active'}
              views={career.view}
              onEdit={() => history.push(`/career/edit/${career.id}`)}
              onArchive={() =>
                dispatchEdit(
                  career.id,
                  { status: career.status == 'active' ? 'archive' : 'active' },
                  () => dispatchList(careersQuery),
                  'career',
                  `Successfully ${career.status == 'active' ? 'Archived' : 'Showed'}`
                )
              }
              onDelete={handleDeleteCareer(career)}
              onClick={() => handleClickCard(career.id)}
            />
          ))}
        </AutoGrid>
        {!loading && !careers?.data?.length && <EmptyState name="Careers" />}
        {careers?.data?.length && (
          <Box
            sx={{
              mt: 5,
              pb: 3,
              display: 'flex',
              flexGrow: 1,
              justifyContent: 'center',
              alignItems: 'flex-end'
            }}
          >
            <Pagination
              totalPage={Math.ceil((careers?.count ?? 0) / careersQuery.limit)}
              currentPage={Math.floor(careersQuery.offset / careersQuery.limit) + 1}
              onChange={handlePagination}
            />
          </Box>
        )}
      </BodyLayout>
    </Page>
  );
};

export default Career;
