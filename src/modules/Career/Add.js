import React from 'react';
// components
import BodyLayout from 'layouts/dashboard/BodyLayout';
import Page from 'components/Page';
import Header from 'components/Header';
import BlueBox from 'components/BlueBox';
import CareerForm from './components/CareerForm';
// utils
import withFormik from 'utils/withFormik';
// redux
import { connect } from 'react-redux';
import { add } from 'redux/actions';
// schema validations
import SchemaValidations from './components/SchemaValidations';

// initial values
const initialValueForm = {
  name: '',
  department: null,
  numberVacancy: '1',
  jobType: 'Full-Time',
  jobDescription: '',
  requirementDescription: '',
  experienceYear: '',
  skillNeeded: [],
  additionalSkillNeeded: [],
  jobStreet: '',
  glints: '',
  techinAsia: '',
  linkedin: '',
  status: 'active'
};

const handleSubmitForm = (payload, ctx) => {
  console.log('handleSubmitForm', payload);
  const {
    useJobStreet,
    useGlints,
    useTechinAsia,
    useLinkedin,
    jobStreet,
    glints,
    techinAsia,
    linkedin,
    department,
    ...others
  } = payload;
  ctx.props.add({
    name: 'career',
    data: {
      ...others,
      departmentId: JSON.parse(department).id,
      jobStreet: useJobStreet ? jobStreet : '',
      glints: useGlints ? glints : '',
      techinAsia: useTechinAsia ? techinAsia : '',
      linkedin: useLinkedin ? linkedin : ''
    },
    customRedirect: 'career'
  });
};

const PAGE_TITLE = 'Add Job Vacancy';
const ROUTES = [
  { label: 'Overview', link: '/' },
  { label: 'Career', link: '/career' },
  { label: 'Add Job Vacancy', link: '/career/add' }
];

const AddCareer = (formikProps) => {
  return (
    <Page title={PAGE_TITLE}>
      <BodyLayout padded>
        <Header withBackButton routes={ROUTES} />
        <BlueBox>
          This post will go public, please be careful with information you will share.
        </BlueBox>
        <CareerForm {...formikProps} />
      </BodyLayout>
    </Page>
  );
};

const FormikAddCareer = withFormik(
  AddCareer,
  SchemaValidations,
  initialValueForm,
  handleSubmitForm
);

export default connect(null, { add })(FormikAddCareer);
