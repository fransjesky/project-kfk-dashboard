import React from 'react';
import htmr from 'htmr';
import ModalPreview from 'components/ModalPreview';
import { styled } from '@mui/material/styles';
import { Box, Typography, Stack, Grid, ButtonBase } from '@mui/material';
import { PreviewTitle, PreviewSubtitle, SectionGroup, LinkedInButton, PreviewChip } from './styled';
import BtnTechinAsia from 'assets/svg/BtnTechinAsia.png';
import BtnJobstreet from 'assets/svg/BtnJobStreet.png';
import BtnGlints from 'assets/svg/BtnGlints.png';

const FlexWrap = styled(Box)({
  display: 'flex',
  flexWrap: 'wrap',
  gap: '8px',
  marginTop: '16px'
});

const CareerPreview = ({ open, onClose, values }) => {
  return (
    <ModalPreview open={open} onClose={onClose} title="Preview post">
      <Box sx={{ py: 5, px: 8 }}>
        <PreviewTitle>{values.name}</PreviewTitle>
        <Box display="flex" sx={{ mt: 7 }}>
          <Grid container spacing={1}>
            <Grid item xs={4}>
              <Stack spacing={4}>
                <Box display="flex">
                  <Stack>
                    <PreviewSubtitle>Job Categories</PreviewSubtitle>
                    <PreviewSubtitle>Job Type</PreviewSubtitle>
                    <PreviewSubtitle>Experience</PreviewSubtitle>
                  </Stack>
                  <Stack>
                    <Typography variant="s4" sx={{ textTransform: 'capitalize' }}>
                      {values.Department?.name}
                    </Typography>
                    <Typography variant="s4">{values.jobType}</Typography>
                    <Typography variant="s4">{values.experienceYear} years</Typography>
                  </Stack>
                </Box>
                <Box>
                  <PreviewSubtitle>Skills Needed</PreviewSubtitle>
                  <FlexWrap>
                    {values.skillNeeded.map((skill) => (
                      <PreviewChip key={skill} label={skill} />
                    ))}
                  </FlexWrap>
                </Box>
                <Box>
                  <PreviewSubtitle>More Skills</PreviewSubtitle>
                  <FlexWrap>
                    {values.additionalSkillNeeded.map((skill) => (
                      <PreviewChip key={skill} label={skill} />
                    ))}
                  </FlexWrap>
                </Box>
              </Stack>
            </Grid>
            <Grid item xs={8}>
              <SectionGroup>
                <Typography variant="h5" component="p" fontWeight="bold">
                  Description
                </Typography>
                <Typography
                  variant="s4"
                  component="p"
                  sx={{ wordWrap: 'break-word', ul: { marginLeft: '18px' } }}
                >
                  {htmr(values.jobDescription)}
                </Typography>
              </SectionGroup>
              <SectionGroup>
                <Typography variant="s2" component="p" fontWeight="bold">
                  Job Requirement
                </Typography>
                <Typography
                  variant="s4"
                  component="p"
                  sx={{ wordWrap: 'break-word', ul: { marginLeft: '18px' } }}
                >
                  {htmr(values.requirementDescription)}
                </Typography>
              </SectionGroup>
              <Box sx={{ pt: 4 }}>
                {values.useLinkedin && (
                  <>
                    <LinkedInButton sx={{ mb: 3 }}>
                      <a target="__blank" href={values.linkedin}>
                        Apply Via Linkedin
                      </a>
                    </LinkedInButton>
                    <Typography component="p" variant="s4" fontWeight="bold">
                      or apply with
                    </Typography>
                  </>
                )}
                <Stack direction="row" spacing={1}>
                  {values.useGlints && (
                    <ButtonBase disableRipple>
                      <a target="__blank" href={values.glints}>
                        <img src={BtnGlints} alt="" />
                      </a>
                    </ButtonBase>
                  )}
                  {values.useJobStreet && (
                    <ButtonBase disableRipple>
                      <a target="__blank" href={values.jobStreet}>
                        <img src={BtnJobstreet} alt="" />
                      </a>
                    </ButtonBase>
                  )}
                  {values.useTechinAsia && (
                    <ButtonBase disableRipple sx={{ transform: 'translateY(2.5px)' }}>
                      <a target="__blank" href={values.techinAsia}>
                        <img src={BtnTechinAsia} alt="" />
                      </a>
                    </ButtonBase>
                  )}
                </Stack>
              </Box>
            </Grid>
          </Grid>
        </Box>
      </Box>
    </ModalPreview>
  );
};

export default CareerPreview;
