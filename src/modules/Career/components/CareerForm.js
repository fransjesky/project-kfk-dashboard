import React from 'react';
import {
  Box,
  TextField,
  Stack,
  FormControl,
  FormLabel,
  FormControlLabel,
  RadioGroup,
  Radio,
  Checkbox,
  Typography
} from '@mui/material';
import MenuItem from 'components/MenuItem';
import TextEditor from 'components/TextEditor';
import MultiSelect from 'components/MultiSelect';
import { PrimaryButton, SecondaryButton } from 'components/Button';
import { SectionGroup, SectionTitle, CustomTextField, ButtonGroups } from './styled';
import CareerPreview from './CareerPreview';
// utils
import { useDepartments, useCareerSkills } from 'utils/hooks';
import { useToggle } from 'utils/hooks';

const getTextFieldProps = (
  { values, handleChange, handleBlur, touched, errors },
  name,
  helperText
) => ({
  name,
  value: values[name],
  onChange: handleChange,
  onBlur: handleBlur,
  error: Boolean(touched[name] && errors[name]),
  helperText: (touched[name] && errors[name]) || helperText
});

const CareerForm = (props) => {
  const { departments } = useDepartments();
  const { mainSkills, additionalSkills } = useCareerSkills({ limit: null });
  const [openPreview, togglePreview] = useToggle(false);
  const {
    setFieldValue,
    values,
    touched,
    errors,
    handleBlur,
    handleSubmit,
    handleChange,
    dirty,
    isValid
  } = props;

  const handleChangeSkillsNeeded = (value) => {
    setFieldValue('skillNeeded', value);
  };

  const handleChangeAdditionalSkillsNeeded = (value) => {
    setFieldValue('additionalSkillNeeded', value);
  };

  const atleatOneJobPortal = Boolean(
    values.useLinkedin || values.useTechinAsia || values.useGlints || values.useJobStreet
  );
  const submitable = dirty && isValid && atleatOneJobPortal;
  return (
    <Box>
      <CareerPreview open={openPreview} onClose={() => togglePreview(false)} values={values} />
      <SectionGroup>
        <SectionTitle>About the Job</SectionTitle>
        <Stack>
          <CustomTextField
            label="Job Name"
            {...getTextFieldProps(props, 'name', 'Please input the title of the job position')}
          />
          <CustomTextField
            label="Department"
            select
            {...getTextFieldProps(
              props,
              'department',
              'Please select a departement or add another departement'
            )}
          >
            {departments?.map((department) => (
              <MenuItem key={department.name} value={JSON.stringify(department)}>
                {department.name}
              </MenuItem>
            ))}
          </CustomTextField>
          <CustomTextField
            label="Vacancy needed"
            select
            {...getTextFieldProps(props, 'numberVacancy')}
          >
            <MenuItem value="1">1</MenuItem>
            <MenuItem value="2">2</MenuItem>
            <MenuItem value="3">3</MenuItem>
            <MenuItem value="4">4</MenuItem>
            <MenuItem value="6">5</MenuItem>
          </CustomTextField>
          <FormControl>
            <FormLabel>Job Type</FormLabel>
            <RadioGroup row name="jobType" value={values.jobType} onChange={handleChange}>
              <FormControlLabel value="Full-Time" control={<Radio />} label="Full-Time" />
              <FormControlLabel value="Part-Time" control={<Radio />} label="Part-Time" />
              <FormControlLabel value="Internship" control={<Radio />} label="Internship" />
              <FormControlLabel value="Contract" control={<Radio />} label="Contract" />
            </RadioGroup>
          </FormControl>
          <TextEditor
            label="Job Description"
            value={values.jobDescription}
            onChange={(val) => setFieldValue('jobDescription', val === '<p><br></p>' ? '' : val)}
            onBlur={() => handleBlur({ target: { name: 'jobDescription' } })}
            error={Boolean(touched.jobDescription && errors.jobDescription)}
            helperText={
              (touched.jobDescription && errors.jobDescription) ||
              'Please tell us about this job and input a minimum of 400 characters'
            }
          />
        </Stack>
      </SectionGroup>
      <SectionGroup>
        <SectionTitle>Job Requirement</SectionTitle>
        <TextEditor
          label="Job Requirement Description"
          value={values.requirementDescription}
          onChange={(val) =>
            setFieldValue('requirementDescription', val === '<p><br></p>' ? '' : val)
          }
          onBlur={() => handleBlur({ target: { name: 'requirementDescription' } })}
          error={Boolean(touched.requirementDescription && errors.requirementDescription)}
          helperText={
            (touched.requirementDescription && errors.requirementDescription) ||
            'Please tell us the job requirements and input  a minimum of 400 characters'
          }
        />
        <TextField
          label="Experience Year (e.g. 0-1)"
          {...getTextFieldProps(props, 'experienceYear')}
          sx={{ maxWidth: '213px', mt: 2 }}
        />
        <MultiSelect
          name="skillNeeded"
          withCustomValue
          label="Skill Needed"
          chipcolor="primary"
          value={values.skillNeeded}
          onBlur={handleBlur}
          error={Boolean(touched.skillNeeded && errors.skillNeeded)}
          onChange={handleChangeSkillsNeeded}
          helperText={touched.skillNeeded && errors.skillNeeded}
          options={mainSkills?.map((skill) => skill.name)}
        />
        <MultiSelect
          name="additionalSkillNeeded"
          withCustomValue
          label="Additional Skill Needed"
          chipcolor="secondary"
          value={values.additionalSkillNeeded}
          onBlur={handleBlur}
          error={Boolean(touched.additionalSkillNeeded && errors.additionalSkillNeeded)}
          onChange={handleChangeAdditionalSkillsNeeded}
          helperText={
            (touched.additionalSkillNeeded && errors.additionalSkillNeeded) ||
            'Please input Additional skill for this job ( ex: Public Speaking, Team work, Communication etc)'
          }
          options={additionalSkills?.map((skill) => skill.name)}
        />
      </SectionGroup>
      <SectionGroup>
        <SectionTitle>Job Portal</SectionTitle>
        <Box display="flex" flexDirection="column">
          <FormControlLabel
            control={
              <Checkbox
                checked={values.useJobStreet}
                onChange={() => setFieldValue('useJobStreet', !values.useJobStreet)}
              />
            }
            label="JobStreet"
          />
          {values.useJobStreet && (
            <CustomTextField
              label="JobStreet URL Link"
              {...getTextFieldProps(props, 'jobStreet')}
            />
          )}
        </Box>
        <Box display="flex" flexDirection="column">
          <FormControlLabel
            control={
              <Checkbox
                checked={values.useGlints}
                onChange={() => setFieldValue('useGlints', !values.useGlints)}
              />
            }
            label="Glints"
          />
          {values.useGlints && (
            <CustomTextField label="Glints URL Link" {...getTextFieldProps(props, 'glints')} />
          )}
        </Box>
        <Box display="flex" flexDirection="column">
          <FormControlLabel
            control={
              <Checkbox
                checked={values.useTechinAsia}
                onChange={() => setFieldValue('useTechinAsia', !values.useTechinAsia)}
              />
            }
            label="TechinAsia"
          />
          {values.useTechinAsia && (
            <CustomTextField
              label="TechinAsia URL Link"
              {...getTextFieldProps(props, 'techinAsia')}
            />
          )}
        </Box>
        <Box display="flex" flexDirection="column">
          <FormControlLabel
            control={
              <Checkbox
                checked={values.useLinkedin}
                onChange={() => setFieldValue('useLinkedin', !values.useLinkedin)}
              />
            }
            label="Linkedin"
          />
          {values.useLinkedin && (
            <CustomTextField label="Linkedin URL Link" {...getTextFieldProps(props, 'linkedin')} />
          )}
        </Box>
        {!(
          values.useLinkedin ||
          values.useTechinAsia ||
          values.useGlints ||
          values.useJobStreet
        ) && (
          <Box mt={4}>
            <Typography color="error">
              This field cannot be empty. Please choose atleast one job portal.
            </Typography>
          </Box>
        )}
      </SectionGroup>

      <ButtonGroups>
        <SecondaryButton onClick={() => togglePreview(true)}>Preview</SecondaryButton>
        <PrimaryButton onClick={handleSubmit} disabled={!submitable}>
          Save
        </PrimaryButton>
      </ButtonGroups>
    </Box>
  );
};

export default CareerForm;
