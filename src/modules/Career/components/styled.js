import { styled } from '@mui/material/styles';
import { TextField, Typography, Button, Chip } from '@mui/material';

export const DepartmentsDropdown = styled((props) => <TextField {...props} />)({
  maxWidth: '257px',
  '& .MuiOutlinedInput-root': {
    padding: 0
  }
});

export const SectionTitle = (props) => (
  <Typography component="p" variant="s1" fontWeight="bold" {...props} />
);

export const CustomTextField = styled((props) => <TextField {...props} />)({
  maxWidth: '450px'
});

export const SectionGroup = styled('div')({
  marginBottom: 24
});

export const ButtonGroups = styled('div')({
  display: 'flex',
  justifyContent: 'flex-end',
  gap: '16px'
});

export const PreviewTitle = styled((props) => (
  <Typography variant="h5" fontWeight="medium" {...props} />
))({
  background: 'linear-gradient(97.35deg, #FF9D42 -4.27%, #E53945 117.55%)',
  'background-clip': 'text',
  'text-fill-color': 'transparent'
});

export const PreviewSubtitle = (props) => (
  <Typography variant="s4" fontWeight="bold" marginRight={2} {...props} />
);

export const LinkedInButton = styled(Button)({
  background: 'linear-gradient(97.35deg, #FF9D42 -4.27%, #E53945 117.55%)',
  width: '228px',
  color: '#fff',
  height: '56px',
  a: {
    textDecoration: 'none',
    color: 'inherit'
  }
});

export const PreviewChip = styled(Chip)({
  backgroundColor: '#E2E8F0',
  borderRadius: '12px'
});
