import * as Yup from 'yup';

const validateTextEditor = () => {
  return Yup.string()
    .trim()
    .required('Field cannot be empty')
    .matches(/<[a-zA-Z]+>[\s]*[\S]+[\s\S]*<\/[a-zA-Z]+>/g, 'Field cannot be empty')
    .test(
      'is-minimum',
      'Please input a minimum of 400 characters',
      (value) => value?.replace(/<[^>]*>/g, '').length >= 400
    );
};

const validateUrl = (name, domains) => {
  return Yup.string()
    .required('Field cannot be empty')
    .matches(
      new RegExp(
        `^((http|https):\/\/)?([a-z]+\.)?(${name})(\.${domains}){1}(\/[-_+@#$%&=?.,*^{}()<>|~:;\\\\\/\\[\\]\\w]+)*(\/)?$`,
        'i'
      ),
      'Please input a valid address'
    );
};

export default Yup.object().shape({
  name: Yup.string().trim().required('Field cannot be empty'),
  department: Yup.mixed().nullable().required('Field cannot be empty'),
  numberVacancy: Yup.string().required('Please choose a number'),
  experienceYear: Yup.string().required('Field cannot be empty'),
  jobDescription: validateTextEditor(),
  requirementDescription: validateTextEditor(),
  skillNeeded: Yup.array().min(1, 'Field cannot be empty'),
  additionalSkillNeeded: Yup.array().min(1, 'Field cannot be empty'),
  linkedin: Yup.string().when('useLinkedin', {
    is: true,
    then: validateUrl('linkedin', 'com'),
    otherwise: Yup.string()
  }),
  glints: Yup.string().when('useGlints', {
    is: true,
    then: validateUrl('glints', 'com'),
    otherwise: Yup.string()
  }),
  techinAsia: Yup.string().when('useTechinAsia', {
    is: true,
    then: validateUrl('techinasia', 'com'),
    otherwise: Yup.string()
  }),
  jobStreet: Yup.string().when('useJobStreet', {
    is: true,
    then: validateUrl('jobstreet', '(com|co.id|com.[a-z]+)'),
    otherwise: Yup.string()
  })
});
