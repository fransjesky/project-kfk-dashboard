import { styled } from '@mui/material/styles';

const MainStyle = styled('div')(({}) => ({
  overflow: 'auto',
  minHeight: '100%',
  paddingTop: 50,
  paddingBottom: 30,
  paddingLeft: 50,
  paddingRight: 50,
  minHeight: '100vh',
  display: 'flex',
  flexDirection: 'column',
  backgroundColor: '#fff',
  borderRadius: '12px'
}));

const PaddedLayout = styled('div')({
  margin: '24px',
  borderRadius: '16px',
  backgroundColor: '#fff'
});

const BodyLayout = ({ children, padded = false }) => {
  if (padded) {
    return (
      <PaddedLayout>
        <MainStyle>{children}</MainStyle>
      </PaddedLayout>
    );
  }

  return <MainStyle>{children}</MainStyle>;
};

export default BodyLayout;
