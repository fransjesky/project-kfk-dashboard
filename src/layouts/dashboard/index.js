import { useState } from 'react';
// material
import { Box, Fab } from '@mui/material';
import { styled } from '@mui/material/styles';
import { Menu } from '@mui/icons-material';
//
import DashboardSidebar from './DashboardSidebar';
import MHidden from 'components/MHidden';

// ----------------------------------------------------------------------

const RootStyle = styled('div')(({ theme }) => ({
  display: 'flex',
  minHeight: '100%',
  overflow: 'hidden',
  flexDirection: 'column',
  [theme.breakpoints.up('lg')]: {
    flexDirection: 'row'
  }
}));

const MainStyle = styled('div')({
  flexGrow: 1,
  overflow: 'auto',
  minHeight: '100%',
  backgroundColor: '#F3F3F3'
});

// ----------------------------------------------------------------------

export default function DashboardLayout({ children }) {
  const [open, setOpen] = useState(false);

  return (
    <RootStyle>
      <MHidden width="lgUp">
        <Box sx={{ width: '100%' }}>
          <Fab
            sx={{
              marginLeft: 4,
              marginTop: 2,
              marginBottom: 2,
              borderRadius: 1,
              backgroundColor: '#fff'
            }}
            size="small"
            onClick={() => setOpen(true)}
          >
            <Menu />
          </Fab>
        </Box>
      </MHidden>
      <DashboardSidebar isOpenSidebar={open} onCloseSidebar={() => setOpen(false)} />
      <MainStyle>{children}</MainStyle>
    </RootStyle>
  );
}
