import { destroyUser } from 'utils/localStorage';
import history from 'utils/history';
// import GroupFIll from '@iconify/icons-ic/baseline-groups';

// ----------------------------------------------------------------------

export const secondSidebarConfig = [
  {
    title: 'Settings',
    path: '/settings',
    icon: 'Setting'
  },
  {
    title: 'Log Out',
    path: '',
    icon: 'Logout',
    onClick: () => {
      destroyUser();
      history.push('/login');
    }
  }
];
