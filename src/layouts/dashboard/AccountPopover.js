import { useRef } from 'react';
// material
import { Box, Typography, Avatar, IconButton } from '@mui/material';
import { Logout as LogoutIcon } from '@mui/icons-material';
// utils
import { destroyUser, getUser } from 'utils/localStorage';
import history from 'utils/history';

export default function AccountPopover() {
  const anchorRef = useRef(null);

  const handleLogout = () => {
    destroyUser();
    history.push('/sign-in');
  };

  const name = getUser()?.user?.user?.fullName;
  const role = getUser()?.user?.user.roles?.join(', ');
  const photoProfile = getUser()?.user?.user?.photoProfile;

  return (
    <>
      <Box sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
        <IconButton
          ref={anchorRef}
          sx={{
            padding: 0,
            width: 44,
            height: 44
          }}
        >
          <Avatar src={photoProfile} alt="photoURL" />
        </IconButton>
        <Box sx={{ ml: '16px' }}>
          <Typography variant="h5" color="#25282B">
            {name}
          </Typography>
          <Typography variant="body2" color="#25282B">
            {role}
          </Typography>
        </Box>
        <IconButton sx={{ ml: '16px' }} onClick={handleLogout}>
          <LogoutIcon sx={{ fill: '#303032' }} />
        </IconButton>
      </Box>
    </>
  );
}
