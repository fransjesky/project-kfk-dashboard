import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import { useLocation, NavLink } from 'react-router-dom';
import Logo from './images/Logo.png';

// material
import { styled } from '@mui/material/styles';
import { makeStyles } from '@mui/styles';
import {
  Box,
  Drawer,
  Grid,
  Typography,
  Avatar,
  TextField,
  InputAdornment,
  IconButton,
  Autocomplete
} from '@mui/material';
import { Search, MenuOpen } from '@mui/icons-material';
// components
import Scrollbar from 'components/Scrollbar';
import NavSection from 'components/NavSection';
import MHidden from 'components/MHidden';
// data
import { secondSidebarConfig } from './SidebarConfig';
import { useSearch } from 'utils/hooks';
import { getUser } from 'utils/localStorage';
import History from 'utils/history';

// ----------------------------------------------------------------------

const DRAWER_WIDTH = 330;

const useStyles = makeStyles({
  input: {
    '& .MuiInputBase-input': {
      boxShadow: 'none',
      color: 'white !important',
      '-webkit-text-fill-color': 'white !important',
      borderColor: 'white !important'
    },
    '& .MuiOutlinedInput-notchedOutline': {
      borderColor: 'white !important',
      boxShadow: 'none',
      color: 'white !important',
      borderRadius: 14
    },
    '& .MuiInputLabel-root': {
      borderColor: 'white !important',
      boxShadow: 'none',
      color: 'white !important'
    }
  }
});

const RootStyle = styled('div')(({ theme }) => ({
  [theme.breakpoints.up('lg')]: {
    flexShrink: 0,
    width: DRAWER_WIDTH
  },
  color: '#F3F3F3'
}));

// ----------------------------------------------------------------------

DashboardSidebar.propTypes = {
  isOpenSidebar: PropTypes.bool,
  onCloseSidebar: PropTypes.func
};

export default function DashboardSidebar({ isOpenSidebar, onCloseSidebar }) {
  const { pathname } = useLocation();
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [valueText, setValueText] = useState('');
  const { user } = getUser();
  const [searchMenu, setSearchMenu] = useState([]);
  const { dispatchSearchGlobal } = useSearch();

  useEffect(() => {
    const noChild = [
      { value: '', title: 'Enquiries', path: '/enquiries' },
      ...user?.user?.masterMenus.filter((menu) => !menu?.children?.length)
    ];
    const childTemp = user?.user?.masterMenus
      .filter((menu) => menu?.children?.length)
      .map((item) => item.children);
    let child = [];
    for (let i = 0; i < childTemp.length; i++) {
      for (let j = 0; j < childTemp[i].length; j++) {
        child = [...child, childTemp[i][j]];
      }
    }
    setSearchMenu(
      noChild
        .concat(child.filter((itm) => itm.title !== 'Company Profile'))
        .map((itmMenu) => ({
          ...itmMenu,
          value: '',
          title: itmMenu.title == 'Overview' ? 'Enquiries' : itmMenu.title,
          path: itmMenu.title == 'Overview' ? '/enquiries' : itmMenu.path
        }))
        .filter((value) => value.path !== pathname)
    );
    // eslint-disable-next-line
  }, [pathname]);

  useEffect(() => {
    if (isOpenSidebar) {
      onCloseSidebar();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pathname]);

  const handleChange = (e, value) => {
    setOpen(true);
    setValueText(value);
    setSearchMenu((prev) => prev.map((itm) => ({ ...itm, value })));
  };

  const renderContent = (
    <Scrollbar
      sx={{
        color: '#F3F3F3',
        height: '100%',
        '& .simplebar-content': {
          height: '100%',
          display: 'flex',
          flexDirection: 'column'
        }
      }}
    >
      <Box sx={{ p: 3 }} display="flex" alignItems="center" marginTop={-4} marginBottom={-2}>
        <MHidden width="lgUp">
          <IconButton color="white" onClick={onCloseSidebar}>
            <MenuOpen sx={{ fontSize: 28 }} />
          </IconButton>
        </MHidden>
        <Box
          to="/"
          component={NavLink}
          sx={{ display: 'flex', justifyContent: 'center', margin: 'auto' }}
        >
          <img src={Logo} alt="logo" style={{ width: 180 }} />
        </Box>
      </Box>
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center'
        }}
      >
        <Box
          sx={{
            display: 'flex',
            alignSelf: 'start',
            marginLeft: 3.5
          }}
        >
          <Avatar>{user.user.name.charAt(0).toUpperCase()}</Avatar>
          <Box sx={{ ml: 1.1 }}>
            <Typography variant="body1" fontWeight={700} fontSize={14.65}>
              {user.user.name}
            </Typography>
            <Typography variant="body2" fontSize={11.57}>
              {user.user.Role.name} ( {user.user.email} )
            </Typography>
          </Box>
        </Box>
        <Autocomplete
          sx={{ marginTop: 2, marginBottom: 2, width: 280 }}
          open={open}
          onClose={() => setOpen(false)}
          options={searchMenu}
          inputValue={valueText}
          getOptionLabel={(option) => option.title}
          filterOptions={(x) => x}
          onInputChange={handleChange}
          renderOption={(props, option) => (
            <Grid
              key={option.title}
              container
              sx={{ ':hover': { backgroundColor: '#FFF5EB' }, padding: 1, cursor: 'pointer' }}
              onClick={() => {
                dispatchSearchGlobal(option.value);
                setOpen(false);
                setValueText('');
                History.push(option.path);
              }}
            >
              <Grid xs={12} minHeight={22} item>
                <Typography fontWeight={700} fontSize={14.65}>
                  {option.value}
                </Typography>
              </Grid>
              <Grid xs={12} color={'#FF9D42'} item>
                <Typography fontSize={11.57}>In "{option.title}"</Typography>
              </Grid>
            </Grid>
          )}
          renderInput={(params) => (
            <TextField
              {...params}
              placeholder="Search"
              className={classes.input}
              value={valueText}
              inputProps={{ ...params.inputProps, autoComplete: 'new-password' }}
              InputProps={{
                ...params.InputProps,
                startAdornment: (
                  <InputAdornment position="start">
                    <Search sx={{ color: '#F3F3F3' }} />
                  </InputAdornment>
                )
              }}
            />
          )}
        />
      </Box>
      <NavSection navConfig={user?.user?.masterMenus} secondConfig={secondSidebarConfig} />
    </Scrollbar>
  );

  return (
    <RootStyle>
      <MHidden width="lgUp">
        <Drawer
          open={isOpenSidebar}
          onClose={onCloseSidebar}
          PaperProps={{
            sx: {
              width: DRAWER_WIDTH,
              backgroundImage: 'linear-gradient(to bottom, #242424, #010101)',
              color: '#F3F3F3'
            }
          }}
        >
          {renderContent}
        </Drawer>
      </MHidden>

      <MHidden width="lgDown">
        <Drawer
          open
          variant="persistent"
          PaperProps={{
            sx: {
              width: DRAWER_WIDTH,
              backgroundImage: 'linear-gradient(to bottom, #242424, #010101)',
              color: '#F3F3F3'
            }
          }}
        >
          {renderContent}
        </Drawer>
      </MHidden>
    </RootStyle>
  );
}
