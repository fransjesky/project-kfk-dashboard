export const BASE_URL = process.env.REACT_APP_BASE_URL || 'https://dev-api.xcidic.com';
export const BASE_API_PATH = process.env.REACT_APP_BASE_API_PATH || '/api/';
export const WEBSITE_URL = process.env.REACT_APP_WEBSITE_URL || 'http://18.231.186.80:3000';
