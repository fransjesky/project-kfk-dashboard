import mime from 'mime-types';

/**
 * Get extension name from Mime-Type
 * @param {string} mimeType
 */
export function getExtension(mimeType) {
  return mime.extension(mimeType);
}

/**
 * Get Mime-Type from extension or full file name
 * @param {string} extension
 * @returns
 */
export function getMimeType(extension) {
  return mime.lookup(extension);
}
