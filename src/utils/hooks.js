import { useMemo, useEffect, useState, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import {
  notif,
  add,
  remove,
  load,
  search,
  edit,
  list,
  loadOnly,
  confirm,
  confirmClose
} from 'redux/actions';
import to_qs from './queryString';

export const useAdder = (name, customName = false, isLink = true) => {
  const dispatch = useDispatch();

  const dispatchAdd = (data, onSuccess = () => {}, redirectPath = false) => {
    dispatch(
      add({
        name,
        customName,
        notLink: !isLink,
        customRedirect: redirectPath,
        data: { ...data },
        onSuccess
      })
    );
  };

  return { dispatchAdd };
};

export const useEditer = (name, customName = false, isLink = true, method = 'PUT') => {
  const dispatch = useDispatch();

  const dispatchEdit = (id, data, onSuccess = () => {}, redirectPath = false, titleNotifSucces) => {
    dispatch(
      edit({
        id,
        name,
        customName,
        titleNotifSucces,
        noLink: !isLink,
        method,
        linkSuccess: redirectPath,
        data: { ...data },
        onSuccess
      })
    );
  };

  return { dispatchEdit };
};

export const useSearch = (name) => {
  const dispatch = useDispatch();

  const dispatchSearch = (keyword) => {
    return dispatch(search({ param: keyword, name }));
  };

  const dispatchSearchGlobal = (keyword) => {
    return dispatch(search({ param: keyword, name: 'keywordGlobal' }));
  };

  return { dispatchSearch, dispatchSearchGlobal };
};

export const useRemover = (name, customName = false, isLink = true) => {
  const dispatch = useDispatch();

  const dispatchRemove = (id, onSuccess = () => {}) => {
    dispatch(
      remove({
        name,
        customName,
        noLink: !isLink,
        id,
        onSuccess
      })
    );
  };

  return { dispatchRemove };
};

export const useLoader = (name, customName = false) => {
  const dispatch = useDispatch();

  const dispatchLoad = useCallback(
    (id, onSuccess = () => {}) => {
      dispatch(
        load({
          name,
          customName,
          id,
          onSuccess
        })
      );
    },
    [dispatch, name, customName]
  );

  return { dispatchLoad };
};

export const useLoaderOnly = (name) => {
  const dispatch = useDispatch();

  const dispatchLoadOnly = (id, onSuccess = () => {}, onError = () => {}) => {
    dispatch(
      loadOnly({
        name,
        id,
        onSuccess,
        onError
      })
    );
  };

  return { dispatchLoadOnly };
};

export const useLister = (name, customName = false) => {
  const dispatch = useDispatch();

  const dispatchList = useCallback(
    (queryObj, onSuccess = () => {}) => {
      dispatch(
        list({
          name,
          customName,
          query: to_qs(queryObj),
          onSuccess
        })
      );
    },
    [dispatch, name, customName]
  );

  return { dispatchList };
};

export const useNotifier = () => {
  const dispatch = useDispatch();

  const notifSuccess = (message, title) => {
    dispatch(
      notif({
        open: true,
        variant: 'success',
        message,
        title
      })
    );
  };

  const notifError = (message, title) => {
    dispatch(
      notif({
        open: true,
        variant: 'error',
        message,
        title
      })
    );
  };

  return { notifSuccess, notifError };
};

export const useConfirm = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const handleClose = () => dispatch(confirmClose());

  const confirmInfo = ({ message, title, handler, redirect }) => {
    dispatch(
      confirm({
        open: true,
        warning: false,
        message,
        title,
        handler: () => {
          handleClose();
          handler();
          if (redirect) history.push(redirect);
        }
      })
    );
  };

  const confirmWarning = ({ message, title, handler, redirect }) => {
    dispatch(
      confirm({
        open: true,
        warning: true,
        message,
        title,
        handler: () => {
          handleClose();
          handler();
          if (redirect) history.push(redirect);
        }
      })
    );
  };

  return { confirmInfo, confirmWarning };
};

export const useCompanyCategories = () => {
  const { dispatchList } = useLister('company/category', 'categoryList');
  const { categoryList } = useSelector((state) => state.module.list);

  const categories = useMemo(() => {
    if (!categoryList || !categoryList.data) return [];
    return categoryList.data;
  }, [categoryList]);

  useEffect(() => {
    dispatchList({ sort: 'name', order: 'ASC', status: 'active', limit: 100 });
  }, []); //eslint-disable-line

  return { categories };
};

export const useCountries = () => {
  const { dispatchList } = useLister('list-countries', 'countryList');
  const { countryList } = useSelector((state) => state.module.list);

  const countries = useMemo(() => {
    if (!countryList || !countryList.data) return [];
    return countryList.data;
  }, [countryList]);

  useEffect(() => {
    dispatchList({});
  }, []); //eslint-disable-line

  return { countries };
};

export const useContainerDimensions = (myRef) => {
  const [dimensions, setDimensions] = useState({ width: 0, height: 0 });

  const getDimensions = (ref) => ({
    width: ref?.offsetWidth || ref?.current?.offsetWidth || dimensions.width,
    height: ref?.offsetHeight || ref?.current?.offsetHeight || dimensions.height
  });

  useEffect(() => {
    if (!myRef && !myRef?.current) return;

    const handleResize = () => {
      setDimensions(getDimensions(myRef));
    };

    setDimensions(getDimensions(myRef));

    window.addEventListener('resize', handleResize);

    // eslint-disable-next-line consistent-return
    return () => window.removeEventListener('resize', handleResize);

    // eslint-disable-next-line
  }, [myRef]);

  return dimensions;
};

// Parameter is the boolean, with default "false" value
export const useToggle = (initialState = false) => {
  // Initialize the state
  const [state, setState] = useState(initialState);
  // Define and memorize toggler function in case we pass down the component,
  // This function change the boolean value to it's opposite value
  const toggle = useCallback((state) => {
    if (state) {
      setState(state);
    } else {
      setState((state) => !state);
    }
  }, []);

  return [state, toggle];
};

export const useDepartments = () => {
  const { dispatchList } = useLister('department/drop-down', 'departments');
  const { departments } = useSelector((state) => state.module.list);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => dispatchList({}), []);

  return { departments: departments?.data };
};

export const useCareerSkills = () => {
  const { dispatchList: dispatchCareerMain } = useLister('career-skill', 'careerSkillMain');
  const { dispatchList: dispatchCareerAdditional } = useLister(
    'career-skill',
    'careerSkillAdditional'
  );
  const { careerSkillMain, careerSkillAdditional } = useSelector((state) => state.module.list);

  useEffect(() => {
    dispatchCareerMain({ type: 'main' });
    dispatchCareerAdditional({ type: 'additional' });
    // eslint-disable-next-line
  }, []);

  return { mainSkills: careerSkillMain?.data, additionalSkills: careerSkillAdditional?.data };
};
