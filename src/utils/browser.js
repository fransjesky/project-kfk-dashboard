/**
 *
 * @param {string} text
 * @returns {Promise<void>}
 */
export function copyTextToClipboard(text, ref = null) {
  if (navigator.clipboard && window.isSecureContext) {
    return navigator.clipboard.writeText(text);
  } else if (ref !== null) {
    return new Promise((resolve, reject) => {
      ref.current.value = text;
      ref.current.focus();

      setTimeout(() => {
        ref.current.select();

        try {
          document.execCommand('copy');
          resolve();
        } catch (err) {
          reject(err);
        }
      }, 200);
    });
  } else {
    let textArea = document.createElement('textarea');
    textArea.value = text;
    textArea.style.position = 'fixed';
    textArea.style.left = '-999999px';
    textArea.style.top = '-999999px';
    document.body.appendChild(textArea);
    textArea.focus();

    return new Promise((resolve, reject) => {
      setTimeout(() => {
        textArea.select();

        try {
          document.execCommand('copy');
          document.body.removeChild(textArea);
          resolve();
        } catch (err) {
          reject(err);
        }
      }, 200);
    });
  }
}
