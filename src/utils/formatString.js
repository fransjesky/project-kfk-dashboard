export function fShortenString(str = '', to = 20, glue = '...') {
  let tr = str.trim();
  let gl = glue.length;

  if (tr.length <= to || tr.length <= gl + 3) return tr;

  let rem = tr.length - to + gl;
  return tr.substring(0, 3) + glue + tr.substring(3 + rem);
}

export function getTrimmedObject(obj) {
  const { ...props } = obj;

  for (let prop in props) {
    if (Object.prototype.hasOwnProperty.call(props, prop) && typeof props[prop] === 'string') {
      props[prop] = props[prop].trim();
    }
  }

  return props;
}
