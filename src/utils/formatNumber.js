import { replace } from 'lodash';
import numeral from 'numeral';

// ----------------------------------------------------------------------

export function fCurrency(number) {
  return numeral(number).format(Number.isInteger(number) ? '$0,0' : '$0,0.00');
}

export function fPercent(number) {
  return numeral(number / 100).format('0.0%');
}

export function fNumber(number) {
  return numeral(number).format();
}

export function fShortenNumber(number) {
  return replace(numeral(number).format('0.00a'), '.00', '');
}

export function fData(number) {
  return numeral(number).format('0.0 b');
}

export function fBytes2Readable(number) {
  if (number < 1000) return number + ' Bytes';

  let val = number;
  let unit = '';

  if (number < 1000000) {
    val = number / 1024;
    unit = 'KB';
  } else {
    val = number / (1024 * 1024);
    unit = 'MB';
  }

  return Math.ceil(val * 10) / 10 + ` ${unit}`;
}
