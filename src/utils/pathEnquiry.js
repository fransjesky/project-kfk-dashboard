const path = (key) => {
  let temp = '';
  switch (key) {
    case 1:
      temp = 'market-data';
      break;
    case 2:
      temp = 'potential-buyers';
      break;
    case 3:
      temp = 'exporters-importers';
      break;
    case 4:
      temp = 'web-promotion';
      break;
    case 5:
      temp = 'business-matching';
      break;
    case 6:
      temp = 'verify-company-partner';
      break;
    case 7:
      temp = 'business-offers';
      break;
    default:
      temp = 'case-complaint';
      break;
  }
  return temp;
};

export default path;
