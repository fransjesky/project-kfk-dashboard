const phoneNumber =
  /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
const lettersAndSpace = /^[A-Za-z\s]*$/;
const letters = /^[A-Za-z]*$/;
const numbers = /^[0-9]*$/;
const lettersAndNumbers = /^[A-Za-z0-9]*$/;
const specialCharacters = /[$-/:-?{-~!"^_`\[\]]/;
const numbersAndSpecialCharacters = /^[0-9*#+\/\-_;:"'`~{}\[\]$%&^!@()\\<>,.?]*$/;
const behanceUrl = /^(?:https:\/\/www[\.]behance[\.]net)+[\w\-\._~:/?%#[\]@!\$&'\(\)\*\+,;=.]+$/;
const blankSpace = /^(?!\s+$).*/;
const passwordValidation = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/;
const passwordValidationChars =
  /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[*#+\/\-_;:"'`~{}\[\]$%&^!@()\\<>,.?])(?=.{8,})/;

export default {
  phoneNumber,
  lettersAndSpace,
  letters,
  numbers,
  specialCharacters,
  lettersAndNumbers,
  numbersAndSpecialCharacters,
  behanceUrl,
  blankSpace,
  passwordValidation,
  passwordValidationChars
};
