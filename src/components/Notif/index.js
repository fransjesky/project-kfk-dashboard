import React from 'react';
import { Snackbar, Alert as MuiAlert, AlertTitle } from '@mui/material';
import { Iconly } from 'react-iconly';
import { notifClose } from 'redux/actions/Global';
import { useDispatch, useSelector } from 'react-redux';

const Alert = React.forwardRef(function Alert(props, ref) {
  return (
    <MuiAlert
      elevation={6}
      ref={ref}
      variant="filled"
      {...props}
      sx={{ backgroundColor: '#FFFFFF' }}
    />
  );
});

const Notif = ({ open, onClose }) => {
  const { notif } = useSelector((state) => state.global);
  const dispatch = useDispatch();

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    if (!open) {
      dispatch(notifClose());
    } else {
      onClose();
    }
  };

  return (
    <Snackbar open={notif.open} autoHideDuration={3000} onClose={handleClose}>
      <Alert
        onClose={handleClose}
        severity={notif.variant}
        variant="outlined"
        icon={
          <Iconly
            primaryColor={notif.variant == 'success' ? 'green' : 'red'}
            name={notif.variant == 'success' ? 'TickSquare' : 'InfoCircle'}
            size="large"
            set="bold"
          />
        }
        sx={{
          border: 'none',
          position: 'absolute',
          bottom: 0,
          left: 0,
          maxWidth: '440px',
          height: 71,
          borderRadius: '4px',
          color: '#858585',
          fontWeight: 400
        }}
      >
        <AlertTitle sx={{ color: '#3D3D3D', fontWeight: 700 }}>{notif.title}</AlertTitle>
        <p style={{ wordBreak: 'break-all', wordWrap: 'break-word', maxWidth: 440 }}>
          {notif.message}
        </p>
        <div
          style={{
            position: 'absolute',
            height: '6px',
            width: '100%',
            left: '0%',
            right: '98.64%',
            bottom: 0.4,
            background: notif.variant == 'success' ? '#00A04A' : '#E01F3D',
            borderRadius: '4px'
          }}
        />
      </Alert>
    </Snackbar>
  );
};

export default Notif;
