import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Avatar, Box, Grid, IconButton } from '@mui/material';
import SvgIconStyle from 'components/SvgIconStyle';
import Card from '../Card';
import Dropdown from 'components/Dropdown';

// icons
import MoreVertIcon from '@mui/icons-material/MoreVert';
import DeleteIcon from 'assets/svg/Delete.svg';
import EditIcon from 'assets/svg/Edit.svg';

function CardTeam({
  name,
  position,
  photo,
  order = null,
  description,
  createdAt,
  color,
  width,
  onEdit = undefined,
  onDelete = undefined
}) {
  const style = {
    container: {
      padding: '1.5rem',
      display: 'flex',
      flexDirection: 'column'
    },
    head: {
      display: 'flex',
      justifyContent: 'space-between'
    },
    main: {
      width: '100%',
      maxWidth: '7.125rem',
      marginLeft: '0.75rem',
      display: 'flex',
      flexDirection: 'column'
    },
    name: {
      color: '#3D3D3D',
      fontFamily: `'satoshi-bold', sans-serif`,
      fontSize: '1em',
      fontWeight: '700',
      lineHeight: '1.5rem',
      textTransform: 'capitalize',
      WebkitBoxOrient: 'vertical',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap'
    },
    position: {
      margin: '0.25rem 0',
      color: '#00A3FF',
      fontFamily: `'satoshi-medium', sans-serif`,
      fontSize: '1em',
      fontWeight: '500',
      lineHeight: '1.2rem',
      textTransform: 'capitalize',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap'
    },
    description: {
      maxHeight: '2.375rem',
      margin: '1rem 0',
      color: '#94A3B8',
      fontFamily: `'satoshi-medium', sans-serif`,
      fontSize: '1em',
      fontWeight: '500',
      lineHeight: '1.2rem',
      textTransform: 'capitalize',
      overflow: 'hidden',
      display: '-webkit-box',
      webkitLineClamp: '2',
      webkitBoxOrient: 'vertical'
    },
    details: {
      marginTop: '0.5rem',
      display: 'flex',
      justifyContent: 'space-between'
    },
    detailsOrder: {
      color: color,
      fontFamily: `'satoshi-bold', sans-serif`,
      fontSize: '0.703em',
      fontWeight: '700',
      lineHeight: '0.843rem'
    },
    detailsCreatedAt: {
      color: '#616161',
      fontFamily: `'satoshi-regular', sans-serif`,
      fontSize: '0.703em',
      fontWeight: '400',
      lineHeight: '0.843rem',
      textTransform: 'capitalize'
    }
  };

  const DropdownButton = () => (
    <IconButton>
      <MoreVertIcon />
    </IconButton>
  );

  return (
    <Card color={color} width={width}>
      <Grid style={style.container}>
        <Grid style={style.head}>
          <Avatar src={photo} sx={{ height: '3.5rem', width: '3.5rem' }} />
          <Box style={style.main}>
            <span style={style.name}>{name}</span>
            <span style={style.position}>{position}</span>
          </Box>
          <Dropdown
            button={<DropdownButton />}
            items={[
              {
                label: 'Edit',
                icon: <SvgIconStyle src={EditIcon} />,
                onClick: onEdit
              },
              {
                label: 'Delete',
                icon: <SvgIconStyle src={DeleteIcon} />,
                onClick: onDelete
              }
            ]}
          />
        </Grid>
        <p style={style.description}>{description}</p>
        <Grid style={style.details}>
          <span style={style.detailsOrder}>Order: {order}</span>
          <span style={style.detailsCreatedAt}>{moment(createdAt).format('D MMM YYYY')}</span>
        </Grid>
      </Grid>
    </Card>
  );
}

CardTeam.propTypes = {
  name: PropTypes.string.isRequired,
  position: PropTypes.string.isRequired,
  photo: PropTypes.string,
  description: PropTypes.string.isRequired,
  order: PropTypes.number,
  color: PropTypes.string,
  width: PropTypes.string,
  createdAt: PropTypes.string,
  onDelete: PropTypes.func,
  onEdit: PropTypes.func
};

export default CardTeam;
