import useSWR from 'swr';
import axios from 'axios';
import FileSaver from 'file-saver';

function getAxiosData(response) {
  return {
    data: response.data,
    statusCode: response.status
  };
}

function getAxiosError(error) {
  let errorObj = {};

  if (error.response) {
    errorObj.error = error.response.data;
    errorObj.statusCode = error.response.status;
  } else if (error.request) {
    errorObj.error = { message: 'Error in making request.' };
    errorObj.statusCode = null;
  } else {
    errorObj.error = { message: 'Error in stting up request.' };
    errorObj.statusCode = null;
  }

  return errorObj;
}

export function axiosFetcherAuth(headerName, headerValue) {
  return (method, url, options = {}) => {
    let optionsWithAuth = { ...options };

    if (
      typeof optionsWithAuth.headers === 'object' &&
      Object.keys(optionsWithAuth.headers).length
    ) {
      optionsWithAuth.headers[headerName] = headerValue;
    } else {
      optionsWithAuth.headers = { [headerName]: headerValue };
    }

    return axiosFetcher(method, url, optionsWithAuth);
  };
}

export function axiosFetcher(method, url, options = {}) {
  let config = { method, url, ...options };

  return new Promise((resolve) => {
    return axios(config)
      .then(function (response) {
        resolve(getAxiosData(response));
      })
      .catch(function (error) {
        resolve(getAxiosError(error));
      });
  });
}

export function axiosFileSaver(url, mimeType, fileName, options = {}) {
  let config = {
    method: 'GET',
    url,
    headers: {
      'Content-Type': mimeType
    },
    responseType: 'blob',
    ...options
  };

  return axios(config)
    .then((response) => {
      FileSaver.saveAs(response.data, fileName);
      return true;
    })
    .catch((error) => {
      if (error.response) {
        console.error(error.response);
      } else if (error.request) {
        console.error(error.request);
      } else {
        console.error('Error Axios Request');
      }
      return false;
    });
}

export function axiosDeleteMany(urls = [], options = {}) {
  let config = {
    method: 'DELETE',
    ...options
  };

  let promises = urls.map((url) => {
    return new Promise((resolve) => {
      return axios({ ...config, url })
        .then((response) => {
          resolve({ ...getAxiosData(response), url });
        })
        .catch((error) => {
          resolve({ ...getAxiosError(error), url });
        });
    });
  });

  return Promise.all(promises);
}

const fetcher = (...args) => fetch(...args).then((res) => res.json());

export function useFetcher(url, fetchOptions) {
  const { data, error } = useSWR([`${url}`, fetchOptions], fetcher);

  return {
    data,
    isLoading: !error && !data,
    isError: error
  };
}

export default function ToDo() {
  const { data, isLoading, isError } = useFetcher('url');

  if (isError) return <div>failed to load</div>;
  if (isLoading) return <div>loading...</div>;

  // render data
  return <div>number of record {data}!</div>;
}
