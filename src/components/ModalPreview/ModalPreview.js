import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Box, Slide, Typography } from '@mui/material';
import { PrimaryButton } from 'components/Button';

const ModalPreview = ({ children, open, onClose, title }) => {
  return (
    <Modal open={open} onClose={onClose}>
      <Slide direction="up" in={open} mountOnEnter unmountOnExit>
        <Box
          sx={{
            pb: 3,
            px: 6,
            background: '#fff',
            mt: 5,
            bottom: 0,
            display: 'flex',
            flexDirection: 'column',
            height: 'calc(100% - 40px)',
            borderRadius: '16px 16px 0 0',
            overflowY: 'auto'
          }}
        >
          <Box
            sx={{
              pt: 3,
              display: 'flex',
              justifyContent: 'space-between',
              position: 'sticky',
              top: 0,
              backgroundColor: 'inherit'
            }}
          >
            <Typography variant="s1" fontWeight="bold">
              {title}
            </Typography>
            <PrimaryButton onClick={onClose}>Back</PrimaryButton>
          </Box>
          {children}
        </Box>
      </Slide>
    </Modal>
  );
};

ModalPreview.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func,
  title: PropTypes.string,
  children: PropTypes.elementType
};

export default ModalPreview;
