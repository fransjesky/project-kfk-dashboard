import React from 'react';
import PropTypes from 'prop-types';
import Button from '@mui/material/Button';
// import { styled } from '@mui/material/styles';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
// import Typography from '@mui/material/Typography';

// const BootstrapDialog = styled(Dialog)(({ theme }) => ({
//   '& .MuiDialogContent-root': {
//     padding: theme.spacing(2),
//   },
//   '& .MuiDialogActions-root': {
//     padding: theme.spacing(1),
//   },
// }));

const BootstrapDialogTitle = (props) => {
  const { children, onClose = () => {}, withClose = true, ...other } = props;

  return (
    <DialogTitle sx={{ m: 0, p: 3, pr: '70px' }} {...other}>
      {children}
      {onClose && withClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: 'absolute',
            right: 15,
            top: 15,
            color: (theme) => theme.palette.grey[500]
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
};

BootstrapDialogTitle.propTypes = {
  children: PropTypes.node,
  onClose: PropTypes.func.isRequired,
  withClose: PropTypes.bool
};

export default function DialogPanel({
  children,
  title = '',
  onClose = () => {},
  isOpen = false,
  withClose = true,
  actions = []
}) {
  return (
    <Dialog onClose={onClose} aria-labelledby="dialogPanel" open={isOpen}>
      <BootstrapDialogTitle id="dialogPanel" onClose={onClose} withClose={withClose}>
        {title}
      </BootstrapDialogTitle>
      <DialogContent sx={{ minWidth: '300px' }}>{children}</DialogContent>
      {actions.length > 0 && (
        <DialogActions sx={{ p: 3 }}>
          {actions.map(
            (
              { variant = 'contained', color = 'primary', label = '', onClick = () => {}, ...rest },
              idx
            ) => {
              return (
                <Button variant={variant} color={color} onClick={onClick} key={idx} {...rest}>
                  {label}
                </Button>
              );
            }
          )}
        </DialogActions>
      )}
    </Dialog>
  );
}

DialogPanel.propTypes = {
  children: PropTypes.node,
  onClose: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
  withClose: PropTypes.bool,
  title: PropTypes.string,
  actions: PropTypes.arrayOf(
    PropTypes.shape({
      variant: PropTypes.oneOf(['text', 'contained', 'outlined']),
      color: PropTypes.oneOf(['primary', 'secondary', 'success', 'error']),
      label: PropTypes.string,
      onClick: PropTypes.func
    })
  )
};
