import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { styled } from '@mui/material/styles';
// components
import { Box, Typography, IconButton } from '@mui/material';
import SvgIconStyle from 'components/SvgIconStyle';
import Card from 'components/Card';
import Dropdown from 'components/Dropdown';
// icons
import MoreVertIcon from '@mui/icons-material/MoreVert';
import DeleteIcon from 'assets/svg/Delete.svg';
import EditIcon from 'assets/svg/Edit.svg';
import HideIcon from 'assets/svg/Hide.svg';
import ShowIcon from 'assets/svg/Show.svg';

const Container = styled(Box)({
  height: '200px',
  padding: '0 16px 24px',
  paddingTop: '12px',
  display: 'grid',
  gridTemplateRows: '40px auto min-content'
});

const Head = styled(Box)({
  display: 'grid',
  gridTemplateColumns: 'auto min-content',
  alignItems: 'center'
});

const Title = styled((props) => (
  <Typography component="p" variant="s3" fontWeight="bold" {...props} />
))({
  overflow: 'hidden',
  WebkitLineClamp: 2,
  display: '-webkit-box',
  WebkitBoxOrient: 'vertical',
  textOverflow: 'ellipsis',
  whiteSpace: 'normal'
});

const DropdownButton = () => (
  <IconButton>
    <MoreVertIcon />
  </IconButton>
);

const CardCareer = (props) => {
  const {
    color,
    date,
    title,
    views,
    isArchived,
    onEdit,
    onArchive,
    onDelete,
    hideActions,
    width = '290px',
    onClick,
    ...otherProps
  } = props;
  const cardRef = useRef();
  const dropdownButtonRef = useRef();

  const handleClick = (event) => {
    if (
      hideActions ||
      (cardRef.current.contains(event.target) && !dropdownButtonRef.current.contains(event.target))
    ) {
      onClick();
    }
  };

  return (
    <div ref={cardRef}>
      <Card color={color} width={width} onClick={handleClick} {...otherProps}>
        <Container sx={{ '&:hover': { cursor: 'pointer' } }}>
          <Head>
            <Typography variant="c1">{moment(date).format('D MMM YYYY')}</Typography>
            {!hideActions && (
              <Dropdown
                button={
                  <div ref={dropdownButtonRef}>
                    <DropdownButton />
                  </div>
                }
                items={[
                  {
                    label: 'Edit',
                    icon: <SvgIconStyle src={EditIcon} />,
                    onClick: onEdit
                  },
                  {
                    label: isArchived ? 'Show' : 'Archive',
                    icon: isArchived ? (
                      <SvgIconStyle src={ShowIcon} />
                    ) : (
                      <SvgIconStyle src={HideIcon} />
                    ),
                    onClick: onArchive
                  },
                  {
                    label: 'Delete',
                    icon: <SvgIconStyle src={DeleteIcon} />,
                    onClick: onDelete
                  }
                ]}
              />
            )}
          </Head>
          <Box sx={{ pt: 1.5, width: '230px', wordWrap: 'break-word' }}>
            <Title variant="s3" fontWeight="bold">
              {title}
            </Title>
          </Box>
          <Box display="flex" alignItems="center" gap={1}>
            {isArchived ? <SvgIconStyle src={HideIcon} /> : <SvgIconStyle src={ShowIcon} />}
            <Typography variant="c1" color="grey.90">
              {views + ' views'}
            </Typography>
          </Box>
        </Container>
      </Card>
    </div>
  );
};

CardCareer.propTypes = {
  color: PropTypes.string,
  date: PropTypes.string,
  title: PropTypes.string,
  views: PropTypes.number,
  isArchived: PropTypes.bool,
  onEdit: PropTypes.func,
  onArchive: PropTypes.func,
  onDelete: PropTypes.func,
  hideActions: PropTypes.bool
};

CardCareer.defaultProps = {
  hideActions: false
};

export default CardCareer;
