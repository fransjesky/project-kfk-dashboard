import React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/material/styles';
// components
import { Avatar, Box, Typography, IconButton } from '@mui/material';
import SvgIconStyle from 'components/SvgIconStyle';
import Card from 'components/Card';
import Dropdown from 'components/Dropdown';
// icons
import MoreVertIcon from '@mui/icons-material/MoreVert';
import DeleteIcon from 'assets/svg/Delete.svg';
import EditIcon from 'assets/svg/Edit.svg';
import moment from 'moment';

const Container = styled(Box)({
  minHeight: '125px',
  padding: '20px 14px 8px 14px',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-between'
});

const Head = styled(Box)({
  display: 'flex',
  justifyContent: 'space-between'
});

const Title = styled(Box)({
  maxWidth: '73%',
  marginLeft: '0.75rem',
  display: 'flex',
  flexDirection: 'column'
});

const DropdownButton = () => (
  <IconButton>
    <MoreVertIcon />
  </IconButton>
);

const CardPartner = (props) => {
  const { color, name, logo, date, order, onEdit, onDelete } = props;
  return (
    <Card color={color} width="290px">
      <Container>
        <Head>
          <Box sx={{ display: 'flex', flexDirection: 'row', width: '80%' }}>
            <Avatar src={logo} sx={{ height: '3.5rem', width: '3.5rem' }} />
            <Title>
              <Typography variant="body1" fontWeight="bold" noWrap={true}>
                {name}
              </Typography>
              <Typography variant="body1" fontWeight="medium">
                {moment(date).format('DD MMMM YYYY')}
              </Typography>
            </Title>
          </Box>

          <Dropdown
            button={<DropdownButton />}
            items={[
              {
                label: 'Edit',
                icon: <SvgIconStyle src={EditIcon} />,
                onClick: onEdit
              },
              {
                label: 'Delete',
                icon: <SvgIconStyle src={DeleteIcon} />,
                onClick: onDelete
              }
            ]}
          />
        </Head>

        <Typography variant="c1" color="orange" fontWeight="bold">
          Order: {order}
        </Typography>
      </Container>
    </Card>
  );
};

CardPartner.propTypes = {
  color: PropTypes.string,
  name: PropTypes.string,
  date: PropTypes.string,
  logo: PropTypes.string,
  order: PropTypes.number,
  onEdit: PropTypes.func,
  onDelete: PropTypes.func
};

CardPartner.defaultProps = {
  hideActions: false
};

export default CardPartner;
