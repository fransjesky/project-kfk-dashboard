import PropTypes from 'prop-types';
import SimpleBarReact from 'simplebar-react';
// material
import { Box } from '@mui/material';
import 'simplebar/src/simplebar.css';

Scrollbar.propTypes = {
  children: PropTypes.node.isRequired,
  sx: PropTypes.object
};

export default function Scrollbar({ children, sx, ...other }) {
  const isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
    navigator.userAgent
  );

  if (isMobile) {
    return (
      <Box sx={{ overflowX: 'auto', ...sx }} {...other}>
        {children}
      </Box>
    );
  }

  return (
    <SimpleBarReact timeout={500} style={{ maxHeight: '100%' }} sx={sx} {...other}>
      {children}
    </SimpleBarReact>
  );
}
