import React from 'react';

// Components
import BaseTable from './base';

const Table = ({
  heads,
  rows,
  specialColumns,
  headCellSx,
  headSx,
  bodySx,
  tableSx,
  clickAble,
  handleClickAble,
  onClickColumn,
  onClickIgnores,
  withAction,
  actions
}) => {
  return (
    <BaseTable
      handleClickAble={handleClickAble}
      clickAble={clickAble}
      onClickColumn={onClickColumn}
      onClickIgnores={onClickIgnores}
      heads={heads}
      rows={rows}
      specialColumns={specialColumns}
      withAction={withAction}
      actions={actions}
      tableSx={{ width: '100%', ...tableSx }}
      headSx={{
        '& tr': {
          borderBottom: '1px solid rgba(241, 243, 244, 1)'
        },
        '& th:first-of-type': {
          borderRadius: '8px 0 0 8px'
        },
        '& th:last-of-type': {
          borderRadius: '0 8px 8px 0'
        },
        ...headSx
      }}
      headCellSx={{
        borderBottom: '1px solid #E7E7E7',
        bgcolor: 'secondary.lighter',
        color: 'grey.100',
        fontWeight: 600,
        ...headCellSx
      }}
      bodySx={{
        '& tr td': {
          borderBottom: '1px solid rgba(0, 0, 0, 0.03);'
        },
        '& td:not(:first-of-type) > div': {
          marginLeft: '16px'
        },
        ...bodySx
      }}
    />
  );
};

export default Table;
