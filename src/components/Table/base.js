import React, { useMemo } from 'react';
import { v4 as uuidv4 } from 'uuid';
import { Box, Divider, Table, TableHead, TableBody, TableRow, TableCell } from '@mui/material';
import ActionMenu from './actionMenu';

const BaseTable = ({
  heads,
  rows,
  specialColumns,
  headCellSx,
  headSx,
  bodySx,
  tableSx,
  clickAble,
  handleClickAble,
  onClickColumn,
  onClickIgnores,
  withAction,
  actions
}) => {
  // use useMemo hook to create uuid so that it always stay the same every render
  const uuid_head = useMemo(() => uuidv4(), []);
  const uuid_row = useMemo(() => uuidv4(), []);

  const handleClickColumn = (rowData, cellIdx) => {
    if (onClickColumn instanceof Function) {
      if (!onClickIgnores || onClickIgnores.length === 0 || !onClickIgnores.includes(cellIdx)) {
        onClickColumn(rowData);
      }
    }
  };

  // create unique id for head, row, and col that always stay the same every render
  // so we remove unnecessary render on table components
  return (
    <Table sx={tableSx}>
      <TableHead sx={headSx}>
        <TableRow>
          {heads.map((head, headIdx) => (
            <TableCell key={`${uuid_head}-${headIdx}`} sx={headCellSx}>
              <Box display={'flex'}>
                {headIdx > 0 && (
                  <Divider orientation="vertical" flexItem sx={{ mr: 2, color: 'black' }} />
                )}
                {head}
              </Box>
            </TableCell>
          ))}
        </TableRow>
      </TableHead>
      <TableBody sx={bodySx}>
        {rows.map((row, rowIdx) => (
          <TableRow
            key={`row-${uuid_row}-${rowIdx}`}
            onClick={clickAble ? () => handleClickAble(row) : null}
            sx={
              clickAble || onClickColumn
                ? { '&:hover': { bgcolor: 'grey.10', cursor: 'pointer' } }
                : null
            }
          >
            {row.map((data, colIdx) => (
              <TableCell
                key={`col-${uuid_row}-${colIdx}`}
                onClick={() => handleClickColumn(row, colIdx)}
              >
                <Box>
                  {specialColumns && specialColumns[colIdx]
                    ? specialColumns[colIdx](data, rowIdx)
                    : data}
                </Box>
              </TableCell>
            ))}
            {withAction ? (
              <TableCell align="right">
                <ActionMenu actions={actions} rowData={row} />
              </TableCell>
            ) : null}
          </TableRow>
        ))}
      </TableBody>
    </Table>
  );
};

export default BaseTable;
