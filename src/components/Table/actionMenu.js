import React, { useState, Fragment } from 'react';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import Box from '@mui/material/Box';
import ListItemText from '@mui/material/ListItemText';
import ListItemIcon from '@mui/material/ListItemIcon';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import { Delete, EditSquare } from 'react-iconly';

const menuItemContent = {
  edit: (
    <Fragment>
      <ListItemIcon>
        <EditSquare set="broken" />
      </ListItemIcon>
      <ListItemText>Edit</ListItemText>
    </Fragment>
  ),
  delete: (
    <Fragment>
      <ListItemIcon>
        <Delete set="broken" />
      </ListItemIcon>
      <ListItemText>Delete</ListItemText>
    </Fragment>
  )
};

/**
 *
 * @param {{
 *  actions: {
 *    type: "edit" | "delete",
 *    handler: (rowData: any) => void,
 *    custom?: (rowData: any) => JSX.Element
 *  }[],
 *  rowData: any
 * }} props
 * @returns {JSX.Element}
 */
export default function ActionMenu(props) {
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleMenuItemClick = (handler) => () => {
    if (handler) {
      handler(props.rowData);
    }
    handleClose();
  };

  return (
    <Box component={'span'}>
      <Tooltip title="Action Menu">
        <IconButton
          id="row-action-button"
          aria-controls="row-action-menu"
          aria-haspopup="true"
          aria-expanded={open ? 'true' : undefined}
          onClick={handleClick}
        >
          <MoreVertIcon />
        </IconButton>
      </Tooltip>
      <Menu
        id="row-action-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        MenuListProps={{
          'aria-labelledby': 'row-action-button'
        }}
      >
        {props.actions.map((act, idx) => {
          if (act.custom) {
            return (
              <div key={idx} onClick={handleMenuItemClick()} role={'button'}>
                {act.custom(props.rowData)}
              </div>
            );
          }

          return (
            <MenuItem onClick={handleMenuItemClick(act.handler)} key={idx}>
              {menuItemContent[act.type]}
            </MenuItem>
          );
        })}
      </Menu>
    </Box>
  );
}
