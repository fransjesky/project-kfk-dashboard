import React from 'react';
import MuiMenuItem from '@mui/material/MenuItem';

const MenuItem = ({ className, ...props }) => {
  return <MuiMenuItem disableRipple className={`XcMenuItem ${className}`} {...props} />;
};

export default MenuItem;
