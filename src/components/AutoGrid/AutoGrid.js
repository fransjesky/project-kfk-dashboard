import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Box from '@mui/material/Box';
import { useContainerDimensions } from 'utils/hooks';

const AutoGrid = ({ children, gap = 16, cardWidth = 290, onChangeWidth }) => {
  const [containerRef, setContainerRef] = useState();
  const { width } = useContainerDimensions(containerRef);

  useEffect(() => {
    if (!width || !onChangeWidth) return;
    onChangeWidth({ width, cardWidth, gap });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [width, containerRef]);

  return (
    <div ref={setContainerRef} style={{ width: '100%' }}>
      {containerRef && (
        <Box sx={{ display: 'flex', flexWrap: 'wrap', gap: `${gap}px` }}>{children}</Box>
      )}
    </div>
  );
};

AutoGrid.propTypes = {
  children: PropTypes.node,
  cardWidth: PropTypes.number,
  gap: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  onChangeWidth: PropTypes.func
};

export default AutoGrid;
