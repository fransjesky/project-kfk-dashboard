import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Icon } from '@iconify/react';
import { NavLink as RouterLink, matchPath, useLocation } from 'react-router-dom';
import { ChevronDown, ChevronUp, Iconly } from 'react-iconly';
// material
import { styled } from '@mui/material/styles';
import {
  Box,
  List,
  Collapse,
  ListItemText,
  ListItemIcon,
  ListItemButton,
  Grid
} from '@mui/material';

// ----------------------------------------------------------------------

const ListItemStyle = styled((props) => <ListItemButton disableGutters {...props} />)(
  ({ theme }) => ({
    ...theme.typography.body2,
    height: 48,
    position: 'relative',
    textTransform: 'capitalize',
    paddingLeft: theme.spacing(5),
    paddingRight: theme.spacing(2.5),
    color: theme.palette.grey[40],
    fontWeight: theme.typography.fontWeightMedium
  })
);

const ListItemIconStyle = styled(ListItemIcon)({
  width: 22,
  height: 22,
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center'
});

// ----------------------------------------------------------------------

NavItem.propTypes = {
  item: PropTypes.object,
  active: PropTypes.func
};

function NavItem({ item, active, sx }) {
  const isActiveRoot = active(item.path);

  const { title, path, icon, children, onClick } = item;
  const [open, setOpen] = useState(false);

  useEffect(() => {
    setOpen(isActiveRoot);
  }, [isActiveRoot]);

  const handleOpen = () => {
    setOpen((prev) => !prev);
  };

  const activeRootStyle = {
    color: 'white.main',
    backgroundImage:
      'linear-gradient(94.2deg, rgba(255, 157, 66, 0.094) -2.34%, rgba(229, 57, 69, 0) 108.25%);',
    fontWeight: 'fontWeightMedium',
    '&:before': { display: 'block' }
  };

  const activeSubStyle = {
    color: 'white.main',
    fontWeight: 'fontWeightMedium'
  };

  if (children?.length) {
    return (
      <>
        <ListItemStyle
          onClick={handleOpen}
          sx={{
            ...(isActiveRoot && activeRootStyle)
          }}
        >
          {isActiveRoot && (
            <div
              style={{
                position: 'absolute',
                top: 0,
                left: 0,
                backgroundColor: '#FF9D42',
                width: '6px',
                height: '48px',
                borderRadius: '0 3.2px 3.2px 0',
                boxShadow: '3.2551px 0px 12.2066px rgba(252, 140, 37, 0.4)'
              }}
            ></div>
          )}
          <Box sx={{ ml: -3.5, mt: 1 }}>
            <Box component={Icon} sx={{ width: 16, height: 16 }}>
              {open ? <ChevronUp set="broken" /> : <ChevronDown set="broken" />}
            </Box>
          </Box>
          <ListItemIconStyle sx={{ ml: 1.5 }}>
            {icon && <Iconly set="broken" name={icon} />}
          </ListItemIconStyle>
          <ListItemText disableTypography primary={title} />
        </ListItemStyle>

        <Collapse in={open} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            {children.map((item) => {
              const { title, path } = item;
              const isActiveSub = active(path);

              return (
                <ListItemStyle
                  key={title}
                  component={RouterLink}
                  to={path}
                  sx={{
                    ...(isActiveSub && activeSubStyle)
                  }}
                >
                  <ListItemText disableTypography primary={title} sx={{ ml: 1.5 }} />
                </ListItemStyle>
              );
            })}
          </List>
        </Collapse>
      </>
    );
  }

  return (
    <ListItemStyle
      component={onClick ? 'div' : RouterLink}
      onClick={onClick ? onClick : ''}
      to={onClick ? null : path}
      sx={{ ...sx, ...(isActiveRoot && activeRootStyle) }}
    >
      {isActiveRoot && (
        <div
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
            backgroundColor: '#FF9D42',
            width: '6px',
            height: '48px',
            borderRadius: '0 3.2px 3.2px 0',
            boxShadow: '3.2551px 0px 12.2066px rgba(252, 140, 37, 0.4)'
          }}
        ></div>
      )}
      <ListItemIconStyle sx={{ ml: 1 }}>
        {icon && <Iconly set="broken" name={icon} />}
      </ListItemIconStyle>
      <ListItemText disableTypography primary={title} />
    </ListItemStyle>
  );
}

NavSection.propTypes = {
  navConfig: PropTypes.array
};

export default function NavSection({ navConfig, secondConfig, ...other }) {
  const { pathname } = useLocation();

  const match = (path) => {
    if (!path) return false;
    if (path === '/') {
      if (pathname == '/enquiries') {
        return matchPath('/', { path, end: false }).isExact;
      }
      return matchPath(pathname, { path, end: false }).isExact;
    }
    return !!matchPath(pathname, { path, end: false });
  };

  const matchExact = (path) => {
    const result = matchPath(pathname, { path, end: false });
    if (!result) return false;
    return result.isExact;
  };

  return (
    <Grid container spacing={12} {...other}>
      <Grid item xs={12} sx={{ ml: 3 }}>
        <List disablePadding>
          <NavItem
            key={'Overview'}
            item={{ title: 'Overview', path: '/', icon: 'Category' }}
            active={match}
            activeExact={matchExact}
          />
          {navConfig.map((item) => (
            <NavItem key={item.title} item={item} active={match} activeExact={matchExact} />
          ))}
        </List>
      </Grid>
      <Grid item xs={12} sx={{ ml: 3 }}>
        <List>
          {secondConfig.map((item) => (
            <NavItem
              key={item.title}
              item={item}
              active={match}
              sx={{ position: 'relative' }}
              activeExact={matchExact}
            />
          ))}
        </List>
      </Grid>
    </Grid>
  );
}
