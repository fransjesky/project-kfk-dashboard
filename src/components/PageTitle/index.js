import React from 'react';
import { useHistory } from 'react-router';
import { Box, IconButton, Typography } from '@mui/material';
import { ArrowLeft } from 'react-iconly';

const PageTitle = ({ title, withBackButton }) => {
  const history = useHistory();

  return (
    <Box display="flex" flexDirection="row" alignItems="center">
      {withBackButton && (
        <IconButton onClick={() => history.goBack()} sx={{ mr: 1.5 }}>
          <ArrowLeft set="broken" primaryColor="black" size="large" />
        </IconButton>
      )}
      <Typography variant="h5" fontWeight={700} sx={{ cursor: 'default' }}>
        {title}
      </Typography>
    </Box>
  );
};

export default PageTitle;
