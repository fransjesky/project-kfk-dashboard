import React, { useState } from 'react';
import { Box, Button } from '@mui/material';
import { styled } from '@mui/material/styles';

const Wrap = styled(Box)(() => ({
  padding: '40px 60px',
  backgroundColor: '#FFFFFF',
  borderRadius: '8px',
  boxShadow: '0 6px 15px -2px rgba(0, 0, 0, 0.05)'
}));

const ActionButton = styled(Box)(() => ({
  display: 'flex',
  justifyContent: 'flex-start',
  flexDirection: 'row-reverse',
  marginTop: '50px'
}));

const ActionComponent = ({ label, disabled, onClick, ...props }) => {
  return (
    <Button disabled={disabled} onClick={onClick} sx={{ ml: '16px' }} {...props}>
      {label}
    </Button>
  );
};

const Board = (props) => {
  // eslint-disable-next-line
  const [state, setState] = useState({
    dialog: false,
    dialogClick: null,
    confirmButton: '',
    confirmTitle: '',
    confirmMessage: '',
    CustomContent: null
  });

  // eslint-disable-next-line
  const { children, actions, loading, disableLink } = props;

  const handleConfirm = ({ confirmAction, ...others }) => {
    setState({ dialog: true, dialogClick: confirmAction, ...others });
  };

  return (
    <Box bgcolor="other.dashboardBackground">
      <Wrap>
        {children}
        {actions && (
          <ActionButton>
            {actions &&
              actions.map((n, i) => {
                const params = {};
                let actionClick = n.onClick;
                if (n.confirm) {
                  params.confirmAction = n.onClick;
                  params.confirmButton = n.confirmButton;
                  params.confirmTitle = n.confirmTitle;
                  params.confirmMessage = n.confirmMessage;
                  actionClick = () => handleConfirm(params);
                }
                return (
                  <ActionComponent
                    key={i}
                    label={n.label}
                    disabled={n.noDisabled ? false : props.disabled}
                    onClick={(e) => {
                      props.confirmForm(false);
                      actionClick(e);
                    }}
                    {...n}
                  />
                );
              })}
          </ActionButton>
        )}
      </Wrap>
    </Box>
  );
};

export default Board;
