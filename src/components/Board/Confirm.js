import React from 'react';
import {
  Dialog,
  DialogTitle,
  DialogActions,
  DialogContent,
  DialogContentText,
  Slide,
  CircularProgress,
  Button
} from '@mui/material';

const Transition = (props) => {
  return <Slide direction="up" {...props} />;
};

const Confirm = (props) => {
  const { CustomContent } = props;

  return (
    <Dialog
      open={props.open}
      TransitionComponent={Transition}
      onClose={props.handleClose}
      className={props.loading ? 'confirm-no-root' : 'confirm-root'}
    >
      {props.loading ? (
        <div className="confirm-loading-root">
          <CircularProgress className="confirm-loading-item" thickness={3} size={60} />
        </div>
      ) : (
        <div className="confirm-root">
          <div className="confirm-head">
            <DialogTitle className="confirm-typo-head">
              {props.handleContent.title || 'Warning'}
            </DialogTitle>
          </div>

          {CustomContent ? (
            <CustomContent />
          ) : (
            <DialogContent className="confirm-content">
              <DialogContentText className="confirm-message">
                {props.handleContent.message || 'Warning'}
              </DialogContentText>
            </DialogContent>
          )}

          <DialogActions>
            <Button onClick={props.handleClose}>Cancel</Button>
            <Button className="mr-2" color="primary" onClick={props.handleAction}>
              {props.handleContent?.button?.title || 'Yes'}
            </Button>
          </DialogActions>
        </div>
      )}
    </Dialog>
  );
};

export default Confirm;
