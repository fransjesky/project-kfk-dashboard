import React from 'react';
import PropTypes from 'prop-types';
// material
import { styled } from '@mui/material/styles';
import { Box, Typography, Avatar as MuiAvatar, IconButton } from '@mui/material';
// components
import Card from 'components/Card';
import Dropdown from 'components/Dropdown';
// icons
import MoreVertIcon from '@mui/icons-material/MoreVert';
import { EditSquare, Delete } from 'react-iconly';
import Vector from 'assets/svg/Vector1.svg';

const AVATAR_SIZE = 56;

const Avatar = styled((props) => <MuiAvatar {...props} />)({
  width: AVATAR_SIZE,
  height: AVATAR_SIZE
});

const Head = styled(Box)({
  display: 'grid',
  gridTemplateColumns: `${AVATAR_SIZE}px 120px auto`,
  gap: '18px'
});

const ClientName = styled((props) => (
  <Typography component="p" variant="b1" fontWeight="bold" {...props} />
))({
  overflow: 'hidden',
  WebkitLineClamp: 2,
  display: '-webkit-box',
  WebkitBoxOrient: 'vertical',
  textOverflow: 'ellipsis',
  maxHeight: '48px',
  whiteSpace: 'normal',
  wordWrap: 'break-word'
});

const ClientPosition = styled((props) => (
  <Typography component="p" variant="b2" color="secondary.100" {...props} />
))({
  overflow: 'hidden',
  WebkitLineClamp: 2,
  display: '-webkit-box',
  WebkitBoxOrient: 'vertical',
  textOverflow: 'ellipsis',
  maxHeight: '48px',
  whiteSpace: 'normal',
  wordWrap: 'break-word'
});

const Testimony = styled((props) => (
  <Typography component="p" variant="b2" color="grey.80" {...props} />
))({
  margin: '16px 0',
  overflow: 'hidden',
  WebkitLineClamp: 5,
  display: '-webkit-box',
  WebkitBoxOrient: 'vertical',
  textOverflow: 'ellipsis',
  whiteSpace: 'normal',
  lineHeight: '17.6px',
  wordWrap: 'break-word'
});

const DropdownButton = () => (
  <IconButton>
    <MoreVertIcon />
  </IconButton>
);

const CardTestimony = (props) => {
  const { clientName, companyName, position, testimony, photo, onDelete, onEdit } = props;
  return (
    <Card variant="secondary" width="290px">
      <Box
        sx={{
          p: 3,
          height: '280px',
          position: 'relative',
          overflow: 'hidden',
          borderRadius: 'inherit'
        }}
      >
        <Head>
          <Avatar src={photo} />
          <Box>
            <ClientName>{clientName}</ClientName>
            <ClientPosition>{position + ' / ' + companyName}</ClientPosition>
          </Box>
          <Dropdown
            button={<DropdownButton />}
            items={[
              {
                label: 'Edit',
                icon: <EditSquare set="broken" />,
                onClick: onEdit
              },
              {
                label: 'Delete',
                icon: <Delete set="broken" />,
                onClick: onDelete
              }
            ]}
          />
        </Head>
        <Testimony>{'"' + testimony + '"'}</Testimony>
        <img src={Vector} alt="" style={{ position: 'absolute', left: '-20px', bottom: '-22px' }} />
      </Box>
    </Card>
  );
};

CardTestimony.defaultProps = {
  clientName: PropTypes.string,
  companyName: PropTypes.string,
  position: PropTypes.string,
  testimony: PropTypes.string,
  photo: PropTypes.string,
  onDelte: PropTypes.func,
  onEdit: PropTypes.func
};

export default CardTestimony;
