import Ajv from 'ajv';

export const fileUploaderSchema = {
  type: 'object',
  properties: {
    elementId: { type: 'string' },
    allowedTypes: {
      type: 'array',
      default: ['jpg', 'png', 'pdf'],
      items: { type: 'string' }
    },
    maxSizeMb: { type: 'number', default: 2 },
    isMulti: { type: 'boolean', default: false },
    onChange: { default: null },
    onLoading: { default: null },
    onError: { default: null },
    handler: { default: null }
  },
  required: ['elementId'],
  additionalProperties: true
};

export const imageUploaderSchema = {
  type: 'object',
  properties: {
    ...fileUploaderSchema.properties,
    allowedTypes: {
      type: 'array',
      default: ['jpg', 'png'],
      items: { type: 'string' }
    },
    initialImageSrc: { type: ['string', 'null'], default: null }
  },
  required: ['elementId'],
  additionalProperties: true
};

export const anyUploaderSchema = {
  type: 'object',
  properties: {
    ...fileUploaderSchema.properties,
    initialImageSrc: { type: ['string', 'null'], default: null },
    type: { type: 'string', default: 'image' },
    size: { type: 'string', default: 'medium' },
    color: { type: 'string', default: 'primary' },
    buttonLabel: { type: 'string', default: 'Upload File' },
    withNote: { type: 'boolean', default: true },
    isErrorInput: { type: 'boolean', default: false }
  },
  required: ['elementId'],
  additionalProperties: true
};

const ajv = new Ajv({ useDefaults: true, allowUnionTypes: true });

export function validateDataSchema(data, schema) {
  let modifiedData = { ...data };
  const validate = ajv.compile(schema);

  if (validate(modifiedData)) {
    return modifiedData;
  }
  return false;
}
