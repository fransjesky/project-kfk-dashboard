import React, { Fragment, useCallback, useEffect, useState, useRef } from 'react';
import { useDispatch } from 'react-redux';
import { styled } from '@mui/material/styles';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import PhotoCameraIcon from '@mui/icons-material/PhotoCamera';
import RemoveIcon from '@mui/icons-material/Remove';
import CloudUploadIcon from '@mui/icons-material/CloudUpload';
import CircularProgress from '@mui/material/CircularProgress';
import noImgSrc from './assets/no-image.png';
import {
  getFileUploaderProps,
  getHtmlInputProps,
  isValidFileSizes,
  isValidFileTypes,
  uploadFiles
} from './UploaderHelper';
import { anyUploaderSchema } from './UploaderValidation';
import { notif } from 'redux/actions/Global';
import Dropzone from '../../components/Form/Dropzone';

// -- file input

const ImageUploaderBox = styled('div')({
  position: 'relative',
  display: 'block'
});

const UploaderLabel = styled('label')({
  position: 'relative',
  display: 'block'
});

const FileInput = styled('input')({
  display: 'none'
});

const NoteBar = styled('div')({
  marginTop: '30px',
  marginBottom: '15px'
});

const NoteTypography = styled(Typography)({
  fontSize: '.75rem',
  marginBottom: '.25rem',
  color: 'GrayText'
});

// -- icon buttons

const UploaderIconButton = styled(IconButton)({
  position: 'absolute',
  right: '0',
  bottom: '0',
  color: 'gray',
  backgroundColor: 'white',
  boxShadow: '0px 2px 8px 1px rgba(0, 0, 0, 0.08)',
  '&:hover': {
    backgroundColor: 'white'
  },
  transform: 'translate(50%, 50%)'
});

const RemoveIconButton = styled(IconButton)({
  position: 'absolute',
  right: '0',
  top: '0',
  color: 'red',
  backgroundColor: 'white',
  boxShadow: '0px 2px 8px 1px rgba(0, 0, 0, 0.08)',
  '&:hover': {
    backgroundColor: 'white'
  },
  transform: 'translate(50%, -50%)'
});

// -- image preview

// regular preview container
const ImagePreviewContainer = styled('div')({
  position: 'relative',
  width: '100%',
  paddingTop: '100%',
  border: '1px solid gray',
  borderRadius: '.5rem',
  '&.error': {
    border: '1px solid #D2243D'
  }
});

const ImagePreviewContent = styled('div')({
  position: 'absolute',
  top: '0',
  left: '0',
  bottom: '0',
  right: '0',
  cursor: 'pointer',
  display: 'flex',
  alignItems: 'center'
});

const ImagePreview = styled('img')({
  width: '100%',
  height: 'auto',
  maxWidth: '100%',
  maxHeight: '100%'
});

const CenteredCircularProgress = styled(CircularProgress)({
  position: 'absolute',
  width: '30px !important',
  height: '30px !important',
  top: 'calc(50% - 15px)',
  left: 'calc(50% - 15px)'
});

const LoadingBackdrop = styled('div')({
  position: 'absolute',
  top: '0',
  left: '0',
  bottom: '0',
  right: '0',
  backgroundColor: 'rgba(255, 255, 255, .5)'
});

// rounded preview container
// rounded is fixed size because
// camera icon is dificult to be positioned with relative size
const RoundedUploaderLabel = styled('label')({
  position: 'relative',
  display: 'inline-block',
  width: 'auto'
});

const RoundedImagePreviewRContainer = styled('div')({
  position: 'relative',
  width: '120px',
  height: '120px',
  borderRadius: '50%',
  overflow: 'hidden',
  border: '1px solid rgba(0, 0, 0, 0.1)',
  '&.large': {
    width: '160px',
    height: '160px'
  },
  '&.small': {
    width: '80px',
    height: '80px'
  }
});

const RoundedImagePreviewContent = styled('div')({
  position: 'absolute',
  top: '0',
  left: '0',
  bottom: '0',
  right: '0',
  cursor: 'pointer',
  display: 'flex',
  alignItems: 'center',
  overflow: 'hidden',
  flex: '1',
  backgroundPosition: 'center center',
  backgroundSize: 'cover'
});

const RoundedUploaderIconButton = styled(IconButton)({
  position: 'absolute',
  right: '0',
  bottom: '0',
  color: 'gray',
  backgroundColor: 'white',
  boxShadow: '0px 2px 8px 1px rgba(0, 0, 0, 0.08)',
  '&:hover': {
    backgroundColor: 'white'
  }
});

// -- button
const ButtonUploaderLabel = styled('label')({
  position: 'relative',
  display: 'inline-block'
});

// -- helpers
function toUpperJoin(arr) {
  return arr.map((v) => v.toUpperCase()).join(', ');
}

// -- components

/**
 * Base uploader component, consists of hidden HTML input element.
 *
 * @param {{
 *  elementId: string,
 *  allowedTypes?: string[],
 *  maxSizeMb?: number,
 *  isMulti?: boolean,
 *  onChange?: (value: any) => void
 *  onLoading?: (isLoading: boolean) => void
 *  onError?: (message: string) => void,
 *  handler?: (files: FileList) => Promise<any>
 * }} props
 * @returns {JSX.Element}
 */
export function FileUploader(props) {
  const [validProps, setValidProps] = useState(getFileUploaderProps(props));
  const [htmlInputProps, setHtmlInputProps] = useState({});
  const fileInputRef = useRef(null);
  const [uploading, setUploading] = useState(false);

  // handle file change
  const handleFileInputChange = useCallback(
    (event) => {
      if (!event.target || !event.target.value || !event.target.files) {
        return;
      }

      // get files
      let files = event.target.files;
      if (files.length <= 0) {
        return;
      }

      // validation
      if (!isValidFileTypes(files, validProps.allowedTypes)) {
        validProps.onError(
          `Sorry, this file type is not supported. Allowed type: ${toUpperJoin(
            validProps.allowedTypes
          )}`
        );
        if (fileInputRef && fileInputRef.current) {
          fileInputRef.current.value = null;
        }
        return;
      }

      if (!isValidFileSizes(files, validProps.maxSizeMb)) {
        validProps.onError(`Sorry, your file exceeds ${validProps.maxSizeMb.toFixed(1)}MB`);
        if (fileInputRef && fileInputRef.current) {
          fileInputRef.current.value = null;
        }
        return;
      }

      // upload
      const uploadHandler = validProps.handler ? validProps.handler : uploadFiles;
      validProps.onLoading(true);
      setUploading(true);
      uploadHandler(files)
        .then((results) => {
          validProps.onChange(results);
        })
        .catch((reason) => {
          validProps.onError(typeof reason === 'string' ? reason : 'Failed to upload image');
        })
        .then(() => {
          validProps.onLoading(false);
          setUploading(false);
          if (fileInputRef && fileInputRef.current) {
            fileInputRef.current.value = null;
          }
        });
    },
    [validProps]
  );

  // init
  useEffect(() => {
    let validated = getFileUploaderProps(props);
    setValidProps(validated);
    setHtmlInputProps(getHtmlInputProps(validated));
  }, [props]);

  return (
    <Fragment>
      <FileInput
        type="file"
        {...htmlInputProps}
        ref={fileInputRef}
        onChange={handleFileInputChange}
        disabled={uploading}
      />
    </Fragment>
  );
}

/**
 * Base dropzone uploader component, utilizes external component.
 *
 * @param {{
 *  elementId: string,
 *  allowedTypes?: string[],
 *  maxSizeMb?: number,
 *  isMulti?: boolean,
 *  onChange?: (value: any) => void
 *  onLoading?: (isLoading: boolean) => void
 *  onError?: (message: string) => void
 *  handler?: (files: FileList) => Promise<any>
 * }} props
 * @returns {JSX.Element}
 */
export function DropzoneUploader(props) {
  const [validProps, setValidProps] = useState(getFileUploaderProps(props));
  const [htmlInputProps, setHtmlInputProps] = useState({});
  const [uploading, setUploading] = useState(false);

  // handle file change
  const handleAcceptedFiles = useCallback(
    (acceptedFiles) => {
      // get files
      if (acceptedFiles.length <= 0) {
        validProps.onChange([]);
        return;
      }

      // upload
      const uploadHandler = validProps.handler ? validProps.handler : uploadFiles;
      validProps.onLoading(true);
      setUploading(true);
      uploadHandler(acceptedFiles)
        .then((results) => {
          validProps.onChange(results);
        })
        .catch((reason) => {
          validProps.onError(typeof reason === 'string' ? reason : 'Failed to upload image');
        })
        .then(() => {
          validProps.onLoading(false);
          setUploading(false);
        });
    },
    [validProps]
  );

  const handleRejectedFiles = useCallback(
    (rejectedFiles) => {
      if (
        rejectedFiles.length &&
        rejectedFiles[0].errors &&
        rejectedFiles[0].errors.length &&
        rejectedFiles[0].errors[0].message
      ) {
        if (rejectedFiles[0].errors[0].message.startsWith('File is larger than')) {
          const splittedMsg = rejectedFiles[0].errors[0].message.split(' ');
          const maxFileSize = parseInt(splittedMsg[splittedMsg.length - 2]) / 1024000;
          validProps.onError(`Sorry, your file exceeds ${maxFileSize.toFixed(1)}MB`);
        } else if (rejectedFiles[0].errors[0].message.startsWith('File type must be')) {
          validProps.onError(`Sorry, this file type is not supported. Allowed type: PNG`);
        } else {
          validProps.onError(`${rejectedFiles[0].errors[0].message}`);
        }
      } else {
        validProps.onError(`File dropping is error.`);
      }
    },
    [validProps]
  );

  // init
  useEffect(() => {
    let validated = getFileUploaderProps(props);
    setValidProps(validated);
    setHtmlInputProps(getHtmlInputProps(validated));
  }, [props]);

  return (
    <Fragment>
      <Dropzone
        maxSize={validProps.maxSizeMb * 1024000}
        customProps={{ ...htmlInputProps }}
        onAcceptedFiles={handleAcceptedFiles}
        onRejectedFiles={handleRejectedFiles}
        uploading={uploading}
      />
    </Fragment>
  );
}

/**
 * Default image uploader
 *
 * @param {*} props
 */
export function ImageUploader(props) {
  const allowedTypes = ['jpg', 'png'];
  return <AnyUploader {...props} allowedTypes={allowedTypes} />;
}

/**
 * Default file (non-image) uploader
 *
 * @param {*} props
 */
export function ButtonFileUploader(props) {
  const allowedTypes = ['pdf', 'xls', 'doc'];
  const type = 'button';
  return <AnyUploader {...props} allowedTypes={allowedTypes} type={type} />;
}

/**
 * General uploader
 *
 * @param {{
 *  elementId: string,
 *  allowedTypes?: string[],
 *  maxSizeMb?: number,
 *  isMulti?: boolean,
 *  onChange?: (value: any) => void,
 *  onLoading?: (isLoading: boolean) => void,
 *  onError?: (message: string) => void,
 *  initialImageSrc?: string | null,
 *  type?: "image" | "button" | "profpic" | "dropzone",
 *  size?: "small" | "medium" | "large",
 *  color?: string,
 *  buttonLabel?: string,
 *  handler?: (files: FileList) => Promise<any>
 *  withNote?: boolean,
 *  isErrorInput?: boolean
 * }} props
 * @returns {JSX.Element}
 */
export function AnyUploader(props) {
  const [validProps, setValidProps] = useState(getFileUploaderProps(props, anyUploaderSchema));
  const [isLoading, setIsLoading] = useState(false);
  const [imageSrc, setImageSrc] = useState(null);
  const dispatch = useDispatch();
  const [value, setValue] = useState([]);

  const handleChange = useCallback(
    (value) => {
      if (value.length) {
        setValue(value);

        if (!validProps.isMulti) {
          let imageSrc = value[0].data.fileOriginal || value[0].data.fileSm || null;
          setImageSrc(imageSrc);
        }

        validProps.onChange(value);
      }
    },
    [validProps]
  );

  const handleError = useCallback(
    (message) => {
      validProps.onError(message);
      dispatch(notif({ open: true, variant: 'error', message, title: 'Failed to upload image' }));
    },
    [validProps, dispatch]
  );

  const handleLoading = useCallback(
    (isLoading) => {
      setIsLoading(isLoading);
      validProps.onLoading(isLoading);
    },
    [validProps]
  );

  const clearImages = useCallback(() => {
    setValue([]);
    setImageSrc(null);
    validProps.onChange([]);
  }, [validProps]);

  // init
  useEffect(() => {
    let validProps = getFileUploaderProps(props, anyUploaderSchema);
    setValidProps(validProps);
  }, [props]);

  if (validProps.type === 'button') {
    return (
      <Fragment>
        <ButtonUploaderLabel htmlFor={validProps.elementId}>
          <Button
            disabled={isLoading}
            variant="contained"
            component="span"
            color={validProps.color}
            startIcon={<CloudUploadIcon />}
          >
            {validProps.buttonLabel}
          </Button>
          <FileUploader
            {...validProps}
            onChange={handleChange}
            onError={handleError}
            onLoading={handleLoading}
          />
        </ButtonUploaderLabel>
      </Fragment>
    );
  }

  if (validProps.type === 'profpic') {
    return (
      <Fragment>
        <RoundedUploaderLabel htmlFor={validProps.elementId}>
          <RoundedImagePreviewRContainer className={validProps.size}>
            <RoundedImagePreviewContent
              style={{
                backgroundImage: `url(${imageSrc || validProps.initialImageSrc || noImgSrc})`
              }}
            >
              {!isLoading ? null : (
                <Fragment>
                  <LoadingBackdrop />
                  <CenteredCircularProgress />
                </Fragment>
              )}
            </RoundedImagePreviewContent>
          </RoundedImagePreviewRContainer>
          <RoundedUploaderIconButton
            color="primary"
            aria-label="upload picture"
            component="span"
            size={validProps.size}
          >
            <PhotoCameraIcon fontSize={validProps.size} />
          </RoundedUploaderIconButton>
          <FileUploader
            {...validProps}
            isMulti={false}
            onChange={handleChange}
            onError={handleError}
            onLoading={handleLoading}
          />
        </RoundedUploaderLabel>
      </Fragment>
    );
  }

  if (validProps.type === 'dropzone') {
    return (
      <DropzoneUploader
        {...validProps}
        onChange={handleChange}
        onError={handleError}
        onLoading={handleLoading}
      />
    );
  }

  return (
    <Fragment>
      <ImageUploaderBox>
        <UploaderLabel htmlFor={validProps.elementId}>
          <ImagePreviewContainer className={validProps.isErrorInput && 'error'}>
            <ImagePreviewContent>
              <ImagePreview src={imageSrc || validProps.initialImageSrc || noImgSrc} alt="" />
              {!isLoading ? null : (
                <Fragment>
                  <LoadingBackdrop />
                  <CenteredCircularProgress />
                </Fragment>
              )}
            </ImagePreviewContent>
          </ImagePreviewContainer>
          <UploaderIconButton color="primary" aria-label="upload picture" component="span">
            <PhotoCameraIcon />
          </UploaderIconButton>
          <FileUploader
            {...validProps}
            onChange={handleChange}
            onError={handleError}
            onLoading={handleLoading}
          />
        </UploaderLabel>
        {!validProps.isMulti && (value.length || validProps.initialImageSrc) ? (
          <RemoveIconButton
            color="primary"
            aria-label="remove picture"
            component="span"
            onClick={() => {
              clearImages();
            }}
          >
            <RemoveIcon />
          </RemoveIconButton>
        ) : null}
      </ImageUploaderBox>
      {validProps.withNote && (
        <NoteBar>
          {validProps.maxSizeMb ? (
            <NoteTypography component={'div'}>
              Maximum Size: {validProps.maxSizeMb}MB
            </NoteTypography>
          ) : null}
          {validProps.allowedTypes ? (
            <NoteTypography component={'div'}>
              {'File Types: '}
              {validProps.allowedTypes.map((type) => type.toUpperCase()).join(', ')}
            </NoteTypography>
          ) : null}
        </NoteBar>
      )}
    </Fragment>
  );
}

/**
 *
 * @param {{
 *  imageUrl: string,
 *  onDelete: (imageUrl: string) => void
 * }} props
 * @returns {JSX.Element}
 */
export function ImagePreviewBox(props) {
  const { imageUrl, onDelete } = props;

  return (
    <ImageUploaderBox>
      <ImagePreviewContainer>
        <ImagePreviewContent>
          <ImagePreview src={imageUrl || noImgSrc} alt="" />
        </ImagePreviewContent>
      </ImagePreviewContainer>
      <RemoveIconButton
        color="primary"
        aria-label="remove image"
        component="span"
        onClick={() => {
          onDelete(imageUrl);
        }}
      >
        <RemoveIcon />
      </RemoveIconButton>
    </ImageUploaderBox>
  );
}

export default function UploaderContainer({ isMulti = false, onChange = () => {} }) {
  return (
    <Grid container spacing={2}>
      <Grid item xs={6} md={4} lg={2}>
        <AnyUploader elementId={'singleImageUploader'} isMulti={isMulti} onChange={onChange} />
      </Grid>
    </Grid>
  );
}
