import { Fragment } from 'react';
import UploaderContainer from './UploaderContainer';

/**
 * @param {
 *  type: 'image'
 * } props
 */
export default function Uploader() {
  return (
    <Fragment>
      <UploaderContainer />
    </Fragment>
  );
}
