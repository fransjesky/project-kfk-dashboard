import { fileUploaderSchema, validateDataSchema } from './UploaderValidation';

// -- data fetching
import { BASE_URL, BASE_API_PATH } from '../../utils/url';
import { getUser } from '../../utils/localStorage';
import { axiosFetcherAuth } from '../Fetcher';

const mimeTypes = {
  pdf: ['application/pdf'],
  jpg: ['image/jpg', 'image/jpeg'],
  png: ['image/png'],
  xls: ['application/vnd.ms-excel'],
  xlsx: ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'],
  doc: ['application/msword'],
  docx: ['application/vnd.openxmlformats-officedocument.wordprocessingml.document']
};

/**
 * Get HTML input properties from valid FileUploader properties
 *
 * @param {{
 *  elementId: string,
 *  allowedTypes?: string[],
 *  isMulti?: boolean
 * }} validProps
 */
export function getHtmlInputProps(validProps) {
  let inputProps = {
    id: validProps.elementId,
    multiple: validProps.isMulti
  };

  if (validProps.allowedTypes.length) {
    inputProps.accept = validProps.allowedTypes.map((ext) => `.${ext}`).join(',');
  }

  return inputProps;
}

/**
 * Get validated properties of FileUploader
 *
 * @param {{
 *  elementId: string,
 *  allowedTypes?: string[],
 *  maxSizeMb?: number,
 *  isMulti?: boolean,
 *  onChange?: (value: any) => void
 *  onLoading?: (isLoading: boolean) => void
 *  onError?: (message: string) => void
 * }} props
 * @param {*} refSchema
 */
export function getFileUploaderProps(props, refSchema = false) {
  let validated = validateDataSchema(props, refSchema ? refSchema : fileUploaderSchema);

  if (!validated) {
    return {};
  }

  if (!validated.onChange) {
    validated.onChange = () => {};
  }

  if (!validated.onLoading) {
    validated.onLoading = () => {};
  }

  if (!validated.onError) {
    validated.onError = () => {};
  }

  return validated;
}

/**
 * Check whether file types are valid
 *
 * @param {FileList} files
 * @param {string[]} allowedTypes
 */
export function isValidFileTypes(files, allowedTypes) {
  let allowedMimes = allowedTypes.reduce((prev, fileType) => [...prev, ...mimeTypes[fileType]], []);

  for (let i = 0; i < files.length; i++) {
    if (!allowedMimes.includes(files[i].type)) {
      return false;
    }
  }

  return true;
}

/**
 * Check whether file sizes are valid
 *
 * @param {FileList} files
 * @param {number} maxSizeMb
 */
export function isValidFileSizes(files, maxSizeMb) {
  let maxSizeBytes = maxSizeMb * 1024000;

  for (let i = 0; i < files.length; i++) {
    if (files[i].size > maxSizeBytes) {
      return false;
    }
  }

  return true;
}

/**
 * Upload multiple files individually
 *
 * @param {FileList} files
 */
export function uploadFiles(files) {
  const url = `${BASE_URL}${BASE_API_PATH}upload`;
  const user = getUser();

  // Temporarily comment out
  // if (!user) {
  //   return Promise.reject('Unauthorized upload');
  // }

  let promises = [];
  for (let i = 0; i < files.length; i++) {
    promises.push(
      new Promise((resolve) => {
        let formData = new FormData();
        formData.append('image', files[i], files[i].name);

        return axiosFetcherAuth('Authorization', 'JWT ' + user.token)('post', url, {
          data: formData
        })
          .then((result) => {
            if (result.data) {
              resolve({ ...result, data: result.data.data });
            } else {
              resolve(result);
            }
          })
          .catch(() => {
            resolve({
              error: { message: 'Unknown uploading error' },
              statusCode: 500
            });
          });
      })
    );
  }

  return Promise.all(promises);
}

/**
 * Import files and return their contents
 *
 * @param {FileList} files
 */
export function importFiles(files, endpointPath = 'upload', inputFieldName = 'file') {
  const url = `${BASE_URL}${BASE_API_PATH}${endpointPath}`;
  const user = getUser();

  if (!user) {
    return Promise.reject('Unauthorized upload');
  }

  let promises = [];
  for (let i = 0; i < files.length; i++) {
    promises.push(
      new Promise((resolve) => {
        let formData = new FormData();
        formData.append(inputFieldName, files[i], files[i].name);

        return axiosFetcherAuth('Authorization', 'JWT ' + user.token)('post', url, {
          data: formData
        })
          .then((result) => {
            resolve(result);
          })
          .catch(() => {
            resolve({
              error: { message: 'Unknown uploading error' },
              statusCode: 500
            });
          });
      })
    );
  }

  return Promise.all(promises);
}
