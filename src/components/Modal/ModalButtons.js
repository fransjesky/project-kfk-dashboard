import React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/material/styles';
import { Box, Stack } from '@mui/material';

const BoxButtons = styled((props) => <Box {...props} />)({
  margin: '0 -56px',
  marginBottom: '-50px',
  padding: '24px 56px',
  display: 'flex',
  justifyContent: 'flex-end',
  marginTop: '16px'
});

const ModalButtons = ({ children, background = '#fff' }) => {
  return (
    <BoxButtons bgcolor={background}>
      <Stack spacing={2} direction="row">
        {children}
      </Stack>
    </BoxButtons>
  );
};

ModalButtons.propTypes = {
  children: PropTypes.node,
  background: PropTypes.string
};

export default ModalButtons;
