import React from 'react';
import PropTypes from 'prop-types';
import { Box, Typography } from '@mui/material';

const ModalHead = ({ title, subtitle }) => {
  return (
    <Box
      sx={{
        borderBottom: 'solid 1px #E5E5E5',
        position: 'sticky',
        top: '-56px',
        backgroundColor: '#fff',
        zIndex: 10,
        marginBottom: '24px',
        paddingBottom: '16px',
        marginTop: '-50px',
        paddingTop: '50px'
      }}
    >
      <Typography component="p" variant="h5" fontWeight="bold">
        {title}
      </Typography>
      <Typography component="p" variant="s1" fontWeight="medium">
        {subtitle}
      </Typography>
    </Box>
  );
};

ModalHead.propTypes = {
  title: PropTypes.string,
  subtitle: PropTypes.string
};

export default ModalHead;
