import { styled } from '@mui/material/styles';
import { Box } from '@mui/material';

const ModalContent = styled((props) => <Box {...props} />)({
  margin: '0 -56px',
  padding: '0 56px'
});

export default ModalContent;
