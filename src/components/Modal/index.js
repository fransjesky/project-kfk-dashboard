export { default as ModalHead } from './ModalHead';
export { default as ModalContent } from './ModalContent';
export { default as ModalButtons } from './ModalButtons';
export { default } from './Modal.js';
