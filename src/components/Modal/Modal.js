import React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/material/styles';
import { Modal as MuiModal, Fade, Backdrop } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';

const CloseButton = styled((props) => <CloseIcon {...props} />)({
  backgroundColor: '#FFF5EB', //orange10
  color: '#FFC186',
  width: '36px',
  height: '36px',
  padding: '6px',
  borderRadius: '50%',
  position: 'fixed',
  right: '56px',
  top: '56px',
  zIndex: 20,
  boxSizing: 'border-box',
  '&:hover': {
    cursor: 'pointer'
  }
});

const ModalContainer = styled('div')({
  left: '50%',
  top: '50%',
  transform: 'translate(-50%, -50%)',
  position: 'absolute'
});

const ScrollContainer = styled('div')({
  maxHeight: 'calc(100vh - 40px)',
  overflowY: 'auto',
  overflowX: 'hidden',
  borderRadius: '8px',
  background: '#fff',
  padding: '50px 56px',
  width: 'min-content'
});

const BackdropComponent = styled(Backdrop)({
  background: 'rgba(0, 0, 0, 0.25)'
});

const Modal = ({ children, isOpen, maxWidth, onClose }) => {
  return (
    <MuiModal
      open={isOpen}
      onClose={onClose}
      closeAfterTransitio
      BackdropProps={{
        timeout: 500
      }}
      BackdropComponent={BackdropComponent}
    >
      <Fade in={isOpen}>
        <ModalContainer className="ModalContainer">
          <CloseButton onClick={onClose} />
          <ScrollContainer sx={{ maxWidth: `${maxWidth}px` }}>{children}</ScrollContainer>
        </ModalContainer>
      </Fade>
    </MuiModal>
  );
};

Modal.propTypes = {
  children: PropTypes.node,
  isOpen: PropTypes.bool.isRequired,
  maxWidth: PropTypes.number,
  onClose: PropTypes.func
};

export default Modal;
