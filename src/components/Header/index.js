import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Search } from 'react-iconly';
import { Box, Button, Divider, InputAdornment, TextField } from '@mui/material';
import { styled } from '@mui/material/styles';
import { Add } from '@mui/icons-material';
import Breadcrumbs from 'components/Breadcrumbs';
import PageTitle from 'components/PageTitle';

const SearchBar = styled(TextField)(({ theme }) => ({
  width: '35ch',
  borderRadius: '8px',
  minHeight: 'unset',
  backgroundColor: `${theme.palette.grey[10]}`,
  '& .MuiOutlinedInput-root': {
    padding: '5px 15px'
  }
}));

const Header = ({
  routes,
  withBackButton,
  withSearchBar,
  keyword,
  onSearch,
  withButton,
  typeSearch = '',
  buttonText,
  onButtonClick
}) => {
  const [searchVal, setSearchVal] = useState(keyword || '');

  const handleSearchChange = (event) => {
    setSearchVal(event.target.value);
    onSearch(event.target.value);
  };

  return (
    <Box width={'100%'} marginBottom={4}>
      <Breadcrumbs items={routes} />
      {withSearchBar ? (
        withButton ? (
          <Box mt={1}>
            <PageTitle title={routes[routes.length - 1].label} withBackButton={withBackButton} />
            <Box display="flex" justifyContent="space-between" alignItems="center" mt={4}>
              <SearchBar
                value={keyword || searchVal}
                type={typeSearch}
                placeholder="Search"
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <Search set="broken" size="small" />
                    </InputAdornment>
                  )
                }}
                onChange={handleSearchChange}
              />
              <Button variant="primary" endIcon={<Add fontSize="large" />} onClick={onButtonClick}>
                {buttonText}
              </Button>
            </Box>
          </Box>
        ) : (
          <Box mt={1}>
            <PageTitle title={routes[routes.length - 1].label} withBackButton={withBackButton} />
            <Box mt={4}>
              <SearchBar
                value={searchVal}
                type={typeSearch}
                placeholder="Search"
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <Search set="broken" size="small" />
                    </InputAdornment>
                  )
                }}
                onChange={handleSearchChange}
              />
            </Box>
          </Box>
        )
      ) : withButton ? (
        <Box display="flex" justifyContent="space-between" alignItems="center" mt={1}>
          <PageTitle title={routes[routes.length - 1].label} withBackButton={withBackButton} />
          <Button variant="primary" endIcon={<Add fontSize="large" />} onClick={onButtonClick}>
            {buttonText}
          </Button>
        </Box>
      ) : (
        <Box mt={1}>
          <PageTitle title={routes[routes.length - 1].label} withBackButton={withBackButton} />
        </Box>
      )}
      <Divider sx={{ mt: 4, color: 'grey.20' }} />
    </Box>
  );
};

Header.propTypes = {
  routes: PropTypes.shape({ label: PropTypes.string, link: PropTypes.string }),
  withBackButton: PropTypes.bool,
  withSearchBar: PropTypes.bool,
  onSearch: PropTypes.func,
  typeSearch: PropTypes.string,
  withButton: PropTypes.bool,
  buttonText: PropTypes.string,
  onButtonClick: PropTypes.func
};

export default Header;
