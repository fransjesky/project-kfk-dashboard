import React from 'react';
import PropTypes from 'prop-types';
import { Grid, IconButton } from '@mui/material';
import SvgIconStyle from 'components/SvgIconStyle';
import Card from '../Card';
import Dropdown from 'components/Dropdown';

// icons
import MoreVertIcon from '@mui/icons-material/MoreVert';
import DeleteIcon from 'assets/svg/Delete.svg';
import EditIcon from 'assets/svg/Edit.svg';

function CardTechStack({
  title,
  category,
  color,
  logo,
  order,
  createdAt,
  id,
  onEdit,
  onDelete,
  width
}) {
  const style = {
    container: {
      padding: '1.625rem',
      display: 'flex'
    },
    image: {
      height: '3.5rem',
      width: '3.5rem',
      objectFit: 'scale-down'
    },
    main: {
      width: '100%',
      maxWidth: '7.125rem',
      marginLeft: '1rem',
      display: 'flex',
      flexDirection: 'column'
    },
    title: {
      marginBottom: '0.25rem',
      color: '#3D3D3D',
      fontFamily: `'satoshi-bold', sans-serif`,
      fontSize: '1em',
      fontWeight: '700',
      lineHeight: '1.5rem',
      textTransform: 'capitalize',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap'
    },
    category: {
      margin: '0',
      color: '#00A3FF',
      fontFamily: `'satoshi-regular', sans-serif`,
      fontSize: '1em',
      fontWeight: '400',
      lineHeight: '1.2rem',
      textTransform: 'capitalize',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap'
    },
    details: {
      margin: '0.75rem 1.5rem',
      display: 'flex',
      justifyContent: 'space-between'
    },
    detailsOrder: {
      color: color,
      fontFamily: `'satoshi-bold', sans-serif`,
      fontSize: '0.703em',
      fontWeight: '700',
      lineHeight: '0.843rem'
    },
    detailsCreatedAt: {
      color: '#616161',
      fontFamily: `'satoshi-regular', sans-serif`,
      fontSize: '0.703em',
      fontWeight: '400',
      lineHeight: '0.843rem',
      textTransform: 'capitalize'
    }
  };

  const DropdownButton = () => (
    <IconButton>
      <MoreVertIcon />
    </IconButton>
  );

  return (
    <Card color={color} width={width}>
      <Grid style={style.container}>
        <img src={logo} alt={title} style={style.image} />
        <Grid style={style.main}>
          <h1 style={style.title}>{title}</h1>
          <p style={style.category}>{category}</p>
        </Grid>
        <Dropdown
          button={<DropdownButton />}
          items={[
            {
              label: 'Edit',
              icon: <SvgIconStyle src={EditIcon} />,
              onClick: () => onEdit(id)
            },
            {
              label: 'Delete',
              icon: <SvgIconStyle src={DeleteIcon} />,
              onClick: () => onDelete(id, title)
            }
          ]}
        />
      </Grid>
      <Grid style={style.details}>
        <span style={style.detailsOrder}>Order: {order}</span>
        <span style={style.detailsCreatedAt}>{createdAt}</span>
      </Grid>
    </Card>
  );
}

CardTechStack.propTypes = {
  title: PropTypes.string.isRequired,
  category: PropTypes.string.isRequired,
  color: PropTypes.string,
  logo: PropTypes.string.isRequired,
  order: PropTypes.number,
  createdAt: PropTypes.string,
  onEdit: PropTypes.func,
  onDelete: PropTypes.func
};

export default CardTechStack;
