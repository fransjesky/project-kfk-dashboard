import React from 'react';
import { styled } from '@mui/material/styles';
import {
  Button,
  Box,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  IconButton,
  Typography,
  Container
} from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';

const StyledButton = styled(Button)(({ confirm }) => ({
  backgroundColor: confirm ? '#2E60FF' : '#FFFFFF',
  color: confirm ? '#FFFFFF' : '#2E60FF',
  border: confirm ? null : '1px solid #2E60FF',
  marginRight: confirm ? 0 : '8px',
  borderRadius: '8px',
  width: '143px',
  height: '42px',
  fontFamily: 'Satoshi',
  fontSize: '14px',
  fontWeight: 500,
  '&:hover': {
    backgroundColor: confirm ? '#2E60FFE0' : '#2E60FF0F'
  }
}));

const ConfirmationPopup = ({
  action,
  type,
  targetItem,
  isOpen,
  handleClose,
  handleConfirm,
  handleArchive,
  handleSave
}) => {
  return (
    <Dialog onClose={handleClose} open={isOpen}>
      <Container>
        {action === 'delete' ? (
          <>
            <Box display="flex" justifyContent="space-between" mt={3} mx={'20px'}>
              <DialogTitle sx={{ paddingRight: 0, width: '90%' }}>
                Are you sure you want to delete this {type}?
              </DialogTitle>
              <Box>
                <IconButton
                  aria-label="close"
                  onClick={handleClose}
                  sx={{ color: '#111111', width: '40px', height: '40px', mt: '10px' }}
                >
                  <CloseIcon />
                </IconButton>
              </Box>
            </Box>
            <DialogContent sx={{ marginX: '20px', paddingY: '8px', width: '90%' }}>
              <Typography gutterBottom>
                The {type} <b>'{targetItem}'</b> will be deleted. You will not be able to recover
                it.
              </Typography>
            </DialogContent>
            {type === 'article' ? (
              <DialogActions sx={{ marginX: '36px', marginTop: '8px', marginBottom: '32px' }}>
                <StyledButton onClick={handleConfirm}>Delete Permanently</StyledButton>
                <StyledButton confirm="true" onClick={handleArchive}>
                  Archive
                </StyledButton>
              </DialogActions>
            ) : (
              <DialogActions sx={{ marginX: '36px', marginTop: '8px', marginBottom: '32px' }}>
                <StyledButton onClick={handleClose}>Cancel</StyledButton>
                <StyledButton confirm="true" onClick={handleConfirm}>
                  Yes, delete
                </StyledButton>
              </DialogActions>
            )}
          </>
        ) : action === 'hide' ? (
          <>
            <Box display="flex" justifyContent="space-between" mt={3} mx={'20px'}>
              <DialogTitle sx={{ paddingRight: 0, width: '90%' }}>
                Are you sure you want to hide this {type}?
              </DialogTitle>
              <Box>
                <IconButton
                  aria-label="close"
                  onClick={handleClose}
                  sx={{ color: '#111111', width: '40px', height: '40px', mt: '10px' }}
                >
                  <CloseIcon />
                </IconButton>
              </Box>
            </Box>
            <DialogContent sx={{ marginX: '20px', paddingY: '8px', width: '90%' }}>
              <Typography gutterBottom>
                The {type} <b>'{targetItem}'</b> will be hidden. You will be able to unhide it.
              </Typography>
            </DialogContent>
            <DialogActions sx={{ marginX: '36px', marginTop: '8px', marginBottom: '32px' }}>
              <StyledButton onClick={handleClose}>Cancel</StyledButton>
              <StyledButton confirm="true" onClick={handleConfirm}>
                Yes, hide
              </StyledButton>
            </DialogActions>
          </>
        ) : action === 'replace' ? (
          <>
            <Box
              display="flex"
              justifyContent="space-between"
              alignItems={'flex-start'}
              mt={3}
              mx={'20px'}
            >
              <DialogTitle sx={{ paddingRight: 0, width: '90%' }}>
                Do you wish to replace the current highlighted {type}?
              </DialogTitle>
              <Box>
                <IconButton
                  aria-label="close"
                  onClick={handleClose}
                  sx={{ color: '#111111', width: '40px', height: '40px', mt: '10px' }}
                >
                  <CloseIcon />
                </IconButton>
              </Box>
            </Box>
            <DialogContent sx={{ marginX: '20px', paddingY: '8px', width: '90%' }}>
              <Typography gutterBottom>
                <b>'{targetItem.current}'</b> is currently being highlighted, would you like to
                disable this and highlight <b>'{targetItem.new}'</b> instead?
              </Typography>
            </DialogContent>
            <DialogActions sx={{ marginX: '36px', marginTop: '8px', marginBottom: '32px' }}>
              <StyledButton onClick={handleClose}>Cancel</StyledButton>
              <StyledButton confirm="true" onClick={handleConfirm}>
                Yes
              </StyledButton>
            </DialogActions>
          </>
        ) : action === 'leave-changes' ? (
          <>
            <Box
              display="flex"
              justifyContent="space-between"
              alignItems="flex-start"
              mt={3}
              mx={'20px'}
            >
              <DialogTitle sx={{ paddingRight: 0, width: '90%' }}>
                You have unsaved changes, are you sure want to leave?
              </DialogTitle>
              <Box>
                <IconButton
                  aria-label="close"
                  onClick={handleClose}
                  sx={{ color: '#111111', width: '40px', height: '40px', mt: '10px' }}
                >
                  <CloseIcon />
                </IconButton>
              </Box>
            </Box>
            <DialogContent sx={{ marginX: '20px', paddingY: '8px', width: '90%' }}>
              <Typography gutterBottom>
                Your news will be unsaved. Click “Save” to save the changes without publish it.
              </Typography>
            </DialogContent>
            <DialogActions sx={{ marginX: '36px', marginTop: '8px', marginBottom: '32px' }}>
              <StyledButton onClick={handleSave}>Save</StyledButton>
              <StyledButton confirm="true" onClick={handleConfirm}>
                Yes, leave
              </StyledButton>
            </DialogActions>
          </>
        ) : null}
      </Container>
    </Dialog>
  );
};

export default ConfirmationPopup;
