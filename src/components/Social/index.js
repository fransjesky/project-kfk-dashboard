import { useRef, useState } from 'react';
import { FacebookShareButton, FacebookIcon, TwitterShareButton, TwitterIcon } from 'react-share';
// materials
import { Button, Menu, MenuItem, SvgIcon, Tooltip } from '@mui/material';
import { styled } from '@mui/material/styles';
// components
import styles from './index.module.scss';
import copyLogo from './assets/copy-link.png';
// utils
import { useNotifier } from 'utils/hooks';
import { copyTextToClipboard } from 'utils/browser';

const ShareIcon = (props) => {
  return (
    <SvgIcon {...props} viewBox="0 0 32 32">
      <path
        d="M24 10.666C26.2091 10.666 28 8.87515 28 6.66602C28 4.45688 26.2091 2.66602 24 2.66602C21.7909 2.66602 20 4.45688 20 6.66602C20 8.87515 21.7909 10.666 24 10.666Z"
        stroke="#2E60FF"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M8 20C10.2091 20 12 18.2091 12 16C12 13.7909 10.2091 12 8 12C5.79086 12 4 13.7909 4 16C4 18.2091 5.79086 20 8 20Z"
        stroke="#2E60FF"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M24 29.334C26.2091 29.334 28 27.5431 28 25.334C28 23.1248 26.2091 21.334 24 21.334C21.7909 21.334 20 23.1248 20 25.334C20 27.5431 21.7909 29.334 24 29.334Z"
        stroke="#2E60FF"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M11.4531 18.0137L20.5598 23.3203"
        stroke="#2E60FF"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M20.5465 8.67969L11.4531 13.9864"
        stroke="#2E60FF"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </SvgIcon>
  );
};

const StyledButton = styled(Button)({
  color: 'initial',
  '&:hover': {
    backgroundColor: 'transparent'
  }
});

export const ShareButton = ({ children }) => {
  return (
    <StyledButton startIcon={<ShareIcon sx={{ color: 'transparent' }} />} size={'large'}>
      {children}
    </StyledButton>
  );
};

export const ShareMenu = ({ url }) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const { notifSuccess } = useNotifier();

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const inputRef = useRef(null);

  return (
    <span>
      <StyledButton
        startIcon={<ShareIcon sx={{ color: 'transparent' }} />}
        size={'large'}
        id="shareBtn"
        aria-controls="shareMenu"
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClick}
      >
        Share
      </StyledButton>
      <Menu
        id="shareMenu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          'aria-labelledby': 'shareBtn'
        }}
        className={styles.shareMenu}
      >
        <MenuItem onClick={handleClose}>
          <Tooltip title={'Share on Facebook'}>
            <FacebookShareButton
              url={url}
              style={{ minHeight: '0', height: '39px', overflow: 'hidden', padding: '2px .3rem' }}
            >
              <FacebookIcon size={35} round={true} />
            </FacebookShareButton>
          </Tooltip>
        </MenuItem>

        <MenuItem onClick={handleClose}>
          <Tooltip title={'Share on Twitter'}>
            <TwitterShareButton
              url={url}
              style={{ minHeight: '0', height: '39px', overflow: 'hidden', padding: '2px .3rem' }}
            >
              <TwitterIcon size={35} round={true} />
            </TwitterShareButton>
          </Tooltip>
        </MenuItem>

        <MenuItem onClick={handleClose}>
          <textarea
            ref={inputRef}
            style={{ position: 'fixed', top: '-9999px', left: '-9999px' }}
          ></textarea>
          <Tooltip title={'Copy link to clipboard'}>
            <Button
              onClick={() => {
                copyTextToClipboard(url, inputRef)
                  .then(() => {
                    notifSuccess('Link has been copied to clipboard');
                  })
                  .catch((err) => {
                    console.error(err);
                  });
              }}
              sx={{
                minWidth: '0',
                minHeight: '0',
                height: '39px',
                overflow: 'hidden',
                padding: '2px .3rem',
                '&:hover': {
                  backgroundColor: 'transparent'
                }
              }}
            >
              <img src={copyLogo} style={{ width: '35px', height: '35px' }} alt="" />
            </Button>
          </Tooltip>
        </MenuItem>
      </Menu>
    </span>
  );
};
