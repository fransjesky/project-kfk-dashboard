import { useRef, useState } from 'react';
import { FacebookShareButton, FacebookIcon, TwitterShareButton, TwitterIcon } from 'react-share';
import { Button, Menu, MenuItem, Tooltip, Typography } from '@mui/material';
import { styled } from '@mui/material/styles';
import { ShareOutlined } from '@mui/icons-material';
import { Box } from '@mui/system';
import copyLogo from './assets/copy-link.png';
import { useNotifier } from 'utils/hooks';
import { copyTextToClipboard } from 'utils/browser';

const StyledMenu = styled(Menu)({
  '& ul': {
    display: 'flex',
    paddingTop: 0,
    paddingBottom: 0,
    '& li': {
      padding: '10px 8px'
    }
  }
});

export const LargeShareMenu = ({ url }) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const { notifSuccess } = useNotifier();

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const inputRef = useRef(null);

  return (
    <span>
      <Box
        display={'flex'}
        alignItems={'center'}
        onClick={handleClick}
        id="shareBtn"
        aria-controls="shareMenu"
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
      >
        <ShareOutlined color="secondary" fontSize="large" />
        <Typography ml={1} fontSize={20} fontWeight={300} color={'#696871'}>
          Share
        </Typography>
      </Box>
      <StyledMenu
        id="shareMenu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          'aria-labelledby': 'shareBtn'
        }}
      >
        <MenuItem onClick={handleClose}>
          <Tooltip title={'Share on Facebook'}>
            <FacebookShareButton
              url={url}
              style={{ minHeight: '0', height: '39px', overflow: 'hidden', padding: '2px .3rem' }}
            >
              <FacebookIcon size={35} round={true} />
            </FacebookShareButton>
          </Tooltip>
        </MenuItem>

        <MenuItem onClick={handleClose}>
          <Tooltip title={'Share on Twitter'}>
            <TwitterShareButton
              url={url}
              style={{ minHeight: '0', height: '39px', overflow: 'hidden', padding: '2px .3rem' }}
            >
              <TwitterIcon size={35} round={true} />
            </TwitterShareButton>
          </Tooltip>
        </MenuItem>

        <MenuItem onClick={handleClose}>
          <textarea
            ref={inputRef}
            style={{ position: 'fixed', top: '-9999px', left: '-9999px' }}
          ></textarea>
          <Tooltip title={'Copy link to clipboard'}>
            <Button
              onClick={() => {
                copyTextToClipboard(url, inputRef)
                  .then(() => {
                    notifSuccess('Link has been copied to clipboard');
                  })
                  .catch((err) => {
                    console.error(err);
                  });
              }}
              sx={{
                minWidth: '0',
                minHeight: '0',
                height: '39px',
                overflow: 'hidden',
                padding: '2px .3rem',
                '&:hover': {
                  backgroundColor: 'transparent'
                }
              }}
            >
              <img src={copyLogo} style={{ width: '35px', height: '35px' }} alt="" />
            </Button>
          </Tooltip>
        </MenuItem>
      </StyledMenu>
    </span>
  );
};
