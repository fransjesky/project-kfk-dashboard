import { Box, IconButton, Typography } from '@mui/material';
import { styled } from '@mui/material/styles';
import CloseIcon from '@mui/icons-material/Close';
import React from 'react';

const AttachmentContainer = styled(Box)({
  position: 'relative',
  display: 'flex',
  alignItems: 'center',
  backgroundColor: 'white',
  borderRadius: '16px',
  padding: '16px',
  boxShadow: '2px 0px 3px rgba(115, 115, 139, 0.1), 0px 4px 20px rgba(115, 115, 139, 0.15)'
});

const FileNameWrapper = styled(Box)({
  overflow: 'hidden',
  textOverflow: 'ellipsis'
});

const ImageAttachment = ({ fileData, onDelete }) => {
  return (
    <AttachmentContainer>
      <img
        src={fileData?.location}
        alt="Attachment"
        style={{ width: 104, height: 75, borderRadius: '8px', objectFit: 'contain' }}
      />
      <Box display="flex" flexDirection="column" ml={5} maxWidth="60%">
        <FileNameWrapper>
          <Typography variant="s3" fontWeight={500} color="grey.100">
            {fileData?.name}
          </Typography>
        </FileNameWrapper>
        <Typography variant="b1" fontWeight={500} color="grey.50">
          IMG
        </Typography>
      </Box>
      <IconButton
        onClick={() => onDelete()}
        sx={{
          position: 'absolute',
          right: '16px',
          top: '16px'
        }}
      >
        <CloseIcon />
      </IconButton>
    </AttachmentContainer>
  );
};

export default ImageAttachment;
