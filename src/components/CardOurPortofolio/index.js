import React from 'react';
import { Card, Box, Icon, Typography } from '@mui/material';
import { MoreVert } from '@mui/icons-material';
import { EditSquare, Hide, Delete, Show } from 'react-iconly';
import Dropdown from '../Dropdown';

function CardPortofolio({
  thumbnail,
  name,
  status,
  projectCompany,
  detailLink,
  onEdit,
  onArchive,
  onDelete,
  hideActions
}) {
  const ButtonIcon = () => (
    <Icon
      aria-label="more"
      id="long-button"
      aria-controls="long-menu"
      aria-haspopup="true"
      sx={{
        backgroundColor: '#2B2B2B',
        opacity: 0.6,
        width: 25,
        height: 25,
        borderRadius: '8px !important',
        ':hover': {
          opacity: 0.8
        }
      }}
    >
      <MoreVert fontSize="small" sx={{ color: '#fff', marginBottom: 0.5 }} />
    </Icon>
  );

  const itemDropdown = [
    { label: 'Edit', icon: <EditSquare set="broken" />, onClick: onEdit },
    {
      label: status == 'archive' ? 'Show' : 'Archive',
      icon: status == 'archive' ? <Show set="broken" /> : <Hide set="broken" />,
      onClick: onArchive
    },
    { label: 'Delete', icon: <Delete set="broken" />, onClick: onDelete }
  ];

  return (
    <Card
      sx={{
        width: '100%',
        boxShadow: '2px 0px 3px rgba(115, 115, 139, 0.1), 0px 4px 20px rgba(115, 115, 139, 0.15)',
        ':hover': {
          boxShadow: '2px 0px 3px rgba(115, 115, 139, 0.2), 0px 5px 20px rgba(115, 115, 139, 0.25)'
        }
      }}
    >
      {!hideActions && (
        <Box sx={{ position: 'absolute', top: 13, right: 11 }}>
          <Dropdown button={<ButtonIcon />} items={itemDropdown} />
        </Box>
      )}
      <Box
        onClick={() => window.open(detailLink)}
        sx={{
          cursor: 'pointer',
          height: 221,
          backgroundImage: `url(${thumbnail})`,
          backgroundSize: 'cover'
        }}
      >
        <Box
          sx={{
            position: 'absolute',
            display: 'flex',
            width: '100%',
            justifyContent: 'space-between',
            alignItems: 'center',
            height: '65px',
            paddingLeft: 2,
            paddingTop: 2,
            paddingRight: 1.8,
            paddingBottom: 1,
            background:
              'linear-gradient(180deg, rgba(27, 27, 27, 0) 0%, rgba(27, 27, 27, 0.5) 100%)',
            bottom: 0,
            left: 0
          }}
        >
          <Box sx={{ color: '#FFFFFF', maxWidth: '77%' }}>
            <Typography fontSize={15} noWrap={true} fontWeight="bold">
              {name}
            </Typography>
            <Typography fontSize={12} fontWeight="normal" noWrap={true}>
              {projectCompany}
            </Typography>
          </Box>
          <Icon
            sx={{
              backgroundColor: '#FFFFFF',
              width: 28,
              height: 28,
              borderRadius: '8px !important'
            }}
          >
            {status === 'archive' ? (
              <Hide
                set="broken"
                style={{
                  width: 20,
                  height: 20,
                  marginBottom: 3
                }}
              />
            ) : (
              <Show
                set="broken"
                style={{
                  width: 20,
                  height: 20,
                  marginBottom: 3
                }}
              />
            )}
          </Icon>
        </Box>
      </Box>
    </Card>
  );
}

export default CardPortofolio;
