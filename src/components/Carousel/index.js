import React, { useState } from 'react';
// materials
import IconButton from '@mui/material/IconButton';
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import { styled } from '@mui/material/styles';
// carousels
import AliceCarousel from 'react-alice-carousel';
import 'react-alice-carousel/lib/alice-carousel.css';
import styles from './Carousel.module.scss';
import './Carousel.scss';

const NavIconButton = styled((props) => <IconButton color={'primary'} {...props} />)({
  backgroundColor: 'rgba(255, 255, 255, .6)',

  '&:hover': {
    backgroundColor: '#fefefe'
  }
});

const getThumbs = (items, [setThumbIndex, setThumbAnimation]) => {
  return items.map((item, i) => (
    <div
      role={'button'}
      className={`thumb ${styles.thumbContainer}`}
      onClick={() => (setThumbIndex(i), setThumbAnimation(true))}
      key={i}
    >
      {item}
    </div>
  ));
};

const Carousel = ({ items, thumbItems }) => {
  const [mainIndex, setMainIndex] = useState(0);
  const [mainAnimation, setMainAnimation] = useState(false);
  const [thumbIndex, setThumbIndex] = useState(0);
  const [thumbAnimation, setThumbAnimation] = useState(false);
  const [thumbs] = useState(getThumbs(thumbItems, [setThumbIndex, setThumbAnimation]));

  const slideNext = () => {
    if (!thumbAnimation && thumbIndex < thumbs.length - 1) {
      setThumbAnimation(true);
      setThumbIndex(thumbIndex + 1);
    }
  };

  const slidePrev = () => {
    if (!thumbAnimation && thumbIndex > 0) {
      setThumbAnimation(true);
      setThumbIndex(thumbIndex - 1);
    }
  };

  const syncMainBeforeChange = () => {
    setMainAnimation(true);
  };

  const syncMainAfterChange = (e) => {
    setMainAnimation(false);

    if (e.type === 'action') {
      setThumbIndex(e.item);
      setThumbAnimation(false);
    } else {
      setMainIndex(thumbIndex);
    }
  };

  const syncThumbs = (e) => {
    setThumbIndex(e.item);
    setThumbAnimation(false);

    if (!mainAnimation) {
      setMainIndex(e.item);
    }
  };

  return [
    <AliceCarousel
      activeIndex={mainIndex}
      animationType="fadeout"
      animationDuration={600}
      disableDotsControls
      disableButtonsControls
      infinite
      items={items}
      mouseTracking={!thumbAnimation}
      onSlideChange={syncMainBeforeChange}
      onSlideChanged={syncMainAfterChange}
      touchTracking={!thumbAnimation}
      key={'preview'}
    />,
    <div className={`thumbs ${styles.thumbsRow}`} key={'slider'}>
      <AliceCarousel
        activeIndex={thumbIndex}
        autoWidth
        disableDotsControls
        disableButtonsControls
        items={thumbs}
        mouseTracking={false}
        onSlideChanged={syncThumbs}
        touchTracking={!mainAnimation}
      />
      <div
        className={`btn-prev ${styles.btnNav} ${styles.btnPrev} ${
          mainIndex === 0 ? styles.btnNone : ''
        }`}
        role={'button'}
        onClick={slidePrev}
      >
        <NavIconButton>
          <ArrowBackIosIcon fontSize={'small'} />
        </NavIconButton>
      </div>
      <div
        className={`btn-next ${styles.btnNav} ${styles.btnNext} ${
          mainIndex === items.length - 1 ? styles.btnNone : ''
        }`}
        role={'button'}
        onClick={slideNext}
      >
        <NavIconButton>
          <ArrowForwardIosIcon fontSize={'small'} />
        </NavIconButton>
      </div>
    </div>
  ];
};

/**
 * @param {{
 *  photos: {
 *    url: string
 *  }[],
 *  photoSize: "cover" | "contain",
 *  thumbnailSize: "cover" | "contain"
 * }} param0
 * @returns {JSX.Element}
 */
export const PhotoCarousel = ({ photos, photoSize = 'contain', thumbnailSize = 'contain' }) => {
  if (!Array.isArray(photos) || photos.length <= 0) return null;

  const items = photos.map((photo, idx) => {
    return (
      <div className="item" data-value={idx} key={idx}>
        <div className={styles.photoWrapper}>
          <div
            className={photoSize === 'cover' ? styles.photoContentCover : styles.photoContent}
            style={{ backgroundImage: `url('${photo.url}')` }}
          ></div>
        </div>
      </div>
    );
  });

  const thumbItems = photos.map((photo, idx) => {
    return (
      <div className="item" data-value={idx} key={idx}>
        <div className={styles.thumbWrapper}>
          <div
            className={`${
              thumbnailSize === 'cover' ? styles.thumbContentCover : styles.thumbContent
            } thumb-content`}
            style={{ backgroundImage: `url('${photo.url}')` }}
          ></div>
        </div>
      </div>
    );
  });

  return <Carousel items={items} thumbItems={thumbItems} />;
};
