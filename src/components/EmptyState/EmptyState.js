import React from 'react';
import PropTypes from 'prop-types';
import { Box, Typography } from '@mui/material';
import Astronaut from 'assets/svg/AstronautFuel.svg';

const EmptyState = ({ name, IconSize }) => {
  return (
    <Box sx={{ width: '100%', height: '100%', display: 'grid', placeItems: 'center' }}>
      <Box sx={{ display: 'grid', placeItems: 'center' }}>
        <img src={Astronaut} width={IconSize.width} height={IconSize.height} alt="" />
        <Typography variant="s1" fontWeight="bold" textAlign="center" mt={4}>
          There are no {name} to show
        </Typography>
      </Box>
    </Box>
  );
};

EmptyState.defaultProps = {
  IconSize: { width: 257, height: 257 }
};

EmptyState.propTypes = {
  name: PropTypes.string.isRequired,
  IconSx: PropTypes.shape({
    width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    height: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
  })
};

export default EmptyState;
