import React, { useState } from 'react';
import PropTypes from 'prop-types';
import palette from 'theme/palette';
import { styled } from '@mui/material/styles';
import {
  Box,
  Typography,
  List,
  ListItem,
  ListItemButton as MuiListItemButton,
  ListItemIcon,
  ListItemText,
  Fade,
  Popover
} from '@mui/material';

// utils
import { useToggle } from 'utils/hooks';

const ListWrapper = styled((props) => <Box {...props} />)({
  backgroundColor: '#fff',
  width: 'min-content',
  minWidth: '178px',
  borderRadius: '8px',
  padding: '8px'
});

const ListItemButton = styled((props) => <MuiListItemButton {...props} />)({
  borderRadius: '8px',
  '&:hover': {
    backgroundColor: palette.grey[10]
  }
});

const Dropdown = ({ button, items = [] }) => {
  const [isOpen, toggleOpen] = useToggle(false);
  const [anchorEl, setAnchorEl] = useState();

  return (
    <>
      <Box ref={setAnchorEl} onClick={toggleOpen}>
        {button}
      </Box>
      <Popover
        open={isOpen}
        anchorEl={anchorEl}
        onClose={() => toggleOpen(false)}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      >
        <ListWrapper>
          <Fade in={isOpen}>
            <List>
              {items?.map((item, idx) => (
                <ListItem
                  key={idx}
                  disablePadding
                  onClick={() => {
                    item.onClick();
                    toggleOpen(false);
                  }}
                >
                  <ListItemButton disableRipple>
                    <ListItemIcon>{item.icon}</ListItemIcon>
                    <ListItemText>
                      <Typography variant="b2" fontWeight="medium">
                        {item.label}
                      </Typography>
                    </ListItemText>
                  </ListItemButton>
                </ListItem>
              ))}
            </List>
          </Fade>
        </ListWrapper>
      </Popover>
    </>
  );
};

Dropdown.propTypes = {
  button: PropTypes.node,
  items: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      icon: PropTypes.node,
      onClick: PropTypes.func
    })
  )
};

export default Dropdown;
