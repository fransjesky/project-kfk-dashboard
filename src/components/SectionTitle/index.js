import React from 'react';
// material
import { Box, Typography } from '@mui/material';

const SectionTitle = ({ label }) => {
  return (
    <Box
      sx={{
        mb: {
          md: 2.5,
          xs: 2
        }
      }}
    >
      <Typography variant="body-1" fontWeight={600}>
        {label}
      </Typography>
    </Box>
  );
};

export default SectionTitle;
