import React from 'react';
import PropTypes from 'prop-types';

function Star({ size = 'default', color = '#EFB008', positioning }) {
  const style = {
    height: size == 'small' ? '0.625rem' : '1.125rem',
    width: size == 'small' ? '0.625rem' : '1.125rem',
    backgroundColor: color,
    borderRadius: '50%',
    position: 'absolute',
    top: positioning ? positioning.top : '0.625rem',
    left: positioning ? positioning.left : '0.625rem'
  };
  return <div style={style} />;
}

Star.propTypes = {
  size: PropTypes.oneOf(['default', 'small']),
  color: PropTypes.string,
  positioning: PropTypes.object
};

export default Star;
