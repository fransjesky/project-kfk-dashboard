import React from 'react';
// material
import { Typography } from '@mui/material';

const DETAIL_TITLE_PROPS = {
  component: 'p',
  variant: 'body-4',
  color: 'grey.60',
  sx: { mb: 1 }
};

const ListItem = ({ title, value, color, component }) => {
  return (
    <>
      <Typography {...DETAIL_TITLE_PROPS}>{title}</Typography>
      {component ? (
        component
      ) : (
        <Typography variant="body-4" color={color}>
          {value}
        </Typography>
      )}
    </>
  );
};

export default ListItem;
