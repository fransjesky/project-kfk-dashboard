import React, { useState } from 'react';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import { styled } from '@mui/material/styles';

const StyledTabs = styled((props) => (
  <Tabs {...props} TabIndicatorProps={{ children: <span className="MuiTabs-indicatorSpan" /> }} />
))(() => ({
  minHeight: '30px',
  '& .MuiTabs-indicator': {
    display: 'flex',
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  '& .MuiTabs-indicatorSpan': {
    width: '100%',
    backgroundColor: '#D2243D'
  }
}));

const StyledTab = styled((props) => <Tab disableRipple {...props} />)(({ theme }) => ({
  textTransform: 'none',
  fontWeight: theme.typography.fontWeightRegular,
  fontSize: theme.typography.pxToRem(15),
  color: theme.palette.dark,
  padding: 0,
  paddingTop: '.1rem',
  paddingBottom: '.1rem',
  minWidth: 0,
  minHeight: '30px',
  marginRight: theme.spacing(2),
  '&.Mui-selected': {
    color: theme.palette.black
  },
  '&.Mui-focusVisible': {
    backgroundColor: 'rgba(100, 95, 228, 0.32)'
  }
}));

export default function TabsPanel({ items = [], initial = null, onChange = () => {} }) {
  const [value, setValue] = useState(initial);

  const handleChange = (event, val) => {
    setValue(val);
    onChange(val);
  };

  return (
    <StyledTabs
      value={value}
      onChange={handleChange}
      textColor="inherit"
      indicatorColor="primary"
      aria-label="Market Data Tabs"
    >
      {items.map((item, idx) => (
        <StyledTab key={idx} value={item.value} label={item.label} />
      ))}
    </StyledTabs>
  );
}
