import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@mui/material/Grid';

function Card({ children, variant = 'default', color = '#FF9D42', width = '100%', ...props }) {
  const style = {
    minHeight: '7.625rem',
    height: 'auto',
    width: width,
    borderRadius: '0.625rem',
    borderLeft: variant == 'secondary' ? 'none' : `0.75rem solid ${color}`,
    display: 'flex',
    flexDirection: 'column',
    boxShadow: '2px 0px 3px rgba(115, 115, 139, 0.1), 0px 4px 20px rgba(115, 115, 139, 0.15)'
  };

  return (
    <Grid style={style} {...props}>
      {children}
    </Grid>
  );
}

Card.propTypes = {
  variant: PropTypes.oneOf(['default', 'secondary']),
  color: PropTypes.string,
  width: PropTypes.string
};

export default Card;
