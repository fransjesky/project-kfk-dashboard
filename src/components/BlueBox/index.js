import { Box, Typography } from '@mui/material';
import palette from 'theme/palette';

const BlueBox = ({ children }) => (
  <Box
    sx={{
      border: `solid 1px ${palette.secondary[100]}`,
      padding: '12px',
      borderRadius: '8px',
      mb: 3,
      width: 'fit-content'
    }}
  >
    <Typography>{children}</Typography>
  </Box>
);

export default BlueBox;
