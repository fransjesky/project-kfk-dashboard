import React from 'react';
import PropTypes from 'prop-types';
import { Grid, IconButton } from '@mui/material';
import SvgIconStyle from 'components/SvgIconStyle';
import Card from '../Card';
import Planet from './Planet';
import Dropdown from 'components/Dropdown';

// icons
import MoreVertIcon from '@mui/icons-material/MoreVert';
import DeleteIcon from 'assets/svg/Delete.svg';
import EditIcon from 'assets/svg/Edit.svg';

function CardJourney({
  id,
  backgroundColor = '#FFDC81',
  foregroundColor = '#EFB008',
  year,
  title,
  onDelete,
  onEdit,
  width
}) {
  const style = {
    cardJourney: {
      padding: '1.5rem',
      minHeight: '9.875rem',
      borderTopRightRadius: '0.75rem',
      borderBottomRightRadius: '0.75rem',
      display: 'flex'
    },
    planetIcon: {
      minHeight: '3.5rem',
      minWidth: '3.5rem',
      height: '3.5rem',
      width: '3.5rem'
    },
    articleContainer: {
      marginLeft: '1rem',
      width: '100%',
      maxWidth: '7.125rem'
    },
    year: {
      margin: 'unset',
      marginBottom: '0.5rem',
      color: '#3d3d3d',
      fontFamily: `'Satoshi-Bold', sans-serif`,
      fontSize: '1em',
      fontWeight: '700',
      lineHeight: '1.5rem',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap'
    },
    title: {
      margin: 'unset',
      color: '#94a3b8',
      fontFamily: `'Satoshi-Regular', sans-serif`,
      fontSize: '1em',
      fontWeight: '400',
      lineHeight: '1.2rem',
      overflow: 'hidden',
      display: '-webkit-box',
      WebkitLineClamp: '4',
      WebkitBoxOrient: 'vertical'
    }
  };

  const DropdownButton = () => (
    <IconButton>
      <MoreVertIcon />
    </IconButton>
  );

  return (
    <Card variant="default" width={width}>
      <Grid style={style.cardJourney}>
        <Planet
          backgroundColor={backgroundColor}
          foregroundColor={foregroundColor}
          style={style.planetIcon}
        />
        <Grid style={style.articleContainer}>
          <h1 style={style.year}>{year}</h1>
          <p style={style.title}>{title}</p>
        </Grid>
        <Dropdown
          button={<DropdownButton />}
          items={[
            {
              label: 'Edit',
              icon: <SvgIconStyle src={EditIcon} />,
              onClick: () => onEdit(id)
            },
            {
              label: 'Delete',
              icon: <SvgIconStyle src={DeleteIcon} />,
              onClick: () => onDelete(id, year)
            }
          ]}
        />
      </Grid>
    </Card>
  );
}

CardJourney.propTypes = {
  backgroundColor: PropTypes.string,
  foregroundColor: PropTypes.string,
  year: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  onDelete: PropTypes.func,
  onEdit: PropTypes.func
};

export default CardJourney;
