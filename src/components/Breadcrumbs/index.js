import React from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router';
import { Breadcrumbs as MUIBreadcrumbs, Box, Link, Typography } from '@mui/material';

const Breadcrumbs = ({ items }) => {
  const currentIdx = items.length - 1;
  const history = useHistory();

  return (
    <MUIBreadcrumbs
      separator={
        <Box mx={2}>
          <Typography variant="s2" color="grey.50" fontWeight={500}>
            /
          </Typography>
        </Box>
      }
      aria-label="breadcrumb"
    >
      {items.map((item, idx) => {
        return idx === currentIdx ? (
          <Typography
            variant="b1"
            color="primary.130"
            fontWeight={500}
            key={idx + 1}
            sx={{ cursor: 'default' }}
          >
            {item.label}
          </Typography>
        ) : !item.link ? (
          <Typography
            variant="b1"
            color="grey.50"
            fontWeight={500}
            key={idx + 1}
            sx={{ cursor: 'default' }}
          >
            {item.label}
          </Typography>
        ) : (
          <Link
            underline="hover"
            color="grey.50"
            onClick={() => history.push(item.link)}
            key={idx + 1}
            sx={{ cursor: 'pointer' }}
          >
            {item.label}
          </Link>
        );
      })}
    </MUIBreadcrumbs>
  );
};

Breadcrumbs.propTypes = {
  items: PropTypes.array.isRequired
};

export default Breadcrumbs;
