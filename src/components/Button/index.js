import Button from '@mui/material/Button';
// import { styled } from '@mui/material/styles';

export const PrimaryButton = (props) => {
  return <Button variant="primary" color="#fff" {...props} />;
};

export const SecondaryButton = (props) => {
  return <Button variant="secondary" color="secondary" {...props} />;
};
