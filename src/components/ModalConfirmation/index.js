import React from 'react';
// components
import { Box, Button, Icon } from '@mui/material';
import { ReportProblemRounded } from '@mui/icons-material';
import { InfoCircle } from 'react-iconly';
import Modal, { ModalContent, ModalButtons } from '../Modal';
// redux
import { useSelector, useDispatch } from 'react-redux';
import { confirmClose } from 'redux/actions';

export default function ModalConfirmation() {
  const dispatch = useDispatch();
  const {
    confirm: { open, warning, title, message, handler }
  } = useSelector((state) => state.global);

  const handleClose = () => dispatch(confirmClose());
  return (
    <Modal isOpen={open} onClose={handleClose}>
      <ModalContent>
        <Box sx={{ width: '555px', marginBottom: 5 }}>
          <Icon sx={{ height: '55px', width: '53px' }}>
            {warning ? (
              <ReportProblemRounded sx={{ color: '#F9CF3C', width: '53px', height: '53px' }} />
            ) : (
              <InfoCircle set="bold" style={{ color: '#E01F3D', width: '53px', height: '53px' }} />
            )}
          </Icon>
          <Box sx={{ wordWrap: 'break-word' }}>
            <h2>{title}</h2>
            <p style={{ maxWidth: '367px' }}>{message}</p>
          </Box>
        </Box>
      </ModalContent>
      <ModalButtons background={'#FFF5EB'}>
        <Button variant="outlined" onClick={handleClose}>
          Cancel
        </Button>
        <Button variant="contained" onClick={handler} sx={{ color: '#FFFFFF' }}>
          {warning ? 'Continue' : 'Yes, delete'}
        </Button>
      </ModalButtons>
    </Modal>
  );
}
