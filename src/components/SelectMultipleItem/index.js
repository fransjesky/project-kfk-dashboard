import React, { useState, useEffect } from 'react';
import {
  Box,
  FormControl,
  InputLabel,
  Select,
  OutlinedInput,
  MenuItem,
  ListItemText,
  Checkbox,
  IconButton,
  InputBase,
  FormHelperText
} from '@mui/material';
import { styled } from '@mui/material/styles';
import { Add } from '@mui/icons-material';

const sxInputBox = {
  minWidth: 450,
  maxWidth: 450
};

const sxInput = {
  fontSize: '.85rem',
  height: '50px'
};

const sxInputLabel = {
  backgroundColor: '#ffffff'
};

const MSMenuProps = {
  PaperProps: {
    style: {
      maxHeight: 48 * 5 + 8,
      minWidth: 450
    }
  }
};

const BpIcon = styled('span')(({ theme }) => ({
  borderRadius: '4px',
  width: 15,
  height: 15,
  boxShadow:
    theme.palette.mode === 'dark'
      ? '0 0 0 1px rgb(16 22 26 / 40%)'
      : 'inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)',
  backgroundColor: theme.palette.mode === 'dark' ? '#394b59' : '#f5f8fa',
  backgroundImage:
    theme.palette.mode === 'dark'
      ? 'linear-gradient(180deg,hsla(0,0%,100%,.05),hsla(0,0%,100%,0))'
      : 'linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))',
  '.Mui-focusVisible &': {
    outline: '2px auto rgba(19,124,189,.6)',
    outlineOffset: 2
  },
  'input:hover ~ &': {
    backgroundColor: theme.palette.mode === 'dark' ? '#30404d' : '#ebf1f5'
  },
  'input:disabled ~ &': {
    boxShadow: 'none',
    background: theme.palette.mode === 'dark' ? 'rgba(57,75,89,.5)' : 'rgba(206,217,224,.5)'
  }
}));

const BpCheckedIcon = styled(BpIcon)({
  backgroundColor: '#3DB9FF',
  backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))',
  '&:before': {
    display: 'block',
    width: 15,
    height: 15,
    backgroundImage:
      "url(\"data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16'%3E%3Cpath" +
      " fill-rule='evenodd' clip-rule='evenodd' d='M12 5c-.28 0-.53.11-.71.29L7 9.59l-2.29-2.3a1.003 " +
      "1.003 0 00-1.42 1.42l3 3c.18.18.43.29.71.29s.53-.11.71-.29l5-5A1.003 1.003 0 0012 5z' fill='%23fff'/%3E%3C/svg%3E\")",
    content: '""'
  },
  'input:hover ~ &': {
    backgroundColor: '#1080DD'
  }
});

function SelectMultipleItem(props) {
  const {
    label,
    values,
    onAddType,
    name,
    options,
    touched,
    errors,
    handleBlur,
    setFieldValue,
    onChange
  } = props;
  const [value, setValue] = useState([]);
  const [labelInput, setLabelInput] = useState([]);
  const [newType, setNewType] = useState(false);
  const [newProjectType, setNewProjectType] = useState('');

  const handleChangeOptions = (event, val) => {
    const {
      target: { value }
    } = event;
    if (val.props.name) {
      if (labelInput.includes(val.props.name)) {
        setLabelInput(labelInput.filter((itm) => itm !== val.props.name));
      } else {
        setLabelInput([...labelInput, val.props.name]);
      }
    }
    setValue(typeof value === 'string' ? value.split(',') : value);
    onChange(value, val.props.value);
  };

  const handleAddType = () => {
    if (!newType) {
      setNewType(true);
    } else {
      onAddType(newProjectType);
      setValue((prev) => prev.unshift(newProjectType));
      setLabelInput((prevVal) => [...prevVal, newProjectType]);
      setFieldValue(name, value);
      setNewProjectType('');
      setNewType(false);
    }
  };

  useEffect(() => {
    if (options.length) {
      const labelValue = [];
      options.forEach((e) => {
        values[name]?.forEach((i) => {
          if (e.value === i) {
            labelValue.push(e.label);
          }
        });
      });
      setLabelInput(labelValue);
    }
    // eslint-disable-next-line
  }, [options]);

  useEffect(() => {
    if (values[name]) {
      setValue(Array.from(values[name]));
    }
  }, [values, name]);

  return (
    <Box sx={sxInputBox}>
      <FormControl fullWidth error={Boolean(touched[name] && errors[name])} name={name}>
        <InputLabel id="tg-ms-checkbox-label" sx={sxInputLabel}>
          {label}
        </InputLabel>
        <Select
          onClose={() => setNewType(false)}
          onBlur={handleBlur}
          name={name}
          id={name}
          multiple
          value={value}
          onChange={handleChangeOptions}
          input={
            <OutlinedInput
              label={label}
              name={name}
              value={values[name]}
              onBlur={handleBlur}
              error={Boolean(touched[name] && errors[name])}
            />
          }
          renderValue={() => labelInput.join(', ')}
          MenuProps={MSMenuProps}
          size="small"
          variant="outlined"
          sx={sxInput}
          displayEmpty
        >
          {options.map((opt, optIdx) => (
            <MenuItem
              key={optIdx}
              value={opt.value}
              name={opt.label}
              sx={{ paddingLeft: '2px', paddingRight: '10px', height: '50px' }}
              divider={optIdx == options.length - 1 ? (onAddType ? true : false) : false}
              disabled={newType}
            >
              <Checkbox
                name={opt.label}
                checked={values[name]?.includes(opt.value)}
                color={'secondary'}
                checkedIcon={<BpCheckedIcon />}
                icon={<BpIcon />}
                sx={{
                  '> svg': {
                    fill: '#2e60ff'
                  }
                }}
              />
              <ListItemText
                primary={opt.label}
                primaryTypographyProps={{ fontSize: '16px', fontWeight: '500px', color: 'grey.90' }}
              />
            </MenuItem>
          ))}
          {onAddType ? (
            <Box
              key={options.length}
              sx={{
                pl: !newType ? '7px' : 0,
                paddingRight: '10px',
                height: '50px',
                display: 'flex',
                alignItems: 'center',
                cursor: !newType ? 'pointer' : 'auto',
                ':hover': { backgroundColor: !newType ? 'grey.10' : 'grey.0' }
              }}
            >
              {!newType ? (
                <>
                  <Add />
                  <ListItemText
                    onClick={handleAddType}
                    primary={'Custom Value'}
                    primaryTypographyProps={{
                      fontSize: '16px',
                      fontWeight: '500px',
                      color: 'grey.90',
                      marginLeft: 1
                    }}
                  />
                </>
              ) : (
                <>
                  <IconButton onClick={handleAddType} disabled={!Boolean(newProjectType)}>
                    <Add />
                  </IconButton>
                  <InputBase
                    value={newProjectType}
                    onChange={(e) => {
                      setNewProjectType(e.target.value);
                    }}
                    placeholder="Custom Value..."
                    sx={{ mb: 0.8, '.MuiInputBase-root': { fontSize: '16px' } }}
                    fullWidth
                  />
                </>
              )}
            </Box>
          ) : (
            ''
          )}
        </Select>
        <FormHelperText>{touched[name] && errors[name]}</FormHelperText>
      </FormControl>
    </Box>
  );
}

export default SelectMultipleItem;
