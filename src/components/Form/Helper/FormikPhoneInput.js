import React, { useState } from 'react';
import PhoneInput from 'react-phone-input-2';
import 'react-phone-input-2/lib/material.css';
import './styles.scss';

/**
 *
 * @param {{
 *  name: string,
 *  label: string,
 *  formikProps: {
 *    values,
 *    touched,
 *    errors,
 *    handleChange,
 *    handleBlur,
 *    handleSubmit,
 *    setFieldValue,
 *    setFieldTouched,
 *    setFieldError,
 *    isValid,
 *    dirty
 *  },
 *  defaultCode: string
 * }} props
 */
const FormikPhoneInput = (props) => {
  const { name, label, formikProps, defaultCode = 'id', ...rest } = props;
  const { setFieldValue, touched, errors, handleBlur, values } = formikProps;

  const defaultValue = values[name];
  const [value] = useState(defaultValue.length ? defaultValue : '62');

  const handlePhoneInputChange = (value) => {
    setFieldValue(name, value);
  };

  const isValid = () => {
    // params: values, country, countries, hiddenAreaCodes
    return true; // instead, use onChange/onBlur event for validation
  };

  return (
    <div className={`formikPhoneInput ${touched[name] && errors[name] ? 'error' : ''}`}>
      <PhoneInput
        value={value}
        specialLabel={label}
        country={defaultCode}
        // preferredCountries={['id', 'pe', 'us']}
        countryCodeEditable={false}
        onChange={handlePhoneInputChange}
        inputProps={{
          onBlur: handleBlur,
          name: name
        }}
        isValid={isValid}
        {...rest}
      />
      {touched[name] && errors[name] ? <p className={'helperText'}>{errors[name]}</p> : null}
    </div>
  );
};

export default FormikPhoneInput;
