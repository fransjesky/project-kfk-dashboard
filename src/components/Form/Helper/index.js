import { TextField, MenuItem } from '@mui/material';
import FullWidthTextField from '../TextField/FullWidthTextField';
// components
import FormikAutocomplete from './FormikAutocomplete';
import FormikChipInput from './FormikChipInput';
import FormikPhoneInput from './FormikPhoneInput';

/**
 *
 * @param {{
 *  type: "text" | "textarea" | "select" | "autocomplete" | "phone" | "tag",
 *  name: string,
 *  label: string,
 *  formikProps: {
 *    values,
 *    touched,
 *    errors,
 *    handleChange,
 *    handleBlur,
 *    handleSubmit,
 *    setFieldValue,
 *    isValid,
 *    dirty
 *  },
 *  items?: any[],
 *  itemValueKey?: string,
 *  itemLabelKey?: string,
 *  rows?: number,
 *  minRows?: number,
 *  maxRows?: number,
 *  maxLength?: number,
 *  defaultCode?: string
 * }} params
 */
export const getFormikInput = ({ type, name, label, formikProps, ...rest }) => {
  const { values, handleChange, handleBlur, touched, errors } = formikProps;
  const { maxLength, helperText = false } = rest;
  let inputProps = {};

  if (maxLength) {
    inputProps.maxLength = maxLength;
  }

  if (type === 'text') {
    return (
      <TextField
        fullWidth
        name={name}
        label={label}
        value={values[name]}
        onChange={handleChange}
        onBlur={handleBlur}
        error={Boolean(touched[name] && errors[name])}
        helperText={
          Boolean(touched[name] && errors[name]) ? errors[name] : helperText ? helperText : ''
        }
        InputLabelProps={{ shrink: true }}
        inputProps={inputProps}
      />
    );
  }

  if (type === 'textarea') {
    const { rows, maxRows, minRows } = rest;
    let conf = {};
    if (rows) {
      conf.rows = rows;
    }
    if (maxRows) {
      conf.maxRows = maxRows;
    }
    if (minRows) {
      conf.minRows = minRows;
    }

    return (
      <TextField
        fullWidth
        name={name}
        label={label}
        value={values[name]}
        onChange={handleChange}
        onBlur={handleBlur}
        error={Boolean(touched[name] && errors[name])}
        helperText={
          Boolean(touched[name] && errors[name]) ? errors[name] : helperText ? helperText : ''
        }
        InputLabelProps={{ shrink: true }}
        multiline
        {...conf}
        inputProps={inputProps}
      />
    );
  }

  if (type === 'select') {
    const { items = [], itemValueKey, itemLabelKey, itemRender } = rest;

    return (
      <FullWidthTextField
        name={name}
        label={label}
        value={values[name]}
        onChange={handleChange}
        onBlur={handleBlur}
        error={Boolean(touched[name] && errors[name])}
        helperText={touched[name] && errors[name]}
        InputLabelProps={{ shrink: true }}
        select
      >
        {items.map((item) => (
          <MenuItem key={item[itemValueKey]} value={item[itemValueKey]}>
            {itemRender ? itemRender(item) : item[itemLabelKey]}
          </MenuItem>
        ))}
      </FullWidthTextField>
    );
  }

  if (type === 'autocomplete') {
    const { items = [], itemValueKey, itemLabelKey } = rest;

    return (
      <FormikAutocomplete
        name={name}
        label={label}
        items={items}
        itemValueKey={itemValueKey}
        itemLabelKey={itemLabelKey}
        formikProps={formikProps}
      />
    );
  }

  if (type === 'phone') {
    return <FormikPhoneInput name={name} label={label} formikProps={formikProps} {...rest} />;
  }

  if (type === 'tag') {
    return <FormikChipInput name={name} label={label} formikProps={formikProps} {...rest} />;
  }

  return null;
};
