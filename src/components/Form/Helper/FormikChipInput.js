import React, { useState } from 'react';
// materials
import { Chip, TextField } from '@mui/material';
import { Fragment } from 'react';

/**
 *
 * @param {{
 *  name: string,
 *  label: string,
 *  formikProps: {
 *    values,
 *    touched,
 *    errors,
 *    handleChange,
 *    handleBlur,
 *    handleSubmit,
 *    setFieldValue,
 *    setFieldTouched,
 *    setFieldError,
 *    isValid,
 *    dirty
 *  }
 * }} props
 */
const FormikChipInput = (props) => {
  const { name, label, formikProps } = props;
  const { values, touched, errors, handleChange, setFieldTouched, setFieldValue } = formikProps;
  const [textFieldValue, setTextFieldValue] = useState('');

  const handleTextFieldChange = (e) => {
    let current = e.target.value;

    if (current.indexOf(',') >= 0) {
      setFieldValue(name, [
        ...values[name],
        ...current
          .split(',')
          .map((s) => s.trim())
          .filter((s) => s.length && values[name].indexOf(s) < 0)
      ]);
      setTextFieldValue('');
    } else {
      setTextFieldValue(current);
    }
  };

  const handleTextFieldKeydown = (e) => {
    if (e.key === 'Enter') {
      let current = e.target.value;

      setFieldValue(name, [
        ...values[name],
        ...[current.trim()].filter((s) => s.length && values[name].indexOf(s) < 0)
      ]);
      setTextFieldValue('');
    }
  };

  const handleTextFieldBlur = () => {
    setFieldTouched(name, true);
  };

  const handleChipDelete = (item) => () => {
    let idx = values[name].indexOf(item);
    setFieldValue(name, [...values[name].slice(0, idx), ...values[name].slice(idx + 1)]);
  };

  return (
    <Fragment>
      <TextField
        onChange={handleTextFieldChange}
        onKeyDown={handleTextFieldKeydown}
        value={textFieldValue}
        label={label}
        error={Boolean(touched[name] && errors[name])}
        onBlur={handleTextFieldBlur}
        fullWidth
        helperText={touched[name] && errors[name]}
        InputLabelProps={{ shrink: true }}
        InputProps={{
          startAdornment: values[name].map((item) => (
            <Chip key={item} tabIndex={-1} label={item} onDelete={handleChipDelete(item)} />
          ))
        }}
      />
      <input type={'hidden'} value={values[name]} name={name} onChange={handleChange} />
    </Fragment>
  );
};

export default FormikChipInput;
