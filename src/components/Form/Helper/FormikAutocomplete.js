import React, { useState } from 'react';
// materials
import { TextField, Autocomplete } from '@mui/material';

/**
 *
 * @param {{
 *  name: string,
 *  label: string,
 *  formikProps: {
 *    values,
 *    touched,
 *    errors,
 *    handleChange,
 *    handleBlur,
 *    handleSubmit,
 *    setFieldValue,
 *    isValid,
 *    dirty
 *  },
 *  items?: any[],
 *  itemValueKey?: string,
 *  itemLabelKey?: string
 * }} props
 */
const FormikAutocomplete = (props) => {
  const { name, label, formikProps, items = [], itemValueKey, itemLabelKey } = props;
  const { values, setFieldValue, handleBlur, touched, errors } = formikProps;

  const defaultTextValue = values[name];
  const filtered = items.filter((opt) => opt[itemValueKey] === defaultTextValue);
  const defaultValue = filtered.length ? filtered[0] : null;
  const [value, setValue] = useState(defaultValue);

  const handleAutocompleteChange = (e, selectedOption) => {
    if (selectedOption && selectedOption[itemValueKey]) {
      setFieldValue(name, selectedOption[itemValueKey]);
      setValue(selectedOption);
    } else {
      setFieldValue(name, null);
      setValue(null);
    }
  };

  return (
    <Autocomplete
      isOptionEqualToValue={(opt, val) => opt._id === val._id}
      value={value}
      disablePortal
      selectOnFocus
      clearOnBlur
      clearOnEscape
      onChange={handleAutocompleteChange}
      options={items}
      getOptionLabel={(option) => option[itemLabelKey]}
      renderInput={(params) => (
        <TextField
          {...params}
          fullWidth
          name={name}
          label={label}
          onBlur={handleBlur}
          error={Boolean(touched[name] && errors[name])}
          helperText={touched[name] && errors[name]}
          InputLabelProps={{ shrink: true }}
        />
      )}
    />
  );
};

export default FormikAutocomplete;
