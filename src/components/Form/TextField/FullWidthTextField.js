import { TextField } from '@mui/material';

const fullWidthSx = {
  position: 'relative',
  '& .MuiSelect-select': { display: 'flex' },
  '& .MuiSvgIcon-root': {
    position: 'absolute',
    top: '50%',
    transform: 'translateY(-50%)',
    right: '0px'
  }
};

export default function FullWidthTextField({ children, sx = {}, ...restProps }) {
  return <TextField fullWidth children={children} sx={{ ...sx, ...fullWidthSx }} {...restProps} />;
}
