import React from 'react';
// material
import { FormGroup, FormControlLabel, Switch as SwitchMui } from '@mui/material';
import { styled } from '@mui/material/styles';

const CustomSwitch = styled(SwitchMui)(({ theme }) => ({
  padding: 8,
  '& .MuiSwitch-track': {
    borderRadius: 22 / 2,
    '&:before, &:after': {
      content: '""',
      position: 'absolute',
      top: '50%',
      transform: 'translateY(-50%)',
      width: 16,
      height: 16
    }
  },
  '& .Mui-checked+ .MuiSwitch-track': {
    opacity: `${1}!important`
  },
  '& .MuiSwitch-thumb': {
    boxShadow: 'none',
    width: 16,
    height: 16,
    margin: 2,
    color: theme.palette.white.main
  }
}));

const Switch = ({ label, onChange, checked, ...props }) => {
  return (
    <FormGroup>
      <FormControlLabel
        control={
          <CustomSwitch onChange={onChange} checked={checked} color="secondary" {...props} />
        }
        label={label}
      />
    </FormGroup>
  );
};

export default Switch;
