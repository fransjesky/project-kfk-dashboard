import React, { useCallback, useState } from 'react';
import { useDropzone } from 'react-dropzone';
// styles
import styles from './Dropzone.module.css';
// assets
import Attachment from 'assets/svg/Attachment.svg';
import RejectedAttachment from 'assets/svg/RejectedAttachment.svg';

const Dropzone = ({
  label,
  customProps = {},
  onAcceptedFiles = () => {},
  onRejectedFiles = () => {},
  uploading = false,
  maxSize = Infinity
}) => {
  // eslint-disable-next-line
  const onDropAccepted = useCallback(
    (acceptedFiles) => {
      setIsRejected(false);
      onAcceptedFiles(acceptedFiles);
    },
    [onAcceptedFiles]
  );

  const [isRejected, setIsRejected] = useState(false);
  const [errorData, setErrorData] = useState('');
  const onDropRejected = useCallback(
    (rejectedFiles) => {
      setIsRejected(true);
      setErrorData(rejectedFiles[0]?.errors[0]);
      onRejectedFiles(rejectedFiles);
    },
    [onRejectedFiles]
  );

  const dropzoneOptions = {
    onDropAccepted,
    onDropRejected,
    maxSize
  };
  if (typeof customProps.accept === 'string') {
    dropzoneOptions.accept = customProps.accept;
  }

  // eslint-disable-next-line
  const { getRootProps, getInputProps, isDragActive } = useDropzone(dropzoneOptions);

  return (
    <div {...getRootProps()} className={styles.wrapper}>
      <input {...getInputProps({ ...customProps })} disabled={uploading} />
      <div className={isRejected ? styles.RejectedDropzoneBox : styles.DropzoneBox}>
        <span>{label}</span>
        <div className={styles.imageContainer}>
          <img src={isRejected ? RejectedAttachment : Attachment} alt="Attachment" />
        </div>
        {isRejected ? (
          errorData?.code == 'file-too-large' ? (
            <p className={styles.errorText}>File exceeds {(maxSize / 1024000).toFixed(1)}MB</p>
          ) : (
            <p className={styles.errorText}>{errorData?.message}</p>
          )
        ) : (
          <p>Max file size is {(maxSize / 1024000).toFixed(1)}MB</p>
        )}
        <p>
          Drag your files here, or <a>browse</a>
        </p>
      </div>
    </div>
  );
};

export default Dropzone;
