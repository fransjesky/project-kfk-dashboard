import { Typography } from '@mui/material';
import { styled } from '@mui/material/styles';

export const grayColor = '#8C8C8D';
export const errorColor = '#d2243d';
export const errorTextColor = '#FF4842';

export const GrayText = styled((props) => <Typography component={'span'} {...props} />)({
  color: grayColor
});

export const BoldText = styled((props) => <Typography component={'span'} {...props} />)({
  fontWeight: 'bold'
});

export const SectionLabel = styled((props) => <Typography component={'div'} {...props} />)({
  color: grayColor,
  marginBottom: '.5rem'
});

export const Paragraph = styled((props) => <Typography component={'p'} {...props} />)({
  marginBottom: '1rem',
  wordWrap: 'break-word'
});

export const FormLabel = styled((props) => <Typography component={'p'} {...props} />)({
  marginBottom: '.75rem',
  wordWrap: 'break-word',
  fontFamily: 'inherit',
  fontSize: '12px',
  fontWeight: 'bold'
});

export const FormNote = styled((props) => <Typography component={'p'} {...props} />)({
  marginBottom: '.6rem',
  wordWrap: 'break-word',
  fontFamily: 'inherit',
  fontSize: '10px',
  color: grayColor
});

export const FormErrorNote = styled((props) => <Typography component={'p'} {...props} />)({
  marginBottom: '.6rem',
  wordWrap: 'break-word',
  fontFamily: 'inherit',
  fontSize: '10px',
  color: errorTextColor
});
