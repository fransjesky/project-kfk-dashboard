import React from 'react';
import { Pagination as MUIPagination } from '@mui/material';
import { styled } from '@mui/material/styles';

const StyledPagination = styled(MUIPagination)(({ theme }) => ({
  '& .MuiPaginationItem-text': {
    color: `${theme.palette.grey[50]} !important`,
    fontWeight: 500
  },
  '& .Mui-selected': {
    backgroundColor: 'white !important',
    color: `${theme.palette.primary.main} !important`
  },
  '& .MuiPaginationItem-previousNext': {
    backgroundColor: `${theme.palette.primary.main} !important`,
    color: 'white !important'
  },
  '& .Mui-disabled': {
    backgroundColor: `${theme.palette.grey[20]} !important`
  }
}));

const Pagination = ({ totalPage, currentPage, onChange }) => {
  return (
    <StyledPagination
      count={totalPage}
      page={currentPage}
      onChange={onChange}
      size="large"
      siblingCount={2.5}
    />
  );
};

export default Pagination;
