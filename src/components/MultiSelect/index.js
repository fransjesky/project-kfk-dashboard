import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/material/styles';
// components
import { Box, TextField, InputAdornment, Chip, Stack, Menu } from '@mui/material';
import MenuItem from 'components/MenuItem';
// icons
import SearchIcon from '@mui/icons-material/Search';

const StyledChip = styled(Chip)(({ theme, bcolor }) => ({
  backgroundColor: bcolor === 'primary' ? theme.palette.secondary.main : '#32BABA',
  borderRadius: '8px',
  color: '#fff',
  'svg.MuiChip-deleteIcon': {
    borderRadius: '50%',
    color: bcolor === 'primary' ? '#009ACB' : '#5DD7D7',
    backgroundColor: '#fff',
    '&:hover': {
      color: bcolor === 'primary' ? '#009ACB' : '#5DD7D7'
    }
  }
}));

const MultiSelect = (props) => {
  const {
    withCustomValue,
    chipcolor,
    name,
    label,
    value,
    error,
    touched,
    onBlur,
    helperText,
    onChange,
    options
  } = props;
  const [anchorEl, setAnchorEl] = useState(null);
  const [menuWidth, setMenuWidth] = useState('auto');
  const [inputValue, setInputValue] = useState('');
  const [isFocus, setIsFocus] = useState(false);

  const handleDeleteValue = (label) => () => onChange(value.filter((name) => name !== label));

  const handleClickSearchField = (event) => {
    const target = event.currentTarget;
    setAnchorEl(target);
    setMenuWidth(`${target.offsetWidth}px`);
  };

  const handleChangeSearchField = (event) => {
    setInputValue(event.target.value);
    if (!anchorEl) {
      const target = event.currentTarget;
      setAnchorEl(target);
      setMenuWidth(`${target.offsetWidth}px`);
    }
  };

  const handleKeyDownSearchField = (event) => {
    if (event.keyCode === 13) {
      if (withCustomValue) {
        handleClickMenu(inputValue);
        setInputValue('');
      }
    }
  };

  const handleCloseMenu = () => setAnchorEl(null);

  const handleClickMenu = (menuValue) => {
    if (value.includes(menuValue)) {
      onChange(value.filter((val) => val !== menuValue));
    } else {
      onChange([...value, menuValue]);
    }
  };

  const getFilteredOptions = (inputValue_, options_, withCustom) => {
    if (inputValue_ === '') return options_;

    let filteredOptions = options_;
    filteredOptions = options_.filter((options_) =>
      options_.toLowerCase().includes(inputValue_.toLowerCase())
    );

    if (withCustom && filteredOptions.length === 0) {
      filteredOptions = [inputValue_];
    }

    return filteredOptions;
  };

  const filteredOptions = getFilteredOptions(inputValue, options, withCustomValue);
  const openMenu = Boolean(anchorEl) && filteredOptions.length > 0;
  return (
    <Box>
      <TextField
        name={name}
        error={error}
        touched={touched}
        value={inputValue}
        label={label}
        onClick={handleClickSearchField}
        onChange={handleChangeSearchField}
        onKeyDown={handleKeyDownSearchField}
        onFocus={() => setIsFocus(true)}
        onBlur={(e) => {
          onBlur(e);
          setIsFocus(false);
        }}
        helperText={helperText}
        inputProps={{ style: { cursor: 'pointer' } }}
        autoComplete="off"
        InputProps={{
          endAdornment: isFocus && (
            <InputAdornment sx={{ position: 'absolute', right: '12px', top: '26px' }}>
              <SearchIcon />
            </InputAdornment>
          )
        }}
      />
      <Menu
        open={openMenu}
        onClose={handleCloseMenu}
        anchorEl={anchorEl}
        anchorOrigin={{ vertical: 'bottom' }}
        sx={{ maxHeight: '240px' }}
        disableAutoFocus
        disableAutoFocusItem
        onFocus={(e) => {
          if (e?.target?.classList?.contains('MuiList-root')) {
            e.relatedTarget.focus();
          }
        }}
      >
        {filteredOptions?.map((option, idx) => (
          <MenuItem
            key={`${option}-${idx}`}
            value={option}
            onClick={() => handleClickMenu(option)}
            className={value.includes(option) ? 'Mui-selected' : ''}
            sx={{ width: `calc(${menuWidth} - 16px)` }}
          >
            {option}
          </MenuItem>
        ))}
      </Menu>
      {value.length > 0 && (
        <Stack direction="row" spacing={1} sx={{ mt: 1, mb: 2 }}>
          {value?.map((label, idx) => (
            <StyledChip
              bcolor={chipcolor}
              label={label}
              onDelete={handleDeleteValue(label)}
              key={`${label}-${idx}`}
            />
          ))}
        </Stack>
      )}
    </Box>
  );
};

MultiSelect.PropTypes = {
  withCustomValue: PropTypes.bool,
  chipcolor: PropTypes.oneOf(['primary', 'secondary']),
  name: PropTypes.string,
  label: PropTypes.string,
  value: PropTypes.array,
  error: PropTypes.bool,
  touched: PropTypes.bool,
  onBlur: PropTypes.func,
  helperText: PropTypes.string,
  onChange: PropTypes.func,
  options: PropTypes.array
};

export default MultiSelect;
