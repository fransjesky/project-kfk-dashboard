import React, { useState, Fragment } from 'react';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import Box from '@mui/material/Box';
import ListItemText from '@mui/material/ListItemText';
import ListItemIcon from '@mui/material/ListItemIcon';

import MoreVertIcon from '@mui/icons-material/MoreVert';
import VisibilityIcon from '@mui/icons-material/Visibility';
import InfoIcon from '@mui/icons-material/Info';
import EditIcon from '@mui/icons-material/Edit';
import SaveIcon from '@mui/icons-material/Save';
import DeleteIcon from '@mui/icons-material/Delete';
import DraftsIcon from '@mui/icons-material/Drafts';

const menuItemContent = {
  view: (
    <Fragment>
      <ListItemIcon>
        <VisibilityIcon fontSize="small" />
      </ListItemIcon>
      <ListItemText>View</ListItemText>
    </Fragment>
  ),
  detail: (
    <Fragment>
      <ListItemIcon>
        <InfoIcon fontSize="small" />
      </ListItemIcon>
      <ListItemText>Detail</ListItemText>
    </Fragment>
  ),
  edit: (
    <Fragment>
      <ListItemIcon>
        <EditIcon fontSize="small" />
      </ListItemIcon>
      <ListItemText>Edit</ListItemText>
    </Fragment>
  ),
  save: (
    <Fragment>
      <ListItemIcon>
        <SaveIcon fontSize="small" />
      </ListItemIcon>
      <ListItemText>Save</ListItemText>
    </Fragment>
  ),
  delete: (
    <Fragment>
      <ListItemIcon>
        <DeleteIcon fontSize="small" />
      </ListItemIcon>
      <ListItemText>Delete</ListItemText>
    </Fragment>
  ),
  draft: (
    <Fragment>
      <ListItemIcon>
        <DraftsIcon fontSize="small" />
      </ListItemIcon>
      <ListItemText>Move to draft</ListItemText>
    </Fragment>
  )
};

/**
 *
 * @param {{
 *  actions: {
 *    type: "view" | "edit" | "delete" | "save",
 *    handler: (rowData: any) => void,
 *    custom?: (rowData: any) => JSX.Element
 *  }[],
 *  rowData: any,
 *  disabled: boolean
 * }} props
 * @returns {JSX.Element}
 */
export default function TableGenericActionMenu(props) {
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleMenuItemClick = (handler) => () => {
    if (handler) {
      handler(props.rowData);
    }
    handleClose();
  };

  return (
    <Box component={'span'}>
      <Tooltip title="Action Menu">
        <IconButton
          id="row-action-button"
          aria-controls="row-action-menu"
          aria-haspopup="true"
          aria-expanded={open ? 'true' : undefined}
          onClick={handleClick}
          disabled={Boolean(props.disabled)}
        >
          <MoreVertIcon />
        </IconButton>
      </Tooltip>
      <Menu
        id="row-action-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          'aria-labelledby': 'row-action-button'
        }}
      >
        {props.actions.map((act, idx) => {
          if (act.custom) {
            return (
              <div key={idx} onClick={handleMenuItemClick()} role={'button'}>
                {act.custom(props.rowData)}
              </div>
            );
          }

          return (
            <MenuItem onClick={handleMenuItemClick(act.handler)} key={idx}>
              {menuItemContent[act.type]}
            </MenuItem>
          );
        })}
      </Menu>
    </Box>
  );
}
