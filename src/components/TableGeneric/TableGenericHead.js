import React from 'react';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import TableCell from '@mui/material/TableCell';
import TableSortLabel from '@mui/material/TableSortLabel';
import Box from '@mui/material/Box';
import Checkbox from '@mui/material/Checkbox';
import Tooltip from '@mui/material/Tooltip';
import { visuallyHidden } from '@mui/utils';
import { styled } from '@mui/material/styles';
import PropTypes from 'prop-types';

const StyledTableRow = styled(TableRow)(() => ({
  backgroundColor: '#f5f7ff'
}));

/**
 *
 * @param {{
 *  order: "asc" | "desc",
 *  orderBy: string,
 *  cellDetails: {
 *    label: string,
 *    propertyName: string,
 *    isSortable?: boolean,
 *    headerAlign?: "center" | "left" | "right"
 *  }[],
 *  withAction: boolean,
 *  withNumbering: false | {
 *    label: string,
 *    align: "left" | "center" | "right",
 *    headerAlign: "left" | "center" | "right"
 *  },
 *  selectActions?: any[],
 *  rows?: any[],
 *  selectedRows?:  any[],
 *  onSelectAll: () => void
 * }} props
 * @returns {JSX.Element}
 */
export default function TableGenericHead(props) {
  const {
    order,
    orderBy,
    onRequestSort,
    cellDetails,
    withAction,
    withNumbering,
    selectActions = [],
    rows = [],
    selectedRows = [],
    onSelectAll = () => {}
  } = props;

  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  const isChecked = rows.length > 0 && selectedRows.length === rows.length;

  return (
    <TableHead>
      <StyledTableRow>
        {selectActions.length ? (
          <TableCell padding="checkbox">
            <Tooltip title={isChecked ? 'Clear all selection' : 'Select all'}>
              <Checkbox
                color="primary"
                indeterminate={selectedRows.length > 0 && selectedRows.length < rows.length}
                checked={isChecked}
                onChange={(e) => {
                  onSelectAll(e.target.checked);
                }}
                inputProps={{
                  'aria-label': 'select all items'
                }}
              />
            </Tooltip>
          </TableCell>
        ) : null}
        {withNumbering !== false ? (
          <TableCell padding={'normal'} align={withNumbering.headerAlign}>
            {withNumbering.label}
          </TableCell>
        ) : null}
        {cellDetails.map((detail) => (
          <TableCell
            key={detail.propertyName}
            align={detail.headerAlign}
            padding={'normal'}
            sortDirection={orderBy === detail.propertyName ? order : false}
          >
            {detail.isSortable === false ? (
              detail.label
            ) : (
              <TableSortLabel
                active={orderBy === detail.propertyName}
                direction={orderBy === detail.propertyName ? order : 'asc'}
                onClick={createSortHandler(detail.propertyName)}
              >
                {detail.label}
                {orderBy === detail.propertyName ? (
                  <Box component="span" sx={visuallyHidden}>
                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                  </Box>
                ) : null}
              </TableSortLabel>
            )}
          </TableCell>
        ))}
        {withAction ? <TableCell padding={'normal'}></TableCell> : null}
      </StyledTableRow>
    </TableHead>
  );
}

TableGenericHead.propTypes = {
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  cellDetails: PropTypes.array.isRequired
};
