import React, { useCallback, useEffect, useMemo, useState } from 'react';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Checkbox from '@mui/material/Checkbox';
import CircularProgress from '@mui/material/CircularProgress';

import TableGenericToolbar from './TableGenericToolbar';
import TableGenericHead from './TableGenericHead';
import TableGenericActionMenu from 'components/TableGeneric/TableGenericAction';
import { TableProvider } from './TableGenericHelper';
import { validateDataSchema, tablePropertySchema } from './TableGenericValidation';

// -- data fetching
import { BASE_URL, BASE_API_PATH } from '../../utils/url';
import { getUser } from '../../utils/localStorage';
import { axiosFetcherAuth, axiosFetcher } from '../Fetcher';

function getQueryParam({ order, orderBy, page, perPage, query, search, queryTerm }) {
  let queryArr = [];

  // order
  if (['asc', 'desc'].includes(order) && typeof orderBy === 'string' && orderBy.length) {
    queryArr.push(queryTerm.order + '=' + order);
    queryArr.push(queryTerm.orderBy + '=' + orderBy);
  }

  // limit
  if (typeof page === 'number' && typeof perPage === 'number') {
    let limit = perPage;
    let offset = page * perPage;
    queryArr.push(queryTerm.limit + '=' + limit);
    queryArr.push(queryTerm.offset + '=' + offset);
  }

  // query
  let propKeys = Object.keys(query);
  if (propKeys.length) {
    propKeys.forEach((key) => {
      if (Array.isArray(query[key])) {
        query[key].forEach((val) => {
          queryArr.push(key + '=' + encodeURIComponent(val));
        });
      } else {
        queryArr.push(key + '=' + encodeURIComponent(query[key]));
      }
    });
  }

  // search
  if (typeof search === 'string') {
    queryArr.push(queryTerm.search + '=' + search.trim());
  }

  return queryArr.join('&');
}

function getRows(path, params, method = 'get') {
  const queryParam = getQueryParam(params);
  const url = `${BASE_URL}${BASE_API_PATH}${path}?${queryParam}`;
  const user = getUser();
  console.log('fetch url:', url);

  if (user) {
    return axiosFetcherAuth('Authorization', 'JWT ' + user.token)(method, url);
  } else {
    return axiosFetcher(method, url);
  }
}

// -- demo

// const sampleFieldNames = ['name', 'calories', 'fat', 'carbs', 'protein', '_id'];
// const sampleRows = [
//   createData(sampleFieldNames, ['Cupcake', 305, 3.7, 67, 4.3, '1']),
//   createData(sampleFieldNames, ['Donut', 452, 25.0, 51, 4.9, '2']),
//   createData(sampleFieldNames, ['Eclair', 262, 16.0, 24, 6.0, '3']),
//   createData(sampleFieldNames, ['Frozen yoghurt', 159, 6.0, 24, 4.0, '4']),
//   createData(sampleFieldNames, ['Gingerbread', 356, 16.0, 49, 3.9, '5']),
//   createData(sampleFieldNames, ['Honeycomb', 408, 3.2, 87, 6.5, '6']),
//   createData(sampleFieldNames, ['Ice cream sandwich', 237, 9.0, 37, 4.3, '7']),
//   createData(sampleFieldNames, ['Jelly Bean', 375, 0.0, 94, 0.0, '8']),
//   createData(sampleFieldNames, ['KitKat', 518, 26.0, 65, 7.0, '9']),
//   createData(sampleFieldNames, ['Lollipop', 392, 0.2, 98, 0.0, '10']),
//   createData(sampleFieldNames, ['Marshmallow', 318, 0, 81, 2.0, '11']),
//   createData(sampleFieldNames, ['Nougat', 360, 19.0, 9, 37.0, '12']),
//   createData(sampleFieldNames, ['Oreo', 437, 18.0, 63, 4.0, '13'])
// ];

// function demoFetchRows(order, orderBy, page, perPage) {
//   const rows = stableSort(sampleRows, getComparator(order, orderBy)).slice(
//     page * perPage,
//     page * perPage + perPage
//   );
//   console.log(rows);
//   return rows;
// }

// function demoLoadRows(obj) {
//   const { page, perPage, order, orderBy } = obj;
//   console.log(obj);

//   return demoFetchRows(order, orderBy, page, perPage);
// }

// -- main

/**
 *
 * @param {{
 *  endpointPath: string,
 *  method?: "get" | "post",
 *  initialPage?: number,
 *  initialPerPage?: number,
 *  initialOrder?: "asc" | "desc",
 *  intialOrderBy?: string,
 *  withNumbering?: false | {
 *    label: string,
 *    align: "left" | "center" | "right",
 *    headerAlign: "left" | "center" | "right"
 *  },
 *  cellDetails: {
 *    label: string,
 *    propertyName: string,
 *    isSortable?: boolean,
 *    align?: "center" | "left" | "right",
 *    headerAlign?: "center" | "left" | "right",
 *    custom?: (rowData: any) => JSX.Element
 *  }[],
 *  rowDetail?: {
 *    key?: string
 *  },
 *  actions?: {
 *    type?: "view" | "edit" | "delete" | "draft",
 *    handler?: (rowData: any) => void,
 *    custom?: (rowData: any) => JSX.Element
 *  }[],
 *  sorts?: {
 *    label: string,
 *    order?: "asc" | "desc",
 *    orderBy: string
 *  }[],
 *  filters?: {
 *    type: "select" | "multiselect" | "date" | "daterange",
 *    label: string,
 *    paramName?: string,
 *    options?: {
 *      value: string | number,
 *      label?: string
 *    }[],
 *    yearOnly?: boolean,
 *    startParamName?: string,
 *    startLabel?: string,
 *    endParamName?: string,
 *    endLabel?: string
 *  }[],
 *  withoutToolbar?: boolean,
 *  withPagination?: boolean,
 *  withSearch?: boolean,
 *  baseQuery?: {[prop: string]: string | number},
 *  queryTerm?: {
 *    order?: string,
 *    orderBy?: string,
 *    search?: string,
 *    limit?: string,
 *    offset?: string
 *  },
 *  label?: false | string | JSX.Element,
 *  onClick?: (rowData: any) => void,
 *  onClickIgnores?: number[],
 *  selectActions?: {
 *    type?: "delete",
 *    handler?: (selectedRows: any[], setSelectedRows: (rows: any[]) => void) => void,
 *    render?: (selectedRows: any[], setSelectedRows: (rows: any[]) => void) => JSX.Element
 *  }[],
 *  isSourceLoading?: boolean
 * }} props
 * @returns {JSX.Element}
 */
export default function TableGeneric(props) {
  const [valid, setValid] = useState(false);
  const [order, setOrder] = useState('asc');
  const [orderBy, setOrderBy] = useState('');
  const [page, setPage] = useState(0);
  const [perPage, setPerPage] = useState(5);
  const [search, setSearch] = useState('');
  const [query, setQuery] = useState({});
  const [requestParams, setRequestParams] = useState({});
  const [rows, setRows] = useState([]);
  const [totalRows, setTotalRows] = useState(0);
  const [emptyRows] = useState(0);
  const [selectedRows, setSelectedRows] = useState([]);
  const [isSourceLoading, setIsSourceLoading] = useState(false);
  const [isRowLoading, setIsRowLoading] = useState(false);

  const withAction = useMemo(() => {
    if (valid && valid.actions.length > 0) {
      return true;
    }
    return false;
  }, [valid]);

  // sort action from table headers
  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleChangePage = (event, newPage) => {
    if (selectedRows.length <= 0 && !isSourceLoading) {
      setPage(newPage);
    }
  };

  const handleChangePerPage = (event) => {
    if (selectedRows.length <= 0 && !isSourceLoading) {
      setPerPage(parseInt(event.target.value, 10));
      setPage(0);
    }
  };

  const handleToolbar = useCallback(
    (value) => {
      // check sort
      if (['asc', 'desc'].includes(value.order) && value.orderBy.length) {
        setOrder(value.order);
        setOrderBy(value.orderBy);
      }
      // check search
      if (typeof value.search === 'string') {
        setSearch(value.search);
        setPage(0);
      }
      // check query
      if (Object.keys(value.query).length) {
        setQuery({ ...valid.baseQuery, ...value.query });
        setPage(0);
      }
    },
    [valid]
  );

  const handleRowClick = (rowData, cellIdx) => () => {
    // console.log('col', cellIdx, rowData);
    if (valid.onClick instanceof Function) {
      if (
        !valid.onClickIgnores ||
        valid.onClickIgnores.length === 0 ||
        !valid.onClickIgnores.includes(cellIdx)
      ) {
        valid.onClick(rowData);
      }
    }
  };

  // -- checkbox handlers

  const isRowSelected = (rowData) => {
    return (
      selectedRows.filter((r) => r[valid.rowDetail.key] === rowData[valid.rowDetail.key]).length > 0
    );
  };

  const updateSelectedRows = (rowData, isAdd = true) => {
    let idx = selectedRows
      .map((sr) => sr[valid.rowDetail.key])
      .indexOf(rowData[valid.rowDetail.key]);

    if (isAdd && idx < 0) {
      setSelectedRows([...selectedRows, rowData]);
    } else if (!isAdd && idx >= 0) {
      setSelectedRows([...selectedRows.slice(0, idx), ...selectedRows.slice(idx + 1)]);
    }
  };

  const handleSelectAll = (isAll = true) => {
    if (isAll) {
      setSelectedRows([...rows]);
    } else {
      setSelectedRows([]);
    }
  };

  // -- --

  // handle first render
  useEffect(() => {
    setValid(validateDataSchema(props, tablePropertySchema));
  }, [props]);

  // handle valid has truthy value
  useEffect(() => {
    if (valid) {
      setOrder(valid.initialOrder);
      setOrderBy(valid.initialOrderBy);
      setPage(valid.initialPage);
      setPerPage(valid.initialPerPage);
      setQuery({ ...valid.baseQuery });
      setRequestParams({
        path: valid.endpointPath,
        method: valid.method,
        queryTerm: valid.queryTerm
      });
      setIsSourceLoading(valid.isSourceLoading);
      console.log('TableGeneric properties:', valid);
    }
  }, [valid]);

  // any change
  useEffect(() => {
    if (requestParams.path && selectedRows.length <= 0 && !isSourceLoading) {
      setIsRowLoading(true);

      getRows(
        requestParams.path,
        {
          page,
          perPage,
          order,
          orderBy,
          search,
          query,
          queryTerm: requestParams.queryTerm
        },
        requestParams.method
      )
        .then((result) => {
          if (result.error) {
            // notification for error
          } else {
            setTotalRows(result.data.count);
            setRows(result.data.data || result.data.rows);
          }
          setIsRowLoading(false);
        })
        .catch(() => {
          setIsRowLoading(false);
        });
    }
  }, [page, perPage, order, orderBy, search, query, requestParams, selectedRows, isSourceLoading]);

  if (valid === false) {
    return null;
  }

  const colLength = valid.cellDetails.length + valid.actions.length + valid.selectActions.length;

  return (
    <TableProvider>
      <Box sx={{ width: '100%' }}>
        <Paper sx={{ width: '100%', mb: 2 }}>
          {!valid.withoutToolbar ? (
            <TableGenericToolbar
              sorts={valid.sorts}
              filters={valid.filters}
              onToolbar={handleToolbar}
              withSearch={valid.withSearch}
              label={valid.label}
              selectActions={valid.selectActions}
              selectedRows={selectedRows}
              setSelectedRows={setSelectedRows}
            />
          ) : null}
          <TableContainer>
            <Table sx={{ minWidth: 750 }} aria-labelledby="tableTitle" size={'medium'}>
              <TableGenericHead
                order={order}
                orderBy={orderBy}
                cellDetails={valid.cellDetails}
                onRequestSort={handleRequestSort}
                withAction={withAction}
                withNumbering={valid.withNumbering}
                selectActions={valid.selectActions}
                rows={rows}
                selectedRows={selectedRows}
                onSelectAll={handleSelectAll}
              />
              {isSourceLoading || isRowLoading ? (
                <TableBody>
                  <TableRow>
                    <TableCell colSpan={colLength} align={'center'}>
                      <CircularProgress color={'primary'} />
                    </TableCell>
                  </TableRow>
                </TableBody>
              ) : (
                <TableBody>
                  {rows.map((rowData, rowIdx) => {
                    const isChecked = isRowSelected(rowData);

                    return (
                      <TableRow hover tabIndex={-1} key={rowData[valid.rowDetail.key]}>
                        {valid.selectActions.length ? (
                          <TableCell padding="checkbox">
                            <Checkbox
                              color="primary"
                              checked={isChecked}
                              onChange={(e) => {
                                updateSelectedRows(rowData, e.target.checked);
                              }}
                              inputProps={{
                                'aria-label': 'select item'
                              }}
                            />
                          </TableCell>
                        ) : null}
                        {valid.withNumbering !== false ? (
                          <TableCell align={valid.withNumbering.align}>
                            {page * perPage + (rowIdx + 1)}
                          </TableCell>
                        ) : null}
                        {valid.cellDetails.map((detail, cellIdx) => {
                          return (
                            <TableCell
                              key={detail.propertyName}
                              align={detail.align}
                              onClick={handleRowClick(rowData, cellIdx)}
                            >
                              {detail.custom
                                ? detail.custom(rowData)
                                : rowData[detail.propertyName]}
                            </TableCell>
                          );
                        })}
                        {withAction ? (
                          <TableCell align="right">
                            <TableGenericActionMenu
                              actions={valid.actions}
                              rowData={rowData}
                              disabled={selectedRows.length > 0}
                            />
                          </TableCell>
                        ) : null}
                      </TableRow>
                    );
                  })}
                  {emptyRows > 0 && (
                    <TableRow
                      style={{
                        height: 53 * emptyRows
                      }}
                    >
                      <TableCell colSpan={5} />
                    </TableRow>
                  )}
                </TableBody>
              )}
            </Table>
          </TableContainer>
          {valid.withPagination && (
            <TablePagination
              rowsPerPageOptions={[5, 10, 25, 50, 100]}
              component="div"
              count={totalRows}
              rowsPerPage={perPage}
              page={page}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangePerPage}
            />
          )}
        </Paper>
      </Box>
    </TableProvider>
  );
}
