import Ajv from 'ajv';

export const tablePropertySchema = {
  type: 'object',
  properties: {
    endpointPath: { type: 'string' },
    method: { type: 'string', default: 'get' },
    initialPage: { type: 'number', default: 0 },
    initialPerPage: { type: 'number', default: 10 },
    initialOrder: { type: 'string', default: 'asc' },
    initialOrderBy: { type: 'string', default: '' },
    withNumbering: {
      type: ['boolean', 'object'],
      default: false,
      properties: {
        label: { type: 'string' },
        align: { type: 'string', default: 'left' },
        headerAlign: { type: 'string', default: 'left' }
      },
      required: ['label']
    },
    cellDetails: {
      type: 'array',
      default: [],
      items: {
        type: 'object',
        properties: {
          label: { type: 'string' },
          propertyName: { type: 'string' },
          isSortable: { type: 'boolean', default: true },
          align: { type: 'string', default: 'left' },
          headerAlign: { type: 'string', default: 'left' }
        },
        required: ['label', 'propertyName'],
        additionalProperties: true
      }
    },
    rowDetail: {
      type: 'object',
      default: { key: '_id' },
      properties: {
        key: { type: 'string', default: '_id' }
      }
    },
    actions: {
      type: 'array',
      default: [],
      items: {
        type: 'object',
        properties: {
          type: { type: 'string', default: 'view' },
          handler: { default: null },
          custom: {}
        },
        additionalProperties: true
      }
    },
    sorts: {
      type: 'array',
      default: [],
      items: {
        type: 'object',
        properties: {
          label: { type: 'string' },
          orderBy: { type: 'string' },
          order: { type: 'string', default: 'asc' }
        },
        required: ['label', 'orderBy'],
        additionalProperties: true
      }
    },
    filters: {
      type: 'array',
      default: [],
      items: {
        type: 'object',
        properties: {
          type: { type: 'string' },
          label: { type: 'string', default: 'Label' },
          paramName: { type: 'string', default: 'param' },
          startParamName: { type: 'string', default: 'start' },
          startLabel: { type: 'string', default: 'Start Date' },
          endParamName: { type: 'string', default: 'end' },
          endLabel: { type: 'string', default: 'End Date' },
          yearOnly: { type: 'boolean', default: false },
          options: {
            type: 'array',
            default: [],
            items: {
              type: 'object',
              properties: {
                label: { type: 'string' },
                value: { type: ['string', 'number'] }
              },
              required: ['value'],
              additionalProperties: true
            }
          }
        },
        required: ['type'],
        additionalProperties: true
      }
    },
    withoutToolbar: { type: 'boolean', default: false },
    withPagination: { type: 'boolean', default: true },
    withSearch: { type: 'boolean', default: true },
    baseQuery: { type: 'object', default: {} },
    queryTerm: {
      type: 'object',
      default: {
        order: 'order',
        orderBy: 'sort',
        search: 'keyword',
        limit: 'limit',
        offset: 'offset'
      },
      properties: {
        order: { type: 'string', default: 'order' },
        orderBy: { type: 'string', default: 'sort' },
        search: { type: 'string', default: 'keyword' },
        limit: { type: 'string', default: 'limit' },
        offset: { type: 'string', default: 'offset' }
      }
    },
    label: { default: false },
    onClick: { default: null },
    onClickIgnores: { default: [] },
    selectActions: { default: [] },
    isSourceLoading: { type: 'boolean', default: false }
  },
  required: ['endpointPath'],
  additionalProperties: true
};

const ajv = new Ajv({ useDefaults: true, allowUnionTypes: true });

export function validateDataSchema(data, schema) {
  let modifiedData = { ...data };
  const validate = ajv.compile(schema);

  if (validate(modifiedData)) {
    return modifiedData;
  }
  return false;
}
