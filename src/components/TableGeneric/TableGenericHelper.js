import React, { createContext, useReducer, useContext } from 'react';

const TableContext = createContext();

export function useTable() {
  const contextValue = useContext(TableContext);
  return contextValue;
}

const initialToolbar = {
  search: '',
  order: 'asc',
  orderBy: '',
  query: {}
};

function toolbarReducer(currentValue, action) {
  if (action.type === 'search') {
    return { ...currentValue, search: action.payload };
  } else if (action.type === 'sort') {
    return { ...currentValue, order: action.payload.order, orderBy: action.payload.orderBy };
  } else if (
    action.type === 'filter-select' ||
    action.type === 'filter-multiselect' ||
    action.type === 'filter-date'
  ) {
    let copyQuery = { ...currentValue.query };
    copyQuery[action.payload.paramName] = action.payload.value;
    return { ...currentValue, query: copyQuery };
  } else if (action.type === 'filter-daterange') {
    let copyQuery = { ...currentValue.query };
    copyQuery[action.payload.startParamName] = action.payload.startValue;
    copyQuery[action.payload.endParamName] = action.payload.endValue;
    return { ...currentValue, query: copyQuery };
  }

  return { ...currentValue };
}

export function TableProvider({ children }) {
  const [toolbar, dispatchToolbar] = useReducer(toolbarReducer, initialToolbar);

  return (
    <TableContext.Provider value={{ toolbar, dispatchToolbar }}>{children}</TableContext.Provider>
  );
}

/**
 *
 * @param {string[]} fieldNames
 * @param {any[]} values
 * @returns {*}
 */
export function createData(fieldNames, values) {
  let obj = {};

  fieldNames.forEach((name, idx) => {
    obj[name] = values[idx];
  });

  return obj;
}

/**
 * This method is created for cross-browser compatibility, if you don't
 * need to support IE11, you can use Array.prototype.sort() directly
 * @param {any[]} array
 * @param {(a:any, b:any) => number} comparator
 * @returns {any[]}
 */
export function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) {
      return order;
    }
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

/**
 * Descending comparison two object based on specific property
 * @param {any} a
 * @param {any} b
 * @param {string} orderBy
 * @returns {0|1|-1}
 */
export function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

/**
 * Generate an object comparator function based on order direction and object property
 * @param {"asc"|"desc"} order
 * @param {string} orderBy
 * @returns {(a:any, b:any) => number}
 */
export function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

/**
 * Return ISO date string plus timezon edifferences
 * @param {Date} date
 * @returns {string}
 */
export function toIsoString(date) {
  const tzo = -date.getTimezoneOffset(),
    dif = tzo >= 0 ? '+' : '-',
    pad = function (num) {
      const norm = Math.floor(Math.abs(num));
      return (norm < 10 ? '0' : '') + norm;
    };

  return (
    date.getFullYear() +
    '-' +
    pad(date.getMonth() + 1) +
    '-' +
    pad(date.getDate()) +
    'T' +
    pad(date.getHours()) +
    ':' +
    pad(date.getMinutes()) +
    ':' +
    pad(date.getSeconds()) +
    dif +
    pad(tzo / 60) +
    ':' +
    pad(tzo % 60)
  );
}
