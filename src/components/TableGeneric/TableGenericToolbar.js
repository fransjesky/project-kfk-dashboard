import React, { useState, Fragment, useEffect } from 'react';
import Toolbar from '@mui/material/Toolbar';
import FilterListIcon from '@mui/icons-material/FilterList';
import SearchIcon from '@mui/icons-material/Search';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';

// search
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import InputAdornment from '@mui/material/InputAdornment';

// select
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

// multi select
import OutlinedInput from '@mui/material/OutlinedInput';
import ListItemText from '@mui/material/ListItemText';
import Checkbox from '@mui/material/Checkbox';

// date picker
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DatePicker from '@mui/lab/DatePicker';
import styles from './TableGeneric.module.scss';
import { toIsoString, useTable } from './TableGenericHelper';

// others
import DeleteIcon from '@mui/icons-material/Delete';
import Typography from '@mui/material/Typography';

// import Input from '@mui/material/Input';
// import InputLabel from '@mui/material/InputLabel';
// import FormControl from '@mui/material/FormControl';

const sxInput = {
  fontSize: '.85rem',
  height: '40px'
};
const sxInputBox = {
  minWidth: 150,
  maxWidth: 150,
  mr: 1
};
const sxInputBothBox = {
  minWidth: 150,
  maxWidth: 150,
  mr: 1,
  ml: 1
};
const sxInputLabel = {
  backgroundColor: '#ffffff',
  py: '2px',
  px: '4px'
};

const MS_ITEM_HEIGHT = 48;
const MS_ITEM_PADDING_TOP = 8;
const MSMenuProps = {
  PaperProps: {
    style: {
      maxHeight: MS_ITEM_HEIGHT * 6.5 + MS_ITEM_PADDING_TOP,
      minWidth: 150
    }
  }
};

function TableGenericSearch() {
  const { dispatchToolbar } = useTable();
  const [value, setValue] = useState('');

  const handleSearchChange = (event) => {
    setValue(event.target.value);
    dispatchToolbar({ type: 'search', payload: event.target.value });
  };

  const handleSearchKeyPress = (event) => {
    if (event.key === 'Enter') {
      dispatchToolbar({ type: 'search', payload: event.target.value });
    }
  };

  return (
    <Fragment>
      <Box sx={{ flex: '1 1 100%' }} noValidate autoComplete="off">
        <TextField
          value={value}
          id="table-search"
          label="Search"
          sx={{ width: '25ch' }}
          size="small"
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <SearchIcon />
              </InputAdornment>
            )
          }}
          variant="outlined"
          onChange={handleSearchChange}
          onKeyPress={handleSearchKeyPress}
        />
      </Box>
    </Fragment>
  );
}

/**
 * @param {{
 *  label: string,
 *  paramName: string,
 *  options: {
 *    value: string | number,
 *    label?: string
 *  }[]
 * }} props
 * @returns {JSX.Element}
 */
function TableGenericSelect({ label, paramName, options }) {
  const [status, setStatus] = useState('');
  const { dispatchToolbar } = useTable();

  const handleChange = (event) => {
    setStatus(event.target.value);
  };

  useEffect(() => {
    dispatchToolbar({
      type: 'filter-select',
      payload: {
        paramName,
        value: status
      }
    });
  }, [status, paramName, dispatchToolbar]);

  return (
    <Box sx={sxInputBox} noValidate autoComplete="off">
      <FormControl fullWidth>
        <InputLabel id="tg-select-label" sx={sxInputLabel}>
          {label}
        </InputLabel>
        <Select
          labelId="tg-select-label"
          id="tg-select"
          value={status}
          label="Status"
          onChange={handleChange}
          size="small"
          variant="outlined"
          sx={sxInput}
          displayEmpty
        >
          <MenuItem value={''}>
            <em>All {label}</em>
          </MenuItem>
          {options.map((opt, optIdx) => {
            return (
              <MenuItem key={optIdx} value={opt.value}>
                {opt.label ? opt.label : opt.value}
              </MenuItem>
            );
          })}
        </Select>
      </FormControl>
    </Box>
  );
}

/**
 * @param {{
 *  label: string,
 *  paramName: string,
 *  options: {
 *    value: string | number,
 *    label?: string
 *  }[]
 * }} props
 * @returns {JSX.Element}
 */
function TableGenericMultiSelect({ label, paramName, options }) {
  const [value, setValue] = useState([]);
  const { dispatchToolbar } = useTable();

  const handleChange = (event) => {
    const {
      target: { value }
    } = event;
    setValue(typeof value === 'string' ? value.split(',') : value);
  };

  useEffect(() => {
    dispatchToolbar({
      type: 'filter-multiselect',
      payload: {
        paramName,
        value
      }
    });
  }, [value, paramName, dispatchToolbar]);

  return (
    <Box sx={sxInputBox} noValidate autoComplete="off">
      <FormControl fullWidth>
        <InputLabel id="tg-ms-checkbox-label" sx={sxInputLabel}>
          {label}
        </InputLabel>
        <Select
          labelId="tg-ms-checkbox-label"
          id="tg-ms-checkbox"
          multiple
          value={value}
          onChange={handleChange}
          input={<OutlinedInput label={label} />}
          renderValue={(selected) => selected.join(', ')}
          MenuProps={MSMenuProps}
          size="small"
          variant="outlined"
          sx={sxInput}
          displayEmpty
        >
          {options.map((opt, optIdx) => (
            <MenuItem
              key={optIdx}
              value={opt.value}
              sx={{ paddingLeft: '5px', paddingRight: '15px' }}
            >
              <Checkbox
                checked={value.indexOf(opt.value) > -1}
                color={'secondary'}
                sx={{
                  '> svg': {
                    fill: '#2e60ff'
                  }
                }}
              />
              <ListItemText primary={opt.label ? opt.label : opt.value} />
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </Box>
  );
}

/**
 * @param {{
 *  label: string,
 *  paramName: string,
 *  yearOnly?: boolean
 * }} props
 * @returns {JSX.Element}
 */
function TableGenericDatePicker({ label, paramName, yearOnly = false }) {
  const [value, setValue] = useState(null);
  const { dispatchToolbar } = useTable();

  useEffect(() => {
    if (value !== null) {
      dispatchToolbar({
        type: 'filter-date',
        payload: {
          paramName,
          value: yearOnly
            ? value.toISOString().substring(0, 4)
            : value.toISOString().substring(0, 10)
        }
      });
    }
  }, [value, paramName, yearOnly, dispatchToolbar]);

  return (
    <Box sx={sxInputBox} noValidate autoComplete="off">
      <DatePicker
        label={label}
        views={yearOnly ? ['year'] : ['day', 'month', 'year']}
        value={value}
        onChange={(newValue) => {
          setValue(newValue);
        }}
        renderInput={(params) => {
          return <TextField {...params} className={styles.inputField} size="small" />;
        }}
      />
    </Box>
  );
}

/**
 * @param {{
 *  label: string,
 *  startParamName: string,
 *  startLabel: string,
 *  endParamName: string,
 *  endLabel: string,
 *  yearOnly?: boolean
 * }} props
 * @returns {JSX.Element}
 */
function TableGenericDateRangePicker({
  startLabel,
  startParamName,
  endLabel,
  endParamName,
  yearOnly = false
}) {
  const [startValue, setStartValue] = useState(null);
  const [endValue, setEndValue] = useState(null);
  const { dispatchToolbar } = useTable();

  useEffect(() => {
    let start = '';
    let end = '';

    if (startValue !== null) {
      start = yearOnly
        ? toIsoString(startValue).substring(0, 4)
        : toIsoString(startValue).substring(0, 10);
      if (yearOnly && isNaN(start)) {
        start = '';
      } else if (!yearOnly) {
        const isAnyNaN = start.split('-').reduce((prev, curr) => prev || isNaN(curr), false);
        if (isAnyNaN) {
          start = '';
        }
      }
    }

    if (endValue !== null) {
      end = yearOnly
        ? toIsoString(endValue).substring(0, 4)
        : toIsoString(endValue).substring(0, 10);
      if (yearOnly && isNaN(end)) {
        end = '';
      } else if (!yearOnly) {
        const isAnyNaN = end.split('-').reduce((prev, curr) => prev || isNaN(curr), false);
        if (isAnyNaN) {
          end = '';
        }
      }
    }

    dispatchToolbar({
      type: 'filter-daterange',
      payload: {
        startParamName,
        endParamName,
        startValue: start,
        endValue: end
      }
    });
  }, [startValue, endValue, startParamName, endParamName, yearOnly, dispatchToolbar]);

  return (
    <Fragment>
      <Box sx={sxInputBothBox} noValidate autoComplete="off">
        <DatePicker
          label={startLabel}
          views={yearOnly ? ['year'] : ['day', 'month', 'year']}
          value={startValue}
          onChange={(newValue) => {
            setStartValue(newValue);
          }}
          renderInput={(params) => {
            return <TextField {...params} className={styles.inputField} size="small" />;
          }}
          maxDate={endValue == null ? undefined : endValue}
        />
      </Box>
      <Typography component={'span'}>-</Typography>
      <Box sx={sxInputBothBox} noValidate autoComplete="off">
        <DatePicker
          label={endLabel}
          views={yearOnly ? ['year'] : ['day', 'month', 'year']}
          value={endValue}
          onChange={(newValue) => {
            setEndValue(newValue);
          }}
          renderInput={(params) => {
            return <TextField {...params} className={styles.inputField} size="small" />;
          }}
          minDate={startValue == null ? undefined : startValue}
        />
      </Box>
    </Fragment>
  );
}

/**
 *
 * @param {{
 *  sorts: {
 *    label: string,
 *    order: "asc" | "desc",
 *    orderBy: string
 *  }[],
 *  filters: {
 *    type: "select" | "multiselect" | "date" | "daterange",
 *    label: string,
 *    paramName?: string,
 *    yearOnly?: boolean,
 *    startParamName?: string,
 *    endParamName?: string,
 *    options?: {
 *      value: string | number,
 *      label?: string
 *    }[]
 *  }[],
 *  onToolbar: (value) => void,
 *  withSearch?: boolean,
 *  label?: false | string | JSX.Element,
 *  selectActions: any[],
 *  selectedRows: any[],
 *  setSelectedRows: (updatedSelectedRows: any[]) => void
 * }} props
 * @returns {JSX.Element}
 */
export default function TableGenericToolbar(props) {
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const {
    sorts,
    filters,
    onToolbar,
    withSearch = true,
    label = false,
    selectActions = [],
    selectedRows = [],
    setSelectedRows = () => {}
  } = props;
  const { toolbar, dispatchToolbar } = useTable();

  const handleSortClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleSortClose = () => {
    setAnchorEl(null);
  };

  /**
   * @param {{ order: "asc"|"desc", orderBy: string }} sort
   */
  const handleSort = (sort) => () => {
    dispatchToolbar({ type: 'sort', payload: sort });
    handleSortClose();
  };

  useEffect(() => {
    if (toolbar) {
      onToolbar(toolbar);
    }
  }, [toolbar, onToolbar]);

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <Toolbar
        sx={{
          pl: { sm: 2 },
          pr: { xs: 1, sm: 1 },
          alignItems: 'center',
          justifyContent: 'right',
          display: selectedRows.length && selectActions.length ? 'none' : 'flex'
        }}
      >
        {label !== false ? label : null}
        {withSearch && <TableGenericSearch />}
        {filters.map((filter, filterIdx) => {
          if (filter.type === 'select') {
            return <TableGenericSelect key={filterIdx} {...filter} />;
          } else if (filter.type === 'multiselect') {
            return <TableGenericMultiSelect key={filterIdx} {...filter} />;
          } else if (filter.type === 'date') {
            return <TableGenericDatePicker key={filterIdx} {...filter} />;
          } else if (filter.type === 'daterange') {
            return <TableGenericDateRangePicker key={filterIdx} {...filter} />;
          }
          return null;
        })}
        {sorts.length <= 0 ? null : (
          <Fragment>
            <Tooltip title="Sort">
              <IconButton
                id="table-sort-button"
                aria-controls="table-sort-menu"
                aria-haspopup="true"
                aria-expanded={open ? 'true' : undefined}
                onClick={handleSortClick}
                sx={{
                  mb: 1
                }}
              >
                <FilterListIcon />
              </IconButton>
            </Tooltip>
            <Menu
              id="table-sort-menu"
              anchorEl={anchorEl}
              open={open}
              onClose={handleSortClose}
              MenuListProps={{
                'aria-labelledby': 'table-sort-button'
              }}
            >
              {sorts.map((sort, sortIdx) => {
                return (
                  <MenuItem key={sortIdx} onClick={handleSort(sort)}>
                    {sort.label}
                  </MenuItem>
                );
              })}
            </Menu>
          </Fragment>
        )}
      </Toolbar>
      {selectedRows.length && selectActions.length ? (
        <Toolbar
          sx={{
            pl: { sm: 2 },
            pr: { xs: 1, sm: 1 },
            alignItems: 'center',
            justifyContent: 'space-between',
            display: 'flex'
          }}
        >
          <Typography component={'div'}>{selectedRows.length} selected</Typography>
          <Box sx={{ display: 'inline-flex', alignItems: 'center' }}>
            {selectActions.map((act, idx) => {
              if (act.type === 'delete') {
                return (
                  <Tooltip title={'Delete all selected'} key={idx}>
                    <IconButton
                      color={'primary'}
                      onClick={() => {
                        if (act.handler instanceof Function) {
                          act.handler(selectedRows, setSelectedRows);
                        }
                      }}
                    >
                      <DeleteIcon />
                    </IconButton>
                  </Tooltip>
                );
              } else if (act.render instanceof Function) {
                return <Fragment key={idx}>{act.render(selectedRows, setSelectedRows)}</Fragment>;
              }
              return null;
            })}
          </Box>
        </Toolbar>
      ) : null}
    </LocalizationProvider>
  );
}
