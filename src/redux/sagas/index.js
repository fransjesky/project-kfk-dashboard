import { all } from 'redux-saga/effects';
import AuthSagas from './Auth';
import ModuleSagas from './Module';

export default function* rootSaga() {
  yield all([AuthSagas(), ModuleSagas()]);
}
