import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import Auth from './Auth';
import Module from './Module';
import Global from './Global';

const index = (history) =>
  combineReducers({
    router: connectRouter(history),
    auth: Auth,
    module: Module,
    global: Global
  });

export default index;
