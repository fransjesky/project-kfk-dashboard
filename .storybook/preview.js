import ThemeConfig from '../src/theme';
import GlobalStyles from '../src/theme/globalStyles';
import { HelmetProvider } from 'react-helmet-async';
import '../src/assets/styles/index.scss';

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/
    }
  },

  // fix bug for Mui5 that cause docs not rendered
  docs: {
    inlineStories: false,
    iframeHeight: '700px'
  }
};

export const decorators = [
  (Story) => (
    <HelmetProvider>
      <ThemeConfig>
        <GlobalStyles />
        <Story />
      </ThemeConfig>
    </HelmetProvider>
  )
];
